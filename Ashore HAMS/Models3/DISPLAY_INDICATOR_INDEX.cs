//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ashore_HAMS.Models3
{
    using System;
    using System.Collections.Generic;
    
    public partial class DISPLAY_INDICATOR_INDEX
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DISPLAY_INDICATOR_INDEX()
        {
            this.AFLOAT_APP_INDEX = new HashSet<AFLOAT_APP_INDEX>();
            this.AUTH_APP_INDEX = new HashSet<AUTH_APP_INDEX>();
            this.AUTH_APP_VENDOR_INDEX = new HashSet<AUTH_APP_VENDOR_INDEX>();
            this.CLASS_TYPE_INDEX = new HashSet<CLASS_TYPE_INDEX>();
            this.COMPLIANCE_STATUS_INDEX = new HashSet<COMPLIANCE_STATUS_INDEX>();
            this.DOWNLOAD_TYPE_INDEX = new HashSet<DOWNLOAD_TYPE_INDEX>();
            this.HULL_INDEX = new HashSet<HULL_INDEX>();
            this.HULL_TYPE_INDEX = new HashSet<HULL_TYPE_INDEX>();
            this.INSTALLED_AFLOAT_APP_VENDOR_INDEX = new HashSet<INSTALLED_AFLOAT_APP_VENDOR_INDEX>();
            this.NIAPS_VERSION_INDEX = new HashSet<NIAPS_VERSION_INDEX>();
            this.PRIORITY_INDEX = new HashSet<PRIORITY_INDEX>();
            this.PUB_INDEX = new HashSet<PUB_INDEX>();
            this.PUB_SCOPE_INDEX = new HashSet<PUB_SCOPE_INDEX>();
            this.PUB_TYPE_INDEX = new HashSet<PUB_TYPE_INDEX>();
            this.SERVER_INDEX = new HashSet<SERVER_INDEX>();
            this.STATUS_INDEX = new HashSet<STATUS_INDEX>();
            this.TARGET_AREA_INDEX = new HashSet<TARGET_AREA_INDEX>();
            this.UNAUTHORIZED_PROGRAMS = new HashSet<UNAUTHORIZED_PROGRAMS>();
            this.USER_INDEX = new HashSet<USER_INDEX>();
        }
    
        public int DISPLAY_INDICATOR_ID { get; set; }
        public string DISPLAY_INDICATOR_NAME { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AFLOAT_APP_INDEX> AFLOAT_APP_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AUTH_APP_INDEX> AUTH_APP_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AUTH_APP_VENDOR_INDEX> AUTH_APP_VENDOR_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CLASS_TYPE_INDEX> CLASS_TYPE_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<COMPLIANCE_STATUS_INDEX> COMPLIANCE_STATUS_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DOWNLOAD_TYPE_INDEX> DOWNLOAD_TYPE_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HULL_INDEX> HULL_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HULL_TYPE_INDEX> HULL_TYPE_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INSTALLED_AFLOAT_APP_VENDOR_INDEX> INSTALLED_AFLOAT_APP_VENDOR_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NIAPS_VERSION_INDEX> NIAPS_VERSION_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRIORITY_INDEX> PRIORITY_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PUB_INDEX> PUB_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PUB_SCOPE_INDEX> PUB_SCOPE_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PUB_TYPE_INDEX> PUB_TYPE_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SERVER_INDEX> SERVER_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STATUS_INDEX> STATUS_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TARGET_AREA_INDEX> TARGET_AREA_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UNAUTHORIZED_PROGRAMS> UNAUTHORIZED_PROGRAMS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USER_INDEX> USER_INDEX { get; set; }
    }
}
