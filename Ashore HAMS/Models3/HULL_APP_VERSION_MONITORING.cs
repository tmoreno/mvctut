//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ashore_HAMS.Models3
{
    using System;
    using System.Collections.Generic;
    
    public partial class HULL_APP_VERSION_MONITORING
    {
        public int HULL_APP_VERSION_MONITORING_ID { get; set; }
        public int HULL_SERVER_ID { get; set; }
        public int AUTH_HULL_APP_RELEASE_ID { get; set; }
        public int AFLOAT_HULL_APP_INSTALLED_ID { get; set; }
        public int STATUS_ID { get; set; }
        public byte[] TIME_DATE_RECORD { get; set; }
    
        public virtual AFLOAT_HULL_APP_INSTALLED AFLOAT_HULL_APP_INSTALLED { get; set; }
        public virtual AUTH_HULL_APP_RELEASE AUTH_HULL_APP_RELEASE { get; set; }
        public virtual HULL_SERVER_INDEX HULL_SERVER_INDEX { get; set; }
        public virtual STATUS_INDEX STATUS_INDEX { get; set; }
    }
}
