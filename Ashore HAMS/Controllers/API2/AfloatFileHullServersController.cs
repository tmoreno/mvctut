﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers.API2
{
    public class AfloatFileHullServersController : ApiController
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: api/AfloatFileHullServers
        public IQueryable<AfloatFileHullServer> GetAfloatFileHullServers()
        {
            return db.AfloatFileHullServers;
        }

        // GET: api/AfloatFileHullServers/5
        [ResponseType(typeof(AfloatFileHullServer))]
        public IHttpActionResult GetAfloatFileHullServer(int id)
        {
            AfloatFileHullServer afloatFileHullServer = db.AfloatFileHullServers.Find(id);
            if (afloatFileHullServer == null)
            {
                return NotFound();
            }

            return Ok(afloatFileHullServer);
        }

        // PUT: api/AfloatFileHullServers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAfloatFileHullServer(int id, AfloatFileHullServer afloatFileHullServer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != afloatFileHullServer.Id)
            {
                return BadRequest();
            }

            db.Entry(afloatFileHullServer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AfloatFileHullServerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        public class AfloatFileHullServerClientModel
        {
            public int Id { get; set; }
            public Nullable<int> HULL_SERVER_ID { get; set; }
            public string FileName { get; set; }
            public string Hash { get; set; }
            public Nullable<System.DateTime> Date { get; set; }
            public Nullable<int> Progress { get; set; }
        }



        // POST: api/AfloatFileHullServers
        [ResponseType(typeof(AfloatFileHullServer))]
        public IHttpActionResult PostAfloatFileHullServer(AfloatFileHullServerClientModel afloatFileHullServerClientModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fileId = (from a in db.AshoreFiles
                         where a.Filename == afloatFileHullServerClientModel.FileName
                         select a.Id).FirstOrDefault<int>();

            AfloatFileHullServer afloatFileHullServer = new AfloatFileHullServer()
            {
                AshoreFileId = fileId,
                Date = afloatFileHullServerClientModel.Date,
                Hash = afloatFileHullServerClientModel.Hash,
                Progress = afloatFileHullServerClientModel.Progress,
                HULL_SERVER_ID = afloatFileHullServerClientModel.HULL_SERVER_ID
            };




            var af = from a in db.AfloatFileHullServers
                     where a.HULL_SERVER_ID == afloatFileHullServer.HULL_SERVER_ID && a.AshoreFileId == afloatFileHullServer.AshoreFileId
                     orderby a.Id descending
                     select a;

            if (af.Count() > 0)
            {
                AfloatFileHullServer item = af.FirstOrDefault();
                item.Date = afloatFileHullServer.Date;
                item.Hash = afloatFileHullServer.Hash;
                item.Progress = afloatFileHullServer.Progress;

                db.Entry(item).State = EntityState.Modified;
                afloatFileHullServer.Id = item.Id;
            }
            else
            {
                db.AfloatFileHullServers.Add(afloatFileHullServer);
            }


            

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (AfloatFileHullServerExists(afloatFileHullServer.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = afloatFileHullServer.Id }, afloatFileHullServer);
        }

        // DELETE: api/AfloatFileHullServers/5
        [ResponseType(typeof(AfloatFileHullServer))]
        public IHttpActionResult DeleteAfloatFileHullServer(int id)
        {
            AfloatFileHullServer afloatFileHullServer = db.AfloatFileHullServers.Find(id);
            if (afloatFileHullServer == null)
            {
                return NotFound();
            }

            db.AfloatFileHullServers.Remove(afloatFileHullServer);
            db.SaveChanges();

            return Ok(afloatFileHullServer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AfloatFileHullServerExists(int id)
        {
            return db.AfloatFileHullServers.Count(e => e.Id == id) > 0;
        }
    }
}