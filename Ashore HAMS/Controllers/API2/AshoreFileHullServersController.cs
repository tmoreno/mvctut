﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers.API2
{
    public class AshoreFileHullServersController : ApiController
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: api/AshoreFileHullServers
        public IQueryable<AshoreFileHullServer> GetAshoreFileHullServers()
        {
            return db.AshoreFileHullServers;
        }

        // GET: api/AshoreFileHullServers/5
        [ResponseType(typeof(AshoreFileHullServer))]
        public IHttpActionResult GetAshoreFileHullServer(int id)
        {
            AshoreFileHullServer ashoreFileHullServer = db.AshoreFileHullServers.Find(id);
            if (ashoreFileHullServer == null)
            {
                return NotFound();
            }

            return Ok(ashoreFileHullServer);
        }

        // PUT: api/AshoreFileHullServers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAshoreFileHullServer(int id, AshoreFileHullServer ashoreFileHullServer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ashoreFileHullServer.Id)
            {
                return BadRequest();
            }

            db.Entry(ashoreFileHullServer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AshoreFileHullServerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AshoreFileHullServers
        [ResponseType(typeof(AshoreFileHullServer))]
        public IHttpActionResult PostAshoreFileHullServer(AshoreFileHullServer ashoreFileHullServer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var af = from a in db.AshoreFileHullServers
                     where a.HULL_SERVER_ID == ashoreFileHullServer.HULL_SERVER_ID && a.AshoreFileId == ashoreFileHullServer.AshoreFileId
                     orderby a.Id descending
                     select a;

            if (af.Count() > 0)
            {
                AshoreFileHullServer item = af.FirstOrDefault();
                ashoreFileHullServer.Id = item.Id;
            }
            else
            {
                db.AshoreFileHullServers.Add(ashoreFileHullServer);
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    if (AshoreFileHullServerExists(ashoreFileHullServer.Id))
                    {
                        return Conflict();
                    }
                    else
                    {
                        throw;
                    }
                }


            }




            return CreatedAtRoute("DefaultApi", new { id = ashoreFileHullServer.Id }, ashoreFileHullServer);
        }

        // DELETE: api/AshoreFileHullServers/5
        [ResponseType(typeof(AshoreFileHullServer))]
        public IHttpActionResult DeleteAshoreFileHullServer(int id)
        {
            AshoreFileHullServer ashoreFileHullServer = db.AshoreFileHullServers.Find(id);
            if (ashoreFileHullServer == null)
            {
                return NotFound();
            }

            db.AshoreFileHullServers.Remove(ashoreFileHullServer);
            db.SaveChanges();

            return Ok(ashoreFileHullServer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AshoreFileHullServerExists(int id)
        {
            return db.AshoreFileHullServers.Count(e => e.Id == id) > 0;
        }
    }
}