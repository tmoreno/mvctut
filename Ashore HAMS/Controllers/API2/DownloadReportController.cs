﻿using Ashore_HAMS.Models3;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Ashore_HAMS.Controllers.API2
{
    public class DownloadReportController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private HAMS9Entities1 db = new HAMS9Entities1();

        public static IList<string> jsonStringToCSV(string jsonContent)
        {
            //used NewtonSoft json nuget package
            XmlNode xml = JsonConvert.DeserializeXmlNode("{records:{record:" + jsonContent + "}}");
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml.InnerXml);
            XmlReader xmlReader = new XmlNodeReader(xml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(xmlReader);
            var dataTable = dataSet.Tables[1];

            //Datatable to CSV
            var lines = new List<string>();
            string[] columnNames = dataTable.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName).
                                              ToArray();
            var header = string.Join(",", columnNames);
            lines.Add(string.Format("{0}\n", header));
            var valueLines = dataTable.AsEnumerable()
                               .Select(row => (string.Join(",", row.ItemArray)) + "\n");
            lines.AddRange(valueLines);
            return lines;
        }
        // GET: DownloadReport
        public FileContentResult Index()
        {
            string csv = Details(5);

            //string json = JsonConvert.SerializeObject(new
            //List(){
            //    Replication = new
            //    {
            //        Id = 1,
            //        AUTH_APP_RELEASE_ID = 3004
            //    }
            //    ,
            //    ApplicationBaseLine = new
            //    {
            //        Id = 2,
            //        AUTH_APP_RELEASE_ID = 4233
            //    }
            //});

            var critical = (from a in db.ReplicationMatchViews
                            where a.Hash == null
                            select a).Count();
            var compliant = (from a in db.ReplicationMatchViews
                             where a.Hash != null
                             select a).Count();

            var o1 = new
            {
                Name = "Replication",
                Compliant = compliant,
                Warning = 0,
                Critical = critical
            };

            var l = new
            {
                o1
            };


            string json = JsonConvert.SerializeObject(l);

            logger.Info(json);

            var list = jsonStringToCSV(json);
            StringBuilder sb = new StringBuilder();

            foreach (string item in list)
            {
                sb.Append(string.Format("{0}\r\n", item));
            }

            logger.Info(sb.ToString());


            return File(new System.Text.UTF8Encoding().GetBytes(sb.ToString()), "text/csv", "Report123.csv");
        }

        // GET: DownloadReport/Details/5
        public string Details(int id)
        {
            var json = @"{
                       ""employees"": [
                       { ""firstName"":""John"" , ""lastName"":""Doe"" },
                       { ""firstName"":""Anna"" , ""lastName"":""Smith"" },
                       { ""firstName"":""Peter"" , ""lastName"":""Jones"" }
                       ]
                       }";
            var list = jsonStringToCSV(json);

            StringBuilder sb = new StringBuilder();

            foreach (string item in list)
            {
               sb.Append(string.Format("{0}\r\n", item));
            }

            logger.Info(sb.ToString());

            return sb.ToString();
        }

        // GET: DownloadReport/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DownloadReport/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DownloadReport/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DownloadReport/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DownloadReport/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DownloadReport/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
