﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class AFLOAT_PUB_LEVELSController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: AFLOAT_PUB_LEVELS
        public async Task<ActionResult> Index()
        {
            var aFLOAT_PUB_LEVELS = db.AFLOAT_PUB_LEVELS.Include(a => a.HULL_PUB_INDEX);
            return View(await aFLOAT_PUB_LEVELS.ToListAsync());
        }

        // GET: AFLOAT_PUB_LEVELS/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFLOAT_PUB_LEVELS aFLOAT_PUB_LEVELS = await db.AFLOAT_PUB_LEVELS.FindAsync(id);
            if (aFLOAT_PUB_LEVELS == null)
            {
                return HttpNotFound();
            }
            return View(aFLOAT_PUB_LEVELS);
        }

        // GET: AFLOAT_PUB_LEVELS/Create
        public ActionResult Create()
        {
            ViewBag.HULL_PUB_ID = new SelectList(db.HULL_PUB_INDEX, "HULL_PUB_ID", "HULL_PUB_ID");
            return View();
        }

        // POST: AFLOAT_PUB_LEVELS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AFLOAT_PUB_LEVELS_ID,HULL_PUB_ID,AFLOAT_PUB_LEVEL,AFLOAT_PUB_RELEASE_TIMESTAMP,TIME_DATE_RECORD")] AFLOAT_PUB_LEVELS aFLOAT_PUB_LEVELS)
        {
            if (ModelState.IsValid)
            {
                db.AFLOAT_PUB_LEVELS.Add(aFLOAT_PUB_LEVELS);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.HULL_PUB_ID = new SelectList(db.HULL_PUB_INDEX, "HULL_PUB_ID", "HULL_PUB_ID", aFLOAT_PUB_LEVELS.HULL_PUB_ID);
            return View(aFLOAT_PUB_LEVELS);
        }

        // GET: AFLOAT_PUB_LEVELS/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFLOAT_PUB_LEVELS aFLOAT_PUB_LEVELS = await db.AFLOAT_PUB_LEVELS.FindAsync(id);
            if (aFLOAT_PUB_LEVELS == null)
            {
                return HttpNotFound();
            }
            ViewBag.HULL_PUB_ID = new SelectList(db.HULL_PUB_INDEX, "HULL_PUB_ID", "HULL_PUB_ID", aFLOAT_PUB_LEVELS.HULL_PUB_ID);
            return View(aFLOAT_PUB_LEVELS);
        }

        // POST: AFLOAT_PUB_LEVELS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AFLOAT_PUB_LEVELS_ID,HULL_PUB_ID,AFLOAT_PUB_LEVEL,AFLOAT_PUB_RELEASE_TIMESTAMP,TIME_DATE_RECORD")] AFLOAT_PUB_LEVELS aFLOAT_PUB_LEVELS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aFLOAT_PUB_LEVELS).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.HULL_PUB_ID = new SelectList(db.HULL_PUB_INDEX, "HULL_PUB_ID", "HULL_PUB_ID", aFLOAT_PUB_LEVELS.HULL_PUB_ID);
            return View(aFLOAT_PUB_LEVELS);
        }

        // GET: AFLOAT_PUB_LEVELS/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFLOAT_PUB_LEVELS aFLOAT_PUB_LEVELS = await db.AFLOAT_PUB_LEVELS.FindAsync(id);
            if (aFLOAT_PUB_LEVELS == null)
            {
                return HttpNotFound();
            }
            return View(aFLOAT_PUB_LEVELS);
        }

        // POST: AFLOAT_PUB_LEVELS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AFLOAT_PUB_LEVELS aFLOAT_PUB_LEVELS = await db.AFLOAT_PUB_LEVELS.FindAsync(id);
            db.AFLOAT_PUB_LEVELS.Remove(aFLOAT_PUB_LEVELS);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
