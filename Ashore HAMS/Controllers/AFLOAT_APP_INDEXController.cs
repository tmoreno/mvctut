﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class AFLOAT_APP_INDEXController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: AFLOAT_APP_INDEX
        public async Task<ActionResult> Index()
        {
            var aFLOAT_APP_INDEX = db.AFLOAT_APP_INDEX.Include(a => a.DISPLAY_INDICATOR_INDEX).Include(a => a.INSTALLED_AFLOAT_APP_VENDOR_INDEX);
            return View(await aFLOAT_APP_INDEX.ToListAsync());
        }

        // GET: AFLOAT_APP_INDEX/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFLOAT_APP_INDEX aFLOAT_APP_INDEX = await db.AFLOAT_APP_INDEX.FindAsync(id);
            if (aFLOAT_APP_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(aFLOAT_APP_INDEX);
        }

        // GET: AFLOAT_APP_INDEX/Create
        public ActionResult Create()
        {
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME");
            ViewBag.INSTALLED_AFLOAT_APP_VENDOR_ID = new SelectList(db.INSTALLED_AFLOAT_APP_VENDOR_INDEX, "INSTALLED_AFLOAT_APP_VENDOR_ID", "INSTALLED_AFLOAT_APP_VENDOR_NAME");
            return View();
        }

        // POST: AFLOAT_APP_INDEX/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AFLOAT_APP_ID,AFLOAT_APP_NAME,INSTALLED_AFLOAT_APP_VENDOR_ID,DISPLAY_INDICATOR_ID")] AFLOAT_APP_INDEX aFLOAT_APP_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.AFLOAT_APP_INDEX.Add(aFLOAT_APP_INDEX);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", aFLOAT_APP_INDEX.DISPLAY_INDICATOR_ID);
            ViewBag.INSTALLED_AFLOAT_APP_VENDOR_ID = new SelectList(db.INSTALLED_AFLOAT_APP_VENDOR_INDEX, "INSTALLED_AFLOAT_APP_VENDOR_ID", "INSTALLED_AFLOAT_APP_VENDOR_NAME", aFLOAT_APP_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID);
            return View(aFLOAT_APP_INDEX);
        }

        // GET: AFLOAT_APP_INDEX/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFLOAT_APP_INDEX aFLOAT_APP_INDEX = await db.AFLOAT_APP_INDEX.FindAsync(id);
            if (aFLOAT_APP_INDEX == null)
            {
                return HttpNotFound();
            }
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", aFLOAT_APP_INDEX.DISPLAY_INDICATOR_ID);
            ViewBag.INSTALLED_AFLOAT_APP_VENDOR_ID = new SelectList(db.INSTALLED_AFLOAT_APP_VENDOR_INDEX, "INSTALLED_AFLOAT_APP_VENDOR_ID", "INSTALLED_AFLOAT_APP_VENDOR_NAME", aFLOAT_APP_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID);
            return View(aFLOAT_APP_INDEX);
        }

        // POST: AFLOAT_APP_INDEX/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AFLOAT_APP_ID,AFLOAT_APP_NAME,INSTALLED_AFLOAT_APP_VENDOR_ID,DISPLAY_INDICATOR_ID")] AFLOAT_APP_INDEX aFLOAT_APP_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aFLOAT_APP_INDEX).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", aFLOAT_APP_INDEX.DISPLAY_INDICATOR_ID);
            ViewBag.INSTALLED_AFLOAT_APP_VENDOR_ID = new SelectList(db.INSTALLED_AFLOAT_APP_VENDOR_INDEX, "INSTALLED_AFLOAT_APP_VENDOR_ID", "INSTALLED_AFLOAT_APP_VENDOR_NAME", aFLOAT_APP_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID);
            return View(aFLOAT_APP_INDEX);
        }

        // GET: AFLOAT_APP_INDEX/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFLOAT_APP_INDEX aFLOAT_APP_INDEX = await db.AFLOAT_APP_INDEX.FindAsync(id);
            if (aFLOAT_APP_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(aFLOAT_APP_INDEX);
        }

        // POST: AFLOAT_APP_INDEX/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AFLOAT_APP_INDEX aFLOAT_APP_INDEX = await db.AFLOAT_APP_INDEX.FindAsync(id);
            db.AFLOAT_APP_INDEX.Remove(aFLOAT_APP_INDEX);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
