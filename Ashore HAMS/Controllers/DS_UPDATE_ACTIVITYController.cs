﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class DS_UPDATE_ACTIVITYController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: DS_UPDATE_ACTIVITY
        public async Task<ActionResult> Index()
        {
            var dS_UPDATE_ACTIVITY = db.DS_UPDATE_ACTIVITY.Include(d => d.HULL_SERVER_INDEX);
            return View(await dS_UPDATE_ACTIVITY.ToListAsync());
        }

        // GET: DS_UPDATE_ACTIVITY/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DS_UPDATE_ACTIVITY dS_UPDATE_ACTIVITY = await db.DS_UPDATE_ACTIVITY.FindAsync(id);
            if (dS_UPDATE_ACTIVITY == null)
            {
                return HttpNotFound();
            }
            return View(dS_UPDATE_ACTIVITY);
        }

        // GET: DS_UPDATE_ACTIVITY/Create
        public ActionResult Create()
        {
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID");
            return View();
        }

        // POST: DS_UPDATE_ACTIVITY/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "DS_UPDATE_ACTIVITY_ID,HULL_SERVER_ID,CONTACT_TIMESTAMP,ACTIVITY,EXECUTOR,TIME_DATE_RECORD")] DS_UPDATE_ACTIVITY dS_UPDATE_ACTIVITY)
        {
            if (ModelState.IsValid)
            {
                db.DS_UPDATE_ACTIVITY.Add(dS_UPDATE_ACTIVITY);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", dS_UPDATE_ACTIVITY.HULL_SERVER_ID);
            return View(dS_UPDATE_ACTIVITY);
        }

        // GET: DS_UPDATE_ACTIVITY/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DS_UPDATE_ACTIVITY dS_UPDATE_ACTIVITY = await db.DS_UPDATE_ACTIVITY.FindAsync(id);
            if (dS_UPDATE_ACTIVITY == null)
            {
                return HttpNotFound();
            }
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", dS_UPDATE_ACTIVITY.HULL_SERVER_ID);
            return View(dS_UPDATE_ACTIVITY);
        }

        // POST: DS_UPDATE_ACTIVITY/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "DS_UPDATE_ACTIVITY_ID,HULL_SERVER_ID,CONTACT_TIMESTAMP,ACTIVITY,EXECUTOR,TIME_DATE_RECORD")] DS_UPDATE_ACTIVITY dS_UPDATE_ACTIVITY)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dS_UPDATE_ACTIVITY).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", dS_UPDATE_ACTIVITY.HULL_SERVER_ID);
            return View(dS_UPDATE_ACTIVITY);
        }

        // GET: DS_UPDATE_ACTIVITY/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DS_UPDATE_ACTIVITY dS_UPDATE_ACTIVITY = await db.DS_UPDATE_ACTIVITY.FindAsync(id);
            if (dS_UPDATE_ACTIVITY == null)
            {
                return HttpNotFound();
            }
            return View(dS_UPDATE_ACTIVITY);
        }

        // POST: DS_UPDATE_ACTIVITY/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DS_UPDATE_ACTIVITY dS_UPDATE_ACTIVITY = await db.DS_UPDATE_ACTIVITY.FindAsync(id);
            db.DS_UPDATE_ACTIVITY.Remove(dS_UPDATE_ACTIVITY);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
