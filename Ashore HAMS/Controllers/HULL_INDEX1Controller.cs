﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;
using Ashore_HAMS.ModelView;

namespace Ashore_HAMS.Controllers
{


    public class HULL_INDEX1Controller : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: HULL_INDEX1
        public async Task<ActionResult> Index()
        {
            var hULL_INDEX = db.HULL_INDEX.Include(h => h.CLASS_TYPE_INDEX).Include(h => h.DISPLAY_INDICATOR_INDEX).Include(h => h.HULL_TYPE_INDEX).Include(h => h.RETURN_SERVER_CODE_INDEX).Include(h => h.STATUS_INDEX).Include(h => h.HULL_SERVER_INDEX);
            
            return View(await hULL_INDEX.ToListAsync());
        }

        // GET: HULL_INDEX1/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HULL_INDEX hULL_INDEX = await db.HULL_INDEX.FindAsync(id);
            if (hULL_INDEX == null)
            {
                return HttpNotFound();
            }


            var s_index =
                from hs in db.HULL_SERVER_INDEX
                join s in db.SERVER_INDEX on hs.SERVER_ID equals s.SERVER_ID
                where hs.HULL_ID == id
                select new HullServer
                {
                    HULL_SERVER_INDEX = hs,
                    SERVER_INDEX = s
                };

            ViewBag.HullServer = s_index;

            var hsr = from hs in db.HULL_SERVER_INDEX
                    where hs.HULL_ID == id
                    select hs;

            ViewBag.HULL_SERVER_INDEX = hsr.ToList<HULL_SERVER_INDEX>(); // Enumerable.Range(0, 10).ToList();


            return View(hULL_INDEX);
        }

        // GET: HULL_INDEX1/Create
        public ActionResult Create()
        {
            ViewBag.CLASS_TYPE_ID = new SelectList(db.CLASS_TYPE_INDEX, "CLASS_TYPE_ID", "CLASS_TYPE_NAME");
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME");
            ViewBag.HULL_TYPE_ID = new SelectList(db.HULL_TYPE_INDEX, "HULL_TYPE_ID", "HULL_TYPE_NAME");
            ViewBag.RETURN_SERVER_CODE_ID = new SelectList(db.RETURN_SERVER_CODE_INDEX, "RETURN_SERVER_CODE_ID", "RETURN_SERVER_CODE_NAME");
            ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME");
            return View();
        }

        // POST: HULL_INDEX1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "HULL_ID,HULL,HULL_NAME,STATUS_ID,HULL_TYPE_ID,CLASS_TYPE_ID,RETURN_SERVER_CODE_ID,DISPLAY_INDICATOR_ID")] HULL_INDEX hULL_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.HULL_INDEX.Add(hULL_INDEX);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CLASS_TYPE_ID = new SelectList(db.CLASS_TYPE_INDEX, "CLASS_TYPE_ID", "CLASS_TYPE_NAME", hULL_INDEX.CLASS_TYPE_ID);
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", hULL_INDEX.DISPLAY_INDICATOR_ID);
            ViewBag.HULL_TYPE_ID = new SelectList(db.HULL_TYPE_INDEX, "HULL_TYPE_ID", "HULL_TYPE_NAME", hULL_INDEX.HULL_TYPE_ID);
            ViewBag.RETURN_SERVER_CODE_ID = new SelectList(db.RETURN_SERVER_CODE_INDEX, "RETURN_SERVER_CODE_ID", "RETURN_SERVER_CODE_NAME", hULL_INDEX.RETURN_SERVER_CODE_ID);
            ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME", hULL_INDEX.STATUS_ID);
            return View(hULL_INDEX);
        }

        // GET: HULL_INDEX1/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HULL_INDEX hULL_INDEX = await db.HULL_INDEX.FindAsync(id);
            if (hULL_INDEX == null)
            {
                return HttpNotFound();
            }
            ViewBag.CLASS_TYPE_ID = new SelectList(db.CLASS_TYPE_INDEX, "CLASS_TYPE_ID", "CLASS_TYPE_NAME", hULL_INDEX.CLASS_TYPE_ID);
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", hULL_INDEX.DISPLAY_INDICATOR_ID);
            ViewBag.HULL_TYPE_ID = new SelectList(db.HULL_TYPE_INDEX, "HULL_TYPE_ID", "HULL_TYPE_NAME", hULL_INDEX.HULL_TYPE_ID);
            ViewBag.RETURN_SERVER_CODE_ID = new SelectList(db.RETURN_SERVER_CODE_INDEX, "RETURN_SERVER_CODE_ID", "RETURN_SERVER_CODE_NAME", hULL_INDEX.RETURN_SERVER_CODE_ID);
            ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME", hULL_INDEX.STATUS_ID);
            return View(hULL_INDEX);
        }

        // POST: HULL_INDEX1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "HULL_ID,HULL,HULL_NAME,STATUS_ID,HULL_TYPE_ID,CLASS_TYPE_ID,RETURN_SERVER_CODE_ID,DISPLAY_INDICATOR_ID")] HULL_INDEX hULL_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hULL_INDEX).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CLASS_TYPE_ID = new SelectList(db.CLASS_TYPE_INDEX, "CLASS_TYPE_ID", "CLASS_TYPE_NAME", hULL_INDEX.CLASS_TYPE_ID);
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", hULL_INDEX.DISPLAY_INDICATOR_ID);
            ViewBag.HULL_TYPE_ID = new SelectList(db.HULL_TYPE_INDEX, "HULL_TYPE_ID", "HULL_TYPE_NAME", hULL_INDEX.HULL_TYPE_ID);
            ViewBag.RETURN_SERVER_CODE_ID = new SelectList(db.RETURN_SERVER_CODE_INDEX, "RETURN_SERVER_CODE_ID", "RETURN_SERVER_CODE_NAME", hULL_INDEX.RETURN_SERVER_CODE_ID);
            ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME", hULL_INDEX.STATUS_ID);
            return View(hULL_INDEX);
        }

        // GET: HULL_INDEX1/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HULL_INDEX hULL_INDEX = await db.HULL_INDEX.FindAsync(id);
            if (hULL_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(hULL_INDEX);
        }

        // POST: HULL_INDEX1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            HULL_INDEX hULL_INDEX = await db.HULL_INDEX.FindAsync(id);
            db.HULL_INDEX.Remove(hULL_INDEX);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
