﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class HULL_APP_VERSION_MONITORINGController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: HULL_APP_VERSION_MONITORING
        public async Task<ActionResult> Index()
        {
            var hULL_APP_VERSION_MONITORING = db.HULL_APP_VERSION_MONITORING.Include(h => h.AFLOAT_HULL_APP_INSTALLED).Include(h => h.AUTH_HULL_APP_RELEASE).Include(h => h.HULL_SERVER_INDEX).Include(h => h.STATUS_INDEX);
            return View(await hULL_APP_VERSION_MONITORING.ToListAsync());
        }

        // GET: HULL_APP_VERSION_MONITORING/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HULL_APP_VERSION_MONITORING hULL_APP_VERSION_MONITORING = await db.HULL_APP_VERSION_MONITORING.FindAsync(id);
            if (hULL_APP_VERSION_MONITORING == null)
            {
                return HttpNotFound();
            }
            return View(hULL_APP_VERSION_MONITORING);
        }

        // GET: HULL_APP_VERSION_MONITORING/Create
        public ActionResult Create()
        {
            ViewBag.AFLOAT_HULL_APP_INSTALLED_ID = new SelectList(db.AFLOAT_HULL_APP_INSTALLED, "AFLOAT_HULL_APP_INSTALLED_ID", "AFLOAT_HULL_APP_INSTALLED_ID");
            ViewBag.AUTH_HULL_APP_RELEASE_ID = new SelectList(db.AUTH_HULL_APP_RELEASE, "AUTH_HULL_APP_RELEASE_ID", "AUTH_HULL_APP_RELEASE_ID");
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID");
            ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME");
            return View();
        }

        // POST: HULL_APP_VERSION_MONITORING/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "HULL_APP_VERSION_MONITORING_ID,HULL_SERVER_ID,AUTH_HULL_APP_RELEASE_ID,AFLOAT_HULL_APP_INSTALLED_ID,STATUS_ID,TIME_DATE_RECORD")] HULL_APP_VERSION_MONITORING hULL_APP_VERSION_MONITORING)
        {
            if (ModelState.IsValid)
            {
                db.HULL_APP_VERSION_MONITORING.Add(hULL_APP_VERSION_MONITORING);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AFLOAT_HULL_APP_INSTALLED_ID = new SelectList(db.AFLOAT_HULL_APP_INSTALLED, "AFLOAT_HULL_APP_INSTALLED_ID", "AFLOAT_HULL_APP_INSTALLED_ID", hULL_APP_VERSION_MONITORING.AFLOAT_HULL_APP_INSTALLED_ID);
            ViewBag.AUTH_HULL_APP_RELEASE_ID = new SelectList(db.AUTH_HULL_APP_RELEASE, "AUTH_HULL_APP_RELEASE_ID", "AUTH_HULL_APP_RELEASE_ID", hULL_APP_VERSION_MONITORING.AUTH_HULL_APP_RELEASE_ID);
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", hULL_APP_VERSION_MONITORING.HULL_SERVER_ID);
            ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME", hULL_APP_VERSION_MONITORING.STATUS_ID);
            return View(hULL_APP_VERSION_MONITORING);
        }

        // GET: HULL_APP_VERSION_MONITORING/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HULL_APP_VERSION_MONITORING hULL_APP_VERSION_MONITORING = await db.HULL_APP_VERSION_MONITORING.FindAsync(id);
            if (hULL_APP_VERSION_MONITORING == null)
            {
                return HttpNotFound();
            }
            ViewBag.AFLOAT_HULL_APP_INSTALLED_ID = new SelectList(db.AFLOAT_HULL_APP_INSTALLED, "AFLOAT_HULL_APP_INSTALLED_ID", "AFLOAT_HULL_APP_INSTALLED_ID", hULL_APP_VERSION_MONITORING.AFLOAT_HULL_APP_INSTALLED_ID);
            ViewBag.AUTH_HULL_APP_RELEASE_ID = new SelectList(db.AUTH_HULL_APP_RELEASE, "AUTH_HULL_APP_RELEASE_ID", "AUTH_HULL_APP_RELEASE_ID", hULL_APP_VERSION_MONITORING.AUTH_HULL_APP_RELEASE_ID);
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", hULL_APP_VERSION_MONITORING.HULL_SERVER_ID);
            ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME", hULL_APP_VERSION_MONITORING.STATUS_ID);
            return View(hULL_APP_VERSION_MONITORING);
        }

        // POST: HULL_APP_VERSION_MONITORING/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "HULL_APP_VERSION_MONITORING_ID,HULL_SERVER_ID,AUTH_HULL_APP_RELEASE_ID,AFLOAT_HULL_APP_INSTALLED_ID,STATUS_ID,TIME_DATE_RECORD")] HULL_APP_VERSION_MONITORING hULL_APP_VERSION_MONITORING)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hULL_APP_VERSION_MONITORING).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AFLOAT_HULL_APP_INSTALLED_ID = new SelectList(db.AFLOAT_HULL_APP_INSTALLED, "AFLOAT_HULL_APP_INSTALLED_ID", "AFLOAT_HULL_APP_INSTALLED_ID", hULL_APP_VERSION_MONITORING.AFLOAT_HULL_APP_INSTALLED_ID);
            ViewBag.AUTH_HULL_APP_RELEASE_ID = new SelectList(db.AUTH_HULL_APP_RELEASE, "AUTH_HULL_APP_RELEASE_ID", "AUTH_HULL_APP_RELEASE_ID", hULL_APP_VERSION_MONITORING.AUTH_HULL_APP_RELEASE_ID);
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", hULL_APP_VERSION_MONITORING.HULL_SERVER_ID);
            ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME", hULL_APP_VERSION_MONITORING.STATUS_ID);
            return View(hULL_APP_VERSION_MONITORING);
        }

        // GET: HULL_APP_VERSION_MONITORING/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HULL_APP_VERSION_MONITORING hULL_APP_VERSION_MONITORING = await db.HULL_APP_VERSION_MONITORING.FindAsync(id);
            if (hULL_APP_VERSION_MONITORING == null)
            {
                return HttpNotFound();
            }
            return View(hULL_APP_VERSION_MONITORING);
        }

        // POST: HULL_APP_VERSION_MONITORING/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            HULL_APP_VERSION_MONITORING hULL_APP_VERSION_MONITORING = await db.HULL_APP_VERSION_MONITORING.FindAsync(id);
            db.HULL_APP_VERSION_MONITORING.Remove(hULL_APP_VERSION_MONITORING);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
