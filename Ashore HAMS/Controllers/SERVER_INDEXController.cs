﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class SERVER_INDEXController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: SERVER_INDEX
        public async Task<ActionResult> Index()
        {
            var sERVER_INDEX = db.SERVER_INDEX.Include(s => s.DISPLAY_INDICATOR_INDEX);
            return View(await sERVER_INDEX.ToListAsync());
        }

        // GET: SERVER_INDEX/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERVER_INDEX sERVER_INDEX = await db.SERVER_INDEX.FindAsync(id);
            if (sERVER_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(sERVER_INDEX);
        }

        // GET: SERVER_INDEX/Create
        public ActionResult Create()
        {
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME");
            return View();
        }

        // POST: SERVER_INDEX/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "SERVER_ID,SERVERNAME,DISPLAY_INDICATOR_ID")] SERVER_INDEX sERVER_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.SERVER_INDEX.Add(sERVER_INDEX);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", sERVER_INDEX.DISPLAY_INDICATOR_ID);
            return View(sERVER_INDEX);
        }

        // GET: SERVER_INDEX/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERVER_INDEX sERVER_INDEX = await db.SERVER_INDEX.FindAsync(id);
            if (sERVER_INDEX == null)
            {
                return HttpNotFound();
            }
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", sERVER_INDEX.DISPLAY_INDICATOR_ID);
            return View(sERVER_INDEX);
        }

        // POST: SERVER_INDEX/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "SERVER_ID,SERVERNAME,DISPLAY_INDICATOR_ID")] SERVER_INDEX sERVER_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sERVER_INDEX).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", sERVER_INDEX.DISPLAY_INDICATOR_ID);
            return View(sERVER_INDEX);
        }

        // GET: SERVER_INDEX/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERVER_INDEX sERVER_INDEX = await db.SERVER_INDEX.FindAsync(id);
            if (sERVER_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(sERVER_INDEX);
        }

        // POST: SERVER_INDEX/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            SERVER_INDEX sERVER_INDEX = await db.SERVER_INDEX.FindAsync(id);
            db.SERVER_INDEX.Remove(sERVER_INDEX);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
