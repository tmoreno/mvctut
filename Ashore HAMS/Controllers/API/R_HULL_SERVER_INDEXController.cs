﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers.API
{
    public class R_HULL_SERVER_INDEXController : ApiController
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // POST: api/HULL_SERVER_INDEX
        [ResponseType(typeof(HULL_SERVER_INDEX))]
        public async Task<IHttpActionResult> PostHULL_SERVER_INDEX(HULL_SERVER_INDEX hULL_SERVER_INDEX)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.HULL_SERVER_INDEX.Add(hULL_SERVER_INDEX);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = hULL_SERVER_INDEX.HULL_SERVER_ID }, hULL_SERVER_INDEX);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}