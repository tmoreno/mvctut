﻿using Ashore_HAMS.Models3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ashore_HAMS.Controllers.API
{
    public class AuthorizedApplicationController : ApiController
    {

        HAMS9Entities1 db = new HAMS9Entities1();

        private bool IsActive(DateTime? lastContact)
        {
            TimeSpan ts = (TimeSpan)(DateTime.Now - lastContact);
            return (ts.TotalSeconds < 10);
        }

        public class JsonObj
        {
            public AuthorizedInstalledView AuthorizedInstalledView { get; set; }
            public float AuthAppSummary { get; set; }
            public float ReleasedVsUnreleased { get; set;}
            public float MsgUpdate { get; set; }
        }

        // GET: api/AuthorizedApplication
        public JsonObj Get()
        {
            AUTH_APP_INDEXController aUTH_APP_INDEXController = new AUTH_APP_INDEXController();

            float unreleasedCount = (from a in db.AUTH_APP_RELEASE
                                   select a).Count();
            float releasedCount = aUTH_APP_INDEXController.AuthorizedReleaseCount.Count();

            IList<int> codes = new List<int>();

            var msgUpdates = (from a in db.DS_UPDATE_ACTIVITY
                              select a).ToList< DS_UPDATE_ACTIVITY>();

            foreach(DS_UPDATE_ACTIVITY item in msgUpdates)
            {
                if(IsActive(item.CONTACT_TIMESTAMP))
                {
                    codes.Add(1);
                }

            }

            float total = (from a in db.HULL_SERVER_INDEX
                         select a).Count();

            JsonObj retval = new JsonObj
            {
                AuthAppSummary = aUTH_APP_INDEXController.GetAuthAppSummary(),
                ReleasedVsUnreleased = (releasedCount / unreleasedCount) * 100,
                MsgUpdate = (codes.Count() / total) * 100
            };

            return retval;
        }


        // POST: api/AuthorizedApplication
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/AuthorizedApplication/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/AuthorizedApplication/5
        public void Delete(int id)
        {
        }
    }
}
