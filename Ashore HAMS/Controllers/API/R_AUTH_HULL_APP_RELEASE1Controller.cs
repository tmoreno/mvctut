﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers.API
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using Ashore_HAMS.Models3;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<AUTH_HULL_APP_RELEASE>("R_AUTH_HULL_APP_RELEASE1");
    builder.EntitySet<AUTH_APP_RELEASE>("AUTH_APP_RELEASE"); 
    builder.EntitySet<HULL_SERVER_INDEX>("HULL_SERVER_INDEX"); 
    builder.EntitySet<HULL_APP_VERSION_MONITORING>("HULL_APP_VERSION_MONITORING"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class R_AUTH_HULL_APP_RELEASE1Controller : ODataController
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: odata/R_AUTH_HULL_APP_RELEASE1
        [EnableQuery]
        public IQueryable<AUTH_HULL_APP_RELEASE> GetR_AUTH_HULL_APP_RELEASE1()
        {
            var x = from a in db.AUTH_HULL_APP_RELEASE
                    select new AUTH_HULL_APP_RELEASE{ HULL_SERVER_ID = a.HULL_SERVER_ID };
            return x;
        }

        // GET: odata/R_AUTH_HULL_APP_RELEASE1(5)
        [EnableQuery]
        public SingleResult<AUTH_HULL_APP_RELEASE> GetAUTH_HULL_APP_RELEASE([FromODataUri] int key)
        {
            return SingleResult.Create(db.AUTH_HULL_APP_RELEASE.Where(aUTH_HULL_APP_RELEASE => aUTH_HULL_APP_RELEASE.AUTH_HULL_APP_RELEASE_ID == key));
        }

        // PUT: odata/R_AUTH_HULL_APP_RELEASE1(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<AUTH_HULL_APP_RELEASE> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            AUTH_HULL_APP_RELEASE aUTH_HULL_APP_RELEASE = await db.AUTH_HULL_APP_RELEASE.FindAsync(key);
            if (aUTH_HULL_APP_RELEASE == null)
            {
                return NotFound();
            }

            patch.Put(aUTH_HULL_APP_RELEASE);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AUTH_HULL_APP_RELEASEExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(aUTH_HULL_APP_RELEASE);
        }

        // POST: odata/R_AUTH_HULL_APP_RELEASE1
        public async Task<IHttpActionResult> Post(AUTH_HULL_APP_RELEASE aUTH_HULL_APP_RELEASE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AUTH_HULL_APP_RELEASE.Add(aUTH_HULL_APP_RELEASE);
            await db.SaveChangesAsync();

            return Created(aUTH_HULL_APP_RELEASE);
        }

        // PATCH: odata/R_AUTH_HULL_APP_RELEASE1(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<AUTH_HULL_APP_RELEASE> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            AUTH_HULL_APP_RELEASE aUTH_HULL_APP_RELEASE = await db.AUTH_HULL_APP_RELEASE.FindAsync(key);
            if (aUTH_HULL_APP_RELEASE == null)
            {
                return NotFound();
            }

            patch.Patch(aUTH_HULL_APP_RELEASE);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AUTH_HULL_APP_RELEASEExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(aUTH_HULL_APP_RELEASE);
        }

        // DELETE: odata/R_AUTH_HULL_APP_RELEASE1(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            AUTH_HULL_APP_RELEASE aUTH_HULL_APP_RELEASE = await db.AUTH_HULL_APP_RELEASE.FindAsync(key);
            if (aUTH_HULL_APP_RELEASE == null)
            {
                return NotFound();
            }

            db.AUTH_HULL_APP_RELEASE.Remove(aUTH_HULL_APP_RELEASE);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/R_AUTH_HULL_APP_RELEASE1(5)/AUTH_APP_RELEASE
        [EnableQuery]
        public SingleResult<AUTH_APP_RELEASE> GetAUTH_APP_RELEASE([FromODataUri] int key)
        {
            return SingleResult.Create(db.AUTH_HULL_APP_RELEASE.Where(m => m.AUTH_HULL_APP_RELEASE_ID == key).Select(m => m.AUTH_APP_RELEASE));
        }

        // GET: odata/R_AUTH_HULL_APP_RELEASE1(5)/HULL_SERVER_INDEX
        [EnableQuery]
        public SingleResult<HULL_SERVER_INDEX> GetHULL_SERVER_INDEX([FromODataUri] int key)
        {
            return SingleResult.Create(db.AUTH_HULL_APP_RELEASE.Where(m => m.AUTH_HULL_APP_RELEASE_ID == key).Select(m => m.HULL_SERVER_INDEX));
        }

        // GET: odata/R_AUTH_HULL_APP_RELEASE1(5)/HULL_APP_VERSION_MONITORING
        [EnableQuery]
        public IQueryable<HULL_APP_VERSION_MONITORING> GetHULL_APP_VERSION_MONITORING([FromODataUri] int key)
        {
            return db.AUTH_HULL_APP_RELEASE.Where(m => m.AUTH_HULL_APP_RELEASE_ID == key).SelectMany(m => m.HULL_APP_VERSION_MONITORING);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AUTH_HULL_APP_RELEASEExists(int key)
        {
            return db.AUTH_HULL_APP_RELEASE.Count(e => e.AUTH_HULL_APP_RELEASE_ID == key) > 0;
        }
    }
}
