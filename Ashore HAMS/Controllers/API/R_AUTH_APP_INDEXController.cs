﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers.API
{
    public class R_AUTH_APP_INDEXController : ApiController
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: api/AUTH_APP_INDEX
        public IQueryable<AUTH_APP_INDEX> GetAUTH_APP_INDEX()
        {
            return db.AUTH_APP_INDEX;
        }

        // GET: api/AUTH_APP_INDEX/5
        [ResponseType(typeof(AUTH_APP_INDEX))]
        public async Task<IHttpActionResult> GetAUTH_APP_INDEX(int id)
        {
            AUTH_APP_INDEX aUTH_APP_INDEX = await db.AUTH_APP_INDEX.FindAsync(id);
            if (aUTH_APP_INDEX == null)
            {
                return NotFound();
            }

            return Ok(aUTH_APP_INDEX);
        }

        // PUT: api/AUTH_APP_INDEX/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAUTH_APP_INDEX(int id, AUTH_APP_INDEX aUTH_APP_INDEX)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aUTH_APP_INDEX.AUTH_APP_ID)
            {
                return BadRequest();
            }

            db.Entry(aUTH_APP_INDEX).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AUTH_APP_INDEXExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AUTH_APP_INDEX
        [ResponseType(typeof(AUTH_APP_INDEX))]
        public async Task<IHttpActionResult> PostAUTH_APP_INDEX(AUTH_APP_INDEX aUTH_APP_INDEX)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AUTH_APP_INDEX.Add(aUTH_APP_INDEX);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = aUTH_APP_INDEX.AUTH_APP_ID }, aUTH_APP_INDEX);
        }

        // DELETE: api/AUTH_APP_INDEX/5
        [ResponseType(typeof(AUTH_APP_INDEX))]
        public async Task<IHttpActionResult> DeleteAUTH_APP_INDEX(int id)
        {
            AUTH_APP_INDEX aUTH_APP_INDEX = await db.AUTH_APP_INDEX.FindAsync(id);
            if (aUTH_APP_INDEX == null)
            {
                return NotFound();
            }

            db.AUTH_APP_INDEX.Remove(aUTH_APP_INDEX);
            await db.SaveChangesAsync();

            return Ok(aUTH_APP_INDEX);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AUTH_APP_INDEXExists(int id)
        {
            return db.AUTH_APP_INDEX.Count(e => e.AUTH_APP_ID == id) > 0;
        }
    }
}