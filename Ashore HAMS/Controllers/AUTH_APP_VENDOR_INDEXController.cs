﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class AUTH_APP_VENDOR_INDEXController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: AUTH_APP_VENDOR_INDEX
        public async Task<ActionResult> Index()
        {
            var aUTH_APP_VENDOR_INDEX = db.AUTH_APP_VENDOR_INDEX.Include(a => a.DISPLAY_INDICATOR_INDEX);
            return View(await aUTH_APP_VENDOR_INDEX.ToListAsync());
        }

        // GET: AUTH_APP_VENDOR_INDEX/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                ViewBag.AUTH_APP_ID = (Int32)id;
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_APP_VENDOR_INDEX aUTH_APP_VENDOR_INDEX = await db.AUTH_APP_VENDOR_INDEX.FindAsync(id);
            if (aUTH_APP_VENDOR_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(aUTH_APP_VENDOR_INDEX);
        }

        // GET: AUTH_APP_VENDOR_INDEX/Create
        public ActionResult Create()
        {
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME");
            return View();
        }

        // POST: AUTH_APP_VENDOR_INDEX/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AUTH_APP_VENDOR_ID,AUTH_APP_VENDOR_NAME,DISPLAY_INDICATOR_ID")] AUTH_APP_VENDOR_INDEX aUTH_APP_VENDOR_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.AUTH_APP_VENDOR_INDEX.Add(aUTH_APP_VENDOR_INDEX);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", aUTH_APP_VENDOR_INDEX.DISPLAY_INDICATOR_ID);
            return View(aUTH_APP_VENDOR_INDEX);
        }

        // GET: AUTH_APP_VENDOR_INDEX/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_APP_VENDOR_INDEX aUTH_APP_VENDOR_INDEX = await db.AUTH_APP_VENDOR_INDEX.FindAsync(id);
            if (aUTH_APP_VENDOR_INDEX == null)
            {
                return HttpNotFound();
            }
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", aUTH_APP_VENDOR_INDEX.DISPLAY_INDICATOR_ID);
            return View(aUTH_APP_VENDOR_INDEX);
        }

        // POST: AUTH_APP_VENDOR_INDEX/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AUTH_APP_VENDOR_ID,AUTH_APP_VENDOR_NAME,DISPLAY_INDICATOR_ID")] AUTH_APP_VENDOR_INDEX aUTH_APP_VENDOR_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aUTH_APP_VENDOR_INDEX).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", aUTH_APP_VENDOR_INDEX.DISPLAY_INDICATOR_ID);
            return View(aUTH_APP_VENDOR_INDEX);
        }

        // GET: AUTH_APP_VENDOR_INDEX/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_APP_VENDOR_INDEX aUTH_APP_VENDOR_INDEX = await db.AUTH_APP_VENDOR_INDEX.FindAsync(id);
            if (aUTH_APP_VENDOR_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(aUTH_APP_VENDOR_INDEX);
        }

        // POST: AUTH_APP_VENDOR_INDEX/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AUTH_APP_VENDOR_INDEX aUTH_APP_VENDOR_INDEX = await db.AUTH_APP_VENDOR_INDEX.FindAsync(id);
            db.AUTH_APP_VENDOR_INDEX.Remove(aUTH_APP_VENDOR_INDEX);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
