﻿using Ashore_HAMS.Controllers.API2;
using Ashore_HAMS.Models3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Linq;
using System.Web.Http.Results;

namespace MVCTut.Controllers
{
    public class UploadController : Controller
    {

        private HAMS9Entities1 db = new HAMS9Entities1();


        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }

        public IList<int> GetHullServerIds()
        {
            IList<int> list = (from a in db.HULL_SERVER_INDEX
                               select a.HULL_SERVER_ID).ToList<int>();
            return list;
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {

            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                // extract only the filename
                var fileName = Path.GetFileName(file.FileName);
                // store the file inside ~/App_Data/uploads folder
                //var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                var path = Path.Combine(@"C:\Users\tmoreno\work\dropbox\", fileName);
                file.SaveAs(path);
                Thread.Sleep(1500);

                long length = new System.IO.FileInfo(path).Length;

                using (var stream = new BufferedStream(System.IO.File.OpenRead(path), 1200000))
                {
                    SHA256Managed sha = new SHA256Managed();
                    byte[] checksum = sha.ComputeHash(stream);
                    string sha256str = BitConverter.ToString(checksum).Replace("-", String.Empty);

                    AshoreFile ashoreFile = new AshoreFile()
                    {
                        Filename = fileName,
                        Date = DateTime.Now,
                        Hash = sha256str,
                        Size = (int)length
                    };

                    AshoreFilesController ashoreFilesController = new AshoreFilesController();
                    IHttpActionResult actionResult = ashoreFilesController.PostAshoreFile(ashoreFile);

                    var result = (CreatedAtRouteNegotiatedContentResult<AshoreFile>)actionResult;
                    var fileId = (int)result.RouteValues["id"];

                    // Release
                    AshoreFileHullServersController ashoreFileHullServersController = new AshoreFileHullServersController();
                    foreach (int sid in GetHullServerIds())
                    {
                        AshoreFileHullServer ashoreFileHullServer = new AshoreFileHullServer()
                        {
                            HULL_SERVER_ID = sid,
                            AshoreFileId = fileId
                        };

                        actionResult = ashoreFileHullServersController.PostAshoreFileHullServer(ashoreFileHullServer);
                        var miniresult = (CreatedAtRouteNegotiatedContentResult<AshoreFileHullServer>)actionResult;
                        //Console.WriteLine(miniresult.RouteValues["id"]);
                    }

                    ClearAfloatFileIfExists(fileId);
                }



            }
            // redirect back to the index action to show the form once again
            return RedirectToAction("Index");
        }

        public void ClearAfloatFileIfExists(int ashoreFileId)
        {
            var x =
            from a in db.AfloatFileHullServers
            where a.AshoreFileId == ashoreFileId
            select a;

            db.AfloatFileHullServers.RemoveRange(x);
            db.SaveChanges();

        }

    }
}