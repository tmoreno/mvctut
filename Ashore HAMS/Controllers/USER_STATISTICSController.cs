﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class USER_STATISTICSController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: USER_STATISTICS
        public async Task<ActionResult> Index()
        {
            var uSER_STATISTICS = db.USER_STATISTICS.Include(u => u.USER_INDEX);
            return View(await uSER_STATISTICS.ToListAsync());
        }

        // GET: USER_STATISTICS/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USER_STATISTICS uSER_STATISTICS = await db.USER_STATISTICS.FindAsync(id);
            if (uSER_STATISTICS == null)
            {
                return HttpNotFound();
            }
            return View(uSER_STATISTICS);
        }

        // GET: USER_STATISTICS/Create
        public ActionResult Create()
        {
            ViewBag.USER_ID = new SelectList(db.USER_INDEX, "USER_ID", "CAC");
            return View();
        }

        // POST: USER_STATISTICS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "USER_STATISTICS_ID,USER_ID,ACCESSED_PAGE,ACCESSED_TIMESTAMP")] USER_STATISTICS uSER_STATISTICS)
        {
            if (ModelState.IsValid)
            {
                db.USER_STATISTICS.Add(uSER_STATISTICS);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.USER_ID = new SelectList(db.USER_INDEX, "USER_ID", "CAC", uSER_STATISTICS.USER_ID);
            return View(uSER_STATISTICS);
        }

        // GET: USER_STATISTICS/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USER_STATISTICS uSER_STATISTICS = await db.USER_STATISTICS.FindAsync(id);
            if (uSER_STATISTICS == null)
            {
                return HttpNotFound();
            }
            ViewBag.USER_ID = new SelectList(db.USER_INDEX, "USER_ID", "CAC", uSER_STATISTICS.USER_ID);
            return View(uSER_STATISTICS);
        }

        // POST: USER_STATISTICS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "USER_STATISTICS_ID,USER_ID,ACCESSED_PAGE,ACCESSED_TIMESTAMP")] USER_STATISTICS uSER_STATISTICS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uSER_STATISTICS).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.USER_ID = new SelectList(db.USER_INDEX, "USER_ID", "CAC", uSER_STATISTICS.USER_ID);
            return View(uSER_STATISTICS);
        }

        // GET: USER_STATISTICS/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USER_STATISTICS uSER_STATISTICS = await db.USER_STATISTICS.FindAsync(id);
            if (uSER_STATISTICS == null)
            {
                return HttpNotFound();
            }
            return View(uSER_STATISTICS);
        }

        // POST: USER_STATISTICS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            USER_STATISTICS uSER_STATISTICS = await db.USER_STATISTICS.FindAsync(id);
            db.USER_STATISTICS.Remove(uSER_STATISTICS);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
