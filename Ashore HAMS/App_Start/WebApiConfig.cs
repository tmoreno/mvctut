﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using Ashore_HAMS.Models3;


namespace Ashore_HAMS
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.Add(config.Formatters.JsonFormatter);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            //ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            //builder.EntitySet<AUTH_HULL_APP_RELEASE>("R_AUTH_HULL_APP_RELEASE1");
            //builder.EntitySet<AUTH_APP_RELEASE>("AUTH_APP_RELEASE");
            //builder.EntitySet<HULL_SERVER_INDEX>("HULL_SERVER_INDEX");
            //builder.EntitySet<HULL_APP_VERSION_MONITORING>("HULL_APP_VERSION_MONITORING");
            //config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());


        }
    }
}
