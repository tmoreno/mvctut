class User {
    
    constructor(name) {
        this._name = name;
        this._fullname = name + "Moreno";

    }
 
    sayHi() {
        console.log(this._name);
    }

    closure(callback) {
        if(typeof callback == "function") {
            var output = callback(1, 2);
            console.log("This is result of function: " + output);
            this._callback = callback;
        }
        else {
            console.log("not a function")
        }
    }

    update() {
        console.log(this._callback(this._lastname, 34));
    }

}


class MultiLineChart {
    constructor(id, w, h) {
        this.svg = d3.select("#" + id).append("svg").attr("width", w).attr("height", h);
        this.margin = { top: 20, right: 80, bottom: 30, left: 50 }
        var margin = this.margin;

        this.width = this.svg.attr("width") - margin.left - margin.right;
        var width = this.width;

        this.height = this.svg.attr("height") - margin.top - margin.bottom;
        var height = this.height;

        this.g = this.svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        this.parseTime = d3.timeParse("%Y%m%d");

        this.x = d3.scaleTime().range([0, width]);
        this.y = d3.scaleLinear().range([height, 0])
        this.z = d3.scaleOrdinal(d3.schemeCategory10);

        var x = this.x;
        var y = this.y;

        this.line = d3.line()
            .curve(d3.curveBasis)
            .x(function (d) { return x(d.date); })
            .y(function (d) { return y(d.temperature); });

    }

    updateChart(data) {
        var cities = data.columns.slice(1).map(function (id) {
            return {
                id: id,
                values: data.map(function (d) {
                    return { date: d.date, temperature: d[id] };
                })
            };
        });

        this.x.domain(d3.extent(data, function (d) { return d.date; }));

        this.y.domain([
            d3.min(cities, function (c) { return d3.min(c.values, function (d) { return d.temperature; }); }),
            d3.max(cities, function (c) { return d3.max(c.values, function (d) { return d.temperature; }); })
        ]);

        this.z.domain(cities.map(function (c) { return c.id; }));

        this.g.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + this.height + ")")
            .call(d3.axisBottom(this.x));

        this.g.append("g")
            .attr("class", "axis axis--y")
            .call(d3.axisLeft(this.y))
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", "0.71em")
            .attr("fill", "#000")
            .text("Temperature, ºF");

        var city = this.g.selectAll(".city")
            .data(cities)
            .enter().append("g")
            .attr("class", "city");

        var line = this.line;
        var x = this.x;
        var y = this.y;
        var z = this.z;

        city.append("path")
            .attr("class", "line")
            .attr("d", function (d) { return line(d.values); })
            .style("stroke", function (d) { return z(d.id); });

        city.append("text")
            .datum(function (d) { return { id: d.id, value: d.values[d.values.length - 1] }; })
            .attr("transform", function (d) { return "translate(" + x(d.value.date) + "," + y(d.value.temperature) + ")"; })
            .attr("x", 3)
            .attr("dy", "0.35em")
            .style("font", "10px sans-serif")
            .text(function (d) { return d.id; });
    }
}

class HeatMapChart {
    constructor(id, w, h, size, domain) {
        this.itemSize = size,
            this.cellSize = this.itemSize - 1,
            this.margin = { top: 10, right: 40, bottom: 20, left: 10 };

        var margin = this.margin;

        this.width = w - margin.right - margin.left,
        this.height = h - margin.top - margin.bottom;

        var width = this.width;
        var height = this.height;

        this._domain = domain;

        this.svg = d3.select(id)
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    }


    colorScale(callback) {
        this._callback = callback;
    }


    updateChart(data) {
        var itemSize = this.itemSize;
        var cellSize = this.cellSize;
        var colorScale = this._callback(this._domain);

        var x_elements = d3.set(data.map(function (item) { return item.product; })).values(),
            y_elements = d3.set(data.map(function (item) { return item.country; })).values();

        var xScale = d3.scaleBand()

            // var xScale = d3.scale.ordinal()
            .domain(x_elements)
            .range([0, x_elements.length * itemSize]);

        var xAxis = d3.axisTop()
            .scale(xScale)
            .tickFormat(function (d) {
                return d;
            });

        var yScale = d3.scaleBand()
        .domain(y_elements)
        .range([0, y_elements.length * itemSize]);

        var yAxis = d3.axisLeft()
            .scale(yScale)
            .tickFormat(function (d) {
                return d;
            });            

        var svg = this.svg;

        var cells = svg.selectAll('rect')
            .data(data)
            .enter().append('g').append('rect')
            .attr('class', 'cell')
            .attr('width', cellSize)
            .attr('height', cellSize)
            .attr('y', function (d) { return yScale(d.country); })
            .attr('x', function (d) { return xScale(d.product); })
            .attr('fill', function (d) { console.log(d.value); return colorScale(d.value); });

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .selectAll('text')
            .attr('font-weight', 'normal');

        svg.append("g")
            .attr("class", "x axis")
            .call(xAxis)
            .selectAll('text')
            .attr('font-weight', 'normal')
            .style("text-anchor", "start")
            .attr("dx", ".8em")
            .attr("dy", ".5em")
            .attr("transform", function (d) {
                return "rotate(-65)";
            });
    }

}
