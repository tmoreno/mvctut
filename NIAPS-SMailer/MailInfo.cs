﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NIAPS.SMailer
{
    public class MailInfo
    {
        public string from { get; set; }
        public string to { get; set; }
        public string subj { get; set; }
        public string text { get; set; }
        public string id { get; set; }
    }
}
