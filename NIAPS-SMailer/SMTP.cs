﻿using Newtonsoft.Json;
using NIAPS.SMailer;
using System;
using System.Net.Http;
using System.Text;

namespace NIAPS_SMailer
{
    public class SMTP
    {
        private string smtpServer = null;
        private int port = Int32.MinValue;
        public SMTP(string smtpServer, int port)
        {
            this.SmtpServer = smtpServer;
            this.Port = port;
        }

        public string SmtpServer { get => smtpServer; set => smtpServer = value; }
        public int Port { get => port; set => port = value; }

        public void Send(MailInfo mailInfo)
        {
            string json = JsonConvert.SerializeObject(mailInfo);

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var result = client.PostAsync(string.Format("http://{0}:{1}/mail", 
                this.SmtpServer, this.Port), content).Result;

            System.Diagnostics.Trace.WriteLine(result);

        }
    }
}
