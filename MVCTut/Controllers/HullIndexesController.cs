﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MVCTut.Models;

namespace MVCTut.Controllers
{
    public class HullIndexesController : ApiController
    {
        private AmendmentIDBContext db = new AmendmentIDBContext();

        // GET: api/HullIndexes
        public IQueryable<HullIndex> GetHullIndex()
        {
            return db.HullIndex;
        }

        // GET: api/HullIndexes/5
        [ResponseType(typeof(HullIndex))]
        public async Task<IHttpActionResult> GetHullIndex(int id)
        {
            HullIndex hullIndex = await db.HullIndex.FindAsync(id);
            if (hullIndex == null)
            {
                return NotFound();
            }

            return Ok(hullIndex);
        }

        // PUT: api/HullIndexes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutHullIndex(int id, HullIndex hullIndex)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hullIndex.ID)
            {
                return BadRequest();
            }

            db.Entry(hullIndex).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HullIndexExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/HullIndexes
        [ResponseType(typeof(HullIndex))]
        public async Task<IHttpActionResult> PostHullIndex(HullIndex hullIndex)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.HullIndex.Add(hullIndex);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = hullIndex.ID }, hullIndex);
        }

        // DELETE: api/HullIndexes/5
        [ResponseType(typeof(HullIndex))]
        public async Task<IHttpActionResult> DeleteHullIndex(int id)
        {
            HullIndex hullIndex = await db.HullIndex.FindAsync(id);
            if (hullIndex == null)
            {
                return NotFound();
            }

            db.HullIndex.Remove(hullIndex);
            await db.SaveChangesAsync();

            return Ok(hullIndex);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HullIndexExists(int id)
        {
            return db.HullIndex.Count(e => e.ID == id) > 0;
        }
    }
}