﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCTut.Models;

namespace MVCTut.Controllers
{
    public class CMsController : Controller
    {
        private AmendmentIDBContext db = new AmendmentIDBContext();

        // GET: CMs
        public async Task<ActionResult> Index()
        {
            return View(await db.CMs.ToListAsync());
        }

        // GET: CMs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CM cM = await db.CMs.FindAsync(id);
            if (cM == null)
            {
                return HttpNotFound();
            }
            return View(cM);
        }

        // GET: CMs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CMs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,ServerId,NIAPSVersion")] CM cM)
        {
            if (ModelState.IsValid)
            {
                db.CMs.Add(cM);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(cM);
        }

        // GET: CMs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CM cM = await db.CMs.FindAsync(id);
            if (cM == null)
            {
                return HttpNotFound();
            }
            return View(cM);
        }

        // POST: CMs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,ServerId,NIAPSVersion")] CM cM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cM).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(cM);
        }

        // GET: CMs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CM cM = await db.CMs.FindAsync(id);
            if (cM == null)
            {
                return HttpNotFound();
            }
            return View(cM);
        }

        // POST: CMs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CM cM = await db.CMs.FindAsync(id);
            db.CMs.Remove(cM);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
