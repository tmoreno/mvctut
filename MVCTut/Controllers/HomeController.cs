﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCTut.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "HAMS/NIAPS Prototype System.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Send Us a Note.";

            return View();
        }
    }
}