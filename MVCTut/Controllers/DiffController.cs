﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MVCTut.Util;
using CatalogCreatorLib;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace MVCTut.Controllers
{
    public class DiffController : ApiController
    {
        // POST api/<controller>

        public class ResultObj
        {
            public DateTime Date { get; set; }
            public string Hash { get; set; }
        }

        public string Get()
        {
            string path = @"c:\Users\tmoreno\axway";
            //DirSearch(path);

            var b = Binder.Instance;
            CatalogFolder root = b.Create(path);

            JsonUtil jsonUtil = new JsonUtil();
            JObject jroot = new JObject();
            jsonUtil.Serialize(root, jroot);

            string serverSHA256Str = Utility.Sha256Hash(jroot.ToString());
            ResultObj obj = new ResultObj()
            {
                Date = DateTime.Now,
                Hash = serverSHA256Str
            };
            return serverSHA256Str;
        }

        public string Post([FromBody] string json)
        {
            return "";
        }
    }
}
