﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCTut.Models.HAMS2;

namespace MVCTut.Controllers
{
    public class SampleController : Controller
    {

        private HAMS2Entities db = new HAMS2Entities();

        // GET: Sample
        public async Task<ActionResult> Index()
        {
            return View(await db.ShipList_Manual.ToListAsync());
        }
    }
}