﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MVCTut.Models.HAMS2;

namespace MVCTut.Controllers.HAMS2
{
    public class JVersionTrackersController : ApiController
    {
        private HAMS2Entities db = new HAMS2Entities();

        delegate string SelectColor(DateTime? status);

        // GET: api/JVersionTrackers
        public IQueryable<VersionTracker> GetVersionTrackers()
        {
            return db.VersionTrackers;
        }

        //// GET: api/JVersionTrackers/5
        //[ResponseType(typeof(VersionTracker))]
        //public async Task<IHttpActionResult> GetVersionTracker(int id)
        //{
        //    VersionTracker versionTracker = await db.VersionTrackers.FindAsync(id);
        //    if (versionTracker == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(versionTracker);
        //}


        SelectColor selectColor = delegate (DateTime? lastContact)
        {
            string retval = "success";
            TimeSpan ts = (TimeSpan)(DateTime.Now - lastContact);
            if(ts.TotalSeconds > 15 && ts.TotalSeconds <= 30)
            {
                retval = "warning";
            }
            else if (ts.TotalSeconds > 30)
            {
                retval = "danger";
            }
                return retval;
        };

        // GET: api/JVersionTrackers/5
        [ResponseType(typeof(ShipStatusView))]
        public IHttpActionResult GetVersionTracker(int id)
        {
            var x = (from a in db.VersionTrackers  
                    join b in db.Ship_Status on a.ServerName equals b.ServerName into ab
                    from c in ab.DefaultIfEmpty()
                    where a.id == id
                    select new ShipStatusView()
                    {
                        id = a.id,
                        ServerName = c.ServerName,
                        ShipName = c.ShipName,
                        Status = c.Status,
                        LastContact = c.LastContact,
                        DSUpdateVer = a.DSUpdateVer
                    }).ToList<ShipStatusView>();

            x.ForEach(item => item.BackgroundColor = selectColor(item.LastContact));

            return Ok(x.FirstOrDefault());

        }


        //    //System.Diagnostics.Trace.WriteLine(x.ToString());

        //    return View(x);


        // PUT: api/JVersionTrackers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVersionTracker(int id, VersionTracker versionTracker)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != versionTracker.id)
            {
                return BadRequest();
            }

            db.Entry(versionTracker).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VersionTrackerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/JVersionTrackers
        [ResponseType(typeof(VersionTracker))]
        public async Task<IHttpActionResult> PostVersionTracker(VersionTracker versionTracker)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.VersionTrackers.Add(versionTracker);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = versionTracker.id }, versionTracker);
        }

        // DELETE: api/JVersionTrackers/5
        [ResponseType(typeof(VersionTracker))]
        public async Task<IHttpActionResult> DeleteVersionTracker(int id)
        {
            VersionTracker versionTracker = await db.VersionTrackers.FindAsync(id);
            if (versionTracker == null)
            {
                return NotFound();
            }

            db.VersionTrackers.Remove(versionTracker);
            await db.SaveChangesAsync();

            return Ok(versionTracker);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VersionTrackerExists(int id)
        {
            return db.VersionTrackers.Count(e => e.id == id) > 0;
        }
    }
}