﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCTut.Models.HAMS2;

namespace MVCTut.Controllers.HAMS2
{
    public class VersionTrackersController : Controller
    {
        private HAMS2Entities db = new HAMS2Entities();

        // GET: VersionTrackers
        public async Task<ActionResult> Index()
        {
            
            var query = (
                    from a in db.VersionTrackers
                    from b in db.ShipList_Manual
                    .Where(b => b.ServerName == a.ServerName)
                    select new { a.id, b.ServerName, b.ShipName, a.DSUpdateVer });
            // select new VersionTracker() { id = a.id, ShipName = a.ShipName, DSUpdateVer = a.DSUpdateVer, ServerName = b.ServerName }).Where(x => x.ServerName.IndexOf("DDG") > -1);

            return View(await db.VersionTrackers.ToListAsync());
            //return View(await query.ToListAsync());
            //return View(await db.VersionTrackers.Where(a => a.ShipName.IndexOf("DDG") > -1).ToListAsync());
        }

        // GET: VersionTrackers/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VersionTracker versionTracker = await db.VersionTrackers.FindAsync(id);
            if (versionTracker == null)
            {
                return HttpNotFound();
            }
            return View(versionTracker);
        }

        // GET: VersionTrackers/Create
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> Index(string searchInput)
        {
            return View(await db.VersionTrackers.Where(a => a.ShipName.IndexOf(searchInput) > -1).ToListAsync());

        }


        // POST: VersionTrackers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,ShipName,ServerName,TwoOne,TwoTwo,TwoThree,TwoFour,WebATIS,Java,CTA,MU1B5,NOM,NOM2,Version,DSUpdateVer,LatestDate")] VersionTracker versionTracker)
        {
            if (ModelState.IsValid)
            {
                db.VersionTrackers.Add(versionTracker);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(versionTracker);
        }

        // GET: VersionTrackers/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VersionTracker versionTracker = await db.VersionTrackers.FindAsync(id);
            if (versionTracker == null)
            {
                return HttpNotFound();
            }
            return View(versionTracker);
        }

        // POST: VersionTrackers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,ShipName,ServerName,TwoOne,TwoTwo,TwoThree,TwoFour,WebATIS,Java,CTA,MU1B5,NOM,NOM2,Version,DSUpdateVer,LatestDate")] VersionTracker versionTracker)
        {
            if (ModelState.IsValid)
            {
                db.Entry(versionTracker).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(versionTracker);
        }

        // GET: VersionTrackers/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VersionTracker versionTracker = await db.VersionTrackers.FindAsync(id);
            if (versionTracker == null)
            {
                return HttpNotFound();
            }
            return View(versionTracker);
        }

        // POST: VersionTrackers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            VersionTracker versionTracker = await db.VersionTrackers.FindAsync(id);
            db.VersionTrackers.Remove(versionTracker);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
