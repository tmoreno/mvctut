﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCTut.Models.HAMS2;

namespace MVCTut.Controllers.HAMS2
{
    public class PubPageController : Controller
    {
        private HAMS2Entities db = new HAMS2Entities();

        // GET: PubPage
        public async Task<ActionResult> Index(string serverName)
        {
            ViewBag.ServerName = serverName;
            return View(await db.Pub_page.Where(a => a.ServerName.IndexOf(serverName) > -1).ToListAsync());
            //return View(await db.Pub_page.ToListAsync());
        }

        // GET: PubPage/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pub_page pub_page = await db.Pub_page.FindAsync(id);
            if (pub_page == null)
            {
                return HttpNotFound();
            }
            return View(pub_page);
        }

        // GET: PubPage/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PubPage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,ServerName,PubName,LocalRevLevel,Available,Auto,Freq")] Pub_page pub_page)
        {
            if (ModelState.IsValid)
            {
                db.Pub_page.Add(pub_page);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(pub_page);
        }

        // GET: PubPage/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pub_page pub_page = await db.Pub_page.FindAsync(id);
            if (pub_page == null)
            {
                return HttpNotFound();
            }
            return View(pub_page);
        }

        // POST: PubPage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,ServerName,PubName,LocalRevLevel,Available,Auto,Freq")] Pub_page pub_page)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pub_page).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(pub_page);
        }

        // GET: PubPage/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pub_page pub_page = await db.Pub_page.FindAsync(id);
            if (pub_page == null)
            {
                return HttpNotFound();
            }
            return View(pub_page);
        }

        // POST: PubPage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Pub_page pub_page = await db.Pub_page.FindAsync(id);
            db.Pub_page.Remove(pub_page);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
