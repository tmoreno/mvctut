﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MVCTut.Models.HAMS2;
using System.Data.SqlClient;

namespace MVCTut.Controllers.HAMS2
{
    public class NotificationsController : ApiController
    {
        private HAMS2Entities db = new HAMS2Entities();

        // GET: api/Notifications
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // POST: api/Notifications
        [ResponseType(typeof(Notification))]
        public IHttpActionResult PostNotification(Notification notification)
        {
            string connStr = @"Data Source = 192.168.56.102; Initial Catalog = HAMS2; Integrated Security = False; User ID = sa; Password = Tm0r3n0@^; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False";

            using (var conn = new SqlConnection(connStr))
            {
                var cmd = new SqlCommand("insert into Notifications (Ship, Date, Code) values (@Ship, @Date, @Code )", conn);
                cmd.Parameters.AddWithValue("@Ship", notification.Ship);
                cmd.Parameters.AddWithValue("@Date", notification.Date);
                cmd.Parameters.AddWithValue("@Code", notification.Code);
                conn.Open();
                cmd.ExecuteNonQuery();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            

            //db.Notifications.Add(notification);
            //await db.Database.ExecuteSqlCommandAsync(string.Format("insert into Notifications (Ship, Date, Code, RDate) values('{0}', {1}, {2}, null);", notification.Ship, notification.Date, notification.Code));

            //await db.SaveChangesAsync();

            return Ok(notification);
        }

        // GET: api/Notifications/5
        [ResponseType(typeof(Notification))]
        public IHttpActionResult GetNotification(string id)
        {
            var x =
                from n in db.Notifications
                where n.Ship == id
                group n by n.Ship into g
                select g.OrderByDescending(t => t.Date).FirstOrDefault<Notification>();

            return Ok(x);
        }

        // PUT: api/Notifications/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Notifications/5
        public void Delete(int id)
        {
        }
    }
}
