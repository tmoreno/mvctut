﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCTut.Models.HAMS2;

namespace MVCTut.Controllers.HAMS2
{
    public class NotificationViewController : Controller
    {
        private HAMS2Entities db = new HAMS2Entities();

        // GET: NotificationView
        public ActionResult Index(string ship_name)
        {

            var q = from v in db.NotificationViews
                    where v.Ship == ship_name
                    select v;
            //return View(await db.VersionTrackers.ToListAsync());
            return View(q);
        }

        // GET: NotificationView/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: NotificationView/Create
        public ActionResult Create()
        {
            return View();
        }


        // GET: NotificationView/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: NotificationView/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: NotificationView/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: NotificationView/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
