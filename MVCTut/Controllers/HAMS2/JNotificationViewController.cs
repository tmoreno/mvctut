﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MVCTut.Models.HAMS2;

namespace MVCTut.Controllers.HAMS2
{
    public class JNotificationViewController : ApiController
    {
        private HAMS2Entities db = new HAMS2Entities();

        // GET: api/JNotificationView
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/JNotificationView/5
        public IQueryable<NotificationView> GetNotification(string shipName)
        {
            var q = from v in db.NotificationViews
                    where v.Ship == shipName
                    orderby v.id ascending
                    select v;
            return q;
        }

        // POST: api/JNotificationView
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/JNotificationView/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/JNotificationView/5
        public void Delete(int id)
        {
        }
    }
}
