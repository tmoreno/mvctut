﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCTut.Models.HAMS2;

namespace MVCTut.Controllers.HAMS2
{
    public class ShipStatusController : Controller
    {
        private HAMS2Entities db = new HAMS2Entities();

        delegate string SelectColor(int? status);

        SelectColor selectColor = delegate (int? status)
        {
            string retval = "success";
            if (status ==  -15)
            {
                retval = "danger";
            }
            else if(status == -10)
            {
                retval = "danger";
            }
            else if (status == -5)
            {
                retval = "danger";
            }
            else if (status < 92)
            {
                retval = "danger";
            }
            else if (status < 96)
            {
                retval = "warning";
            }
            return retval;
        };

        //private string SelectColor(int? status)
        //{
        //    return "white";
        //}


        // GET: ShipStatus
        public ActionResult Index()
        {
           var x = (from a in db.VersionTrackers
            from b in db.Ship_Status
            .Where(b => b.ServerName == a.ServerName)
            select new ShipStatusView()
            {
                id = b.id,
                ServerName = b.ServerName,
                ShipName = b.ShipName,
                Status = b.Status,
                LastContact = b.LastContact,
                DSUpdateVer = a.DSUpdateVer
            }).ToList<ShipStatusView>();

            x.ForEach(item => item.BackgroundColor = selectColor(item.Status));

            //System.Diagnostics.Trace.WriteLine(x.ToString());

            return View(x);
        }

        private IQueryable<ShipStatusView> GetShipStatusView()
        {
            return from a in db.VersionTrackers
                   from b in db.Ship_Status
                   .Where(b => b.ServerName == a.ServerName)
                   select new ShipStatusView()
                   {
                       id = b.id,
                       ServerName = b.ServerName,
                       ShipName = b.ShipName,
                       Status = b.Status
                       //LastContact = b.LastContact,
                       //Version = a.Version,
                       //BackgroundColor = statusColor(b.Status)
                   };
        }

        private async Task<List<Ship_Status>> GetShipList()
        {
            return await db.Ship_Status.ToListAsync();
        }

        // GET: ShipStatus/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ship_Status ship_Status = await db.Ship_Status.FindAsync(id);
            if (ship_Status == null)
            {
                return HttpNotFound();
            }
            return View(ship_Status);
        }

        // GET: ShipStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ShipStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,ShipName,ServerName,Status,Version,LastContact,LastUpdatesComplete")] Ship_Status ship_Status)
        {
            if (ModelState.IsValid)
            {
                db.Ship_Status.Add(ship_Status);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ship_Status);
        }

        // GET: ShipStatus/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ship_Status ship_Status = await db.Ship_Status.FindAsync(id);
            if (ship_Status == null)
            {
                return HttpNotFound();
            }
            return View(ship_Status);
        }

        // POST: ShipStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,ShipName,ServerName,Status,Version,LastContact,LastUpdatesComplete")] Ship_Status ship_Status)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ship_Status).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(ship_Status);
        }

        // GET: ShipStatus/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ship_Status ship_Status = await db.Ship_Status.FindAsync(id);
            if (ship_Status == null)
            {
                return HttpNotFound();
            }
            return View(ship_Status);
        }

        // POST: ShipStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Ship_Status ship_Status = await db.Ship_Status.FindAsync(id);
            db.Ship_Status.Remove(ship_Status);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
