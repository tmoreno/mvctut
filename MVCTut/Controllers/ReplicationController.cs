﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCTut.Controllers
{
    public class ReplicationController : Controller
    {
        // GET: Replication
        public ActionResult Index()
        {
            return View();
        }

        // GET: Default/Edit/5
        public ActionResult Spec(String folder_id)
        {
            ViewBag.FolderId = folder_id;
            return View();
        }
    }
}