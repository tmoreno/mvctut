﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCTut.Models;

namespace MVCTut.Controllers
{
    public class HullController : Controller
    {
        private AmendmentIDBContext db = new AmendmentIDBContext();

        // GET: Hull
        public async Task<ActionResult> Index()
        {
            return View(await db.HullIndex.ToListAsync());
        }

        // GET: Hull/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HullIndex hullIndex = await db.HullIndex.FindAsync(id);
            if (hullIndex == null)
            {
                return HttpNotFound();
            }
            return View(hullIndex);
        }

        // GET: Hull/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Hull/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Hull,HullName,ServerName")] HullIndex hullIndex)
        {
            if (ModelState.IsValid)
            {
                db.HullIndex.Add(hullIndex);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(hullIndex);
        }

        // GET: Hull/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HullIndex hullIndex = await db.HullIndex.FindAsync(id);
            if (hullIndex == null)
            {
                return HttpNotFound();
            }
            return View(hullIndex);
        }

        // POST: Hull/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Hull,HullName,ServerName")] HullIndex hullIndex)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hullIndex).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(hullIndex);
        }

        // GET: Hull/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HullIndex hullIndex = await db.HullIndex.FindAsync(id);
            if (hullIndex == null)
            {
                return HttpNotFound();
            }
            return View(hullIndex);
        }

        // POST: Hull/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            HullIndex hullIndex = await db.HullIndex.FindAsync(id);
            db.HullIndex.Remove(hullIndex);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
