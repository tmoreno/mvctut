﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCTut.Models;

namespace MVCTut.Controllers
{
    public class AmendmentInfosController : Controller
    {
        private AmendmentInfoDBContext db = new AmendmentInfoDBContext();

        // GET: AmendmentInfoes
        public async Task<ActionResult> Index()
        {
            return View(await db.AmendmentInfos.ToListAsync());
        }

        // GET: AmendmentInfoes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmendmentInfo amendmentInfo = await db.AmendmentInfos.FindAsync(id);
            if (amendmentInfo == null)
            {
                return HttpNotFound();
            }
            return View(amendmentInfo);
        }

        // GET: AmendmentInfoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AmendmentInfoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,PubLevelsId,AmendmentName,Size,IsCompulsory,PostProcessing,dependencies")] AmendmentInfo amendmentInfo)
        {
            if (ModelState.IsValid)
            {
                db.AmendmentInfos.Add(amendmentInfo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(amendmentInfo);
        }

        // GET: AmendmentInfoes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmendmentInfo amendmentInfo = await db.AmendmentInfos.FindAsync(id);
            if (amendmentInfo == null)
            {
                return HttpNotFound();
            }
            return View(amendmentInfo);
        }

        // POST: AmendmentInfoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,PubLevelsId,AmendmentName,Size,IsCompulsory,PostProcessing,dependencies")] AmendmentInfo amendmentInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(amendmentInfo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(amendmentInfo);
        }

        // GET: AmendmentInfoes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmendmentInfo amendmentInfo = await db.AmendmentInfos.FindAsync(id);
            if (amendmentInfo == null)
            {
                return HttpNotFound();
            }
            return View(amendmentInfo);
        }

        // POST: AmendmentInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AmendmentInfo amendmentInfo = await db.AmendmentInfos.FindAsync(id);
            db.AmendmentInfos.Remove(amendmentInfo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
