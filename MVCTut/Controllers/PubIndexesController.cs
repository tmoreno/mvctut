﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCTut.Models;

namespace MVCTut.Controllers
{
    public class PubIndexesController : Controller
    {
        private AmendmentIDBContext db = new AmendmentIDBContext();

        // GET: PubIndexes
        public async Task<ActionResult> Index()
        {
            return View(await db.PubIndexes.ToListAsync());
        }

        // GET: PubIndexes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PubIndex pubIndex = await db.PubIndexes.FindAsync(id);
            if (pubIndex == null)
            {
                return HttpNotFound();
            }
            return View(pubIndex);
        }

        // GET: PubIndexes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PubIndexes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,PubName")] PubIndex pubIndex)
        {
            if (ModelState.IsValid)
            {
                db.PubIndexes.Add(pubIndex);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(pubIndex);
        }

        // GET: PubIndexes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PubIndex pubIndex = await db.PubIndexes.FindAsync(id);
            if (pubIndex == null)
            {
                return HttpNotFound();
            }
            return View(pubIndex);
        }

        // POST: PubIndexes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,PubName")] PubIndex pubIndex)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pubIndex).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(pubIndex);
        }

        // GET: PubIndexes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PubIndex pubIndex = await db.PubIndexes.FindAsync(id);
            if (pubIndex == null)
            {
                return HttpNotFound();
            }
            return View(pubIndex);
        }

        // POST: PubIndexes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            PubIndex pubIndex = await db.PubIndexes.FindAsync(id);
            db.PubIndexes.Remove(pubIndex);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
