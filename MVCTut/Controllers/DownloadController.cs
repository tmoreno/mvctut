﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCTut.Controllers
{
    public class DownloadController : Controller
    {
        // GET: Download
        public ActionResult Index()
        {
            var path = @"c:\Users\tmoreno\axway\Folder 1\test.jpg";
            var stream = new System.IO.FileStream(path, FileMode.Open);
            var response = File(stream, "application /octet-stream"); // FileStreamResult
            return response;
        }

        public ActionResult GetImage(string folder_id, string id)
        {
            var path = String.Format(@"c:\Users\tmoreno\axway\{0}\{1}", folder_id, id);
            var stream = new System.IO.FileStream(path, FileMode.Open);
            var response = File(stream, "application /octet-stream"); // FileStreamResult
            return response;
        }
    }
}