﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCTut.Models;

namespace MVCTut.Controllers
{
    public class AppIndexesController : Controller
    {
        private AmendmentIDBContext db = new AmendmentIDBContext();

        // GET: AppIndexes
        public async Task<ActionResult> Index()
        {
            return View(await db.AppIndexes.ToListAsync());
        }

        // GET: AppIndexes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppIndex appIndex = await db.AppIndexes.FindAsync(id);
            if (appIndex == null)
            {
                return HttpNotFound();
            }
            return View(appIndex);
        }

        // GET: AppIndexes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AppIndexes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,AppName,Version,Vendor")] AppIndex appIndex)
        {
            if (ModelState.IsValid)
            {
                db.AppIndexes.Add(appIndex);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(appIndex);
        }

        // GET: AppIndexes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppIndex appIndex = await db.AppIndexes.FindAsync(id);
            if (appIndex == null)
            {
                return HttpNotFound();
            }
            return View(appIndex);
        }

        // POST: AppIndexes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,AppName,Version,Vendor")] AppIndex appIndex)
        {
            if (ModelState.IsValid)
            {
                db.Entry(appIndex).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(appIndex);
        }

        // GET: AppIndexes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppIndex appIndex = await db.AppIndexes.FindAsync(id);
            if (appIndex == null)
            {
                return HttpNotFound();
            }
            return View(appIndex);
        }

        // POST: AppIndexes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AppIndex appIndex = await db.AppIndexes.FindAsync(id);
            db.AppIndexes.Remove(appIndex);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
