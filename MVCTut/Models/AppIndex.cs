﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTut.Models
{
    public class AppIndex
    {
        public int ID { get; set; }
        public string AppName { get; set; }
        public string Version { get; set; }
        public string Vendor { get; set; }
    }
}