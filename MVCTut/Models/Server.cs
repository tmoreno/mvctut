﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTut.Models
{
    public class Server
    {
        public int ID { get; set; }
        public int HULL_ID { get; set; }
        public string Name { get; set; }
        public int CPU { get; set; }
        public int MEM { get; set; }
    }
}