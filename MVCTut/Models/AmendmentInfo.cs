﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVCTut.Models
{
    public class AmendmentInfo
    {
        public int ID { get; set; }
        public int PubLevelsId { get; set; }
        public string AmendmentName { get; set; }
        public long Size { get; set; }
        public bool IsCompulsory { get; set; }
        public string PostProcessing { get; set; }
        public string dependencies { get; set; }
    }

    public class AmendmentInfoDBContext : DbContext
    {
        public DbSet<AmendmentInfo> AmendmentInfos { get; set; }
    }
}