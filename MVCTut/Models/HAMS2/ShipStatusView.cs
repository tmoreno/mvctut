﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTut.Models.HAMS2
{
    public class ShipStatusView : Ship_Status
    {
        private string backgroundColor;
        private string dsUpdateVer;

        public string BackgroundColor { get => backgroundColor; set => backgroundColor = value; }
        public string DSUpdateVer { get => dsUpdateVer; set => dsUpdateVer = value; }
    }
}