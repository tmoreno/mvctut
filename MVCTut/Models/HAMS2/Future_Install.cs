//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCTut.Models.HAMS2
{
    using System;
    using System.Collections.Generic;
    
    public partial class Future_Install
    {
        public int id { get; set; }
        public string Ship { get; set; }
        public string Hull { get; set; }
        public string CurrentVer { get; set; }
        public string NewVer { get; set; }
        public Nullable<System.DateTime> InstallDate { get; set; }
        public string Notes { get; set; }
    }
}
