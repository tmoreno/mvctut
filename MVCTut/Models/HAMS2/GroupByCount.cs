﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTut.Models.HAMS2
{
    public class GroupByCount
    {
        public string Key { get; set; }
        public int Count { get; set; }
    }
}