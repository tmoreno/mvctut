﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace MVCTut.Models
{
    public class HullIndex
    {
        public int ID { get; set; }
        public string Hull { get; set; }
        public string HullName { get; set; }
        public string ServerName { get; set; }
    }


    public class AmendmentIDBContext : DbContext
    {
        public DbSet<HullIndex> HullIndex { get; set; }

        public System.Data.Entity.DbSet<MVCTut.Models.AmendmentInfo> AmendmentInfos { get; set; }

        public System.Data.Entity.DbSet<MVCTut.Models.AppIndex> AppIndexes { get; set; }

        public System.Data.Entity.DbSet<MVCTut.Models.CM> CMs { get; set; }

        public System.Data.Entity.DbSet<MVCTut.Models.PubIndex> PubIndexes { get; set; }

        public System.Data.Entity.DbSet<MVCTut.Models.Server> Servers { get; set; }
    }
}