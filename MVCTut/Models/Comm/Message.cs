﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTut.Models.Comm
{
    public class Message
    {
        private string serverName;
        private string messageName;

        public string ServerName { get => serverName; set => serverName = value; }
        public string MessageName { get => messageName; set => messageName = value; }
    }
}