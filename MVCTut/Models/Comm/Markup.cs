﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTut.Models.Comm
{
    public class Markup
    {
        public string Hash { get; set; }
        public string JsonString { get; set; }
    }
}