﻿USE [HAMS9]
GO

DECLARE	@return_value Int

EXEC	@return_value = [dbo].[USP_ADD_AUTH_HULL_APP_RELEASE3]
		@hullServerId = 1,
		@authAppReleaseId = 3004,
		@hullAppReleaseDate = '2/13/2017'

SELECT	@return_value as 'Return Value'

GO
