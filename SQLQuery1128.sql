﻿alter PROCEDURE [dbo].[USP_Update_Dashboard]
	@module_id int,
	@normal int,
	@warning int,
	@critical int
AS
	UPDATE Dashboard SET normal = @normal, warning = @warning, critical = @critical
	WHERE module_id = @module_id

RETURN 0



exec USP_Update_Dashboard 1, 22, 33, 44



select * from DisplayIndicatorView;