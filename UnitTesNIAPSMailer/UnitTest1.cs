﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NIAPS_SMailer;
using NIAPS.SMailer;

namespace UnitTesNIAPSMailer
{
    [TestClass]
    public class UnitTestMail
    {
        [TestMethod]
        public void MailTestMethod()
        {
            SMTP smtp = new SMTP("dcoms-ssdbs.sd.spawar.navy.mil", 3000);
            MailInfo mailInfo = new MailInfo();
            mailInfo.id = "UnitTest=NIAPSMailer";
            mailInfo.subj = "THis is a test";
            mailInfo.to = "toby.moreno1@navy.mil";
            mailInfo.from = string.Format("system@{0}", smtp.SmtpServer);
            mailInfo.text = "This is a test.";
            smtp.Send(mailInfo);
        }
    }
}
