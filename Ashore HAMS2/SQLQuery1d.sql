﻿GO
CREATE VIEW [dbo].AuthAppView
	AS SELECT 
		a.AUTH_APP_ID, 
		c.AUTH_APP_RELEASE_ID,
		a.AUTH_APP_NAME,
		b.AUTH_APP_VENDOR_NAME,
		c.AUTH_VERSION,
		c.AUTH_APP_RELEASE_DATE,
		c.TIME_DATE_RECORD

	FROM AUTH_APP_INDEX a
	INNER JOIN AUTH_APP_VENDOR_INDEX b
	ON a.AUTH_APP_VENDOR_ID = b.AUTH_APP_VENDOR_ID
	LEFT JOIN AUTH_APP_RELEASE c
	ON a.AUTH_APP_ID = c.AUTH_APP_ID;
GO
PRINT N'Creating [dbo].[AuthorizedInstalledView]...';


GO
create VIEW AuthorizedInstalledView
AS
select COUNT(a.HULL_SERVER_ID) AS [AuthorizedInstalledCount], b.VendorAppVersion from 
(
	select HULL_SERVER_ID, b.AUTH_APP_VENDOR_NAME + '_' + b.AUTH_APP_NAME + '_' + b.AUTH_VERSION AS [VendorAppVersion] from AUTH_HULL_APP_RELEASE a
	INNER JOIN 
	(
		select 
			a.AUTH_APP_RELEASE_ID, a.AUTH_VERSION, b.AUTH_APP_NAME, c.AUTH_APP_VENDOR_NAME 
		from 
			AUTH_APP_RELEASE a INNER JOIN AUTH_APP_INDEX b
		ON a.AUTH_APP_ID = b.AUTH_APP_ID
			INNER JOIN AUTH_APP_VENDOR_INDEX c
		ON b.AUTH_APP_VENDOR_ID = c.AUTH_APP_VENDOR_ID
	) b
	ON a.AUTH_APP_RELEASE_ID = b.AUTH_APP_RELEASE_ID
) a
INNER JOIN 
(
	select d.HULL_SERVER_ID, b.INSTALLED_AFLOAT_APP_VENDOR_NAME + '_' + a.AFLOAT_APP_NAME + '_' + c.AFLOAT_VERSION AS [VendorAppVersion] from AFLOAT_APP_INDEX a
	INNER JOIN INSTALLED_AFLOAT_APP_VENDOR_INDEX b
	ON a.INSTALLED_AFLOAT_APP_VENDOR_ID = b.INSTALLED_AFLOAT_APP_VENDOR_ID
	INNER JOIN AFLOAT_APP_VERSION_INDEX c
	ON c.AFLOAT_APP_ID = a.AFLOAT_APP_ID
	INNER JOIN AFLOAT_HULL_APP_INSTALLED d
	ON c.AFLOAT_APP_VERSION_ID = d.AFLOAT_APP_VERSION_ID
) b
ON a.HULL_SERVER_ID = b.HULL_SERVER_ID AND a.VendorAppVersion = b.VendorAppVersion
GROUP BY b.VendorAppVersion
GO
PRINT N'Creating [dbo].[DisplayIndicatorView]...';


GO
CREATE VIEW [dbo].[DisplayIndicatorView]
	AS SELECT * FROM DISPLAY_INDICATOR_INDEX;
GO
PRINT N'Creating [dbo].[GetReplicationFileForDownloadView]...';


GO
CREATE VIEW [GetReplicationFileForDownloadView]
	AS 
select  b.Id, a.HULL_SERVER_ID, c.Filename, b.Hash, b.Progress from AshoreFileHullServer a
left join 

	(
		select Max(a.Id) as Id, a.HULL_SERVER_ID
		from AfloatFileHullServer a
		group by a.HULL_SERVER_ID, a.AshoreFileId
	)
b1
on a.AshoreFileId = b1.Id and a.HULL_SERVER_ID = b1.HULL_SERVER_ID
left join AfloatFileHullServer b
on a.HULL_SERVER_ID = b.HULL_SERVER_ID and a.AshoreFileId = b.AshoreFileId
left join AshoreFile c
on c.Id = a.AshoreFileId
GO
PRINT N'Creating [dbo].[HULL_SERVER_VIEW]...';


GO
create view HULL_SERVER_VIEW
as
select b.HULL_SERVER_ID, a.HULL_ID, a.HULL, a.HULL_NAME, b.STATUS_ID, c.SERVERNAME from HULL_INDEX a
INNER JOIN HULL_SERVER_INDEX b
on a.HULL_ID = b.HULL_ID
INNER JOIN SERVER_INDEX c
on b.SERVER_ID = c.SERVER_ID
GO
PRINT N'Creating [dbo].[ReplicationMatchView]...';


GO


CREATE TABLE [dbo].[AshoreFileHullServer] (
    [Id]             INT IDENTITY (1, 1) NOT NULL,
    [HULL_SERVER_ID] INT NULL,
    [AshoreFileId]   INT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

PRINT N'Creating [dbo].[AfloatFileHullServer]...';


GO
CREATE TABLE [dbo].[AfloatFileHullServer] (
    [Id]             INT          IDENTITY (1, 1) NOT NULL,
    [HULL_SERVER_ID] INT          NULL,
    [AshoreFileId]   INT          NULL,
    [Hash]           VARCHAR (64) NULL,
    [Date]           DATETIME     NULL,
    [Progress]       INT          NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


CREATE TABLE [dbo].[AshoreFile] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [Filename] VARCHAR (255) NULL,
    [Hash]     VARCHAR (64)  NULL,
    [Date]     DATETIME      NULL,
    [Size]     INT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


CREATE VIEW [dbo].ReplicationMatchView
as
select a.Id, a.HULL_SERVER_ID, a.AshoreFileId, c.Hash, b.Date, b.Progress, d.Size
from AshoreFileHullServer a
left join 

(select *
	from AfloatFileHullServer a
	where id IN
	(
		select Max(id) as id
		from AfloatFileHullServer a
		group by HULL_SERVER_ID, AshoreFileId
	)
) b

on a.HULL_SERVER_ID = b.HULL_SERVER_ID and a.AshoreFileId = b.AshoreFileId
left join AshoreFile c
on b.AshoreFileId = c.id and b.Hash = c.Hash
left join AshoreFile d
on b.AshoreFileId = d.Id
GO
PRINT N'Creating [dbo].[Procedure]...';


GO
CREATE PROCEDURE [dbo].[Procedure]
	@param1 int = 0,
	@param2 int
AS
	SELECT @param1, @param2
RETURN 0
GO
