﻿using Ashore_HAMS.Models3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ashore_HAMS.ModelView
{
    public class HullServer
    {
        public HULL_SERVER_INDEX HULL_SERVER_INDEX { get; set; }
        public SERVER_INDEX SERVER_INDEX { get; set; }

    }
}