﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Linq;
using Hams3xLib;

namespace Ashore_HAMS.App_Start
{


    public class UserLogin
    {
        HAMS3XEntities db = new HAMS3XEntities();
        public bool Check(string cac)
        {
            bool retval = false;
            var user = from u in db.UserIndexes
                       where u.Cac == cac
                       select u;

            if (user.Count() > 0)
            {
                retval = true;
            }

            return retval;
        }
    }

    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            string rawUrl = filterContext.HttpContext.Request.RawUrl;
            if(rawUrl.IndexOf("Banner") == -1)
            {
                if (HttpContext.Current.Session["ID"] == null)
                {
                    filterContext.Result = new RedirectResult("~/Banner");
                    return;
                }
            }
            else
            {
                // don't do anything FIX ME
                string method = filterContext.HttpContext.Request.HttpMethod;
                if(method == "POST")
                {
                    
                    HttpClientCertificate cc = filterContext.HttpContext.Request.ClientCertificate;
                    if(cc.IsPresent)
                    {
                        string subj = cc.Subject;
                        Match match = Regex.Match(subj, "[0-9]{10}$");
                        if(match.Success)
                        {
                            var userLogin = new UserLogin();
                            string cac = match.Value;
                            if(userLogin.Check(cac))
                            {
                                HttpContext.Current.Session["ID"] = cac;
                                filterContext.Result = new RedirectResult("/");
                            }
                            else
                            {
                                filterContext.Result = new RedirectResult("/Banner/Logout?" + cac);
                            }
                            return;

                            //k
                        }
                        // get the client cert
                    }
                    else
                    {
                        HttpContext.Current.Session["ID"] = 1;
                        filterContext.Result = new RedirectResult("/");
                        return;
                    }

                }

            }

            base.OnActionExecuting(filterContext);
        }
    }
}