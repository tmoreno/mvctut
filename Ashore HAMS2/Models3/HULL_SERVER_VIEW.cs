//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ashore_HAMS.Models3
{
    using System;
    using System.Collections.Generic;
    
    public partial class HULL_SERVER_VIEW
    {
        public int HULL_SERVER_ID { get; set; }
        public int HULL_ID { get; set; }
        public string HULL { get; set; }
        public string HULL_NAME { get; set; }
        public int STATUS_ID { get; set; }
        public string SERVERNAME { get; set; }
    }
}
