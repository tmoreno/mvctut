USE [HAMS3]
GO
/*CREATE UPSERT FOR DISPLAY INDICATOR INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_DISPLAY_INDICATOR]---DISPLAY_INDICATOR_ID(GENERATED OR RETURNED), DISPLAY_INDICATOR_NAME(PASSED)
	@displayIndicatorName varchar(50)--PASSED FOR INSERT
AS
	DECLARE @displayIndicatorId int;--declare DISPLAY_INDICATOR_ID
	SET @displayIndicatorId = 0;--set DISPLAY_INDICATOR_ID

	--
	SELECT @displayIndicatorId = DISPLAY_INDICATOR_ID FROM DISPLAY_INDICATOR_INDEX
	WHERE DISPLAY_INDICATOR_NAME = @displayIndicatorName;--write in DISPLAY_INDICATOR_ID if already present

	IF @displayIndicatorId = 0--if DISPLAY_INDICATOR_ID is not present INSERT new DISPLAY_INDICATOR_NAME and generate new DISPLAY_INDICATOR_ID
	BEGIN
		INSERT INTO DISPLAY_INDICATOR_INDEX(DISPLAY_INDICATOR_NAME)
		VALUES (@displayIndicatorName);
		--set the inserted displayIndicatorId
		set @displayIndicatorId = @@IDENTITY
	END
return @displayIndicatorId--PROC RETURNS DISPLAY_INDICATOR_ID
GO
/*
SAMPLE
EXEC USP_ADD_DISPLAY_INDICATOR 'ACTIVE';
EXEC USP_ADD_DISPLAY_INDICATOR 'INACTIVE';
EXEC USP_ADD_DISPLAY_INDICATOR 'NOT SET';
END UPSERT FOR DISPLAY INDICATOR INDEX
*/

/*CREATE UPSERT FOR TARGET_AREA INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_TARGET_AREA_INDEX]---TARGET_AREA_ID(GENERATED OR RETURNED),TARGET_AREA_NAME(PASSED),DISPLAY_INDICATOR_ID(COLLECTED)
	@targetAreaName varchar(50),--PASSED FOR INSERT
	@displayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @targetAreaId int;--DECLARE TARGET_AREA_ID
	DECLARE @displayIndicatorId int;--DECLARE DISPLAY_INDICATOR_ID
	SET @targetAreaId = 0;--SET TARGET_AREA_ID

	SELECT @targetAreaId = TARGET_AREA_ID FROM TARGET_AREA_INDEX
	WHERE TARGET_AREA_NAME = @targetAreaName;--write in TARGET_AREA_ID if already present

	--find displayIndicatorID insert if it does not exist
	EXEC @displayIndicatorId = USP_ADD_DISPLAY_INDICATOR @displayIndicatorName;--collect DISPLAY_INDICATOR_ID

	---if current targetAreaId is not present create new targetAreaId
	IF @targetAreaId = 0
	BEGIN
		INSERT INTO TARGET_AREA_INDEX (TARGET_AREA_NAME,DISPLAY_INDICATOR_ID)
		VALUES (@targetAreaName,@displayIndicatorId);
		--set the inserted targetAreaId
		set @targetAreaId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE TARGET_AREA_INDEX
		SET DISPLAY_INDICATOR_ID = @displayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @displayIndicatorId AND TARGET_AREA_ID = @targetAreaId;
	END
return @targetAreaId--RETURNS TARGET_AREA_ID
GO
/*
SAMPLE
EXEC USP_ADD_TARGET_AREA_INDEX 'OVERALL','ACTIVE';
EXEC USP_ADD_TARGET_AREA_INDEX 'REPLICATION','ACTIVE';
EXEC USP_ADD_TARGET_AREA_INDEX 'SYSTEM','ACTIVE';
EXEC USP_ADD_TARGET_AREA_INDEX 'APP','ACTIVE';
EXEC USP_ADD_TARGET_AREA_INDEX 'DS_UPDATE_ACTIVITY','ACTIVE';
END UPSERT FOR TARGET_AREA INDEX
*/

/*CREATE UPSERT FOR STATUS INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_STATUS]--STATUS_ID(GENERATED OR RETURNED),STATUS_NAME(PASSED),DISPLAY_INDICTOR_ID(COLLECTED)
	@statusName varchar(50),--PASS STATUS_NAME FOR INSERT
	@displayIndicatorName varchar(50)--PASS DISPLAY_INDICATOR_NAME FOR USP CALL
AS
	DECLARE @statusId int;--DECLARE STATUS_ID
	DECLARE @displayIndicatorId int;--DECLARE DISPLAY_INDICATOR_ID
	SET @statusId = 0;

	-- find STATUS_ID if STATUS_NAME already exists
	SELECT @statusId = STATUS_ID FROM STATUS_INDEX
	WHERE STATUS_NAME = @statusName;
	
	--find displayIndicatorID insert if it does not exist
	EXEC @displayIndicatorId = USP_ADD_DISPLAY_INDICATOR @displayIndicatorName;--collect DISPLAY_INDICATOR_ID

	IF @statusId = 0--if STATUS_NAME does not exist create new STATUS_ID
	BEGIN
		INSERT INTO STATUS_INDEX (STATUS_NAME,DISPLAY_INDICATOR_ID)
		VALUES (@statusName,@displayIndicatorId);
		--set the inserted statusId
		set @statusId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE STATUS_INDEX
		SET DISPLAY_INDICATOR_ID = @displayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @displayIndicatorId AND STATUS_ID = @statusId;
	END
RETURN @statusId--return statuId
GO
/*
SAMPLE
EXEC USP_ADD_STATUS 'RED','ACTIVE';
EXEC USP_ADD_STATUS 'YELLOW','ACTIVE';
EXEC USP_ADD_STATUS 'GREEN','ACTIVE';
EXEC USP_ADD_STATUS 'NOT SET','ACTIVE';
END UPSERT FOR STATUS INDEX
*/

/*CREATE UPSERT FOR HULL TYPE INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_HULL_TYPE]--HULL_TYPE_ID(GENERATED OR RETURNED),HULL_TYPE_NAME(PASSED),DISPLAY_INDICATOR_ID(COLLECTED)
	@hullTypeName varchar(50),--PASSED FOR INSERT
	@displayIndicatorName varchar(50)--PASSED FOR DISPLAY_INDICATOR_NAME USP CALL
AS
	DECLARE @hullTypeId int;--DECLARE HULL_TYPE_ID
	DECLARE @displayIndicatorId int;--DECLARE DISPLAY_INDICATOR_ID
	SET @hullTypeId = 0;--SET HULL_TYPE_ID

	--find HULL_TYPE_ID if HULL_TYPE_NAME already exists
	SELECT @hullTypeId = HULL_TYPE_ID FROM HULL_TYPE_INDEX
	WHERE HULL_TYPE_NAME = @hullTypeName;

	--find displayIndicatorID, insert if it does not exist
	EXEC @displayIndicatorId = USP_ADD_DISPLAY_INDICATOR @displayIndicatorName;--collect DISPLAY_INDICATOR_ID

	---if HULL_TYPE_NAME does not exist, INSERT new HULL_TYPE_NAME
	IF @hullTypeId = 0
	BEGIN
		INSERT INTO HULL_TYPE_INDEX(HULL_TYPE_NAME,DISPLAY_INDICATOR_ID)
		VALUES (@hullTypeName,@displayIndicatorId);
		--set the inserted hullTypeId
		set @hullTypeId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE HULL_TYPE_INDEX
		SET DISPLAY_INDICATOR_ID = @displayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @displayIndicatorId AND HULL_TYPE_ID = @hullTypeId;
	END
return @hullTypeId--RETURN HULL_TYPE_ID
GO
/*
SAMPLE
EXEC USP_ADD_HULL_TYPE 'SURF','ACTIVE';
EXEC USP_ADD_HULL_TYPE 'AIR','ACTIVE';
EXEC USP_ADD_HULL_TYPE 'SUB','ACTIVE';
EXEC USP_ADD_HULL_TYPE 'FAC','ACTIVE';
END UPSERT FOR HULL TYPE INDEX
*/

/*CREATE UPSERT FOR CLASS TYPE INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_CLASS_TYPE]--CLASS_TYPE_ID(GENERATED OR RETURNED),CLASS_TYPE_NAME(PASSED),DISPLAY_INDICATOR_ID(COLLECTED)
	@classTypeName varchar(50),--PASSED FOR INSERT
	@displayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @classTypeId int;--DECLARE CLASS_TYPE_ID
	DECLARE @displayIndicatorId int;--DECLARE DISPLAY_INDICATOR_ID
	SET @classTypeId = 0;--SET CLASS_TYPE_ID

	-- find the CLASS_TYPE_ID if the CLASS_TYPE_NAME already exists
	SELECT @classTypeId = CLASS_TYPE_ID FROM CLASS_TYPE_INDEX
	WHERE CLASS_TYPE_NAME = @classTypeName;

	--find displayIndicatorID, insert if it does not exist
	EXEC @displayIndicatorId = USP_ADD_DISPLAY_INDICATOR @displayIndicatorName;--collect DISPLAY_INDICATOR_ID

	---if current CLASS_TYPE_NAME does not exist INSERT it
	IF @classTypeId = 0
	BEGIN
		INSERT INTO CLASS_TYPE_INDEX(CLASS_TYPE_NAME,DISPLAY_INDICATOR_ID)
		VALUES (@classTypeName,@displayIndicatorId);
		--set the inserted classTypeId
		set @classTypeId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE CLASS_TYPE_INDEX
		SET DISPLAY_INDICATOR_ID = @displayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @displayIndicatorId AND CLASS_TYPE_ID = @classTypeId;
	END
return @classTypeId--return CLASS_TYPE_ID 
GO
/*
SAMPLE
EXEC USP_ADD_CLASS_TYPE 'DDG-51','ACTIVE';
EXEC USP_ADD_CLASS_TYPE 'DDG-1000','ACTIVE';
EXEC USP_ADD_CLASS_TYPE 'LHD-1','ACTIVE';
EXEC USP_ADD_CLASS_TYPE 'LCC-19','ACTIVE';
END UPSERT FOR CLASS TYPE INDEX
*/

/*CREATE UPSERT FOR RETURN SERVER CODE INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_RETURN_SERVER_CODE]--RETURN_SERVER_CODE(GENERATED OR RETURNED),RETURN_SERVER_CODE_NAME(PASSED),DISPLAY_INDICATOR_ID(COLLECTED)
	@returnServerCodeName varchar(50),--PASSED FOR INSERT
	@displayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @returnServerCodeId int;--DECLARE RETURN_SERVER_CODE_ID
	DECLARE @displayIndicatorId int;--DECLARE DISPLAY_INDICATOR_ID
	SET @returnServerCodeId = 0;--SET RETURN_SERVER_CODE_ID

	-- find the RETURN_SERVER_CODE_NAME if it exists
	SELECT @returnServerCodeId = RETURN_SERVER_CODE_ID FROM RETURN_SERVER_CODE_INDEX
	WHERE RETURN_SERVER_CODE_NAME = @returnServerCodeName;

	--find displayIndicatorID, insert if it does not exist
	EXEC @displayIndicatorId = USP_ADD_DISPLAY_INDICATOR @displayIndicatorName;--collect DISPLAY_INDICATOR_ID

	---if current returnServerCodeName is not present insert it
	IF @returnServerCodeId = 0
	BEGIN
		INSERT INTO RETURN_SERVER_CODE_INDEX(RETURN_SERVER_CODE_NAME,DISPLAY_INDICATOR_ID)
		VALUES (@returnServerCodeName,@displayIndicatorId);
		--set the inserted returnServerCodeId
		set @returnServerCodeId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE RETURN_SERVER_CODE_INDEX
		SET DISPLAY_INDICATOR_ID = @displayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @displayIndicatorId AND RETURN_SERVER_CODE_ID = @returnServerCodeId;
	END
return @returnServerCodeId--return the RETURN_CODE_SERVER_ID
GO
/*
SAMPLE
EXEC USP_ADD_RETURN_SERVER_CODE 'AA101','ACTIVE';
EXEC USP_ADD_RETURN_SERVER_CODE 'AA102','ACTIVE';
EXEC USP_ADD_RETURN_SERVER_CODE 'AA103','ACTIVE';
EXEC USP_ADD_RETURN_SERVER_CODE 'AA104','ACTIVE';
END UPSERT FOR RETURN SERVER CODE INDEX
*/

/*CREATE UPSERT FOR HULL INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_HULL_INDEX]--HULL(PASSED),HULL_NAME(PASSED),STATUS_ID(COLLECTED),HULL_TYPE_ID(COLLECTED),CLASS_TYPE_ID(COLLECTED),RETURN_SERVER_CODE_ID(COLLECTED),DISPLAY_INDICATOR_ID(COLLECTED)
	@hull varchar(16),--PASSED FOR INSERT
	@hullName varchar(50),--PASSED FOR INSERT
	@hullStatusName varchar(50),--PASSED FOR USP CALL
	@statusDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullTypeName varchar(50),--PASSED FOR USP CALL
	@hullTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@classTypeName varchar(50),--PASSED FOR USP CALL
	@classTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@returnServerCodeName varchar(50),--PASSED FOR USP CALL
	@returnServerCodeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullDisplayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @hullId int;--DECLARE HULL_ID
	DECLARE @hullStatusId int;--DECLARE HULL_INDEX.STATUS_ID
	DECLARE @hullTypeId int;--DECLARE HULL_TYPE_ID
	DECLARE @classTypeId int;--DECLARE CLASS_TYPE_ID
	DECLARE @returnServerCodeId int;--DECLARE RETURN_SERVER_CODE_ID
	DECLARE @hullDisplayIndicatorId int;--DECLARE HULL_INDEX.DISPLAY_INDICATOR_ID
	SET @hullId = 0;--SET HULL_ID

	-- find the current hullId if HULL AND HULL_NAME already exist
	SELECT @hullId = HULL_ID FROM HULL_INDEX
	WHERE HULL = @hull AND HULL_NAME = @hullName;

	--find statusId, insert if it does not exist
	EXEC @hullStatusId = USP_ADD_STATUS @hullStatusName,@statusDisplayIndicatorName;
	--find hullTypeId, insert it it does not exist
	EXEC @hullTypeId = USP_ADD_HULL_TYPE @hullTypeName,@hullTypeDisplayIndicatorName;
	--find classTypeId, insert if it does not exist
	EXEC @classTypeId = USP_ADD_CLASS_TYPE @classTypeName,@classTypeDisplayIndicatorName;
	--find returnServerCodeId, insert if it does not exist
	EXEC @returnServerCodeId = USP_ADD_RETURN_SERVER_CODE @returnServerCodeName,@returnServerCodeDisplayIndicatorName;
	--find displayIndicatorID, insert if it does not exist
	EXEC @hullDisplayIndicatorId = USP_ADD_DISPLAY_INDICATOR @hullDisplayIndicatorName;

	---if current hullId is not present insert the hullId
	IF @hullId = 0
	BEGIN
		INSERT INTO HULL_INDEX (HULL,HULL_NAME,STATUS_ID,HULL_TYPE_ID,CLASS_TYPE_ID,RETURN_SERVER_CODE_ID,DISPLAY_INDICATOR_ID)
		VALUES (@hull,@hullName,@hullStatusId,@hullTypeId,@classTypeId,@returnServerCodeId,@hullDisplayIndicatorId);
		--set the inserted hullId
		set @hullId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE HULL_INDEX
		SET STATUS_ID = @hullStatusId
		WHERE STATUS_ID <> @hullStatusId AND HULL_ID = @hullId;
		UPDATE HULL_INDEX
		SET HULL_TYPE_ID = @hullTypeId
		WHERE HULL_TYPE_ID <> @hullTypeId AND HULL_ID = @hullId;
		UPDATE HULL_INDEX
		SET CLASS_TYPE_ID = @classTypeId
		WHERE CLASS_TYPE_ID <> @classTypeId AND HULL_ID = @hullId;
		UPDATE HULL_INDEX
		SET RETURN_SERVER_CODE_ID = @returnServerCodeId
		WHERE RETURN_SERVER_CODE_ID <> @returnServerCodeId AND HULL_ID = @hullId;
		UPDATE HULL_INDEX
		SET DISPLAY_INDICATOR_ID = @hullDisplayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @hullDisplayIndicatorId AND HULL_ID = @hullId;
	END
return @hullId--return HULL_ID
GO
/*
SAMPLE
EXEC USP_ADD_HULL_INDEX 'DDG-1001','USS MICHAEL MONSOOR','GREEN','ACTIVE','SURF','ACTIVE','DDG-1000','ACTIVE','AA101','ACTIVE','ACTIVE';
END UPSERT FOR HULL INDEX
*/

/*CREATE UPSERT FOR SERVER INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_SERVER_INDEX]--SERVER_ID(GENERATED OR RETURNED),SERVERNAME(PASSED),DISPLAY_INDICATOR_ID(COLLECTED)
	@servername varchar(50),--PASSED FOR INSERT
	@displayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @serverId int;--DECLARE SERVER_ID
	DECLARE @displayIndicatorId int;--DECLARE DISPLAY_INDICATOR_ID
	SET @serverId = 0;--SET SERVER_ID

	-- find the serverId for SERVERNAME if SERVERNAME already exists
	SELECT @serverId = SERVER_ID FROM SERVER_INDEX
	WHERE SERVERNAME = @servername;
	
	--find displayIndicatorID, insert if it does not exist
	EXEC @displayIndicatorId = USP_ADD_DISPLAY_INDICATOR @displayIndicatorName;--collect DISPLAY_INDICATOR_ID

	IF @serverId = 0
	BEGIN
		INSERT INTO SERVER_INDEX (SERVERNAME,DISPLAY_INDICATOR_ID)
		VALUES (@servername,@displayIndicatorId);
		--set the inserted serverId
		set @serverId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE SERVER_INDEX
		SET DISPLAY_INDICATOR_ID = @displayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @displayIndicatorId AND SERVER_ID = @serverId;
	END
RETURN @serverId--return serverId
GO
/*
SAMPLE
EXEC USP_ADD_SERVER_INDEX 'DDG1001USV01','ACTIVE';
END UPSERT FOR SERVER INDEX
*/

/*CREATE UPSERT FOR HULL SERVER INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_HULL_SERVER_INDEX]--SERVER_ID(GENERATED OR RETURNED),HULL_ID(COLLECTED),STATUS_ID(COLLECTED)
	@hull varchar(16),--PASSED FOR USP CALL
	@hullName varchar(50),--PASSED FOR USP CALL
	@hullStatusName varchar(50),--PASSED FOR USP CALL
	@statusDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullTypeName varchar(50),--PASSED FOR USP CALL
	@hullTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@classTypeName varchar(50),--PASSED FOR USP CALL
	@classTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@returnServerCodeName varchar(50),--PASSED FOR USP CALL
	@returnServerCodeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@servername varchar(50),--PASSED FOR USP CALL
	@serverDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@hullServerStatusName varchar(50),--PASSED FOR USP CALL
	@hullServerStatusDisplayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @hullServerId int;
	DECLARE @hullId int;
	DECLARE @serverId int;
	DECLARE @hullServerStatusId int;

	---init hull_server_id var
	SET @hullServerId = 0;

	---find hullId, insert if it does not exist
	EXEC @hullId = USP_ADD_HULL_INDEX @hull,@hullName,@hullStatusName,@statusDisplayIndicatorName,@hullTypeName,@hullTypeDisplayIndicatorName,@classTypeName,@classTypeDisplayIndicatorName,@returnServerCodeName,@returnServerCodeDisplayIndicatorName,@hullDisplayIndicatorName;

	--find serverId, insert if it does not exist
	EXEC @serverId = USP_ADD_SERVER_INDEX @servername,@serverDisplayIndicatorName;
	
	--find statusId, insert if it does not exist
	EXEC @hullServerStatusId = USP_ADD_STATUS @hullServerStatusName,@hullServerStatusDisplayIndicatorName; 

	---find HULL_SERVER_ID if HULL_ID + SERVER_ID already exist
	SELECT @hullServerId = HULL_SERVER_ID FROM HULL_SERVER_INDEX
	WHERE HULL_ID = @hullId AND SERVER_ID = @serverId

	---insert new HULL_SERVER if non exist
	IF @hullServerId = 0
	BEGIN
		INSERT INTO HULL_SERVER_INDEX (SERVER_ID, HULL_ID,STATUS_ID)
		VALUES (@serverId, @hullId,@hullServerStatusId);
		SET @hullServerId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE HULL_SERVER_INDEX
		SET STATUS_ID = @hullServerStatusId
		WHERE STATUS_ID <> @hullServerStatusId AND HULL_SERVER_ID = @hullServerId;
	END
RETURN @hullServerId--returns HULL_SERVER_ID
GO
/*END UPSERT FOR HULL SERVER INDEX
SAMPLE
EXEC USP_ADD_HULL_SERVER_INDEX 'DDG-1001','USS MICHAEL MONSOOR','GREEN','ACTIVE','SURF','ACTIVE','DDG-1000','ACTIVE','AA101','ACTIVE','ACTIVE','DDG1001USV01','ACTIVE','GREEN','ACTIVE';
*/

/*CREATE UPSERT FOR HULL TARGET_AREA INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_HULL_SERVER_TARGET_AREA_INDEX]--HULL_SERVER_TARGET_AREA_ID(GENERATED OR RETURNED),HULL_SERVER_ID(COLLECTED),TARGET_AREA_ID(COLLECTED),STATUS_ID(DERIVED)
	--hullServer parameters
	@hull varchar(16),--PASSED FOR USP CALL
	@hullName varchar(50),--PASSED FOR USP CALL
	@hullStatusName varchar(50),--PASSED FOR USP CALL
	@statusDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullTypeName varchar(50),--PASSED FOR USP CALL
	@hullTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@classTypeName varchar(50),--PASSED FOR USP CALL
	@classTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@returnServerCodeName varchar(50),--PASSED FOR USP CALL
	@returnServerCodeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@servername varchar(50),--PASSED FOR USP CALL
	@serverDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@hullServerStatusName varchar(50),--PASSED FOR USP CALL
	@hullServerStatusDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	--targetArea parameters
	@targetAreaName varchar(50),--PASSED FOR USP CALL
	@targetAreaDisplayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @hullServerTargetAreaStatusName varchar(50);--declare common var for logic
	DECLARE @hullServerTargetAreaStatusDisplayIndicatorName varchar(50);--declare common var for logic
	DECLARE @hullServerTargetAreaId int;--DECLARE HULL_SERVER_TARGET_AREA_ID
	DECLARE @hullServerId int;--DECLARE HULL_SERVER_ID
	DECLARE @targetAreaId int;--DECLARE TARGET_AREA_ID
	DECLARE @hullServerTargetAreaStatusId int;--DECLARE HULL_SERVER_TARGET_AREA.STATUS.ID

	---init HULL_SERVER_TARGET_AREA_ID
	SET @hullServerTargetAreaId = 0;
	SET @hullServerTargetAreaStatusName = 'NOT SET';
	SET @hullServerTargetAreaStatusDisplayIndicatorName = 'ACTIVE';

	---collect HULL_SERVER_ID
	EXEC @hullServerId = USP_ADD_HULL_SERVER_INDEX @hull,@hullName,@hullStatusName,@statusDisplayIndicatorName,@hullTypeName,@hullTypeDisplayIndicatorName,@classTypeName,@classTypeDisplayIndicatorName,@returnServerCodeName,@returnServerCodeDisplayIndicatorName,@hullDisplayIndicatorName,@servername,@serverDisplayIndicatorName,@hullServerStatusName,@hullServerStatusDisplayIndicatorName;

	--collect TARGET_AREA_ID
	EXEC @targetAreaId = USP_ADD_TARGET_AREA_INDEX @targetAreaName,@targetAreaDisplayIndicatorName;

	---collect HULL_SERVER_TARGET_AREA_ID if it HULL_SERVER_TARGET_AREA ALREADY EXISTS
	SELECT @hullServerTargetAreaId = HULL_SERVER_TARGET_AREA_ID FROM HULL_SERVER_TARGET_AREA_INDEX
	WHERE HULL_SERVER_ID = @hullServerId AND TARGET_AREA_ID = @targetAreaId;

	--find statusId, insert if it does not exist
	EXEC @hullServerTargetAreaStatusId = USP_ADD_STATUS @hullServerTargetAreaStatusName,@hullServerTargetAreaStatusDisplayIndicatorName;

	---insert hull_targetArea_id and associated values if not hull_targetArea_id does not exist
	IF @hullServerTargetAreaId = 0
	BEGIN
		INSERT INTO HULL_SERVER_TARGET_AREA_INDEX (HULL_SERVER_ID,TARGET_AREA_ID,STATUS_ID)
		VALUES (@hullServerId,@targetAreaId,@hullServerTargetAreaStatusId);
		SET @hullServerTargetAreaId = @@IDENTITY
	END
	
	--UPDATE STATUS
	DECLARE @currentTime datetime2;--declare current time var
	DECLARE @contactTime datetime2;--declare dsUpdateContactTime var
	DECLARE @timeDifference int;--declare time difference var

	SET @contactTime = '1950-10-02 11:40:32.537';--set contact time var

	SELECT @contactTime = CONTACT_TIMESTAMP FROM DS_UPDATE_ACTIVITY
	WHERE HULL_SERVER_ID = @hullServerId AND @targetAreaName = 'DS_UPDATE_ACTIVITY';

	IF @contactTime <> '1950-10-02 11:40:32.537'
	BEGIN
		SET @currentTime = CURRENT_TIMESTAMP;
		SELECT @timeDifference = DATEDIFF(HOUR,@contactTime,@currentTime);

		IF @timeDifference > 48
		BEGIN
			SET @hullServerTargetAreaStatusName = 'RED';
			SET @hullServerTargetAreaStatusDisplayIndicatorName = 'ACTIVE';
		END 
		ELSE IF @timeDifference > 24 OR @timeDifference = 24
		BEGIN
			SET @hullServerTargetAreaStatusName = 'YELLOW';
			SET @hullServerTargetAreaStatusDisplayIndicatorName = 'ACTIVE';
		END
		ELSE
		BEGIN
			SET @hullServerTargetAreaStatusName = 'GREEN';
			SET @hullServerTargetAreaStatusDisplayIndicatorName = 'ACTIVE';
		END
		--COLLECT NEW STATUS_ID
		EXEC @hullServerTargetAreaStatusId = USP_ADD_STATUS @hullServerTargetAreaStatusName,@hullServerTargetAreaStatusDisplayIndicatorName;
		--UPDATE
		UPDATE HULL_SERVER_TARGET_AREA_INDEX
		SET STATUS_ID = @hullServerTargetAreaStatusId
		WHERE STATUS_ID <> @hullServerTargetAreaStatusId AND HULL_SERVER_TARGET_AREA_ID = @hullServerTargetAreaId;
	END
RETURN @hullServerTargetAreaId--returns HULL_SERVER_TARGET_AREA_ID
GO
/*END UPSERT FOR HULL SERVER TARGET AREA INDEX
SAMPLE
EXEC USP_ADD_HULL_SERVER_TARGET_AREA_INDEX 'DDG-1001','USS MICHAEL MONSOOR','GREEN','ACTIVE','SURF','ACTIVE','DDG-1000','ACTIVE','AA101','ACTIVE','ACTIVE','DDG1001USV01','ACTIVE','GREEN','ACTIVE','DS_UPDATE_ACTIVITY','ACTIVE';
*/

/*CREATE UPSERT FOR DS_UPDATE_ACTIVITY*/
CREATE PROCEDURE [dbo].[USP_ADD_DS_UPDATE_ACTIVITY]--HULL_SERVER_ID(COLLECTED),CONTACT_TIMESTAMP(PASSED),ACTIVITY(PASSED),EXECUTOR(PASSED)
	--hullServer parameters
	@hull varchar(16),--PASSED FOR USP CALL
	@hullName varchar(50),--PASSED FOR USP CALL
	@hullStatusName varchar(50),--PASSED FOR USP CALL
	@statusDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullTypeName varchar(50),--PASSED FOR USP CALL
	@hullTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@classTypeName varchar(50),--PASSED FOR USP CALL
	@classTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@returnServerCodeName varchar(50),--PASSED FOR USP CALL
	@returnServerCodeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@servername varchar(50),--PASSED FOR USP CALL
	@serverDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@hullServerStatusName varchar(50),--PASSED FOR USP CALL
	@hullServerStatusDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	--dsUpdateActivity parameters
	@contactTimestamp datetime2,
	@activity varchar(255),
	@executor varchar(50)
AS
	DECLARE @dsUpdateActivityId int;--DECLARE DS_UPDATE_ACTIVITY_ID
	DECLARE @hullServerId int;--DECLARE HULL_SERVER_ID

	SET @dsUpdateActivityId = 0;--SET DS_UPDATE_ACTIVITY_ID

	---collect HULL_SERVER_ID
	EXEC @hullServerId = USP_ADD_HULL_SERVER_INDEX @hull,@hullName,@hullStatusName,@statusDisplayIndicatorName,@hullTypeName,@hullTypeDisplayIndicatorName,@classTypeName,@classTypeDisplayIndicatorName,@returnServerCodeName,@returnServerCodeDisplayIndicatorName,@hullDisplayIndicatorName,@servername,@serverDisplayIndicatorName,@hullServerStatusName,@hullServerStatusDisplayIndicatorName;

	-- find if DS_UPDATE_ACTIVITY_ID if HULL_SERVER already exists
	SELECT @dsUpdateActivityId = DS_UPDATE_ACTIVITY_ID FROM DS_UPDATE_ACTIVITY
	WHERE HULL_SERVER_ID = @hullServerId;

	---if a hull+server does not already exist insert one
	IF @dsUpdateActivityId = 0
	BEGIN
		INSERT INTO DS_UPDATE_ACTIVITY (HULL_SERVER_ID,CONTACT_TIMESTAMP,ACTIVITY,EXECUTOR)
		VALUES (@hullServerId,@contactTimestamp,@activity,@executor);
		--set the inserted hullId
		set @dsUpdateActivityId = @@IDENTITY
	END
	---else update contact time if it more recent
	ELSE
	BEGIN
		UPDATE DS_UPDATE_ACTIVITY
		SET CONTACT_TIMESTAMP = @contactTimestamp, ACTIVITY = @activity, EXECUTOR = @activity
		WHERE @dsUpdateActivityId = DS_UPDATE_ACTIVITY_ID AND @contactTimestamp > CONTACT_TIMESTAMP;
	END
RETURN @dsUpdateActivityId --RETURNS DS_UPDATE_ACTIVITY_ID
GO
/*
SAMPLE
EXEC USP_ADD_DS_UPDATE_ACTIVITY 'DDG-1001','USS MICHAEL MONSOOR','GREEN','ACTIVE','SURF','ACTIVE','DDG-1000','ACTIVE','AA101','ACTIVE','ACTIVE','DDG1001USV01','ACTIVE','GREEN','ACTIVE','2017-10-02 11:40:32.537','CHECKING FOR UPDATES','WAKE UP';
END UPSERT FOR DS_UPDATE_ACTIVITY
*/

/*CREATE UPSERT FOR AUTH_APP_VENDOR_INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_AUTH_APP_VENDOR]
	@authAppVendorName varchar(50),--PASSED FOR INSERT
	@displayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @authAppVendorId int;--DECLARE AUTH_APP_VENDOR_ID
	DECLARE @displayIndicatorId int;--DECLARE DISPLAY_INDICATOR_ID
	SET @authAppVendorId = 0;--SET SET AUTH_APP_VENDOR_ID

	--write in AUTH_APP_VENDOR_ID if already present
	SELECT @authAppVendorId = AUTH_APP_VENDOR_ID FROM AUTH_APP_VENDOR_INDEX
	WHERE AUTH_APP_VENDOR_NAME = @authAppVendorName;

	--find displayIndicatorID insert if it does not exist
	EXEC @displayIndicatorId = USP_ADD_DISPLAY_INDICATOR @displayIndicatorName;--collect DISPLAY_INDICATOR_ID

	---if current authAppVendorName is not present create new authAppVendorName and id
	IF @authAppVendorId = 0
	BEGIN
		INSERT INTO AUTH_APP_VENDOR_INDEX (AUTH_APP_VENDOR_NAME,DISPLAY_INDICATOR_ID)
		VALUES (@authAppVendorName,@displayIndicatorId);
		--set the inserted authAppVendorId
		set @authAppVendorId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE AUTH_APP_VENDOR_INDEX
		SET DISPLAY_INDICATOR_ID = @displayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @displayIndicatorId AND AUTH_APP_VENDOR_ID = @authAppVendorId;
	END
return @authAppVendorId--RETURNS AUTH_APP_VENDOR_ID
GO
/*
SAMPLE
EXEC USP_ADD_AUTH_APP_VENDOR 'ORACLE','ACTIVE';
END UPSERT FOR AUTH_APP_VENDOR_INDEX
*/

/*CREATE UPSERT FOR AUTH_APP_INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_AUTH_APP]
	@authAppName varchar(50),--PASSED FOR INSERT
	@authAppDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@authAppVendorName varchar(50),--PASSED FOR USP CALL
	@authAppVendorDisplayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @authAppId int;--DECLARE AUTH_APP_ID
	DECLARE @authAppDisplayIndicatorId int;--DECLARE AUTH_APP_INDEX.DISPLAY_INDICATOR_ID
	DECLARE @authAppVendorId int;--DECLARE AUTH_APP_VENDOR_ID
	SET @authAppId = 0;--SET SET AUTH_APP_ID

	--write in AUTH_APP_ID if already present
	SELECT @authAppId = AUTH_APP_ID FROM AUTH_APP_INDEX
	WHERE AUTH_APP_NAME = @authAppName;

	--find displayIndicatorId insert if it does not exist for authApp
	EXEC @authAppDisplayIndicatorId = USP_ADD_DISPLAY_INDICATOR @authAppDisplayIndicatorName;--collect DISPLAY_INDICATOR_ID

	--find authAppVendorId
	EXEC @authAppVendorId = USP_ADD_AUTH_APP_VENDOR @authAppVendorName,@authAppVendorDisplayIndicatorName;

	---if current authAppName is not present create new authAppName and id
	IF @authAppId = 0
	BEGIN
		INSERT INTO AUTH_APP_INDEX (AUTH_APP_NAME,AUTH_APP_VENDOR_ID,DISPLAY_INDICATOR_ID)
		VALUES (@authAppName,@authAppVendorId,@authAppDisplayIndicatorId);
		--set the inserted authAppId
		set @authAppId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE AUTH_APP_INDEX
		SET DISPLAY_INDICATOR_ID = @authAppDisplayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @authAppDisplayIndicatorId AND AUTH_APP_ID = @authAppId;
		UPDATE AUTH_APP_INDEX
		SET AUTH_APP_VENDOR_ID = @authAppVendorId
		WHERE AUTH_APP_VENDOR_ID <> @authAppVendorId AND AUTH_APP_ID = @authAppId;
	END
return @authAppId--RETURNS AUTH_APP_ID
GO
/*
SAMPLE
EXEC USP_ADD_AUTH_APP 'JAVA','ACTIVE','ORACLE','ACTIVE';
END UPSERT FOR AUTH_APP_INDEX 
*/

/*CREATE UPSERT FOR AUTH_APP_RELEASE*/
CREATE PROCEDURE [dbo].[USP_ADD_AUTH_APP_RELEASE]
	--for release
	@authVersion varchar(50),--PASSED FOR INSERT
	@authAppReleaseDate datetime2,--PASSED FOR INSERT
	--for authAppId call
	@authAppName varchar(50),--PASSED FOR USP CALL
	@authAppDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@authAppVendorName varchar(50),--PASSED FOR USP CALL
	@authAppVendorDisplayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @authAppReleaseId int;--DECLARE AUTH_APP_RELEASE_ID
	DECLARE @authAppId int;--DECLARE AUTH_APP_ID
	SET @authAppReleaseId = 0;--SET SET AUTH_APP_RELEASE_ID

	--collect authAppId
	EXEC @authAppId = USP_ADD_AUTH_APP @authAppName,@authAppDisplayIndicatorName,@authAppVendorName,@authAppVendorDisplayIndicatorName;

	--find authAppReleaseId if APP+VERSION has already been released
	SELECT @authAppReleaseId = AUTH_APP_RELEASE_ID FROM AUTH_APP_RELEASE
	WHERE AUTH_APP_ID = @authAppId AND AUTH_VERSION = @authVersion;

	---if APP+VERSION is has not been released, release APP+VERSION and create an id
	IF @authAppReleaseId = 0
	BEGIN
		INSERT INTO AUTH_APP_RELEASE (AUTH_APP_ID,AUTH_VERSION,AUTH_APP_RELEASE_DATE)
		VALUES (@authAppId,@authVersion,@authAppReleaseDate);
		--set the inserted authAppReleaseId
		set @authAppReleaseId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE AUTH_APP_RELEASE
		SET AUTH_APP_RELEASE_DATE = @authAppReleaseDate
		WHERE AUTH_APP_RELEASE_DATE <> @authAppReleaseDate AND AUTH_APP_RELEASE_ID = @authAppReleaseId;
	END
return @authAppReleaseId--RETURNS AUTH_APP_RELEASE_ID
GO
/*
SAMPLE
EXEC USP_ADD_AUTH_APP_RELEASE '1.8_101','2017-05-02 11:40:32.537','JAVA','ACTIVE','ORACLE','ACTIVE';
END UPSERT FOR AUTH_APP_RELEASE
*/

/*CREATE UPSERT FOR AUTH_HULL_APP_RELEASE*/
CREATE PROCEDURE [dbo].[USP_ADD_AUTH_HULL_APP_RELEASE]
	--for authHullAppRelease call
	@authVersion varchar(50),--PASSED FOR INSERT
	@authAppReleaseDate datetime2,--PASSED FOR INSERT
	@authAppName varchar(50),--PASSED FOR USP CALL
	@authAppDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@authAppVendorName varchar(50),--PASSED FOR USP CALL
	@authAppVendorDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	--for hullServer call
	@hull varchar(16),--PASSED FOR USP CALL
	@hullName varchar(50),--PASSED FOR USP CALL
	@hullStatusName varchar(50),--PASSED FOR USP CALL
	@statusDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullTypeName varchar(50),--PASSED FOR USP CALL
	@hullTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@classTypeName varchar(50),--PASSED FOR USP CALL
	@classTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@returnServerCodeName varchar(50),--PASSED FOR USP CALL
	@returnServerCodeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@servername varchar(50),--PASSED FOR USP CALL
	@serverDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@hullServerStatusName varchar(50),--PASSED FOR USP CALL
	@hullServerStatusDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	--for insert
	@hullAppReleaseDate datetime2
AS
	DECLARE @authHullAppReleaseId int;--DECLARE AUTH_HULL_APP_RELEASE_ID
	DECLARE @authAppReleaseId int;--DECLARE AUTH_APP_RELEASE_ID
	DECLARE @hullServerId int;--DECLARE HULL_SERVER_ID
	SET @authAppReleaseId = 0;--DECLARE AUTH_APP_RELEASE_ID
	SET @authHullAppReleaseId = 0;--SET SET AUTH_HULL_APP_RELEASE_ID

	--collect authAppReleaseId if App has already been released, if not release it
	EXEC @authAppReleaseId = USP_ADD_AUTH_APP_RELEASE @authVersion,@authAppReleaseDate,@authAppName,@authAppDisplayIndicatorName,@authAppVendorName,@authAppVendorDisplayIndicatorName;

	---collect hullServerId, if HULL+SERVER does not exist create it
	EXEC @hullServerId = USP_ADD_HULL_SERVER_INDEX @hull,@hullName,@hullStatusName,@statusDisplayIndicatorName,@hullTypeName,@hullTypeDisplayIndicatorName,@classTypeName,@classTypeDisplayIndicatorName,@returnServerCodeName,@returnServerCodeDisplayIndicatorName,@hullDisplayIndicatorName,@servername,@serverDisplayIndicatorName,@hullServerStatusName,@hullServerStatusDisplayIndicatorName;

	--find authAppReleaseId if APP+VERSION has already been released
	SELECT @authHullAppReleaseId = AUTH_HULL_APP_RELEASE_ID FROM AUTH_HULL_APP_RELEASE
	WHERE AUTH_APP_RELEASE_ID = @authAppReleaseId AND HULL_SERVER_ID = @hullServerId;

	---if APP+VERSION is has not been released to the appropriated hull, release HULL+APP+VERSION and create an id
	IF @authHullAppReleaseId = 0
	BEGIN
		INSERT INTO AUTH_HULL_APP_RELEASE (HULL_SERVER_ID,AUTH_APP_RELEASE_ID,HULL_APP_RELEASE_DATE)
		VALUES (@hullServerId,@authAppReleaseId,@hullAppReleaseDate);
		--set the inserted authHullAppReleaseId
		set @authHullAppReleaseId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE AUTH_HULL_APP_RELEASE
		SET HULL_APP_RELEASE_DATE = @hullAppReleaseDate
		WHERE HULL_APP_RELEASE_DATE <> @hullAppReleaseDate AND AUTH_HULL_APP_RELEASE_ID = @authHullAppReleaseId;
	END
return @authHullAppReleaseId--RETURNS AUTH_HULL_APP_RELEASE_ID
GO
/*
SAMPLE
EXEC USP_ADD_AUTH_HULL_APP_RELEASE '1.8_101','2017-05-02 11:40:32.537','JAVA','ACTIVE','ORACLE','ACTIVE','DDG-1001','USS MICHAEL MONSOOR','GREEN','ACTIVE','SURF','ACTIVE','DDG-1000','ACTIVE','AA101','ACTIVE','ACTIVE','DDG1001USV01','ACTIVE','GREEN','ACTIVE','2017-10-02 11:40:32.537';
END UPSERT FOR AUTH_HULL_APP_RELEASE
*/
/*CREATE UPSERT FOR INSTALLED_AFLOAT_APP_VENDOR_INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_INSTALLED_AFLOAT_APP_VENDOR_INDEX]
	@installedAfloatAppVendorName varchar(50),--PASSED FOR INSERT
	@installedAfloatAppVendorDisplayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @installedAfloatAppVendorId int;--DECLARE INSTALLED_AFLOAT_APP_VENDOR_ID
	DECLARE @installedAfloatAppVendorDisplayIndicatorId int;--DECLARE INSTALLED_AFLOAT_APP_VENDOR_INDEX.DISPLAY_INDICATOR_ID

	SET @installedAfloatAppVendorId = 0;--SET INSTALLED_AFLOAT_APP_VENDOR_ID

	--write in INSTALLED_AFLOAT_APP_VENDOR_ID if already present
	SELECT @installedAfloatAppVendorId = INSTALLED_AFLOAT_APP_VENDOR_ID FROM INSTALLED_AFLOAT_APP_VENDOR_INDEX
	WHERE INSTALLED_AFLOAT_APP_VENDOR_NAME = @installedAfloatAppVendorName;

	--find installedAfloatAppVendorDisplayIndicatorID insert if it does not exist
	EXEC @installedAfloatAppVendorDisplayIndicatorId = USP_ADD_DISPLAY_INDICATOR @installedAfloatAppVendorDisplayIndicatorName;--collect DISPLAY_INDICATOR_ID

	---if current installedAfloatAppVendorName is not present create new installedAfloatAppVendorName and id
	IF @installedAfloatAppVendorId = 0
	BEGIN
		INSERT INTO INSTALLED_AFLOAT_APP_VENDOR_INDEX (INSTALLED_AFLOAT_APP_VENDOR_NAME,DISPLAY_INDICATOR_ID)
		VALUES (@installedAfloatAppVendorName,@installedAfloatAppVendorDisplayIndicatorId);
		--set the inserted installedAfloatAppVendorId
		set @installedAfloatAppVendorId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE INSTALLED_AFLOAT_APP_VENDOR_INDEX
		SET DISPLAY_INDICATOR_ID = @installedAfloatAppVendorDisplayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @installedAfloatAppVendorDisplayIndicatorId AND INSTALLED_AFLOAT_APP_VENDOR_ID = @installedAfloatAppVendorId;
	END
return @installedAfloatAppVendorId--RETURNS INSTALLED_AFLOAT_APP_VENDOR_ID
GO
/*
SAMPLE
EXEC USP_ADD_INSTALLED_AFLOAT_APP_VENDOR 'ORACLE','ACTIVE';
END UPSERT FOR INSTALLED_AFLOAT_APP_VENDOR_INDEX
*/


/*CREATE UPSERT FOR AFLOAT_APP_INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_AFLOAT_APP]
	--for afloatAppIndex
	@afloatAppName varchar(50),--PASSED FOR INSERT
	@afloatAppDisplayIndicatorName varchar(50),--PASSED FOR USP CALL to collect id
	--for installedAfloatAppVendor stored procedure
	@installedAfloatAppVendorName varchar(50),--PASSED FOR USP CALL
	@installedAfloatAppVendorDisplayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @afloatAppId int;--DECLARE AFLOAT_APP_ID
	DECLARE @afloatAppDisplayIndicatorId int;--DECLARE AFLOAT_APP_INDEX.DISPLAY_INDICATOR_ID
	DECLARE @installedAfloatAppVendorId int;--DECLARE AFLOAT_APP_VENDOR_ID
	SET @afloatAppId = 0;--SET SET AFLOAT_APP_ID

	--find installedAfloatAppVendorId
	EXEC @installedAfloatAppVendorId = USP_ADD_INSTALLED_AFLOAT_APP_VENDOR_INDEX @installedAfloatAppVendorName,@installedAfloatAppVendorDisplayIndicatorName;

	--write in AFLOAT_APP_ID if already present
	SELECT @afloatAppId = AFLOAT_APP_ID FROM AFLOAT_APP_INDEX
	WHERE AFLOAT_APP_NAME = @afloatAppName AND INSTALLED_AFLOAT_APP_VENDOR_ID = @installedAfloatAppVendorId;

	--find displayIndicatorId insert if it does not exist for afloatApp
	EXEC @afloatAppDisplayIndicatorId = USP_ADD_DISPLAY_INDICATOR @afloatAppDisplayIndicatorName;--collect DISPLAY_INDICATOR_ID

	---if current afloatAppName is not present create new afloatAppName and id
	IF @afloatAppId = 0
	BEGIN
		INSERT INTO AFLOAT_APP_INDEX (AFLOAT_APP_NAME,INSTALLED_AFLOAT_APP_VENDOR_ID,DISPLAY_INDICATOR_ID)
		VALUES (@afloatAppName,@installedAfloatAppVendorId,@afloatAppDisplayIndicatorId);
		--set the inserted afloatAppId
		set @afloatAppId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE AFLOAT_APP_INDEX
		SET DISPLAY_INDICATOR_ID = @afloatAppDisplayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @afloatAppDisplayIndicatorId AND AFLOAT_APP_ID = @afloatAppId;
	END
return @afloatAppId--RETURNS AFLOAT_APP_ID
GO
/*
SAMPLE
EXEC USP_ADD_AFLOAT_APP 'JAVA','ACTIVE','ORACLE','ACTIVE';
END UPSERT FOR AFLOAT_APP_INDEX 
*/

/*CREATE UPSERT FOR AFLOAT_APP_VERSION_INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_AFLOAT_APP_VERSION]
	--for release
	@afloatVersion varchar(50),--PASSED FOR INSERT
	--for afloatAppId call
	@afloatAppName varchar(50),--PASSED FOR USP CALL
	@afloatAppDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@afloatAppVendorName varchar(50),--PASSED FOR USP CALL
	@afloatAppVendorDisplayIndicatorName varchar(50)--PASSED FOR USP CALL
AS
	DECLARE @afloatAppVersionId int;--DECLARE AFLOAT_APP_VERSION_ID
	DECLARE @afloatAppId int;--DECLARE AFLOAT_APP_ID
	SET @afloatAppVersionId = 0;--SET SET AFLOAT_APP_VERSION_ID

	--collect afloatAppId
	EXEC @afloatAppId = USP_ADD_AFLOAT_APP @afloatAppName,@afloatAppDisplayIndicatorName,@afloatAppVendorName,@afloatAppVendorDisplayIndicatorName;

	--find afloatAppVersionId if APP+VERSION has already been installed
	SELECT @afloatAppVersionId = AFLOAT_APP_VERSION_ID FROM AFLOAT_APP_VERSION_INDEX
	WHERE AFLOAT_APP_ID = @afloatAppId AND AFLOAT_VERSION = @afloatVersion;

	---if APP+VERSION is has not been released, release APP+VERSION and create an id
	IF @afloatAppVersionId = 0
	BEGIN
		INSERT INTO AFLOAT_APP_VERSION_INDEX (AFLOAT_APP_ID,AFLOAT_VERSION)
		VALUES (@afloatAppId,@afloatVersion);
		--set the inserted afloatAppVersionId
		set @afloatAppVersionId = @@IDENTITY
	END
return @afloatAppVersionId--RETURNS AFLOAT_APP_VERSION_ID
GO
/*
SAMPLE
EXEC USP_ADD_AFLOAT_APP_VERSION '1.8_101','JAVA','ACTIVE','ORACLE','ACTIVE';
END UPSERT FOR AFLOAT_APP_VERSION
*/

/*CREATE UPSERT FOR COMPLIANCE_STATUS_INDEX*/
CREATE PROCEDURE [dbo].[USP_ADD_COMPLIANCE_STATUS]--COMPLIANCE_STATUS_ID(GENERATED OR RETURNED),COMPLIANCE_STATUS_NAME(PASSED),DISPLAY_INDICTOR_ID(COLLECTED)
	@complianceStatusName varchar(50),--PASS COMPLIANCE_STATUS_NAME FOR INSERT
	@complianceStatusDisplayIndicator varchar(50)--PASS DISPLAY_INDICATOR_NAME FOR USP CALL
AS
	DECLARE @complianceStatusId int;
	DECLARE @complianceStatusDisplayIndicatorId int;
	SET @complianceStatusId = 0;

	-- find COMPLIANCE_STATUS_ID if COMPLIANCE_STATUS_NAME already exists
	SELECT @complianceStatusId = COMPLIANCE_STATUS_ID FROM COMPLIANCE_STATUS_INDEX
	WHERE COMPLIANCE_STATUS_NAME = @complianceStatusName;
	
	--find displayIndicatorID insert if it does not exist
	EXEC @complianceStatusDisplayIndicatorId = USP_ADD_DISPLAY_INDICATOR @complianceStatusDisplayIndicator;--collect DISPLAY_INDICATOR_ID

	IF @complianceStatusId = 0--if COMPLIANCE_STATUS_NAME does not exist create new COMPLIANCE_STATUS_NAME and id
	BEGIN
		INSERT INTO COMPLIANCE_STATUS_INDEX (COMPLIANCE_STATUS_NAME,DISPLAY_INDICATOR_ID)
		VALUES (@complianceStatusName,@complianceStatusDisplayIndicatorId);
		--set the inserted complianceStatusId
		set @complianceStatusId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE COMPLIANCE_STATUS_INDEX
		SET DISPLAY_INDICATOR_ID = @complianceStatusDisplayIndicatorId
		WHERE DISPLAY_INDICATOR_ID <> @complianceStatusDisplayIndicatorId AND COMPLIANCE_STATUS_ID = @complianceStatusId;
	END
RETURN @complianceStatusId--return COMPLIANCE_STATUS_ID
GO
/*
SAMPLE
EXEC USP_ADD_COMPLIANCE_STATUS_INDEX 'NOTIFICATION SENT','ACTIVE';
EXEC USP_ADD_COMPLIANCE_STATUS_INDEX 'UNDER INVESTIGATION','ACTIVE';
END UPSERT FOR COMPLIANCE_STATUS INDEX
*/
/*VALIDATE IF APP IS UNAUTHORIZED BECAUSE IT APP/VERSION/VENDOR HAS NOT BEEN RELEASED TO THIS HULL*/
CREATE PROCEDURE [dbo].[USP_VALIDATE_HULL_SERVER_APP_VERSION_VENDOR]
	@afloatHullAppInstalledId int
AS
	DECLARE @hullServerId int;
	DECLARE @afloatVersion varchar(50);
	DECLARE @afloatAppName varchar(50);
	DECLARE @afloatAppVendorName varchar(50);
	DECLARE @hullServerAppVendorValid int;
	DECLARE @authHullAppReleaseId int;

	CREATE TABLE #AFLOAT_INFO
	(TEMP_AFLOAT_VENDOR varchar(50),TEMP_AFLOAT_APP varchar(50),TEMP_AFLOAT_VERSION varchar(50),TEMP_AFLOAT_HULL_SERVER_ID int,TEMP_HULL_APP_AFLOAT_ID int)

	INSERT INTO #AFLOAT_INFO(TEMP_AFLOAT_VENDOR,TEMP_AFLOAT_APP,TEMP_AFLOAT_VERSION,TEMP_AFLOAT_HULL_SERVER_ID,TEMP_HULL_APP_AFLOAT_ID)
	SELECT
	INSTALLED_AFLOAT_APP_VENDOR_INDEX.INSTALLED_AFLOAT_APP_VENDOR_NAME,
	AFLOAT_APP_INDEX.AFLOAT_APP_NAME,
	AFLOAT_APP_VERSION_INDEX.AFLOAT_VERSION,
	AFLOAT_HULL_APP_INSTALLED.HULL_SERVER_ID,
	AFLOAT_HULL_APP_INSTALLED.AFLOAT_HULL_APP_INSTALLED_ID
	FROM AFLOAT_HULL_APP_INSTALLED
	LEFT JOIN HULL_SERVER_INDEX
	ON AFLOAT_HULL_APP_INSTALLED.HULL_SERVER_ID = HULL_SERVER_INDEX.HULL_SERVER_ID
	LEFT JOIN AFLOAT_APP_VERSION_INDEX
	ON AFLOAT_HULL_APP_INSTALLED.AFLOAT_APP_VERSION_ID = AFLOAT_APP_VERSION_INDEX.AFLOAT_APP_VERSION_ID
	LEFT JOIN AFLOAT_APP_INDEX
	ON AFLOAT_APP_VERSION_INDEX.AFLOAT_APP_ID = AFLOAT_APP_INDEX.AFLOAT_APP_ID
	LEFT JOIN INSTALLED_AFLOAT_APP_VENDOR_INDEX
	ON AFLOAT_APP_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID = INSTALLED_AFLOAT_APP_VENDOR_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID
	WHERE  @afloatHullAppInstalledId = AFLOAT_HULL_APP_INSTALLED_ID;

	SELECT @hullServerId = TEMP_AFLOAT_HULL_SERVER_ID FROM #AFLOAT_INFO;
	SELECT @afloatVersion = TEMP_AFLOAT_VERSION FROM #AFLOAT_INFO;
	SELECT @afloatAppName = TEMP_AFLOAT_APP FROM #AFLOAT_INFO;
	SELECT @afloatAppVendorName = TEMP_AFLOAT_VENDOR FROM #AFLOAT_INFO;

			CREATE TABLE #HULL_SERVER_APP_VERSION_VENDOR
			(TEMP_AUTH_VENDOR varchar(50),TEMP_AUTH_APP varchar(50),TEMP_AUTH_VERSION varchar(50),TEMP_AUTH_HULL_SERVER varchar(50),TEMP_AUTH_HULL_APP_RELEASE_ID int);
	
			INSERT INTO #HULL_SERVER_APP_VERSION_VENDOR(TEMP_AUTH_VENDOR,TEMP_AUTH_APP,TEMP_AUTH_VERSION,TEMP_AUTH_HULL_SERVER,TEMP_AUTH_HULL_APP_RELEASE_ID)
			SELECT 
			AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_NAME,
			AUTH_APP_INDEX.AUTH_APP_NAME,
			AUTH_APP_RELEASE.AUTH_VERSION,
			AUTH_HULL_APP_RELEASE.HULL_SERVER_ID,
			AUTH_HULL_APP_RELEASE.AUTH_HULL_APP_RELEASE_ID
			FROM AUTH_HULL_APP_RELEASE
			LEFT JOIN AUTH_APP_RELEASE
			ON AUTH_HULL_APP_RELEASE.AUTH_APP_RELEASE_ID = AUTH_APP_RELEASE.AUTH_APP_RELEASE_ID
			LEFT JOIN AUTH_APP_INDEX
			ON AUTH_APP_RELEASE.AUTH_APP_ID = AUTH_APP_INDEX.AUTH_APP_ID
			LEFT JOIN AUTH_APP_VENDOR_INDEX
			ON AUTH_APP_INDEX.AUTH_APP_VENDOR_ID = AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_ID
			WHERE @hullServerId = AUTH_HULL_APP_RELEASE.HULL_SERVER_ID AND @afloatVersion = AUTH_APP_RELEASE.AUTH_VERSION AND @afloatAppName = AUTH_APP_INDEX.AUTH_APP_NAME AND @afloatAppVendorName = AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_NAME;

			SELECT @hullServerAppVendorValid = COUNT(TEMP_AUTH_VERSION) FROM #HULL_SERVER_APP_VERSION_VENDOR;

			IF @hullServerAppVendorValid = 0
				BEGIN
					SET @authHullAppReleaseId = 0;
				END
			ELSE
				BEGIN
					SELECT @authHullAppReleaseId = TEMP_AUTH_HULL_APP_RELEASE_ID FROM #HULL_SERVER_APP_VERSION_VENDOR;
				END

RETURN @authHullAppReleaseId;
GO
/*VALIDATE IF PROGRAM/APP+VERSION+VENDOR IS RELEASED*/
CREATE PROCEDURE [dbo].[USP_VALIDATE_APP_VERSION_VENDOR]
	@afloatHullAppInstalledId int
AS
	DECLARE @afloatVersion varchar(50);
	DECLARE @afloatAppName varchar(50);
	DECLARE @afloatAppVendorName varchar(50);
	DECLARE @appVersionVendorValid int;

	CREATE TABLE #AFLOAT_INFO
	(TEMP_AFLOAT_VENDOR varchar(50),TEMP_AFLOAT_APP varchar(50),TEMP_AFLOAT_VERSION varchar(50),TEMP_AFLOAT_HULL_SERVER_ID int,TEMP_HULL_APP_AFLOAT_ID int)

	INSERT INTO #AFLOAT_INFO(TEMP_AFLOAT_VENDOR,TEMP_AFLOAT_APP,TEMP_AFLOAT_VERSION,TEMP_AFLOAT_HULL_SERVER_ID,TEMP_HULL_APP_AFLOAT_ID)
	SELECT
	INSTALLED_AFLOAT_APP_VENDOR_INDEX.INSTALLED_AFLOAT_APP_VENDOR_NAME,
	AFLOAT_APP_INDEX.AFLOAT_APP_NAME,
	AFLOAT_APP_VERSION_INDEX.AFLOAT_VERSION,
	AFLOAT_HULL_APP_INSTALLED.HULL_SERVER_ID,
	AFLOAT_HULL_APP_INSTALLED.AFLOAT_HULL_APP_INSTALLED_ID
	FROM AFLOAT_HULL_APP_INSTALLED
	LEFT JOIN HULL_SERVER_INDEX
	ON AFLOAT_HULL_APP_INSTALLED.HULL_SERVER_ID = HULL_SERVER_INDEX.HULL_SERVER_ID
	LEFT JOIN AFLOAT_APP_VERSION_INDEX
	ON AFLOAT_HULL_APP_INSTALLED.AFLOAT_APP_VERSION_ID = AFLOAT_APP_VERSION_INDEX.AFLOAT_APP_VERSION_ID
	LEFT JOIN AFLOAT_APP_INDEX
	ON AFLOAT_APP_VERSION_INDEX.AFLOAT_APP_ID = AFLOAT_APP_INDEX.AFLOAT_APP_ID
	LEFT JOIN INSTALLED_AFLOAT_APP_VENDOR_INDEX
	ON AFLOAT_APP_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID = INSTALLED_AFLOAT_APP_VENDOR_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID
	WHERE  @afloatHullAppInstalledId = AFLOAT_HULL_APP_INSTALLED_ID;

	SELECT @afloatVersion = TEMP_AFLOAT_VERSION FROM #AFLOAT_INFO;
	SELECT @afloatAppName = TEMP_AFLOAT_APP FROM #AFLOAT_INFO;
	SELECT @afloatAppVendorName = TEMP_AFLOAT_VENDOR FROM #AFLOAT_INFO;

			CREATE TABLE #APP_VERSION_VENDOR_RELEASED
			(TEMP_AUTH_VENDOR varchar(50),TEMP_AUTH_APP varchar(50),TEMP_AUTH_VERSION varchar(50));
	
			INSERT INTO #APP_VERSION_VENDOR_RELEASED(TEMP_AUTH_VENDOR,TEMP_AUTH_APP,TEMP_AUTH_VERSION)
			SELECT 
			AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_NAME,
			AUTH_APP_INDEX.AUTH_APP_NAME,
			AUTH_APP_RELEASE.AUTH_VERSION
			FROM AUTH_APP_RELEASE
			LEFT JOIN AUTH_APP_INDEX
			ON AUTH_APP_INDEX.AUTH_APP_ID = AUTH_APP_RELEASE.AUTH_APP_ID
			LEFT JOIN AUTH_APP_VENDOR_INDEX
			ON AUTH_APP_INDEX.AUTH_APP_VENDOR_ID = AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_ID
			WHERE
			@afloatVersion = AUTH_APP_RELEASE.AUTH_VERSION AND @afloatAppName = AUTH_APP_INDEX.AUTH_APP_NAME AND @afloatAppVendorName = AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_NAME;

			SELECT @appVersionVendorValid = COUNT(TEMP_AUTH_VERSION) FROM #APP_VERSION_VENDOR_RELEASED;


RETURN @appVersionVendorValid;
GO
/*VALIDATE IF APP+VENDOR RELEASED*/
CREATE PROCEDURE [dbo].[USP_VALIDATE_APP_VENDOR]
	@afloatHullAppInstalledId int
AS
	DECLARE @afloatAppName varchar(50);
	DECLARE @afloatAppVendorName varchar(50);
	DECLARE @appVendorValid int;

	CREATE TABLE #AFLOAT_INFO
	(TEMP_AFLOAT_VENDOR varchar(50),TEMP_AFLOAT_APP varchar(50),TEMP_AFLOAT_VERSION varchar(50),TEMP_AFLOAT_HULL_SERVER_ID int,TEMP_HULL_APP_AFLOAT_ID int)

	INSERT INTO #AFLOAT_INFO(TEMP_AFLOAT_VENDOR,TEMP_AFLOAT_APP,TEMP_AFLOAT_VERSION,TEMP_AFLOAT_HULL_SERVER_ID,TEMP_HULL_APP_AFLOAT_ID)
	SELECT
	INSTALLED_AFLOAT_APP_VENDOR_INDEX.INSTALLED_AFLOAT_APP_VENDOR_NAME,
	AFLOAT_APP_INDEX.AFLOAT_APP_NAME,
	AFLOAT_APP_VERSION_INDEX.AFLOAT_VERSION,
	AFLOAT_HULL_APP_INSTALLED.HULL_SERVER_ID,
	AFLOAT_HULL_APP_INSTALLED.AFLOAT_HULL_APP_INSTALLED_ID
	FROM AFLOAT_HULL_APP_INSTALLED
	LEFT JOIN HULL_SERVER_INDEX
	ON AFLOAT_HULL_APP_INSTALLED.HULL_SERVER_ID = HULL_SERVER_INDEX.HULL_SERVER_ID
	LEFT JOIN AFLOAT_APP_VERSION_INDEX
	ON AFLOAT_HULL_APP_INSTALLED.AFLOAT_APP_VERSION_ID = AFLOAT_APP_VERSION_INDEX.AFLOAT_APP_VERSION_ID
	LEFT JOIN AFLOAT_APP_INDEX
	ON AFLOAT_APP_VERSION_INDEX.AFLOAT_APP_ID = AFLOAT_APP_INDEX.AFLOAT_APP_ID
	LEFT JOIN INSTALLED_AFLOAT_APP_VENDOR_INDEX
	ON AFLOAT_APP_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID = INSTALLED_AFLOAT_APP_VENDOR_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID
	WHERE  @afloatHullAppInstalledId = AFLOAT_HULL_APP_INSTALLED_ID;

	SELECT @afloatAppName = TEMP_AFLOAT_APP FROM #AFLOAT_INFO;
	SELECT @afloatAppVendorName = TEMP_AFLOAT_VENDOR FROM #AFLOAT_INFO;

			CREATE TABLE #VALIDATE_APP_VENDOR
			(TEMP_AUTH_VENDOR varchar(50),TEMP_AUTH_APP varchar(50));
	
			INSERT INTO #VALIDATE_APP_VENDOR(TEMP_AUTH_VENDOR,TEMP_AUTH_APP)
			SELECT 
			AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_NAME,
			AUTH_APP_INDEX.AUTH_APP_NAME
			FROM AUTH_APP_INDEX
			LEFT JOIN AUTH_APP_VENDOR_INDEX
			ON AUTH_APP_INDEX.AUTH_APP_VENDOR_ID = AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_ID
			WHERE
			@afloatAppName = AUTH_APP_INDEX.AUTH_APP_NAME AND @afloatAppVendorName = AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_NAME;

			SELECT @appVendorValid = COUNT(TEMP_AUTH_APP) FROM #VALIDATE_APP_VENDOR;


RETURN @appVendorValid;--returns 0 if app was not found, 1 if app was found
GO
/*VALIDATE IF APP VERSION IS BEHIND OR UP TO DATE*/
CREATE PROCEDURE [dbo].[USP_VALIDATE_VERSION_BEHIND_OR_UPTODATE]
	@afloatHullAppInstalledId int
AS
	DECLARE @hullServerId int;
	DECLARE @afloatVersion varchar(50);
	DECLARE @afloatAppName varchar(50);
	DECLARE @afloatAppVendorName varchar(50);
	DECLARE @hullServerAppVendorCurrentOrBehind int;
	DECLARE @authHullAppReleaseId int;
	DECLARE @validateVersionCurrent varchar(50);
	DECLARE @currentVersionReleaseDate datetime2;

	CREATE TABLE #AFLOAT_INFO
	(TEMP_AFLOAT_VENDOR varchar(50),TEMP_AFLOAT_APP varchar(50),TEMP_AFLOAT_VERSION varchar(50),TEMP_AFLOAT_HULL_SERVER_ID int,TEMP_HULL_APP_AFLOAT_ID int)

	INSERT INTO #AFLOAT_INFO(TEMP_AFLOAT_VENDOR,TEMP_AFLOAT_APP,TEMP_AFLOAT_VERSION,TEMP_AFLOAT_HULL_SERVER_ID,TEMP_HULL_APP_AFLOAT_ID)
	SELECT
	INSTALLED_AFLOAT_APP_VENDOR_INDEX.INSTALLED_AFLOAT_APP_VENDOR_NAME,
	AFLOAT_APP_INDEX.AFLOAT_APP_NAME,
	AFLOAT_APP_VERSION_INDEX.AFLOAT_VERSION,
	AFLOAT_HULL_APP_INSTALLED.HULL_SERVER_ID,
	AFLOAT_HULL_APP_INSTALLED.AFLOAT_HULL_APP_INSTALLED_ID
	FROM AFLOAT_HULL_APP_INSTALLED
	LEFT JOIN HULL_SERVER_INDEX
	ON AFLOAT_HULL_APP_INSTALLED.HULL_SERVER_ID = HULL_SERVER_INDEX.HULL_SERVER_ID
	LEFT JOIN AFLOAT_APP_VERSION_INDEX
	ON AFLOAT_HULL_APP_INSTALLED.AFLOAT_APP_VERSION_ID = AFLOAT_APP_VERSION_INDEX.AFLOAT_APP_VERSION_ID
	LEFT JOIN AFLOAT_APP_INDEX
	ON AFLOAT_APP_VERSION_INDEX.AFLOAT_APP_ID = AFLOAT_APP_INDEX.AFLOAT_APP_ID
	LEFT JOIN INSTALLED_AFLOAT_APP_VENDOR_INDEX
	ON AFLOAT_APP_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID = INSTALLED_AFLOAT_APP_VENDOR_INDEX.INSTALLED_AFLOAT_APP_VENDOR_ID
	WHERE  @afloatHullAppInstalledId = AFLOAT_HULL_APP_INSTALLED_ID;

	SELECT @hullServerId = TEMP_AFLOAT_HULL_SERVER_ID FROM #AFLOAT_INFO;
	SELECT @afloatVersion = TEMP_AFLOAT_VERSION FROM #AFLOAT_INFO;
	SELECT @afloatAppName = TEMP_AFLOAT_APP FROM #AFLOAT_INFO;
	SELECT @afloatAppVendorName = TEMP_AFLOAT_VENDOR FROM #AFLOAT_INFO;

			CREATE TABLE #HULL_SERVER_APP_VERSION_VENDOR_CURRENT_OR_BEHIND
			(TEMP_AUTH_VENDOR varchar(50),TEMP_AUTH_APP varchar(50),TEMP_AUTH_VERSION varchar(50),TEMP_AUTH_RELEASE_DATE datetime2,TEMP_AUTH_HULL_SERVER varchar(50),TEMP_AUTH_HULL_APP_RELEASE_ID int);
	
			INSERT INTO #HULL_SERVER_APP_VERSION_VENDOR_CURRENT_OR_BEHIND(TEMP_AUTH_VENDOR,TEMP_AUTH_APP,TEMP_AUTH_VERSION,TEMP_AUTH_RELEASE_DATE,TEMP_AUTH_HULL_SERVER,TEMP_AUTH_HULL_APP_RELEASE_ID)
			SELECT 
			AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_NAME,
			AUTH_APP_INDEX.AUTH_APP_NAME,
			AUTH_APP_RELEASE.AUTH_VERSION,
			AUTH_APP_RELEASE.AUTH_APP_RELEASE_DATE,
			AUTH_HULL_APP_RELEASE.HULL_SERVER_ID,
			AUTH_HULL_APP_RELEASE.AUTH_HULL_APP_RELEASE_ID
			FROM AUTH_HULL_APP_RELEASE
			LEFT JOIN AUTH_APP_RELEASE
			ON AUTH_HULL_APP_RELEASE.AUTH_APP_RELEASE_ID = AUTH_APP_RELEASE.AUTH_APP_RELEASE_ID
			LEFT JOIN AUTH_APP_INDEX
			ON AUTH_APP_RELEASE.AUTH_APP_ID = AUTH_APP_INDEX.AUTH_APP_ID
			LEFT JOIN AUTH_APP_VENDOR_INDEX
			ON AUTH_APP_INDEX.AUTH_APP_VENDOR_ID = AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_ID
			WHERE @hullServerId = AUTH_HULL_APP_RELEASE.HULL_SERVER_ID AND @afloatAppName = AUTH_APP_INDEX.AUTH_APP_NAME AND @afloatAppVendorName = AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_NAME;

			SELECT @currentVersionReleaseDate = MAX(TEMP_AUTH_RELEASE_DATE) FROM #HULL_SERVER_APP_VERSION_VENDOR_CURRENT_OR_BEHIND;
			SELECT @validateVersionCurrent = TEMP_AUTH_VERSION FROM #HULL_SERVER_APP_VERSION_VENDOR_CURRENT_OR_BEHIND WHERE TEMP_AUTH_RELEASE_DATE = @currentVersionReleaseDate;

			IF @afloatVersion = @validateVersionCurrent
			BEGIN
				SET @hullServerAppVendorCurrentOrBehind = 1;
			END
			ELSE
			BEGIN
				SET @hullServerAppVendorCurrentOrBehind = 0;
			END

RETURN @hullServerAppVendorCurrentOrBehind;
GO
/*STORED PROCEDURE FOR INDEXING AFLOAT INSTALLED PROGRAMS/APPS AND ADDING THEM TO
MONITORING OR UNAUTHORIRZED PROGRAMS LISTING*/
CREATE PROCEDURE [dbo].[USP_ADD_AFLOAT_HULL_APP_INSTALLED]
	--var list for ADD_HULL_SERVER_INDEX
	@hull varchar(16),--PASSED FOR USP CALL
	@hullName varchar(50),--PASSED FOR USP CALL
	@hullStatusName varchar(50),--PASSED FOR USP CALL
	@statusDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullTypeName varchar(50),--PASSED FOR USP CALL
	@hullTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@classTypeName varchar(50),--PASSED FOR USP CALL
	@classTypeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@returnServerCodeName varchar(50),--PASSED FOR USP CALL
	@returnServerCodeDisplayIndicatorName varchar(50),--PASS FOR USP CALL
	@hullDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@servername varchar(50),--PASSED FOR USP CALL
	@serverDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@hullServerStatusName varchar(50),--PASSED FOR USP CALL
	@hullServerStatusDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	--var list for ADD_AFLOAT_APP_VERSION
	@afloatVersion varchar(50),--PASSED FOR USP CALL
	@afloatAppName varchar(50),--PASSED FOR USP CALL
	@afloatAppDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	@afloatAppVendorName varchar(50),--PASSED FOR USP CALL
	@afloatAppVendorDisplayIndicatorName varchar(50),--PASSED FOR USP CALL
	--var for date program/app was installed
	@installedDate datetime2 --for insert
AS
	--var list for USP_ADD_COMPLIANCE_STATUS
	DECLARE @complianceStatusId int;
	DECLARE @complianceStatusName varchar(50);
	DECLARE @complianceStatusDisplayIndicatorId int;
	DECLARE @complianceStatusDisplayIndicatorName varchar(50);
	--var to collect HULL_APP_RELEASE_ID
	DECLARE @authHullAppReleaseId int;
	--var to collect HULL_SERVER_ID
	DECLARE @hullServerId int;
	--var to collect AFLOAT_HULL_APP_VERSION_ID
	DECLARE @afloatAppVersionId int;
	--var to collect AFLOAT_HULL_APP_INSTALLED_ID
	DECLARE @afloatHullAppInstalledId int;
	--vars for HULL_APP_VERSION_MONITORING inserts
	DECLARE @hullAppVersionMonitoringStatusId int;
	DECLARE @hullAppVersionMonitoringStatusName varchar(50);
	DECLARE @hullAppVersionMonitoringDisplayIndicatorName varchar(50);
	--vars for UNAUTHORIZED_PROGRAMS inserts
	DECLARE @unauthorizedProgramsDisplayIndicatorId int;
	DECLARE @unauthorizedProgramsDisplayIndicatorName varchar(50);
	--var for app+vendor validation
	DECLARE @appVendorValid int;
	--var for app+version+vendor validation
	DECLARE @appVersionVendorValid int;
	--var for hull+server+app+version+vendor validation
	DECLARE @hullServerAppVendorValid int;
	--var for hull+sever+app+version-currentorbehind+vendor validation
	DECLARE @hullServerAppVendorCurrentOrBehind int;
	--var to control inserts
	DECLARE @recordCount int;
	--var to see if authorized application used to be unauthorized
	DECLARE @previouslyUnauthorized int;

	--var inits
	SET @afloatHullAppInstalledId = 0;
	SET @authHullAppReleaseId = 0;
	SET @hullAppVersionMonitoringDisplayIndicatorName = 'INACTIVE';
	SET @unauthorizedProgramsDisplayIndicatorName = 'INACTIVE';

	-------------------START-----------------------
	-------AFLOAT_HULL_APP_INSTALLED INSERT--------
	-------------------START-----------------------

	--collect hullServerId, if HULL+SERVER does not exist create it
	EXEC @hullServerId = USP_ADD_HULL_SERVER_INDEX @hull,@hullName,@hullStatusName,@statusDisplayIndicatorName,@hullTypeName,@hullTypeDisplayIndicatorName,@classTypeName,@classTypeDisplayIndicatorName,@returnServerCodeName,@returnServerCodeDisplayIndicatorName,@hullDisplayIndicatorName,@servername,@serverDisplayIndicatorName,@hullServerStatusName,@hullServerStatusDisplayIndicatorName;

	--collect afloatHullAppVersionId, if APP+VERSION does not exist, create it
	EXEC @afloatAppVersionId = USP_ADD_AFLOAT_APP_VERSION @afloatVersion,@afloatAppName,@afloatAppDisplayIndicatorName,@afloatAppVendorName,@afloatAppVendorDisplayIndicatorName;

	--find afloatHullAppInstalledId if HULL+SERVER+APP+VERSION has already been installed
	SELECT @afloatHullAppInstalledId = AFLOAT_HULL_APP_INSTALLED_ID FROM AFLOAT_HULL_APP_INSTALLED
	WHERE AFLOAT_APP_VERSION_ID = @afloatAppVersionId AND HULL_SERVER_ID = @hullServerId;
	
	---if APP+VERSION is has not been released to the appropriated hull, release HULL+APP+VERSION and create an id
	IF @afloatHullAppInstalledId = 0
	BEGIN
		INSERT INTO AFLOAT_HULL_APP_INSTALLED (HULL_SERVER_ID,AFLOAT_APP_VERSION_ID,INSTALLED_DATE)
		VALUES (@hullServerId,@afloatAppVersionId,@installedDate);
		--set the inserted afloatHullAppInstalledId
		set @afloatHullAppInstalledId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE AFLOAT_HULL_APP_INSTALLED
		SET INSTALLED_DATE = @installedDate
		WHERE INSTALLED_DATE <> @installedDate AND AFLOAT_HULL_APP_INSTALLED_ID = @afloatHullAppInstalledId;
	END

	------------------END--------------------------
	-------AFLOAT_HULL_APP_INSTALLED INSERT--------
	------------------END--------------------------
	------------------START----------------------
	-------PROGRAM/APP COMPLIANCE SECTION--------
	------------------START----------------------

--VALIDATE IF APP+VENDOR RELEASED

--returns 1 for app+vendor released returns 0 for app+vendor not released
EXEC @appVendorValid = USP_VALIDATE_APP_VENDOR @afloatHullAppInstalledId;
IF @appVendorValid = 0--prepare information for insert to UNAUTHORIZED_PROGRAMS
	BEGIN
		--set compliance var's
		SET @complianceStatusName = 'UNAUTHORIZED PROGRAM OR APPLICATION';
		SET @complianceStatusDisplayIndicatorName = 'ACTIVE';
		SET @unauthorizedProgramsDisplayIndicatorName = 'ACTIVE';
		--get COMPLIANCE_STATUS_ID or create one
		EXEC @complianceStatusId = USP_ADD_COMPLIANCE_STATUS @complianceStatusName,@complianceStatusDisplayIndicatorName;
		--find displayIndicatorID insert if it does not exist
		EXEC @unauthorizedProgramsDisplayIndicatorId = USP_ADD_DISPLAY_INDICATOR @unauthorizedProgramsDisplayIndicatorName;--collect DISPLAY_INDICATOR_ID
	END
ELSE--test for app+version+vendor is released
	BEGIN
		EXEC @appVersionVendorValid = USP_VALIDATE_APP_VERSION_VENDOR @afloatHullAppInstalledId;
		IF @appVersionVendorValid = 0--prepare info for insert to UNAUTHORIZED_PROGRAMS
			BEGIN
				--set compliance var's
				SET @complianceStatusName = 'UNAUTHORIZED PROGRAM OR APPLICATION VERSION';
				SET @complianceStatusDisplayIndicatorName = 'ACTIVE';
				SET @unauthorizedProgramsDisplayIndicatorName = 'ACTIVE';
				--get COMPLIANCE_STATUS_ID or create one
				EXEC @complianceStatusId = USP_ADD_COMPLIANCE_STATUS @complianceStatusName,@complianceStatusDisplayIndicatorName;
				--find displayIndicatorID insert if it does not exist
				EXEC @unauthorizedProgramsDisplayIndicatorId = USP_ADD_DISPLAY_INDICATOR @unauthorizedProgramsDisplayIndicatorName;--collect DISPLAY_INDICATOR_ID
			END
		ELSE--test for hull+server+app+version+vendor
			BEGIN
				--if not valid returns 0, if valid returns AUTH_HULL_APP_RELEASE_ID
				EXEC @hullServerAppVendorValid = USP_VALIDATE_HULL_SERVER_APP_VERSION_VENDOR @afloatHullAppInstalledId;
				IF @hullServerAppVendorValid = 0--prepare info for insert to UNAUTHORIZED_PROGRAMS
					BEGIN
						--set compliance var's
						SET @complianceStatusName = 'NOT RELEASED TO HULL';
						SET @complianceStatusDisplayIndicatorName = 'ACTIVE';
						SET @unauthorizedProgramsDisplayIndicatorName = 'ACTIVE';
						--get COMPLIANCE_STATUS_ID or create one
						EXEC @complianceStatusId = USP_ADD_COMPLIANCE_STATUS @complianceStatusName,@complianceStatusDisplayIndicatorName;
						--find displayIndicatorID insert if it does not exist
						EXEC @unauthorizedProgramsDisplayIndicatorId = USP_ADD_DISPLAY_INDICATOR @unauthorizedProgramsDisplayIndicatorName;--collect DISPLAY_INDICATOR_ID
					END
				ELSE--test if version is current or behind
					BEGIN
						SET @authHullAppReleaseId = @hullServerAppVendorValid;
						--if version current returns 1, if not current returns 0
						EXEC @hullServerAppVendorCurrentOrBehind = USP_VALIDATE_VERSION_BEHIND_OR_UPTODATE @afloatHullAppInstalledId;
						IF @hullServerAppVendorCurrentOrBehind = 0--version behind
							BEGIN
								SET @hullAppVersionMonitoringStatusName = 'YELLOW';
								SET @hullAppVersionMonitoringDisplayIndicatorName = 'ACTIVE';
								--find or create statusId
								EXEC @hullAppVersionMonitoringStatusId = USP_ADD_STATUS @hullAppVersionMonitoringStatusName,@hullAppVersionMonitoringDisplayIndicatorName;
							END
						ELSE--version current
							BEGIN
								SET @hullAppVersionMonitoringStatusName = 'GREEN';
								SET @hullAppVersionMonitoringDisplayIndicatorName = 'ACTIVE';
								--find or create statusId
								EXEC @hullAppVersionMonitoringStatusId = USP_ADD_STATUS @hullAppVersionMonitoringStatusName,@hullAppVersionMonitoringDisplayIndicatorName;
							END
						
						SELECT @previouslyUnauthorized = COUNT(AFLOAT_HULL_APP_INSTALLED_ID) FROM UNAUTHORIZED_PROGRAMS WHERE AFLOAT_HULL_APP_INSTALLED_ID = @afloatHullAppInstalledId;
						
						IF @previouslyUnauthorized <> 0
							BEGIN

								--set compliance var's
								SET @complianceStatusName = 'RESOLVED';
								SET @complianceStatusDisplayIndicatorName = 'ACTIVE';
								SET @unauthorizedProgramsDisplayIndicatorName = 'ACTIVE';
								--get COMPLIANCE_STATUS_ID or create one
								EXEC @complianceStatusId = USP_ADD_COMPLIANCE_STATUS @complianceStatusName,@complianceStatusDisplayIndicatorName;
								--find displayIndicatorID insert if it does not exist
								EXEC @unauthorizedProgramsDisplayIndicatorId = USP_ADD_DISPLAY_INDICATOR @unauthorizedProgramsDisplayIndicatorName;--collect DISPLAY_INDICATOR_ID

								UPDATE UNAUTHORIZED_PROGRAMS
								SET COMPLIANCE_STATUS_ID = @complianceStatusId
								WHERE COMPLIANCE_STATUS_ID <> @complianceStatusId AND AFLOAT_HULL_APP_INSTALLED_ID = @afloatHullAppInstalledId;
								UPDATE UNAUTHORIZED_PROGRAMS
								SET DISPLAY_INDICATOR_ID = @unauthorizedProgramsDisplayIndicatorId
								WHERE DISPLAY_INDICATOR_ID <> @unauthorizedProgramsDisplayIndicatorId AND AFLOAT_HULL_APP_INSTALLED_ID = @afloatHullAppInstalledId;
							END


					END
			END
	END

--INSERT into HULL_APP_VERSION_MONITORING TABLE if app is authorized

		IF @hullAppVersionMonitoringDisplayIndicatorName = 'ACTIVE'
		BEGIN--insert relevant data into app version monitoring table

			SELECT @recordCount = COUNT(AFLOAT_HULL_APP_INSTALLED_ID) FROM HULL_APP_VERSION_MONITORING WHERE AFLOAT_HULL_APP_INSTALLED_ID = @afloatHullAppInstalledId;

			IF @recordCount = 0
			BEGIN
				INSERT INTO HULL_APP_VERSION_MONITORING(HULL_SERVER_ID,AUTH_HULL_APP_RELEASE_ID,AFLOAT_HULL_APP_INSTALLED_ID,STATUS_ID)
				VALUES(@hullServerId,@authHullAppReleaseId,@afloatHullAppInstalledId,@hullAppVersionMonitoringStatusId)
			END
			ELSE
			BEGIN
				UPDATE HULL_APP_VERSION_MONITORING
				SET STATUS_ID = @hullAppVersionMonitoringStatusId
				WHERE STATUS_ID <> @hullAppVersionMonitoringStatusId AND AFLOAT_HULL_APP_INSTALLED_ID = @afloatHullAppInstalledId;
			END
		END
--INSERT into UNAUTHORIZED_PROGRAMS TABLE if app/version/hull is not authorized
		IF @complianceStatusDisplayIndicatorName = 'ACTIVE'
		BEGIN--insert relevant data into unauthorized programs table
		
			SELECT @recordCount = COUNT(AFLOAT_HULL_APP_INSTALLED_ID) FROM UNAUTHORIZED_PROGRAMS WHERE AFLOAT_HULL_APP_INSTALLED_ID = @afloatHullAppInstalledId;

			IF @recordCount = 0
			BEGIN
				INSERT INTO UNAUTHORIZED_PROGRAMS (AFLOAT_HULL_APP_INSTALLED_ID,COMPLIANCE_STATUS_ID,DISPLAY_INDICATOR_ID)
				VALUES(@afloatHullAppInstalledId,@complianceStatusId,@unauthorizedProgramsDisplayIndicatorId)
			END
			ELSE
			BEGIN
				UPDATE UNAUTHORIZED_PROGRAMS
				SET COMPLIANCE_STATUS_ID = @complianceStatusId
				WHERE COMPLIANCE_STATUS_ID <> @complianceStatusId AND AFLOAT_HULL_APP_INSTALLED_ID = @afloatHullAppInstalledId;
				UPDATE UNAUTHORIZED_PROGRAMS
				SET DISPLAY_INDICATOR_ID = @unauthorizedProgramsDisplayIndicatorId
				WHERE DISPLAY_INDICATOR_ID <> @unauthorizedProgramsDisplayIndicatorId AND AFLOAT_HULL_APP_INSTALLED_ID = @afloatHullAppInstalledId;
			END
		END

RETURN @afloatHullAppInstalledId--RETURNS AFLOAT_HULL_APP_INSTALLED_ID
GO

