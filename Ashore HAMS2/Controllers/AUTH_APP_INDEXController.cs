﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;
using System.Collections;

namespace Ashore_HAMS.Controllers
{

    public class AuthAppReleaseCompletedModel
    {
        public int AUTH_APP_RELEASE_ID { get; set; }
        public string AUTH_APP_VENDOR_NAME { get; set; }
        public string AUTH_APP_NAME { get; set; }
        public string AUTH_VERSION { get; set; }
        public string VENDOR_APP_VERSION { get; set; }
        public float PercentCompleted { get; set; }
        public float Total { get; set; }
        public int BarColorCode { get; set; }
    }

    public class AUTH_APP_INDEXController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        delegate int SelectColor(float percentCompleted);

        SelectColor selectColor = delegate (float percentCompleted)
        {
            int barcode = 5;
            if (percentCompleted <= 50)
            {
                barcode = 1;
            }
            else if (percentCompleted > 50 && percentCompleted <= 80)
            {
                barcode = 2;
            }
            return barcode;
        };


        public int RunningTotal
        {
            get
            {
                var sum = (
                    from a in
                                (
                                    from a in db.AUTH_HULL_APP_RELEASE
                                    group a by a.AUTH_APP_RELEASE_ID into grpAPRID
                                    select new { grpAPRID.Key, Count = grpAPRID.Count() }
                                )
                    select a.Count).Sum();
                return sum;
            }
        }

        //public float GetAuthAppSummary()
        //{
        //    float result = 0.0f;
        //    int runningTotal = RunningTotal;
        //    var ret = from a in db.AUTH_HULL_APP_RELEASE
        //              group a by a.AUTH_APP_RELEASE_ID into grpAPRID
        //              select new { grpAPRID.Key, Count = grpAPRID.Count() };
        //    return result + runningTotal;
        //}

        public float GetAuthAppSummary()
        {
            float result = 0.0f;
            var authAppSummary =
                from a in db.AuthorizedInstalledViews
                join b in
                (
                    from x in
                    (
                        from a in db.AUTH_APP_RELEASE
                        join b in db.AUTH_APP_INDEX
                        on a.AUTH_APP_ID equals b.AUTH_APP_ID
                        join c in db.AUTH_APP_VENDOR_INDEX
                        on b.AUTH_APP_VENDOR_ID equals c.AUTH_APP_VENDOR_ID
                        join d in db.AUTH_HULL_APP_RELEASE
                        on a.AUTH_APP_RELEASE_ID equals d.AUTH_APP_RELEASE_ID
                        select new { VendorAppName = c.AUTH_APP_VENDOR_NAME + "_" + b.AUTH_APP_NAME + "_" + a.AUTH_VERSION, a.AUTH_APP_RELEASE_ID }
                    )
                    group x by x.VendorAppName into GrpByVendorAppName
                    select new { GrpByVendorAppName.Key, Count = GrpByVendorAppName.Count() }
                )
                on a.VendorAppVersion equals b.Key
                select new { b.Key, a.AuthorizedInstalledCount, b.Count };


            float aic = (float)authAppSummary.Sum(x => x.AuthorizedInstalledCount);
            float grandTotal = (float)authAppSummary.Sum(x => x.Count);
            result = (aic / grandTotal) * 100;

            return result;
        }


        public IEnumerable<AuthorizedInstalledView> AuthorizedInstalledViews
        {
            get
            {
                IEnumerable < AuthorizedInstalledView > ret = db.AuthorizedInstalledViews;
                return ret;
            }
        }


        public IEnumerable<AuthAppReleaseCompletedModel> AuthorizedReleaseCount
        {
            get
            {
                var ret =
                (from a in
                            (from a in db.AUTH_APP_INDEX
                             join b in db.AUTH_APP_RELEASE on a.AUTH_APP_ID equals b.AUTH_APP_ID
                             select new { b.AUTH_APP_RELEASE_ID,  a.AUTH_APP_VENDOR_INDEX.AUTH_APP_VENDOR_NAME, a.AUTH_APP_NAME, b.AUTH_VERSION })
                join b in
                            (from a in db.AUTH_APP_INDEX
                             join b in db.AUTH_APP_RELEASE on a.AUTH_APP_ID equals b.AUTH_APP_ID
                             join c in db.AUTH_HULL_APP_RELEASE on b.AUTH_APP_RELEASE_ID equals c.AUTH_APP_RELEASE_ID
                             group b by b.AUTH_APP_RELEASE_ID into grpAPRID
                             select new { AUTH_APP_RELEASE_ID = grpAPRID.Key, Count = grpAPRID.Count() })
                on a.AUTH_APP_RELEASE_ID equals b.AUTH_APP_RELEASE_ID

                let Total = db.HULL_SERVER_VIEW.Count()
                
                select new AuthAppReleaseCompletedModel
                {
                    AUTH_APP_RELEASE_ID = a.AUTH_APP_RELEASE_ID,
                    AUTH_APP_VENDOR_NAME = a.AUTH_APP_VENDOR_NAME,
                    AUTH_APP_NAME = a.AUTH_APP_NAME,
                    AUTH_VERSION = a.AUTH_VERSION,
                    Total = Total,
                    VENDOR_APP_VERSION = a.AUTH_APP_VENDOR_NAME + "_" + a.AUTH_APP_NAME + "_" + a.AUTH_VERSION,
                    PercentCompleted = ((float)b.Count / (float)Total) * 100,
                }).ToList<AuthAppReleaseCompletedModel>();

                if(ret != null)
                {
                    ret.ForEach(item => item.BarColorCode = selectColor(item.PercentCompleted));
                }

                return ret;
            }
        }


        // GET: AUTH_APP_INDEX
        public async Task<ActionResult> Index()
        {
            Dictionary<int, AuthAppReleaseCompletedModel> dict = new Dictionary<int, AuthAppReleaseCompletedModel>();
            foreach(AuthAppReleaseCompletedModel item in AuthorizedReleaseCount)
            {
                dict.Add(item.AUTH_APP_RELEASE_ID, item);
            }

            ViewBag.AuthAppReleaseCount = dict;

            foreach(AuthorizedInstalledView item in AuthorizedInstalledViews)
            {
                ViewData[item.VendorAppVersion] = item.AuthorizedInstalledCount;
            }

            var aUTH_APP_INDEX = db.AUTH_APP_INDEX.Include(a => a.AUTH_APP_VENDOR_INDEX).Include(a => a.DISPLAY_INDICATOR_INDEX);

            ViewBag.AuthAppSummary = GetAuthAppSummary();

            return View(await aUTH_APP_INDEX.ToListAsync());
        }

        // GET: AUTH_APP_INDEX/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_APP_INDEX aUTH_APP_INDEX = await db.AUTH_APP_INDEX.FindAsync(id);
            if (aUTH_APP_INDEX == null)
            {
                return HttpNotFound();
            }

            ViewBag.DX = System.DateTime.Now;

            return View(aUTH_APP_INDEX);
        }

        // GET: AUTH_APP_INDEX/Create
        public ActionResult Create(int? id)
        {
            ViewBag.AUTH_APP_VENDOR_ID = new SelectList(db.AUTH_APP_VENDOR_INDEX, "AUTH_APP_VENDOR_ID", "AUTH_APP_VENDOR_NAME", id);
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", 3);
            return View();
        }

        // POST: AUTH_APP_INDEX/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AUTH_APP_ID,AUTH_APP_NAME,AUTH_APP_VENDOR_ID,DISPLAY_INDICATOR_ID")] AUTH_APP_INDEX aUTH_APP_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.AUTH_APP_INDEX.Add(aUTH_APP_INDEX);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AUTH_APP_VENDOR_ID = new SelectList(db.AUTH_APP_VENDOR_INDEX, "AUTH_APP_VENDOR_ID", "AUTH_APP_VENDOR_NAME", aUTH_APP_INDEX.AUTH_APP_VENDOR_ID);
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", aUTH_APP_INDEX.DISPLAY_INDICATOR_ID);
            return View(aUTH_APP_INDEX);
        }

        // GET: AUTH_APP_INDEX/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_APP_INDEX aUTH_APP_INDEX = await db.AUTH_APP_INDEX.FindAsync(id);
            if (aUTH_APP_INDEX == null)
            {
                return HttpNotFound();
            }
            ViewBag.AUTH_APP_VENDOR_ID = new SelectList(db.AUTH_APP_VENDOR_INDEX, "AUTH_APP_VENDOR_ID", "AUTH_APP_VENDOR_NAME", aUTH_APP_INDEX.AUTH_APP_VENDOR_ID);
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", aUTH_APP_INDEX.DISPLAY_INDICATOR_ID);
            return View(aUTH_APP_INDEX);
        }

        // POST: AUTH_APP_INDEX/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AUTH_APP_ID,AUTH_APP_NAME,AUTH_APP_VENDOR_ID,DISPLAY_INDICATOR_ID")] AUTH_APP_INDEX aUTH_APP_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aUTH_APP_INDEX).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AUTH_APP_VENDOR_ID = new SelectList(db.AUTH_APP_VENDOR_INDEX, "AUTH_APP_VENDOR_ID", "AUTH_APP_VENDOR_NAME", aUTH_APP_INDEX.AUTH_APP_VENDOR_ID);
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", aUTH_APP_INDEX.DISPLAY_INDICATOR_ID);
            return View(aUTH_APP_INDEX);
        }

        // GET: AUTH_APP_INDEX/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_APP_INDEX aUTH_APP_INDEX = await db.AUTH_APP_INDEX.FindAsync(id);
            if (aUTH_APP_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(aUTH_APP_INDEX);
        }

        public async Task<ActionResult> Report(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_APP_INDEX aUTH_APP_INDEX = await db.AUTH_APP_INDEX.FindAsync(id);
            if (aUTH_APP_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(aUTH_APP_INDEX);
        }

        // POST: AUTH_APP_INDEX/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AUTH_APP_INDEX aUTH_APP_INDEX = await db.AUTH_APP_INDEX.FindAsync(id);
            db.AUTH_APP_INDEX.Remove(aUTH_APP_INDEX);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
