﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;
using Ashore_HAMS.ModelView;

namespace Ashore_HAMS.Controllers
{
    public class AUTH_HULL_APP_RELEASEController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: AUTH_HULL_APP_RELEASE
        public async Task<ActionResult> Index(int? id)
        {
            var aUTH_HULL_APP_RELEASE = db.AUTH_HULL_APP_RELEASE.Include(a => a.AUTH_APP_RELEASE) //.Include(a => a.HULL_SERVER_INDEX)
                .Where(a => a.HULL_SERVER_ID == id);

            //IList< AUTH_HULL_APP_RELEASE> result = (from ahar in db.AUTH_HULL_APP_RELEASE
            //             //join hsi in db.HULL_SERVER_INDEX.Include(h => h.SERVER_INDEX) on ahar.HULL_SERVER_ID equals hsi.HULL_SERVER_ID
            //             where ahar.HULL_SERVER_ID == id
            //             select ahar).ToList<AUTH_HULL_APP_RELEASE>();

            //AUTH_HULL_APP_RELEASE a = new AUTH_HULL_APP_RELEASE();
            //if(result.Count > 0)
            //{
            //    ViewBag.SERVER_NAME = result[0].
            //}
            //ViewBag.SERVER_NAME = aUTH_HULL_APP_RELEASE.SERVER_INDEX.SERVER_NAME;

            if(id==null)
            {
                return RedirectToAction("../");
            }
            else
            {
                ViewBag.ID = id;
                return View(await aUTH_HULL_APP_RELEASE.ToListAsync());
            }
            //return View(result);
        }

        // GET: AUTH_HULL_APP_RELEASE/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_HULL_APP_RELEASE aUTH_HULL_APP_RELEASE = await db.AUTH_HULL_APP_RELEASE.FindAsync(id);
            if (aUTH_HULL_APP_RELEASE == null)
            {
                return HttpNotFound();
            }
            return View(aUTH_HULL_APP_RELEASE);
        }

        // GET: AUTH_HULL_APP_RELEASE/Create
        public ActionResult Create(int? id)
        {
            var auth_app_release = from a in db.AUTH_APP_RELEASE
                                   select new { a.AUTH_APP_RELEASE_ID, a.AUTH_APP_ID, APP = a.AUTH_APP_INDEX.AUTH_APP_NAME + " " + a.AUTH_VERSION };

            //ViewBag.AUTH_APP_RELEASE_ID = new SelectList(auth_app_release, "AUTH_APP_RELEASE_ID", "AUTH_VERSION");
            ViewBag.AUTH_APP_RELEASE_ID = new SelectList(auth_app_release, "AUTH_APP_RELEASE_ID", "APP");

            ViewBag.HULL_SERVER_ID = id; // new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID");
            var authHullAppRelease = new AUTH_HULL_APP_RELEASE();
            authHullAppRelease.HULL_SERVER_ID = (Int32)id;
            return View(authHullAppRelease);
        }

        // POST: AUTH_HULL_APP_RELEASE/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AUTH_HULL_APP_RELEASE_ID,HULL_SERVER_ID,AUTH_APP_RELEASE_ID,HULL_APP_RELEASE_DATE,TIME_DATE_RECORD")] AUTH_HULL_APP_RELEASE aUTH_HULL_APP_RELEASE)
        {
            if (ModelState.IsValid)
            {
                db.AUTH_HULL_APP_RELEASE.Add(aUTH_HULL_APP_RELEASE);
                await db.SaveChangesAsync();
                return RedirectToAction(string.Format("Index/{0}", aUTH_HULL_APP_RELEASE.HULL_SERVER_ID));
            }

            //ViewBag.AUTH_APP_RELEASE_ID = new SelectList(db.AUTH_APP_RELEASE, "AUTH_APP_RELEASE_ID", "AUTH_VERSION", aUTH_HULL_APP_RELEASE.AUTH_APP_RELEASE_ID);

            var auth_app_release = from a in db.AUTH_APP_RELEASE
                                   select new { a.AUTH_APP_RELEASE_ID, a.AUTH_APP_ID, APP = a.AUTH_APP_INDEX.AUTH_APP_NAME + " " + a.AUTH_VERSION };

            ViewBag.AUTH_APP_RELEASE_ID = new SelectList(auth_app_release, "AUTH_APP_RELEASE_ID", "APP", aUTH_HULL_APP_RELEASE.AUTH_APP_RELEASE_ID);


            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", aUTH_HULL_APP_RELEASE.HULL_SERVER_ID);
            return View(aUTH_HULL_APP_RELEASE);
        }

        // GET: AUTH_HULL_APP_RELEASE/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_HULL_APP_RELEASE aUTH_HULL_APP_RELEASE = await db.AUTH_HULL_APP_RELEASE.FindAsync(id);
            if (aUTH_HULL_APP_RELEASE == null)
            {
                return HttpNotFound();
            }

            //ViewBag.AUTH_APP_RELEASE_ID = new SelectList(db.AUTH_APP_RELEASE, "AUTH_APP_RELEASE_ID", "AUTH_VERSION", aUTH_HULL_APP_RELEASE.AUTH_APP_RELEASE_ID);
            var auth_app_release = from a in db.AUTH_APP_RELEASE
                                   select new { a.AUTH_APP_RELEASE_ID, a.AUTH_APP_ID, APP = a.AUTH_APP_INDEX.AUTH_APP_NAME + " " + a.AUTH_VERSION };

            ViewBag.AUTH_APP_RELEASE_ID = new SelectList(auth_app_release, "AUTH_APP_RELEASE_ID", "APP", aUTH_HULL_APP_RELEASE.AUTH_APP_RELEASE_ID);

            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", aUTH_HULL_APP_RELEASE.HULL_SERVER_ID);
            return View(aUTH_HULL_APP_RELEASE);
        }

        // POST: AUTH_HULL_APP_RELEASE/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AUTH_HULL_APP_RELEASE_ID,HULL_SERVER_ID,AUTH_APP_RELEASE_ID,HULL_APP_RELEASE_DATE,TIME_DATE_RECORD")] AUTH_HULL_APP_RELEASE aUTH_HULL_APP_RELEASE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aUTH_HULL_APP_RELEASE).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction(string.Format("Index/{0}", aUTH_HULL_APP_RELEASE.HULL_SERVER_ID));
            }

            ViewBag.AUTH_APP_RELEASE_ID = new SelectList(db.AUTH_APP_RELEASE, "AUTH_APP_RELEASE_ID", "AUTH_VERSION", aUTH_HULL_APP_RELEASE.AUTH_APP_RELEASE_ID);
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", aUTH_HULL_APP_RELEASE.HULL_SERVER_ID);
            return View(aUTH_HULL_APP_RELEASE);
        }

        // GET: AUTH_HULL_APP_RELEASE/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_HULL_APP_RELEASE aUTH_HULL_APP_RELEASE = await db.AUTH_HULL_APP_RELEASE.FindAsync(id);
            if (aUTH_HULL_APP_RELEASE == null)
            {
                return HttpNotFound();
            }
            return View(aUTH_HULL_APP_RELEASE);
        }

        // POST: AUTH_HULL_APP_RELEASE/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AUTH_HULL_APP_RELEASE aUTH_HULL_APP_RELEASE = await db.AUTH_HULL_APP_RELEASE.FindAsync(id);
            db.AUTH_HULL_APP_RELEASE.Remove(aUTH_HULL_APP_RELEASE);
            await db.SaveChangesAsync();
            return RedirectToAction(string.Format("Index/{0}", aUTH_HULL_APP_RELEASE.HULL_SERVER_ID));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
