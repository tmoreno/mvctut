﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers.API
{
    public class R_HULL_INDEX2Controller : ApiController
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: api/R_HULL_INDEX2
        public IQueryable<HULL_INDEX> GetHULL_INDEX()
        {
            var n = from d in db.HULL_INDEX select d;
            return n;
        }

        // GET: api/R_HULL_INDEX2/5
        [ResponseType(typeof(HULL_INDEX))]
        public async Task<IHttpActionResult> GetHULL_INDEX(int id)
        {
            HULL_INDEX hULL_INDEX = await db.HULL_INDEX.FindAsync(id);
            if (hULL_INDEX == null)
            {
                return NotFound();
            }

            return Ok(hULL_INDEX);
        }

        // PUT: api/R_HULL_INDEX2/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutHULL_INDEX(int id, HULL_INDEX hULL_INDEX)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hULL_INDEX.HULL_ID)
            {
                return BadRequest();
            }

            db.Entry(hULL_INDEX).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HULL_INDEXExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/R_HULL_INDEX2
        [ResponseType(typeof(HULL_INDEX))]
        public async Task<IHttpActionResult> PostHULL_INDEX(HULL_INDEX hULL_INDEX)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.HULL_INDEX.Add(hULL_INDEX);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = hULL_INDEX.HULL_ID }, hULL_INDEX);
        }

        // DELETE: api/R_HULL_INDEX2/5
        [ResponseType(typeof(HULL_INDEX))]
        public async Task<IHttpActionResult> DeleteHULL_INDEX(int id)
        {
            HULL_INDEX hULL_INDEX = await db.HULL_INDEX.FindAsync(id);
            if (hULL_INDEX == null)
            {
                return NotFound();
            }

            db.HULL_INDEX.Remove(hULL_INDEX);
            await db.SaveChangesAsync();

            return Ok(hULL_INDEX);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HULL_INDEXExists(int id)
        {
            return db.HULL_INDEX.Count(e => e.HULL_ID == id) > 0;
        }
    }
}