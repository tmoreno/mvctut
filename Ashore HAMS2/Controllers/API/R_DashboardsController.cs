﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers.API
{
    public class R_DashboardsController : ApiController
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        delegate string SelectModule(int moduleId);

        SelectModule selectModule = delegate (int moduleId)
        {
            string retval = "Replication Status";
            switch(moduleId)
            {
                case 2:
                    retval = "System Communication Health";
                    break;
                case 3:
                    retval = "Software Configuration Compliance";
                    break;
                case 4:
                    retval = "Latest NIAPS Version";
                    break;
                case 5:
                    retval = "Active Users";
                    break;
                case 6:
                    retval = "Current Survey Tally";
                    break;
            }
            return retval;
        };

        delegate string SelectNormalMessage(int moduleId);

        SelectModule selectNormalMessage = delegate (int moduleId)
        {
            string retval = "Up2Date";
            switch (moduleId)
            {
                case 2:
                    retval = "Operational";
                    break;
                case 3:
                    retval = "Compliant";
                    break;
            }
            return retval;
        };

        // GET: api/R_Dashboards
        //public IQueryable<Dashboard> GetDashboards()
        //{
        //    db.Configuration.ProxyCreationEnabled = false;
        //    return db.Dashboards;
        //}

        public HttpResponseMessage Get()
        {
            db.Configuration.ProxyCreationEnabled = false;

            var dashboardView = (from d in db.Dashboards.ToList<Dashboard>()
                     select new
                     {
                         Id = d.Id,
                         normal = d.normal,
                         warning = d.warning,
                         critical = d.critical,
                         total = d.total,
                         module = selectModule((int)d.module_id),
                         normal_message = selectNormalMessage((int)d.module_id)
                     });

            return this.Request.CreateResponse(
                HttpStatusCode.OK,
                dashboardView);
        }

        // GET: api/R_Dashboards/5
        [ResponseType(typeof(Dashboard))]
        public async Task<IHttpActionResult> GetDashboard(int id)
        {
            Dashboard dashboard = await db.Dashboards.FindAsync(id);
            if (dashboard == null)
            {
                return NotFound();
            }

            return Ok(dashboard);
        }

        // PUT: api/R_Dashboards/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDashboard(int id, Dashboard dashboard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dashboard.Id)
            {
                return BadRequest();
            }

            db.Entry(dashboard).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DashboardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/R_Dashboards
        [ResponseType(typeof(Dashboard))]
        public async Task<IHttpActionResult> PostDashboard(Dashboard dashboard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Dashboards.Add(dashboard);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DashboardExists(dashboard.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = dashboard.Id }, dashboard);
        }

        // DELETE: api/R_Dashboards/5
        [ResponseType(typeof(Dashboard))]
        public async Task<IHttpActionResult> DeleteDashboard(int id)
        {
            Dashboard dashboard = await db.Dashboards.FindAsync(id);
            if (dashboard == null)
            {
                return NotFound();
            }

            db.Dashboards.Remove(dashboard);
            await db.SaveChangesAsync();

            return Ok(dashboard);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DashboardExists(int id)
        {
            return db.Dashboards.Count(e => e.Id == id) > 0;
        }
    }
}