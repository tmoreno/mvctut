﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers.API
{
    public class App2ShipController : ApiController
    {

        private HAMS9Entities1 db = new HAMS9Entities1();

        [Route("api/App2Ship/Get")]
        public HttpResponseMessage Get(string key)
        {
            
            var result = from a in (
                from a in db.AFLOAT_APP_INDEX
                join b in db.INSTALLED_AFLOAT_APP_VENDOR_INDEX
                on a.INSTALLED_AFLOAT_APP_VENDOR_ID equals b.INSTALLED_AFLOAT_APP_VENDOR_ID
                join c in db.AFLOAT_APP_VERSION_INDEX
                on a.AFLOAT_APP_ID equals c.AFLOAT_APP_ID
                join d in db.AFLOAT_HULL_APP_INSTALLED
                on c.AFLOAT_APP_VERSION_ID equals d.AFLOAT_APP_VERSION_ID
                select new { d.HULL_SERVER_ID, d.HULL_SERVER_INDEX.HULL_INDEX.HULL, d.HULL_SERVER_INDEX.HULL_INDEX.HULL_NAME, d.HULL_SERVER_INDEX.SERVER_INDEX.SERVERNAME, VendorAppName = b.INSTALLED_AFLOAT_APP_VENDOR_NAME + "_" + a.AFLOAT_APP_NAME + "_" + c.AFLOAT_VERSION }
            )
            where a.VendorAppName == key
            select a;

            return this.Request.CreateResponse(
                HttpStatusCode.OK,
                result);
        }

        public class J
        {
            public string label { get; set; }
            public int value { get; set; }
        }

        public HttpResponseMessage Get()
        {
            var result = from a in db.AuthorizedInstalledViews
                         select new J { label = a.VendorAppVersion, value = (int)a.AuthorizedInstalledCount };

            var y =
                from a in db.UNAUTHORIZED_PROGRAMS
                where !(a.COMPLIANCE_STATUS_INDEX.COMPLIANCE_STATUS_NAME.Contains("NOTIFICATION SENT"))
                group a by new
                {
                    a.AFLOAT_HULL_APP_INSTALLED.AFLOAT_APP_VERSION_INDEX.AFLOAT_APP_INDEX.AFLOAT_APP_NAME,
                    a.AFLOAT_HULL_APP_INSTALLED.AFLOAT_APP_VERSION_INDEX.AFLOAT_APP_INDEX.INSTALLED_AFLOAT_APP_VENDOR_INDEX.INSTALLED_AFLOAT_APP_VENDOR_NAME,
                    a.AFLOAT_HULL_APP_INSTALLED.AFLOAT_APP_VERSION_INDEX.AFLOAT_VERSION,
                } into grp
                select new J
                {
                    label = "[U]:" + grp.Key.INSTALLED_AFLOAT_APP_VENDOR_NAME + "_" + grp.Key.AFLOAT_APP_NAME + "_" + grp.Key.AFLOAT_VERSION,
                    value = grp.Count()
                };

            return this.Request.CreateResponse(
                HttpStatusCode.OK,
                result.Union(y).OrderBy(x => x.label));
        }

    }
}
