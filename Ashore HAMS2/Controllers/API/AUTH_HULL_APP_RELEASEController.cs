﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ashore_HAMS.Models3;
using System.Web;
using System.Web.Hosting;
using System.Threading;
using System.Data.SqlClient;
using System.Collections;

namespace Ashore_HAMS.Controllers.API
{
    public class AUTH_HULL_APP_RELEASEController : ApiController
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        public HttpResponseMessage Get()
        {

            var authHullAppRelease = from a in db.AUTH_HULL_APP_RELEASE
                    select new {
                                    AUTH_HULL_APP_RELEASE_ID = a.AUTH_APP_RELEASE_ID,
                                    HULL_SERVER_ID = a.HULL_SERVER_ID,
                                    AUTH_APP_RELEASE_ID = a.AUTH_APP_RELEASE_ID,
                                    HULL_APP_RELEASE_DATE = a.HULL_APP_RELEASE_DATE,
                                    TIME_DATE_RECORD = a.TIME_DATE_RECORD
                                };

            return this.Request.CreateResponse(
                HttpStatusCode.OK,
                authHullAppRelease);
        }

        // GET: api/AUTH_HULL_APP_RELEASE/5
        //[ResponseType(typeof(AUTH_HULL_APP_RELEASE))]
        public HttpResponseMessage Get(string id)
        {
            var progressReport = (ProgressReport)HttpRuntime.Cache[id];
            if(progressReport == null)
            {
                progressReport = new ProgressReport
                {
                    Total = -1,
                    Completed = -1
                };
            }

            return this.Request.CreateResponse(
                HttpStatusCode.OK,
                progressReport);
        }


        public class PostedClass
        {
            public int HULL_SERVER_ID { get; set; }
            public int AUTH_APP_RELEASE_ID { get; set; }
        }

        public class ProgressReport
        {
            public int Total { get; set; }
            public int Completed { get; set; }
        }

        private void LongRunningTask(HAMS9Entities1 db1, IEnumerable<PostedClass> ndata)
        {
            int completed = 0;
            var progressReport = new ProgressReport
            {
                Total = ndata.Count(),
                Completed = completed
            };

            var date = System.DateTime.Now;


            foreach (PostedClass pc in ndata)
            {
                SqlParameter hullServerId = new SqlParameter("@hullServerId", pc.HULL_SERVER_ID);
                SqlParameter authAppReleaseId = new SqlParameter("@authAppReleaseId", pc.AUTH_APP_RELEASE_ID);
                SqlParameter hullAppReleaseDate = new SqlParameter("@hullAppReleaseDate", date);

                db1.Database.ExecuteSqlCommand("[dbo].USP_ADD_AUTH_HULL_APP_RELEASE3 @hullServerId, @authAppReleaseId, @hullAppReleaseDate",
                    hullServerId,
                    authAppReleaseId,
                    hullAppReleaseDate);

                progressReport.Completed++;

                HttpRuntime.Cache["job"] = progressReport;

                Thread.Sleep(500);
            }

            db1.Dispose();
        }
        
        public IHttpActionResult Post(PostedClass pc)
        {
            var ndata = (from h in db.HULL_SERVER_INDEX
                        select new PostedClass { HULL_SERVER_ID = h.HULL_SERVER_ID, AUTH_APP_RELEASE_ID = pc.AUTH_APP_RELEASE_ID }).ToList<PostedClass>();
            HostingEnvironment.QueueBackgroundWorkItem(ct => LongRunningTask(db, ndata));
            return Ok(new ProgressReport{ Total = ndata.Count(), Completed = 0 });
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AUTH_HULL_APP_RELEASEExists(int id)
        {
            return db.AUTH_HULL_APP_RELEASE.Count(e => e.AUTH_HULL_APP_RELEASE_ID == id) > 0;
        }
    }
}