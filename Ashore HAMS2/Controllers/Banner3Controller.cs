﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hams3xLib;

namespace Ashore_HAMS.Controllers
{
    public class Banner3Controller : Controller
    {
        private HAMS3XEntities db = new HAMS3XEntities();

        // GET: Banner3
        public async Task<ActionResult> Index()
        {
            return View(await db.UserIndexes.ToListAsync());
        }

        // GET: Banner3/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserIndex userIndex = await db.UserIndexes.FindAsync(id);
            if (userIndex == null)
            {
                return HttpNotFound();
            }
            return View(userIndex);
        }

        // GET: Banner3/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Banner3/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "UserIndexId,Cac,FirstName,LastName")] UserIndex userIndex)
        {
            if (ModelState.IsValid)
            {
                db.UserIndexes.Add(userIndex);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(userIndex);
        }

        // GET: Banner3/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserIndex userIndex = await db.UserIndexes.FindAsync(id);
            if (userIndex == null)
            {
                return HttpNotFound();
            }
            return View(userIndex);
        }

        // POST: Banner3/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit() { 
            return RedirectToAction("~/Home/Index");
        }

        // GET: Banner3/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserIndex userIndex = await db.UserIndexes.FindAsync(id);
            if (userIndex == null)
            {
                return HttpNotFound();
            }
            return View(userIndex);
        }

        // POST: Banner3/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            UserIndex userIndex = await db.UserIndexes.FindAsync(id);
            db.UserIndexes.Remove(userIndex);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
