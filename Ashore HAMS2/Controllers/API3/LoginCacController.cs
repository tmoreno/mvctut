﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ashore_HAMS.Controllers.API3
{
    public class LoginCacController : ApiController
    {
        // GET: api/LoginCac
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/LoginCac/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/LoginCac
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/LoginCac/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/LoginCac/5
        public void Delete(int id)
        {
        }
    }
}
