﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ashore_HAMS.Controllers.API3
{
    public class KController : Controller
    {
        // GET: K
        public ActionResult Index()
        {
            return View();
        }

        // GET: K/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: K/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: K/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: K/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: K/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: K/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: K/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
