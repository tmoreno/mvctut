﻿using Ashore_HAMS.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Ashore_HAMS.Controllers
{
    public class BannerController : Controller
    {
        // GET: Banner
        [SessionTimeout]
        public ActionResult Index()
        {
            System.Diagnostics.Debug.WriteLine("test");
            return View();
        }

        public ActionResult UserSessionInfo()
        {
            return View();
        }

        public ActionResult Logout()
        {
            HttpClientCertificate cc = HttpContext.Request.ClientCertificate;
            if (cc.IsPresent)
            {
                string subj = cc.Subject;
                ViewBag.CAC_SUBJ = subj;
                // get the client cert
            }

            // set session to null
            HttpContext.Session["ID"] = null;

            return View();
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Submit(string foo)
        //{
        //    HttpContext.Session["ID"] = 1;
        //    return Redirect("/Home");
        //}
        public ActionResult SimCacLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SimCacLogin(FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                HttpContext.Session["ID"] = collection["CacNumber"];
                return RedirectToAction("../Home/Index");
            }
            catch
            {
                return View();
            }
        }

    }
}