﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class NIAPS_VERSION_INDEXController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: NIAPS_VERSION_INDEX
        public async Task<ActionResult> Index()
        {
            var nIAPS_VERSION_INDEX = db.NIAPS_VERSION_INDEX.Include(n => n.DISPLAY_INDICATOR_INDEX);
            return View(await nIAPS_VERSION_INDEX.ToListAsync());
        }

        // GET: NIAPS_VERSION_INDEX/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NIAPS_VERSION_INDEX nIAPS_VERSION_INDEX = await db.NIAPS_VERSION_INDEX.FindAsync(id);
            if (nIAPS_VERSION_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(nIAPS_VERSION_INDEX);
        }

        // GET: NIAPS_VERSION_INDEX/Create
        public ActionResult Create()
        {
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME");
            return View();
        }

        // POST: NIAPS_VERSION_INDEX/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "NIAPS_VERSION_ID,NIAPS_VERSION,DISPLAY_INDICATOR_ID")] NIAPS_VERSION_INDEX nIAPS_VERSION_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.NIAPS_VERSION_INDEX.Add(nIAPS_VERSION_INDEX);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", nIAPS_VERSION_INDEX.DISPLAY_INDICATOR_ID);
            return View(nIAPS_VERSION_INDEX);
        }

        // GET: NIAPS_VERSION_INDEX/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NIAPS_VERSION_INDEX nIAPS_VERSION_INDEX = await db.NIAPS_VERSION_INDEX.FindAsync(id);
            if (nIAPS_VERSION_INDEX == null)
            {
                return HttpNotFound();
            }
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", nIAPS_VERSION_INDEX.DISPLAY_INDICATOR_ID);
            return View(nIAPS_VERSION_INDEX);
        }

        // POST: NIAPS_VERSION_INDEX/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "NIAPS_VERSION_ID,NIAPS_VERSION,DISPLAY_INDICATOR_ID")] NIAPS_VERSION_INDEX nIAPS_VERSION_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nIAPS_VERSION_INDEX).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", nIAPS_VERSION_INDEX.DISPLAY_INDICATOR_ID);
            return View(nIAPS_VERSION_INDEX);
        }

        // GET: NIAPS_VERSION_INDEX/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NIAPS_VERSION_INDEX nIAPS_VERSION_INDEX = await db.NIAPS_VERSION_INDEX.FindAsync(id);
            if (nIAPS_VERSION_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(nIAPS_VERSION_INDEX);
        }

        // POST: NIAPS_VERSION_INDEX/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            NIAPS_VERSION_INDEX nIAPS_VERSION_INDEX = await db.NIAPS_VERSION_INDEX.FindAsync(id);
            db.NIAPS_VERSION_INDEX.Remove(nIAPS_VERSION_INDEX);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
