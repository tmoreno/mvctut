﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class AUTH_APP_RELEASEController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: AUTH_APP_RELEASE
        public async Task<ActionResult> Index(int? id)
        {
            if (id == null)
            {
                
                return RedirectToAction("../");
            }
            else
            {
                var aUTH_APP_RELEASE = db.AUTH_APP_RELEASE
                    .Where(a => a.AUTH_APP_ID == id)
                    .Include(a => a.AUTH_APP_INDEX);

                ViewBag.AUTH_APP_ID = id;
                return View(await aUTH_APP_RELEASE.ToListAsync());
            }

        }

        // GET: AUTH_APP_RELEASE/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_APP_RELEASE aUTH_APP_RELEASE = await db.AUTH_APP_RELEASE.FindAsync(id);
            if (aUTH_APP_RELEASE == null)
            {
                return HttpNotFound();
            }
            return View(aUTH_APP_RELEASE);
        }

        // GET: AUTH_APP_RELEASE/Create
        public ActionResult Create(int id)
        {
            //ViewBag.AUTH_APP_ID = new SelectList(db.AUTH_APP_INDEX, "AUTH_APP_ID", "AUTH_APP_NAME");
            var authAppRelease = new AUTH_APP_RELEASE();
            authAppRelease.AUTH_APP_ID = (Int32)id;
            return View(authAppRelease);
        }

        // POST: AUTH_APP_RELEASE/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AUTH_APP_RELEASE_ID,AUTH_APP_ID,AUTH_VERSION,AUTH_APP_RELEASE_DATE,TIME_DATE_RECORD")] AUTH_APP_RELEASE aUTH_APP_RELEASE)
        {
            if (ModelState.IsValid)
            {
                db.AUTH_APP_RELEASE.Add(aUTH_APP_RELEASE);
                await db.SaveChangesAsync();
                return RedirectToAction(string.Format("Index/{0}", aUTH_APP_RELEASE.AUTH_APP_ID));
            }

            ViewBag.AUTH_APP_ID = new SelectList(db.AUTH_APP_INDEX, "AUTH_APP_ID", "AUTH_APP_NAME", aUTH_APP_RELEASE.AUTH_APP_ID);
            return View(aUTH_APP_RELEASE);
        }

        // GET: AUTH_APP_RELEASE/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_APP_RELEASE aUTH_APP_RELEASE = await db.AUTH_APP_RELEASE.FindAsync(id);
            if (aUTH_APP_RELEASE == null)
            {
                return HttpNotFound();
            }
            ViewBag.AUTH_APP_ID = new SelectList(db.AUTH_APP_INDEX, "AUTH_APP_ID", "AUTH_APP_NAME", aUTH_APP_RELEASE.AUTH_APP_ID);
            
            return View(aUTH_APP_RELEASE);
        }

        // POST: AUTH_APP_RELEASE/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AUTH_APP_RELEASE_ID,AUTH_APP_ID,AUTH_VERSION,AUTH_APP_RELEASE_DATE,TIME_DATE_RECORD")] AUTH_APP_RELEASE aUTH_APP_RELEASE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aUTH_APP_RELEASE).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction(string.Format("Index/{0}", aUTH_APP_RELEASE.AUTH_APP_ID));
            }
            ViewBag.AUTH_APP_ID = new SelectList(db.AUTH_APP_INDEX, "AUTH_APP_ID", "AUTH_APP_NAME", aUTH_APP_RELEASE.AUTH_APP_ID);
            return View(aUTH_APP_RELEASE);
        }

        // GET: AUTH_APP_RELEASE/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTH_APP_RELEASE aUTH_APP_RELEASE = await db.AUTH_APP_RELEASE.FindAsync(id);
            if (aUTH_APP_RELEASE == null)
            {
                return HttpNotFound();
            }
            return View(aUTH_APP_RELEASE);
        }

        // POST: AUTH_APP_RELEASE/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AUTH_APP_RELEASE aUTH_APP_RELEASE = await db.AUTH_APP_RELEASE.FindAsync(id);
            db.AUTH_APP_RELEASE.Remove(aUTH_APP_RELEASE);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
