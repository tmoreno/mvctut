﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class USAGE_STATISTICSController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: USAGE_STATISTICS
        public async Task<ActionResult> Index()
        {
            var uSAGE_STATISTICS = db.USAGE_STATISTICS.Include(u => u.HULL_SERVER_INDEX);
            return View(await uSAGE_STATISTICS.ToListAsync());
        }

        // GET: USAGE_STATISTICS/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USAGE_STATISTICS uSAGE_STATISTICS = await db.USAGE_STATISTICS.FindAsync(id);
            if (uSAGE_STATISTICS == null)
            {
                return HttpNotFound();
            }
            return View(uSAGE_STATISTICS);
        }

        // GET: USAGE_STATISTICS/Create
        public ActionResult Create()
        {
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID");
            return View();
        }

        // POST: USAGE_STATISTICS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "USAGE_STATISTICS_ID,HULL_SERVER_ID,CPU,CPU_DATA_DATE,MEM,MEM_DATA_DATE,SERVER_CODE,CODE_CAPTURE_TIMESTAMP,TIME_DATE_RECORD")] USAGE_STATISTICS uSAGE_STATISTICS)
        {
            if (ModelState.IsValid)
            {
                db.USAGE_STATISTICS.Add(uSAGE_STATISTICS);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", uSAGE_STATISTICS.HULL_SERVER_ID);
            return View(uSAGE_STATISTICS);
        }

        // GET: USAGE_STATISTICS/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USAGE_STATISTICS uSAGE_STATISTICS = await db.USAGE_STATISTICS.FindAsync(id);
            if (uSAGE_STATISTICS == null)
            {
                return HttpNotFound();
            }
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", uSAGE_STATISTICS.HULL_SERVER_ID);
            return View(uSAGE_STATISTICS);
        }

        // POST: USAGE_STATISTICS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "USAGE_STATISTICS_ID,HULL_SERVER_ID,CPU,CPU_DATA_DATE,MEM,MEM_DATA_DATE,SERVER_CODE,CODE_CAPTURE_TIMESTAMP,TIME_DATE_RECORD")] USAGE_STATISTICS uSAGE_STATISTICS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uSAGE_STATISTICS).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", uSAGE_STATISTICS.HULL_SERVER_ID);
            return View(uSAGE_STATISTICS);
        }

        // GET: USAGE_STATISTICS/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USAGE_STATISTICS uSAGE_STATISTICS = await db.USAGE_STATISTICS.FindAsync(id);
            if (uSAGE_STATISTICS == null)
            {
                return HttpNotFound();
            }
            return View(uSAGE_STATISTICS);
        }

        // POST: USAGE_STATISTICS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            USAGE_STATISTICS uSAGE_STATISTICS = await db.USAGE_STATISTICS.FindAsync(id);
            db.USAGE_STATISTICS.Remove(uSAGE_STATISTICS);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
