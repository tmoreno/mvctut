﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class HULL_SERVER_VIEWController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: HULL_SERVER_VIEW
        public async Task<ActionResult> Index()
        {
            return View(await db.HULL_SERVER_VIEW.ToListAsync());
        }

        // GET: HULL_SERVER_VIEW/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HULL_SERVER_VIEW hULL_SERVER_VIEW = await db.HULL_SERVER_VIEW.FindAsync(id);
            if (hULL_SERVER_VIEW == null)
            {
                return HttpNotFound();
            }
            return View(hULL_SERVER_VIEW);
        }

        // GET: HULL_SERVER_VIEW/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HULL_SERVER_VIEW/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "HULL_SERVER_ID,HULL_ID,HULL,HULL_NAME,STATUS_ID,SERVERNAME")] HULL_SERVER_VIEW hULL_SERVER_VIEW)
        {
            if (ModelState.IsValid)
            {
                db.HULL_SERVER_VIEW.Add(hULL_SERVER_VIEW);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(hULL_SERVER_VIEW);
        }

        // GET: HULL_SERVER_VIEW/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HULL_SERVER_VIEW hULL_SERVER_VIEW = await db.HULL_SERVER_VIEW.FindAsync(id);
            if (hULL_SERVER_VIEW == null)
            {
                return HttpNotFound();
            }
            return View(hULL_SERVER_VIEW);
        }

        // POST: HULL_SERVER_VIEW/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "HULL_SERVER_ID,HULL_ID,HULL,HULL_NAME,STATUS_ID,SERVERNAME")] HULL_SERVER_VIEW hULL_SERVER_VIEW)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hULL_SERVER_VIEW).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(hULL_SERVER_VIEW);
        }

        // GET: HULL_SERVER_VIEW/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HULL_SERVER_VIEW hULL_SERVER_VIEW = await db.HULL_SERVER_VIEW.FindAsync(id);
            if (hULL_SERVER_VIEW == null)
            {
                return HttpNotFound();
            }
            return View(hULL_SERVER_VIEW);
        }

        // POST: HULL_SERVER_VIEW/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            HULL_SERVER_VIEW hULL_SERVER_VIEW = await db.HULL_SERVER_VIEW.FindAsync(id);
            db.HULL_SERVER_VIEW.Remove(hULL_SERVER_VIEW);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
