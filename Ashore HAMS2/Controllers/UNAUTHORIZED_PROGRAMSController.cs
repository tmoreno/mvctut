﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class UNAUTHORIZED_PROGRAMSController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: UNAUTHORIZED_PROGRAMS
        public async Task<ActionResult> Index()
        {
            var uNAUTHORIZED_PROGRAMS = db.UNAUTHORIZED_PROGRAMS.Include(u => u.AFLOAT_HULL_APP_INSTALLED).Include(u => u.COMPLIANCE_STATUS_INDEX).Include(u => u.DISPLAY_INDICATOR_INDEX);
            return View(await uNAUTHORIZED_PROGRAMS.ToListAsync());
        }

        // GET: UNAUTHORIZED_PROGRAMS/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UNAUTHORIZED_PROGRAMS uNAUTHORIZED_PROGRAMS = await db.UNAUTHORIZED_PROGRAMS.FindAsync(id);
            if (uNAUTHORIZED_PROGRAMS == null)
            {
                return HttpNotFound();
            }
            return View(uNAUTHORIZED_PROGRAMS);
        }

        // GET: UNAUTHORIZED_PROGRAMS/Create
        public ActionResult Create()
        {
            ViewBag.AFLOAT_HULL_APP_INSTALLED_ID = new SelectList(db.AFLOAT_HULL_APP_INSTALLED, "AFLOAT_HULL_APP_INSTALLED_ID", "AFLOAT_HULL_APP_INSTALLED_ID");
            ViewBag.COMPLIANCE_STATUS_ID = new SelectList(db.COMPLIANCE_STATUS_INDEX, "COMPLIANCE_STATUS_ID", "COMPLIANCE_STATUS_NAME");
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME");
            return View();
        }

        // POST: UNAUTHORIZED_PROGRAMS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "UNAUTHORIZED_PROGRAMS_ID,AFLOAT_HULL_APP_INSTALLED_ID,COMPLIANCE_STATUS_ID,DISPLAY_INDICATOR_ID,TIME_DATE_RECORD")] UNAUTHORIZED_PROGRAMS uNAUTHORIZED_PROGRAMS)
        {
            if (ModelState.IsValid)
            {
                db.UNAUTHORIZED_PROGRAMS.Add(uNAUTHORIZED_PROGRAMS);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AFLOAT_HULL_APP_INSTALLED_ID = new SelectList(db.AFLOAT_HULL_APP_INSTALLED, "AFLOAT_HULL_APP_INSTALLED_ID", "AFLOAT_HULL_APP_INSTALLED_ID", uNAUTHORIZED_PROGRAMS.AFLOAT_HULL_APP_INSTALLED_ID);
            ViewBag.COMPLIANCE_STATUS_ID = new SelectList(db.COMPLIANCE_STATUS_INDEX, "COMPLIANCE_STATUS_ID", "COMPLIANCE_STATUS_NAME", uNAUTHORIZED_PROGRAMS.COMPLIANCE_STATUS_ID);
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", uNAUTHORIZED_PROGRAMS.DISPLAY_INDICATOR_ID);
            return View(uNAUTHORIZED_PROGRAMS);
        }

        // GET: UNAUTHORIZED_PROGRAMS/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UNAUTHORIZED_PROGRAMS uNAUTHORIZED_PROGRAMS = await db.UNAUTHORIZED_PROGRAMS.FindAsync(id);
            if (uNAUTHORIZED_PROGRAMS == null)
            {
                return HttpNotFound();
            }
            ViewBag.AFLOAT_HULL_APP_INSTALLED_ID = new SelectList(db.AFLOAT_HULL_APP_INSTALLED, "AFLOAT_HULL_APP_INSTALLED_ID", "AFLOAT_HULL_APP_INSTALLED_ID", uNAUTHORIZED_PROGRAMS.AFLOAT_HULL_APP_INSTALLED_ID);
            ViewBag.COMPLIANCE_STATUS_ID = new SelectList(db.COMPLIANCE_STATUS_INDEX, "COMPLIANCE_STATUS_ID", "COMPLIANCE_STATUS_NAME", uNAUTHORIZED_PROGRAMS.COMPLIANCE_STATUS_ID);
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", uNAUTHORIZED_PROGRAMS.DISPLAY_INDICATOR_ID);
            return View(uNAUTHORIZED_PROGRAMS);
        }

        // POST: UNAUTHORIZED_PROGRAMS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UNAUTHORIZED_PROGRAMS_ID,AFLOAT_HULL_APP_INSTALLED_ID,COMPLIANCE_STATUS_ID,DISPLAY_INDICATOR_ID,TIME_DATE_RECORD")] UNAUTHORIZED_PROGRAMS uNAUTHORIZED_PROGRAMS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uNAUTHORIZED_PROGRAMS).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AFLOAT_HULL_APP_INSTALLED_ID = new SelectList(db.AFLOAT_HULL_APP_INSTALLED, "AFLOAT_HULL_APP_INSTALLED_ID", "AFLOAT_HULL_APP_INSTALLED_ID", uNAUTHORIZED_PROGRAMS.AFLOAT_HULL_APP_INSTALLED_ID);
            ViewBag.COMPLIANCE_STATUS_ID = new SelectList(db.COMPLIANCE_STATUS_INDEX, "COMPLIANCE_STATUS_ID", "COMPLIANCE_STATUS_NAME", uNAUTHORIZED_PROGRAMS.COMPLIANCE_STATUS_ID);
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", uNAUTHORIZED_PROGRAMS.DISPLAY_INDICATOR_ID);
            return View(uNAUTHORIZED_PROGRAMS);
        }

        // GET: UNAUTHORIZED_PROGRAMS/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UNAUTHORIZED_PROGRAMS uNAUTHORIZED_PROGRAMS = await db.UNAUTHORIZED_PROGRAMS.FindAsync(id);
            if (uNAUTHORIZED_PROGRAMS == null)
            {
                return HttpNotFound();
            }
            return View(uNAUTHORIZED_PROGRAMS);
        }

        // POST: UNAUTHORIZED_PROGRAMS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            UNAUTHORIZED_PROGRAMS uNAUTHORIZED_PROGRAMS = await db.UNAUTHORIZED_PROGRAMS.FindAsync(id);
            db.UNAUTHORIZED_PROGRAMS.Remove(uNAUTHORIZED_PROGRAMS);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
