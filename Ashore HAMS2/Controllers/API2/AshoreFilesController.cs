﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers.API2
{
    public class AshoreFilesController : ApiController
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: api/AshoreFiles
        public IQueryable<AshoreFile> GetAshoreFiles()
        {
            return db.AshoreFiles;
        }

        // GET: api/AshoreFiles/5
        [ResponseType(typeof(AshoreFile))]
        public async Task<IHttpActionResult> GetAshoreFile(int id)
        {
            AshoreFile ashoreFile = await db.AshoreFiles.FindAsync(id);
            if (ashoreFile == null)
            {
                return NotFound();
            }

            return Ok(ashoreFile);
        }

        // PUT: api/AshoreFiles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAshoreFile(int id, AshoreFile ashoreFile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ashoreFile.Id)
            {
                return BadRequest();
            }

            db.Entry(ashoreFile).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AshoreFileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AshoreFiles
        [ResponseType(typeof(AshoreFile))]
        public IHttpActionResult PostAshoreFile(AshoreFile ashoreFile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var af = from a in db.AshoreFiles
                    where a.Filename == ashoreFile.Filename
                    orderby a.Id descending
                    select a;

            if(af.Count() > 0)
            {
                AshoreFile item = af.FirstOrDefault();
                item.Date = ashoreFile.Date;
                item.Hash = ashoreFile.Hash;
                item.Size = ashoreFile.Size;
                db.Entry(item).State = EntityState.Modified;
                ashoreFile.Id = item.Id;
            }
            else
            {
                db.AshoreFiles.Add(ashoreFile);
            }


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (AshoreFileExists(ashoreFile.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = ashoreFile.Id }, ashoreFile);
        }

        // DELETE: api/AshoreFiles/5
        [ResponseType(typeof(AshoreFile))]
        public async Task<IHttpActionResult> DeleteAshoreFile(int id)
        {
            AshoreFile ashoreFile = await db.AshoreFiles.FindAsync(id);
            if (ashoreFile == null)
            {
                return NotFound();
            }

            db.AshoreFiles.Remove(ashoreFile);
            await db.SaveChangesAsync();

            return Ok(ashoreFile);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AshoreFileExists(int id)
        {
            return db.AshoreFiles.Count(e => e.Id == id) > 0;
        }
    }
}