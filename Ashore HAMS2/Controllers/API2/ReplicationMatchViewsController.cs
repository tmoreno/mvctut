﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers.API2
{
    public class ReplicationMatchViewsController : ApiController
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        public class ReplicationDashboardModel
        {
            public int Compliant { get; set; }
            public int Critical { get; set; }
            public IQueryable<ReplicationMatchView> ReplicationMatchViews { get; set; }
            public IQueryable<HullServerView> HullServers { get; set; }
        }


        // GET: api/ReplicationMatchViews
        [ResponseType(typeof(ReplicationDashboardModel))]
        public IHttpActionResult GetReplicationMatchViews()
        {
            ReplicationDashboardModel replicationDashboardModel = new ReplicationDashboardModel();

            var critical = (from a in db.ReplicationMatchViews
                            where a.Hash == null
                            select a).Count();
            var compliant = (from a in db.ReplicationMatchViews
                             where a.Hash != null
                             select a).Count();

            replicationDashboardModel.Critical = critical;
            replicationDashboardModel.Compliant = compliant;

            replicationDashboardModel.ReplicationMatchViews = db.ReplicationMatchViews;

            var hullServers = from a in db.HULL_SERVER_INDEX
                    select new HullServerView
                    {
                        HULL_SERVER_ID = a.HULL_SERVER_ID,
                        SERVERNAME = a.SERVER_INDEX.SERVERNAME,
                        HULL = a.HULL_INDEX.HULL,
                        HULL_NAME = a.HULL_INDEX.HULL_NAME
                    };

            replicationDashboardModel.HullServers = hullServers;

            return Ok(replicationDashboardModel); 
        }

        public class GetReplicationFileForDownloadModel
        {
            public long Id { get; set; }
            public int? AfloatFileHullServerId { get; set; }
            public int HULL_SERVER_ID {get; set;}
            public string Filename { get; set; }
            public string Hash { get; set; }
            public int? Progress { get; set; }
        }

        // GET: api/ReplicationMatchViews/5
        [ResponseType(typeof(GetReplicationFileForDownloadModel))]
        public IHttpActionResult GetReplicationMatchView(int id)
        {
            SqlParameter hullServerId = new SqlParameter("@hullServerId", id);

            var result = db.Database.SqlQuery<GetReplicationFileForDownloadModel>("select  * from [GetReplicationFileForDownloadView] where HULL_SERVER_ID = @hullServerId order by AfloatFileHullServerId desc",
                hullServerId);

            if (result == null)
            {
                return NotFound();
            }

            var filtered = result.Where(a => a.AfloatFileHullServerId == null);
            return Ok(filtered);
        }

        // PUT: api/ReplicationMatchViews/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutReplicationMatchView(int id, ReplicationMatchView replicationMatchView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != replicationMatchView.Id)
            {
                return BadRequest();
            }

            db.Entry(replicationMatchView).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReplicationMatchViewExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        public class HullServerView
        {
            public int HULL_SERVER_ID { get; set; }
            public string HULL { get; set; }
            public string HULL_NAME { get; set; }
            public string SERVERNAME { get; set; }
        }

        // POST: api/ReplicationMatchViews
        [ResponseType(typeof(IList<HullServerView>))]
        public IHttpActionResult PostReplicationMatchView(int[] ids)
        {
            var q = from a in db.HULL_SERVER_INDEX
            where ids.Contains(a.HULL_SERVER_ID)
            select new HullServerView { HULL_SERVER_ID = a.HULL_SERVER_ID,
                SERVERNAME = a.SERVER_INDEX.SERVERNAME,
                HULL = a.HULL_INDEX.HULL,
                HULL_NAME = a.HULL_INDEX.HULL_NAME };

            return Ok(q.ToList<HullServerView>());
        }

        // DELETE: api/ReplicationMatchViews/5
        [ResponseType(typeof(ReplicationMatchView))]
        public IHttpActionResult DeleteReplicationMatchView(int id)
        {
            ReplicationMatchView replicationMatchView = db.ReplicationMatchViews.Find(id);
            if (replicationMatchView == null)
            {
                return NotFound();
            }

            db.ReplicationMatchViews.Remove(replicationMatchView);
            db.SaveChanges();

            return Ok(replicationMatchView);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReplicationMatchViewExists(int id)
        {
            return db.ReplicationMatchViews.Count(e => e.Id == id) > 0;
        }
    }
}