﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class HULL_INDEXController : Controller
    {
        //private HAMS7Entities db = new HAMS7Entities();

        //// GET: HULL_INDEX
        //public async Task<ActionResult> Index()
        //{
        //    var hULL_INDEX = db.HULL_INDEX.Include(h => h.STATUS_INDEX).Include(h => h.HULL_APP_INDEX);
        //    return View(await hULL_INDEX.ToListAsync());
        //}

        //// GET: HULL_INDEX/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    HULL_INDEX hULL_INDEX = await db.HULL_INDEX.FindAsync(id);
        //    if (hULL_INDEX == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(hULL_INDEX);
        //}

        //// GET: HULL_INDEX/Create
        //public ActionResult Create()
        //{
        //    ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME");
        //    return View();
        //}

        //// POST: HULL_INDEX/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "HULL,HULL_TYPE,HULL_NAME,STATUS_ID,RETURN_SERVER_CODE,DISPLAY_INDICATOR,HULL_ID,SERVER_NAME")] HULL_INDEX hULL_INDEX)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.HULL_INDEX.Add(hULL_INDEX);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME", hULL_INDEX.STATUS_ID);
        //    return View(hULL_INDEX);
        //}

        //// GET: HULL_INDEX/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    HULL_INDEX hULL_INDEX = await db.HULL_INDEX.FindAsync(id);
        //    if (hULL_INDEX == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME", hULL_INDEX.STATUS_ID);
        //    return View(hULL_INDEX);
        //}

        //// POST: HULL_INDEX/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "HULL,HULL_TYPE,HULL_NAME,STATUS_ID,RETURN_SERVER_CODE,DISPLAY_INDICATOR,HULL_ID,SERVER_NAME")] HULL_INDEX hULL_INDEX)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(hULL_INDEX).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.STATUS_ID = new SelectList(db.STATUS_INDEX, "STATUS_ID", "STATUS_NAME", hULL_INDEX.STATUS_ID);
        //    return View(hULL_INDEX);
        //}

        //// GET: HULL_INDEX/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    HULL_INDEX hULL_INDEX = await db.HULL_INDEX.FindAsync(id);
        //    if (hULL_INDEX == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(hULL_INDEX);
        //}

        //// POST: HULL_INDEX/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    HULL_INDEX hULL_INDEX = await db.HULL_INDEX.FindAsync(id);
        //    db.HULL_INDEX.Remove(hULL_INDEX);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
