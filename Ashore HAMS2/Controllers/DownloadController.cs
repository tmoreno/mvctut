﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ashore_HAMS.Controllers
{
    public class DownloadController : Controller
    {
        // GET: Download
        public ActionResult GetFile(string filename)
        {
            var path = String.Format(@"C:\Users\tmoreno\work\dropbox\{0}", filename);
            var stream = new System.IO.FileStream(path, FileMode.Open);
            var response = File(stream, "application/octet-stream"); // FileStreamResult
            response.FileDownloadName = filename;

            return response;
        }
    }
}