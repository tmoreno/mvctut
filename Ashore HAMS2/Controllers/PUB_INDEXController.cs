﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class PUB_INDEXController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: PUB_INDEX
        public async Task<ActionResult> Index()
        {
            var pUB_INDEX = db.PUB_INDEX.Include(p => p.DISPLAY_INDICATOR_INDEX).Include(p => p.DOWNLOAD_TYPE_INDEX).Include(p => p.PRIORITY_INDEX).Include(p => p.PUB_SCOPE_INDEX).Include(p => p.PUB_TYPE_INDEX);
            return View(await pUB_INDEX.ToListAsync());
        }

        // GET: PUB_INDEX/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PUB_INDEX pUB_INDEX = await db.PUB_INDEX.FindAsync(id);
            if (pUB_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(pUB_INDEX);
        }

        // GET: PUB_INDEX/Create
        public ActionResult Create()
        {
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME");
            ViewBag.DOWNLOAD_TYPE_ID = new SelectList(db.DOWNLOAD_TYPE_INDEX, "DOWNLOAD_TYPE_ID", "DOWNLOAD_TYPE");
            ViewBag.PRIORITY_ID = new SelectList(db.PRIORITY_INDEX, "PRIORITY_ID", "PRIORITY");
            ViewBag.PUB_SCOPE_ID = new SelectList(db.PUB_SCOPE_INDEX, "PUB_SCOPE_ID", "PUB_SCOPE_NAME");
            ViewBag.PUB_TYPE_ID = new SelectList(db.PUB_TYPE_INDEX, "PUB_TYPE_ID", "PUB_TYPE_NAME");
            return View();
        }

        // POST: PUB_INDEX/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "PUB_ID,PUB_NAME,DISPLAY_INDICATOR_ID,PUB_TYPE_ID,PUB_SCOPE_ID,CONTENT,DOWNLOAD_TYPE_ID,PRIORITY_ID")] PUB_INDEX pUB_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.PUB_INDEX.Add(pUB_INDEX);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", pUB_INDEX.DISPLAY_INDICATOR_ID);
            ViewBag.DOWNLOAD_TYPE_ID = new SelectList(db.DOWNLOAD_TYPE_INDEX, "DOWNLOAD_TYPE_ID", "DOWNLOAD_TYPE", pUB_INDEX.DOWNLOAD_TYPE_ID);
            ViewBag.PRIORITY_ID = new SelectList(db.PRIORITY_INDEX, "PRIORITY_ID", "PRIORITY", pUB_INDEX.PRIORITY_ID);
            ViewBag.PUB_SCOPE_ID = new SelectList(db.PUB_SCOPE_INDEX, "PUB_SCOPE_ID", "PUB_SCOPE_NAME", pUB_INDEX.PUB_SCOPE_ID);
            ViewBag.PUB_TYPE_ID = new SelectList(db.PUB_TYPE_INDEX, "PUB_TYPE_ID", "PUB_TYPE_NAME", pUB_INDEX.PUB_TYPE_ID);
            return View(pUB_INDEX);
        }

        // GET: PUB_INDEX/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PUB_INDEX pUB_INDEX = await db.PUB_INDEX.FindAsync(id);
            if (pUB_INDEX == null)
            {
                return HttpNotFound();
            }
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", pUB_INDEX.DISPLAY_INDICATOR_ID);
            ViewBag.DOWNLOAD_TYPE_ID = new SelectList(db.DOWNLOAD_TYPE_INDEX, "DOWNLOAD_TYPE_ID", "DOWNLOAD_TYPE", pUB_INDEX.DOWNLOAD_TYPE_ID);
            ViewBag.PRIORITY_ID = new SelectList(db.PRIORITY_INDEX, "PRIORITY_ID", "PRIORITY", pUB_INDEX.PRIORITY_ID);
            ViewBag.PUB_SCOPE_ID = new SelectList(db.PUB_SCOPE_INDEX, "PUB_SCOPE_ID", "PUB_SCOPE_NAME", pUB_INDEX.PUB_SCOPE_ID);
            ViewBag.PUB_TYPE_ID = new SelectList(db.PUB_TYPE_INDEX, "PUB_TYPE_ID", "PUB_TYPE_NAME", pUB_INDEX.PUB_TYPE_ID);
            return View(pUB_INDEX);
        }

        // POST: PUB_INDEX/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "PUB_ID,PUB_NAME,DISPLAY_INDICATOR_ID,PUB_TYPE_ID,PUB_SCOPE_ID,CONTENT,DOWNLOAD_TYPE_ID,PRIORITY_ID")] PUB_INDEX pUB_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pUB_INDEX).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DISPLAY_INDICATOR_ID = new SelectList(db.DISPLAY_INDICATOR_INDEX, "DISPLAY_INDICATOR_ID", "DISPLAY_INDICATOR_NAME", pUB_INDEX.DISPLAY_INDICATOR_ID);
            ViewBag.DOWNLOAD_TYPE_ID = new SelectList(db.DOWNLOAD_TYPE_INDEX, "DOWNLOAD_TYPE_ID", "DOWNLOAD_TYPE", pUB_INDEX.DOWNLOAD_TYPE_ID);
            ViewBag.PRIORITY_ID = new SelectList(db.PRIORITY_INDEX, "PRIORITY_ID", "PRIORITY", pUB_INDEX.PRIORITY_ID);
            ViewBag.PUB_SCOPE_ID = new SelectList(db.PUB_SCOPE_INDEX, "PUB_SCOPE_ID", "PUB_SCOPE_NAME", pUB_INDEX.PUB_SCOPE_ID);
            ViewBag.PUB_TYPE_ID = new SelectList(db.PUB_TYPE_INDEX, "PUB_TYPE_ID", "PUB_TYPE_NAME", pUB_INDEX.PUB_TYPE_ID);
            return View(pUB_INDEX);
        }

        // GET: PUB_INDEX/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PUB_INDEX pUB_INDEX = await db.PUB_INDEX.FindAsync(id);
            if (pUB_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(pUB_INDEX);
        }

        // POST: PUB_INDEX/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            PUB_INDEX pUB_INDEX = await db.PUB_INDEX.FindAsync(id);
            db.PUB_INDEX.Remove(pUB_INDEX);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
