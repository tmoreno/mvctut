﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class AFLOAT_AMENDMENT_INFOController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: AFLOAT_AMENDMENT_INFO
        public async Task<ActionResult> Index()
        {
            var aFLOAT_AMENDMENT_INFO = db.AFLOAT_AMENDMENT_INFO.Include(a => a.AFLOAT_PUB_LEVELS);
            return View(await aFLOAT_AMENDMENT_INFO.ToListAsync());
        }

        // GET: AFLOAT_AMENDMENT_INFO/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFLOAT_AMENDMENT_INFO aFLOAT_AMENDMENT_INFO = await db.AFLOAT_AMENDMENT_INFO.FindAsync(id);
            if (aFLOAT_AMENDMENT_INFO == null)
            {
                return HttpNotFound();
            }
            return View(aFLOAT_AMENDMENT_INFO);
        }

        // GET: AFLOAT_AMENDMENT_INFO/Create
        public ActionResult Create()
        {
            ViewBag.AFLOAT_PUB_LEVELS_ID = new SelectList(db.AFLOAT_PUB_LEVELS, "AFLOAT_PUB_LEVELS_ID", "AFLOAT_PUB_LEVELS_ID");
            return View();
        }

        // POST: AFLOAT_AMENDMENT_INFO/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AMENDMENT_ID,AFLOAT_PUB_LEVELS_ID,AMENDMENT_NAME,SIZE,PRIORITY,COMPULSORY,POST_PROCESSING,DEPENDENCIES,AMENDMENT_CREATION_TIMESTAMP,TIME_DATE_RECORD")] AFLOAT_AMENDMENT_INFO aFLOAT_AMENDMENT_INFO)
        {
            if (ModelState.IsValid)
            {
                db.AFLOAT_AMENDMENT_INFO.Add(aFLOAT_AMENDMENT_INFO);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AFLOAT_PUB_LEVELS_ID = new SelectList(db.AFLOAT_PUB_LEVELS, "AFLOAT_PUB_LEVELS_ID", "AFLOAT_PUB_LEVELS_ID", aFLOAT_AMENDMENT_INFO.AFLOAT_PUB_LEVELS_ID);
            return View(aFLOAT_AMENDMENT_INFO);
        }

        // GET: AFLOAT_AMENDMENT_INFO/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFLOAT_AMENDMENT_INFO aFLOAT_AMENDMENT_INFO = await db.AFLOAT_AMENDMENT_INFO.FindAsync(id);
            if (aFLOAT_AMENDMENT_INFO == null)
            {
                return HttpNotFound();
            }
            ViewBag.AFLOAT_PUB_LEVELS_ID = new SelectList(db.AFLOAT_PUB_LEVELS, "AFLOAT_PUB_LEVELS_ID", "AFLOAT_PUB_LEVELS_ID", aFLOAT_AMENDMENT_INFO.AFLOAT_PUB_LEVELS_ID);
            return View(aFLOAT_AMENDMENT_INFO);
        }

        // POST: AFLOAT_AMENDMENT_INFO/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AMENDMENT_ID,AFLOAT_PUB_LEVELS_ID,AMENDMENT_NAME,SIZE,PRIORITY,COMPULSORY,POST_PROCESSING,DEPENDENCIES,AMENDMENT_CREATION_TIMESTAMP,TIME_DATE_RECORD")] AFLOAT_AMENDMENT_INFO aFLOAT_AMENDMENT_INFO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aFLOAT_AMENDMENT_INFO).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AFLOAT_PUB_LEVELS_ID = new SelectList(db.AFLOAT_PUB_LEVELS, "AFLOAT_PUB_LEVELS_ID", "AFLOAT_PUB_LEVELS_ID", aFLOAT_AMENDMENT_INFO.AFLOAT_PUB_LEVELS_ID);
            return View(aFLOAT_AMENDMENT_INFO);
        }

        // GET: AFLOAT_AMENDMENT_INFO/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AFLOAT_AMENDMENT_INFO aFLOAT_AMENDMENT_INFO = await db.AFLOAT_AMENDMENT_INFO.FindAsync(id);
            if (aFLOAT_AMENDMENT_INFO == null)
            {
                return HttpNotFound();
            }
            return View(aFLOAT_AMENDMENT_INFO);
        }

        // POST: AFLOAT_AMENDMENT_INFO/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AFLOAT_AMENDMENT_INFO aFLOAT_AMENDMENT_INFO = await db.AFLOAT_AMENDMENT_INFO.FindAsync(id);
            db.AFLOAT_AMENDMENT_INFO.Remove(aFLOAT_AMENDMENT_INFO);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
