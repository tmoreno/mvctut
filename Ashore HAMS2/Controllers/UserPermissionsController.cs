﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hams3xLib;

namespace Ashore_HAMS.Controllers
{
    public class UserPermissionsController : Controller
    {
        private HAMS3XEntities db = new HAMS3XEntities();

        // GET: UserPermissions
        public async Task<ActionResult> Index()
        {
            var userPermissions = db.UserPermissions.Include(u => u.Permission).Include(u => u.UserIndex);
            return View(await userPermissions.ToListAsync());
        }

        // GET: UserPermissions/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserPermission userPermission = await db.UserPermissions.FindAsync(id);
            if (userPermission == null)
            {
                return HttpNotFound();
            }
            return View(userPermission);
        }

        // GET: UserPermissions/Create
        public ActionResult Create()
        {
            ViewBag.ModuleId = new SelectList(db.Permissions, "ModuleId", "ModuleName");
            ViewBag.UserIndexId = new SelectList(db.UserIndexes, "UserIndexId", "Cac");
            return View();
        }

        // POST: UserPermissions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "UserPermissionId,UserIndexId,ModuleId")] UserPermission userPermission)
        {
            if (ModelState.IsValid)
            {
                db.UserPermissions.Add(userPermission);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ModuleId = new SelectList(db.Permissions, "ModuleId", "ModuleName", userPermission.ModuleId);
            ViewBag.UserIndexId = new SelectList(db.UserIndexes, "UserIndexId", "Cac", userPermission.UserIndexId);
            return View(userPermission);
        }

        // GET: UserPermissions/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserPermission userPermission = await db.UserPermissions.FindAsync(id);
            if (userPermission == null)
            {
                return HttpNotFound();
            }
            ViewBag.ModuleId = new SelectList(db.Permissions, "ModuleId", "ModuleName", userPermission.ModuleId);
            ViewBag.UserIndexId = new SelectList(db.UserIndexes, "UserIndexId", "Cac", userPermission.UserIndexId);
            return View(userPermission);
        }

        // POST: UserPermissions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserPermissionId,UserIndexId,ModuleId")] UserPermission userPermission)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userPermission).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ModuleId = new SelectList(db.Permissions, "ModuleId", "ModuleName", userPermission.ModuleId);
            ViewBag.UserIndexId = new SelectList(db.UserIndexes, "UserIndexId", "Cac", userPermission.UserIndexId);
            return View(userPermission);
        }

        // GET: UserPermissions/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserPermission userPermission = await db.UserPermissions.FindAsync(id);
            if (userPermission == null)
            {
                return HttpNotFound();
            }
            return View(userPermission);
        }

        // POST: UserPermissions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            UserPermission userPermission = await db.UserPermissions.FindAsync(id);
            db.UserPermissions.Remove(userPermission);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
