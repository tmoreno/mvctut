﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class DRIVE_INDEXController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: DRIVE_INDEX
        public async Task<ActionResult> Index()
        {
            var dRIVE_INDEX = db.DRIVE_INDEX.Include(d => d.HULL_SERVER_INDEX);
            return View(await dRIVE_INDEX.ToListAsync());
        }

        // GET: DRIVE_INDEX/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DRIVE_INDEX dRIVE_INDEX = await db.DRIVE_INDEX.FindAsync(id);
            if (dRIVE_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(dRIVE_INDEX);
        }

        // GET: DRIVE_INDEX/Create
        public ActionResult Create()
        {
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID");
            return View();
        }

        // POST: DRIVE_INDEX/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "DRIVE_INDEX_ID,HULL_SERVER_ID,DRIVE_LETTER,AVAIL_SPACE,USED_SPACE,COLLECTED_TIMESTAMP,TIME_DATE_RECORD")] DRIVE_INDEX dRIVE_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.DRIVE_INDEX.Add(dRIVE_INDEX);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", dRIVE_INDEX.HULL_SERVER_ID);
            return View(dRIVE_INDEX);
        }

        // GET: DRIVE_INDEX/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DRIVE_INDEX dRIVE_INDEX = await db.DRIVE_INDEX.FindAsync(id);
            if (dRIVE_INDEX == null)
            {
                return HttpNotFound();
            }
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", dRIVE_INDEX.HULL_SERVER_ID);
            return View(dRIVE_INDEX);
        }

        // POST: DRIVE_INDEX/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "DRIVE_INDEX_ID,HULL_SERVER_ID,DRIVE_LETTER,AVAIL_SPACE,USED_SPACE,COLLECTED_TIMESTAMP,TIME_DATE_RECORD")] DRIVE_INDEX dRIVE_INDEX)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dRIVE_INDEX).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.HULL_SERVER_ID = new SelectList(db.HULL_SERVER_INDEX, "HULL_SERVER_ID", "HULL_SERVER_ID", dRIVE_INDEX.HULL_SERVER_ID);
            return View(dRIVE_INDEX);
        }

        // GET: DRIVE_INDEX/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DRIVE_INDEX dRIVE_INDEX = await db.DRIVE_INDEX.FindAsync(id);
            if (dRIVE_INDEX == null)
            {
                return HttpNotFound();
            }
            return View(dRIVE_INDEX);
        }

        // POST: DRIVE_INDEX/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DRIVE_INDEX dRIVE_INDEX = await db.DRIVE_INDEX.FindAsync(id);
            db.DRIVE_INDEX.Remove(dRIVE_INDEX);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
