﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class ASHORE_CLIENT_LEVELSController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: ASHORE_CLIENT_LEVELS
        public async Task<ActionResult> Index()
        {
            var aSHORE_CLIENT_LEVELS = db.ASHORE_CLIENT_LEVELS.Include(a => a.HULL_PUB_INDEX);
            return View(await aSHORE_CLIENT_LEVELS.ToListAsync());
        }

        // GET: ASHORE_CLIENT_LEVELS/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ASHORE_CLIENT_LEVELS aSHORE_CLIENT_LEVELS = await db.ASHORE_CLIENT_LEVELS.FindAsync(id);
            if (aSHORE_CLIENT_LEVELS == null)
            {
                return HttpNotFound();
            }
            return View(aSHORE_CLIENT_LEVELS);
        }

        // GET: ASHORE_CLIENT_LEVELS/Create
        public ActionResult Create()
        {
            ViewBag.HULL_PUB_ID = new SelectList(db.HULL_PUB_INDEX, "HULL_PUB_ID", "HULL_PUB_ID");
            return View();
        }

        // POST: ASHORE_CLIENT_LEVELS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ASHORE_CLIENT_LEVELS_ID,HULL_PUB_ID,ASHORE_CLIENT_LEVEL,ASHORE_CLIENT_PUB_EXTRACTION_TIMESTAMP,TIME_DATE_RECORD")] ASHORE_CLIENT_LEVELS aSHORE_CLIENT_LEVELS)
        {
            if (ModelState.IsValid)
            {
                db.ASHORE_CLIENT_LEVELS.Add(aSHORE_CLIENT_LEVELS);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.HULL_PUB_ID = new SelectList(db.HULL_PUB_INDEX, "HULL_PUB_ID", "HULL_PUB_ID", aSHORE_CLIENT_LEVELS.HULL_PUB_ID);
            return View(aSHORE_CLIENT_LEVELS);
        }

        // GET: ASHORE_CLIENT_LEVELS/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ASHORE_CLIENT_LEVELS aSHORE_CLIENT_LEVELS = await db.ASHORE_CLIENT_LEVELS.FindAsync(id);
            if (aSHORE_CLIENT_LEVELS == null)
            {
                return HttpNotFound();
            }
            ViewBag.HULL_PUB_ID = new SelectList(db.HULL_PUB_INDEX, "HULL_PUB_ID", "HULL_PUB_ID", aSHORE_CLIENT_LEVELS.HULL_PUB_ID);
            return View(aSHORE_CLIENT_LEVELS);
        }

        // POST: ASHORE_CLIENT_LEVELS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ASHORE_CLIENT_LEVELS_ID,HULL_PUB_ID,ASHORE_CLIENT_LEVEL,ASHORE_CLIENT_PUB_EXTRACTION_TIMESTAMP,TIME_DATE_RECORD")] ASHORE_CLIENT_LEVELS aSHORE_CLIENT_LEVELS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aSHORE_CLIENT_LEVELS).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.HULL_PUB_ID = new SelectList(db.HULL_PUB_INDEX, "HULL_PUB_ID", "HULL_PUB_ID", aSHORE_CLIENT_LEVELS.HULL_PUB_ID);
            return View(aSHORE_CLIENT_LEVELS);
        }

        // GET: ASHORE_CLIENT_LEVELS/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ASHORE_CLIENT_LEVELS aSHORE_CLIENT_LEVELS = await db.ASHORE_CLIENT_LEVELS.FindAsync(id);
            if (aSHORE_CLIENT_LEVELS == null)
            {
                return HttpNotFound();
            }
            return View(aSHORE_CLIENT_LEVELS);
        }

        // POST: ASHORE_CLIENT_LEVELS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ASHORE_CLIENT_LEVELS aSHORE_CLIENT_LEVELS = await db.ASHORE_CLIENT_LEVELS.FindAsync(id);
            db.ASHORE_CLIENT_LEVELS.Remove(aSHORE_CLIENT_LEVELS);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
