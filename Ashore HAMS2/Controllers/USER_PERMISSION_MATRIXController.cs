﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ashore_HAMS.Models3;

namespace Ashore_HAMS.Controllers
{
    public class USER_PERMISSION_MATRIXController : Controller
    {
        private HAMS9Entities1 db = new HAMS9Entities1();

        // GET: USER_PERMISSION_MATRIX
        public async Task<ActionResult> Index()
        {
            var uSER_PERMISSION_MATRIX = db.USER_PERMISSION_MATRIX.Include(u => u.PERMISSION_INDEX).Include(u => u.USER_INDEX);
            return View(await uSER_PERMISSION_MATRIX.ToListAsync());
        }

        // GET: USER_PERMISSION_MATRIX/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USER_PERMISSION_MATRIX uSER_PERMISSION_MATRIX = await db.USER_PERMISSION_MATRIX.FindAsync(id);
            if (uSER_PERMISSION_MATRIX == null)
            {
                return HttpNotFound();
            }
            return View(uSER_PERMISSION_MATRIX);
        }

        // GET: USER_PERMISSION_MATRIX/Create
        public ActionResult Create()
        {
            ViewBag.MODULE_ID = new SelectList(db.PERMISSION_INDEX, "MODULE_ID", "MODULE");
            ViewBag.USER_ID = new SelectList(db.USER_INDEX, "USER_ID", "CAC");
            return View();
        }

        // POST: USER_PERMISSION_MATRIX/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "USER_PERMISSION_ID,USER_ID,MODULE_ID")] USER_PERMISSION_MATRIX uSER_PERMISSION_MATRIX)
        {
            if (ModelState.IsValid)
            {
                db.USER_PERMISSION_MATRIX.Add(uSER_PERMISSION_MATRIX);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.MODULE_ID = new SelectList(db.PERMISSION_INDEX, "MODULE_ID", "MODULE", uSER_PERMISSION_MATRIX.MODULE_ID);
            ViewBag.USER_ID = new SelectList(db.USER_INDEX, "USER_ID", "CAC", uSER_PERMISSION_MATRIX.USER_ID);
            return View(uSER_PERMISSION_MATRIX);
        }

        // GET: USER_PERMISSION_MATRIX/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USER_PERMISSION_MATRIX uSER_PERMISSION_MATRIX = await db.USER_PERMISSION_MATRIX.FindAsync(id);
            if (uSER_PERMISSION_MATRIX == null)
            {
                return HttpNotFound();
            }
            ViewBag.MODULE_ID = new SelectList(db.PERMISSION_INDEX, "MODULE_ID", "MODULE", uSER_PERMISSION_MATRIX.MODULE_ID);
            ViewBag.USER_ID = new SelectList(db.USER_INDEX, "USER_ID", "CAC", uSER_PERMISSION_MATRIX.USER_ID);
            return View(uSER_PERMISSION_MATRIX);
        }

        // POST: USER_PERMISSION_MATRIX/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "USER_PERMISSION_ID,USER_ID,MODULE_ID")] USER_PERMISSION_MATRIX uSER_PERMISSION_MATRIX)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uSER_PERMISSION_MATRIX).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.MODULE_ID = new SelectList(db.PERMISSION_INDEX, "MODULE_ID", "MODULE", uSER_PERMISSION_MATRIX.MODULE_ID);
            ViewBag.USER_ID = new SelectList(db.USER_INDEX, "USER_ID", "CAC", uSER_PERMISSION_MATRIX.USER_ID);
            return View(uSER_PERMISSION_MATRIX);
        }

        // GET: USER_PERMISSION_MATRIX/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USER_PERMISSION_MATRIX uSER_PERMISSION_MATRIX = await db.USER_PERMISSION_MATRIX.FindAsync(id);
            if (uSER_PERMISSION_MATRIX == null)
            {
                return HttpNotFound();
            }
            return View(uSER_PERMISSION_MATRIX);
        }

        // POST: USER_PERMISSION_MATRIX/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            USER_PERMISSION_MATRIX uSER_PERMISSION_MATRIX = await db.USER_PERMISSION_MATRIX.FindAsync(id);
            db.USER_PERMISSION_MATRIX.Remove(uSER_PERMISSION_MATRIX);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
