﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVCTut.Controllers.HAMS2;
using System.Web.Mvc;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace MVCUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1Async()
        {
            PubPageController pubPageController = new PubPageController();
            var t = GetR(pubPageController);
            var r = t.Result;

            System.Diagnostics.Trace.WriteLine(r.ToString());
            


        }

        [TestMethod]
        public void TestMethod2Async()
        {
            ShipStatusController shipStatusController = new ShipStatusController();
            ActionResult a = shipStatusController.Index();
            
            //System.Diagnostics.Trace.WriteLine(r.ToString());



        }

        private static async System.Threading.Tasks.Task<ActionResult> GetR(PubPageController pubPageController)
        {
            return await pubPageController.Index("AH-19");
        }

        [TestMethod]
        public void PostAH19USV01ToNotification()
        {

            int[] n = new int[] { 1, 2, 3, 4 };

            foreach(int x in n)
            {
                Random rnd = new Random();
                int code = rnd.Next(4);

                string[] values = new string[] { "AH19USV01", "AH20USV01", "AS39USV01", "AS40USV01", "DDG63USV01" };

                Notification notification = new Notification()
                {
                    Ship = values[code],
                    Code = rnd.Next(100) + 1 * rnd.Next(100) + 1,
                    Date = DateTime.Now
                };

                string json = JsonConvert.SerializeObject(notification);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpClient client = new HttpClient();
                var result = client.PostAsync("http://localhost:1972/api/Notifications", content).Result;

                System.Diagnostics.Trace.WriteLine(result);
            }




        }


    }
}
