﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCUnitTest
{
    using System;
    using System.Collections.Generic;

    public class Notification
    {
        public long id { get; set; }
        public string Ship { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Code { get; set; }
    }
}