﻿USE HAMS9
GO

/****** Object: Table [dbo].[Dashboard] Script Date: 11/14/2017 11:28:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Dashboard] (
    [Id]        INT IDENTITY (1, 1) NOT NULL,
    [module_id] INT NULL,
    [normal]    INT NULL,
    [warning]   INT NULL,
    [critical]  INT NULL,
    [total]     INT NULL
);


