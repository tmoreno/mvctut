﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MVCTut.Controllers;

namespace AxwayS.Model
{
    public class TransferEntry
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("CustomIdName")]
        public string OperationIndex { get; set; }
        public string Status { get; set; }
        public bool Secure { get; set; }
        public string Filename { get; set; }
        public string Account { get; set; }
        public string Login { get; set; }
        public string Direction { get; set; }
        public string ActionBy { get; set; }
        public int? Filesize { get; set; }
        public string Protocol { get; set; }
        public DateTime StartTime { get; set; }
        public string Duration { get; set; }
        public int Id { get; set; }
        public string Hash { get; set; }

        public static implicit operator TransferEntry(UploadController.CachedTransferFilesObject v)
        {
            throw new NotImplementedException();
        }
    }
}
