﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AxwayS.Controllers
{
    public class DownloadController : Controller
    {
        // GET: Download
        public ActionResult Index()
        {
            return View();
        }

        // GET: Download
        public ActionResult GetFile(string filename)
        {
            string downloadFolder = ConfigurationManager.AppSettings["downloadFolder"];
            var path = String.Format(@"{0}\{1}", downloadFolder, filename);
            var stream = new System.IO.FileStream(path, FileMode.Open);
            var response = File(stream, "application/octet-stream"); // FileStreamResult
            response.FileDownloadName = filename;

            return response;
        }
    }
}