﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace AxwayS.Controllers
{
    public class DsUpdateStatusController : ApiController
    {
        [HttpPost]
        public string Post(HttpRequestMessage request)
        {
            var doc = new XmlDocument();
            var str = request.Content.ReadAsStreamAsync().Result;

            doc.Load(request.Content.ReadAsStreamAsync().Result);

            string dsUpdateStatusLog = ConfigurationManager.AppSettings["dsUpdateStatusLog"];
            File.WriteAllText(string.Format(@"{0}\\{1}.xml",dsUpdateStatusLog, DateTime.Now.Ticks), doc.OuterXml.ToString());

            return doc.OuterXml.ToString();
        }
    }
}
