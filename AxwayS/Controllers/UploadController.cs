﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Linq;
using System.Web.Http.Results;
using AxwayS.Model;
using System.Collections;
using System.Configuration;

namespace MVCTut.Controllers
{
    public class UploadController : Controller
    {

        public class CachedTransferFilesObject
        {
            public int? Id { get; set; }
            public string Filename { get; set; }
            public DateTime Date { get; set; }
            public string Hash { get; set; }
            public int? Size { get; set; }

            //{
            //    Filename = fileName,
            //    Date = DateTime.Now,
            //    Hash = sha256str,
            //    Size = (int)length
            //};
        }


        // GET: Upload
        public ActionResult Index()
        {



            var list = (IList)HttpRuntime.Cache["x"];

            if(list == null)
            {
                list = new List<TransferEntry>();
            }
            return View(list);
        }



        [System.Web.Mvc.HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {


            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                // extract only the filename
                var fileName = Path.GetFileName(file.FileName);
                // store the file inside ~/App_Data/uploads folder
                //var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                string rootFolder = ConfigurationManager.AppSettings["rootFolder"];
                var path = Path.Combine(rootFolder, fileName);
                file.SaveAs(path);
                Thread.Sleep(1500);
                
                long length = new System.IO.FileInfo(path).Length;

                using (var stream = new BufferedStream(System.IO.File.OpenRead(path), 1200000))
                {
                    SHA256Managed sha = new SHA256Managed();
                    byte[] checksum = sha.ComputeHash(stream);
                    string sha256str = BitConverter.ToString(checksum).Replace("-", String.Empty);


                    var list = (IList<TransferEntry>)HttpRuntime.Cache["x"];
                    if (list == null)
                    {
                        list = new List<TransferEntry>();

                        HttpRuntime.Cache["x"] = list;
                    }

                    var c = new TransferEntry()
                    {
                        Id = 12,
                        Filename = fileName,
                        Login = "TestUser",
                        StartTime = DateTime.Now,
                        Hash = sha256str,
                        Filesize = (int)length,
                        Protocol = "HTTP",


                    };

                    list.Add(c);

                    //AshoreFile ashoreFile = new AshoreFile()
                    //{
                    //    Filename = fileName,
                    //    Date = DateTime.Now,
                    //    Hash = sha256str,
                    //    Size = (int)length
                    //};

                    //ClearAfloatFileIfExists(fileId);

                    if (c.Filename.EndsWith(".mf"))
                    {

                    }

                }



            }
            // redirect back to the index action to show the form once again
            return RedirectToAction("Index");
        }

    }
}