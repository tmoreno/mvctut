﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;

namespace AxwayS.Controllers
{
    public class C
    {
        public long Timestamp { get; set; }
        public string Name { get; set; }
        public string Hash { get; set; }
        public string Delta { get; set; }
    }

    public class CatalogController : ApiController
    {
        private string GetHash(string path)
        {
            string sha256str = string.Empty;

            using (var stream = new BufferedStream(System.IO.File.OpenRead(path), 1200000))
            {
                SHA256Managed sha = new SHA256Managed();
                byte[] checksum = sha.ComputeHash(stream);
                sha256str = BitConverter.ToString(checksum).Replace("-", String.Empty);
            }
            return sha256str;
        }

        public C Get(string hash)
        {
            string rootFolder = ConfigurationManager.AppSettings["downloadFolder"];
            DirectoryInfo di = new DirectoryInfo(rootFolder);
            var res = di.GetFiles("*.era")
            .OrderByDescending(x => x.CreationTime)
            .Take(1).ToArray();

            string localhash = String.Empty;

            string deltaName = String.Empty;

            if(res.Count() == 1)
            {
                localhash = GetHash(res[0].FullName);

                // find the delta name

                string tryDeltaName = res[0].Name;
                tryDeltaName = tryDeltaName.Substring(0, tryDeltaName.Length - 4) + ".delta";

                FileInfo file = new FileInfo(string.Format(@"{0}\\{1}", rootFolder, tryDeltaName));
                if(file.Exists)
                {
                    Console.WriteLine("yes delta file exists ...");
                    deltaName = tryDeltaName;
                }


                

                if(localhash == hash)
                {
                    return new C
                    {
                        Timestamp = -1,
                        Name = res[0].Name,
                        Hash = localhash,
                        Delta = String.Empty
                    };
                }


            }


           

            var objs = from eItem in res
            select new C
            {
                Timestamp = eItem.CreationTime.Ticks,
                Name = eItem.Name,
                Hash = localhash,
                Delta = deltaName
            };
            
            return objs.FirstOrDefault<C>();
        }
    }
}
