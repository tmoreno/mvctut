﻿using AxwayS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;


namespace AxwayS.Controllers
{
    public class TransfersController : ApiController
    {
        // GET: api/Transfers
        public IList<TransferEntry> Get()
        {
            var list = (IList<TransferEntry>)HttpRuntime.Cache["x"];

            if (list == null)
            {
                list = new List<TransferEntry>();
            }
            return list;
        }

        // GET: api/Transfers/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Transfers
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Transfers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Transfers/5
        public void Delete(int id)
        {
        }
    }
}
