﻿USE [HAMS9]
GO

/****** Object: View [dbo].[ReplicationMatchView] Script Date: 1/11/2018 3:24:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].ReplicationMatchView
as
select a.Id, a.HULL_SERVER_ID, a.AshoreFileId, c.Hash, b.Date, b.Progress
from AshoreFileHullServer a
left join 

(select *
	from AfloatFileHullServer a
	where id IN
	(
		select Max(id) as id
		from AfloatFileHullServer a
		group by HULL_SERVER_ID, AshoreFileId
	)
) b

on a.HULL_SERVER_ID = b.HULL_SERVER_ID and a.AshoreFileId = b.AshoreFileId
left join AshoreFile c
on b.AshoreFileId = c.id and b.Hash = c.Hash
