﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AfloatHAMS.Models
{

    public class DirectoryStructure
    {
        public HULL_SERVER_INDEX HullServer { get; set; }
        public IList<FileModel> Files { get; set; }
    }

    public class FileModel
    {
        public string FileName { get; set; }
        public long FileSize { get; set; }
    }
}