//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AfloatHAMS
{
    using System;
    using System.Collections.Generic;
    
    public partial class USER_INDEX
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public USER_INDEX()
        {
            this.USER_PERMISSION_MATRIX = new HashSet<USER_PERMISSION_MATRIX>();
            this.USER_STATISTICS = new HashSet<USER_STATISTICS>();
        }
    
        public int USER_ID { get; set; }
        public string CAC { get; set; }
        public string FNAME { get; set; }
        public string LNAME { get; set; }
        public int DISPLAY_INDICATOR_ID { get; set; }
    
        public virtual DISPLAY_INDICATOR_INDEX DISPLAY_INDICATOR_INDEX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USER_PERMISSION_MATRIX> USER_PERMISSION_MATRIX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USER_STATISTICS> USER_STATISTICS { get; set; }
    }
}
