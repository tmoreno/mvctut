﻿using AfloatHAMS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AfloatHAMS.Controllers
{
    public class HomeController : Controller
    {

        private HAMS9Entities db = new HAMS9Entities();

        public ActionResult Index(int? serverId)
        {

            if(serverId == null)
            {
                serverId = 1;
            }

            var server = from a in db.HULL_SERVER_INDEX where a.HULL_SERVER_ID == serverId
                         select a;

            DirectoryStructure directoryStructure = new DirectoryStructure();

            directoryStructure.HullServer = server.FirstOrDefault<HULL_SERVER_INDEX>();

            DirectoryInfo di = new DirectoryInfo(string.Format(@"C:\Users\tmoreno\work\simulated_ships\{0}\dropbox", serverId));
            directoryStructure.Files = new List<FileModel>();
            foreach(FileInfo fileInfo in di.GetFiles())
            {
                FileModel fileModel = new FileModel
                {
                    FileName = fileInfo.Name,
                    FileSize = fileInfo.Length
                };
                directoryStructure.Files.Add(fileModel);
            }

            return View(directoryStructure);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}