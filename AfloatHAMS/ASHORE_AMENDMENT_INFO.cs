//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AfloatHAMS
{
    using System;
    using System.Collections.Generic;
    
    public partial class ASHORE_AMENDMENT_INFO
    {
        public int AMENDMENT_ID { get; set; }
        public int ASHORE_PUB_LEVELS_ID { get; set; }
        public string AMENDMENT_NAME { get; set; }
        public long SIZE { get; set; }
        public string PRIORITY { get; set; }
        public string COMPULSORY { get; set; }
        public string POST_PROCESSING { get; set; }
        public string DEPENDENCIES { get; set; }
        public System.DateTime AMENDMENT_CREATION_TIMESTAMP { get; set; }
        public byte[] TIME_DATE_RECORD { get; set; }
    
        public virtual ASHORE_PUB_LEVELS ASHORE_PUB_LEVELS { get; set; }
    }
}
