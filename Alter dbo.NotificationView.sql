﻿USE [HAMS2]
GO

/****** Object: View [dbo].[NotificationView] Script Date: 11/7/2017 2:11:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[NotificationView]
	AS SELECT id, TRIM(Ship) AS [Ship], Code, convert(varchar,Date,103) as [Date] FROM Notifications 
WHERE convert(varchar,Date,103) = convert(varchar,getDate(),103) and RDate IS NULL
