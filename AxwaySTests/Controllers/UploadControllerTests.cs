﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVCTut.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MVCTut.Controllers.Tests
{
    [TestClass()]
    public class UploadControllerTests
    {
        [TestMethod()]
        public void IndexTest()
        {
            string url = "http://localhost:8000/Upload";

            using (WebClient wc = new WebClient())
            {
                wc.UploadProgressChanged += wc_UploadProgressChanged;
                wc.UploadFileCompleted += wc_Complete;
                wc.UploadFileAsync(new Uri(url), @"C:\Users\tmoreno\Downloads\test.txt");

            }
        }

        private static void wc_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {

        }

        private static void wc_Complete(object sender, AsyncCompletedEventArgs e)
        {

        }
    }
}