using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using Xunit;

namespace XUnitTestRM
{

    class TransferEntry
    {
        public string OperationIndex { get; set; }
        public string Status { get; set; }
        public bool Secure { get; set; }
        public string Filename { get; set; }
        public string Account { get; set; }
        public string Login { get; set; }
        public string Direction { get; set; }
        public string ActionBy { get; set; }
        public int? Filesize { get; set; }
        public string Protocol { get; set; }
        public DateTime StartTime { get; set; }
        public string Duration { get; set; }
        public int Id { get; set; }
        public string Hash { get; set; }
    }

    public class UnitTest1
    {
        [Fact]
        public void PostStatusTest1()
        {
            string json = JsonConvert.SerializeObject(new TransferEntry
            {
                Id = 3,
                Account = "account",
                Filename = "x.zip"
            });

            Console.WriteLine(json);
            //System.Diagnostics.Trace.WriteLine(json);

            //var content = new StringContent(json, Encoding.UTF8, "application/json");

            //HttpClient client = new HttpClient();
            //var response = client.PostAsync("http://localhost:59033/api/AUTH_HULL_APP_RELEASE", content).Result;

            //var jsonString = response.Content.ReadAsStringAsync();
            //System.Diagnostics.Trace.WriteLine(jsonString.Result);
        }
    }
}
