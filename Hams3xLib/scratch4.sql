﻿/*CREATE HAMS3 DATABASE*/
USE [master]
GO
--changes niaps version schema
DROP DATABASE IF EXISTS [HAMS3X]
CREATE DATABASE [HAMS3X]
GO

USE [HAMS3X]
GO

/*CREATE HAMS3 TABLES*/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--case matched table
CREATE TABLE [dbo].[Status](
[StatusId] [int] IDENTITY(1,1) NOT NULL,	
[StatusName] [varchar](50) NOT NULL,
CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[AshorePubLevel](
[AshorePubLevelId] [int] IDENTITY(1,1) NOT NULL,
[ServerPubId] [int] NOT NULL,
[AshorePubLevel] [int] NOT NULL,
[AshorePubReleaseTimestamp] [datetime2] NOT NULL,
[AmendmentName] [varchar](255) NOT NULL,
[AmendmentContent] [varchar](max) NOT NULL,
[Size] [bigint] NOT NULL,
[Compulsory] [varchar](50) NOT NULL,
[PostProcessingFile] [varchar](255) NOT NULL,
[Dependency] [varchar](50) NOT NULL,
[AmendmentCreationTimestamp] [datetime2] NOT NULL,
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_AshorePubLevel] PRIMARY KEY CLUSTERED 
(
[AshorePubLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[AshoreClientLevel](
[AshoreClientLevelId] [int] IDENTITY(1,1) NOT NULL,
[ServerPubId] [int] NOT NULL,
[AshoreClientLevel] [int] NOT NULL,
[PostProcessingSuccess] [varchar] (3) NOT NULL,
[AshoreClientPubExtractionTimestamp] [datetime2] NOT NULL,
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_AshoreClientLevel] PRIMARY KEY CLUSTERED 
(
[AshoreClientLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--case matched table
CREATE TABLE [dbo].[AfloatPubLevel](
[AfloatPubLevelId] [int] IDENTITY(1,1) NOT NULL,
[ServerPubId] [int] NOT NULL,
[AfloatPubLevel] [int] NOT NULL,
[AfloatPubReleaseTimestamp] [datetime2] NOT NULL,
[AmendmentName] [varchar](255) NOT NULL,
[AmendmentContent] [varchar](max) NOT NULL,
[Size] [bigint] NOT NULL,
[Compulsory] [varchar](50) NOT NULL,
[PostProcessingFile] [varchar](255) NOT NULL,
[Dependency] [varchar](50) NOT NULL,
[AmendmentCreationTimestamp] [datetime2] NOT NULL,
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_AfloatPubLevel] PRIMARY KEY CLUSTERED 
(
[AfloatPubLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[AfloatClientLevel](
[AfloatClientLevelId] [int] IDENTITY(1,1) NOT NULL,
[ServerPubId] [int] NOT NULL,
[AfloatClientLevel] [int] NOT NULL,
[PostProcessingSuccess] [varchar] (3) NOT NULL,
[AfloatClientPubExtractionTimestamp] [datetime2] NOT NULL,
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_AfloatClientLevel] PRIMARY KEY CLUSTERED 
(
[AfloatClientLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--case matched table
CREATE TABLE [dbo].[Server](
[ServerId] [int] IDENTITY(1,1) NOT NULL,
[Servername] [varchar](50) NOT NULL,
[ReturnServerCodeId] [int] NOT NULL,
CONSTRAINT [PK_Server] PRIMARY KEY CLUSTERED 
(
[ServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--case matched table
CREATE TABLE [dbo].[HullServer](
[HullServerId] [int] IDENTITY(1,1) NOT NULL,
[HullId] [int] NOT NULL,
[ServerId] [int] NOT NULL,
CONSTRAINT [PK_HullServer] PRIMARY KEY CLUSTERED 
(
[HullServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[TargetAnchor](
[TargetAnchorId] [int] IDENTITY(1,1) NOT NULL,
[TargetAnchorName] [varchar](100) NOT NULL,
CONSTRAINT [PK_TargetAnchor] PRIMARY KEY CLUSTERED 
(
[TargetAnchorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[ExecutionMode](
[ExecutionModeId] [int] IDENTITY(1,1) NOT NULL,
[ExecutionMode] [varchar](50) NOT NULL,
CONSTRAINT [PK_ExecutionMode] PRIMARY KEY CLUSTERED 
(
[ExecutionModeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--case matched table
CREATE TABLE [dbo].[Priority](
[PriorityId] [int] IDENTITY(1,1) NOT NULL,
[Priority] [varchar](50) NOT NULL,
CONSTRAINT [PK_Priority] PRIMARY KEY CLUSTERED 
(
[PriorityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--case matched table
CREATE TABLE [dbo].[Pub](
[PubId] [int] IDENTITY(1,1) NOT NULL,
[PubName] [varchar](100) NOT NULL,
[PubTypeId] [int] NOT NULL,
[PubScopeId] [int] NOT NULL,
[PubDescription] [varchar](255) NOT NULL,
[ExecutionModeId] [int] NOT NULL,
[PriorityId] [int] NOT NULL,
CONSTRAINT [PK_Pub] PRIMARY KEY CLUSTERED 
(
[PubId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[ServerPub](
[ServerPubId] [int] IDENTITY(1,1) NOT NULL,
[PubId] [int] NOT NULL,
[ServerId] [int] NOT NULL,
CONSTRAINT [PK_ServerPub] PRIMARY KEY CLUSTERED 
(
[ServerPubId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[PubType](
[PubTypeId] [int] IDENTITY(1,1) NOT NULL,
[PubTypeName] [varchar](50) NOT NULL,
CONSTRAINT [PK_PubType] PRIMARY KEY CLUSTERED 
(
[PubTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--case matched table
CREATE TABLE [dbo].[PubScope](
[PubScopeId] [int] IDENTITY(1,1) NOT NULL,	
[PubScopeName] [varchar](50) NOT NULL,
CONSTRAINT [PK_PubScope] PRIMARY KEY CLUSTERED 
(
[PubScopeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[ReturnServerCode](
[ReturnServerCodeId] [int] IDENTITY(1,1) NOT NULL,
[ReturnServerCodeName] [varchar](50) NOT NULL,
CONSTRAINT [PK_ReturnServerCode] PRIMARY KEY CLUSTERED 
(
[ReturnServerCodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/*ADDED Hull TYPE INDEX 20171016*/
--case matched table
CREATE TABLE [dbo].[HullType](
[HullTypeId] [int] IDENTITY(1,1) NOT NULL,
[HullTypeName] [varchar](50) NOT NULL,
CONSTRAINT [PK_HullType] PRIMARY KEY CLUSTERED 
(
[HullTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/*ADDED CLASS TYPE INDEX 20171016*/
--case matched table
CREATE TABLE [dbo].[ClassType](
[ClassTypeId] [int] IDENTITY(1,1) NOT NULL,
[ClassTypeName] [varchar](50) NOT NULL,
CONSTRAINT [PK_ClassType] PRIMARY KEY CLUSTERED 
(
[ClassTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[Hull](
[HullId] [int] IDENTITY(1,1) NOT NULL,	
[Hull] [varchar](16) NOT NULL,
---REMOVED ON 10/16/17 [Hull_TYPE] [varchar](10) NOT NULL,
[HullName] [varchar](50) NOT NULL,
[HullTypeId] [int] NOT NULL,---added 10/16/17
[ClassTypeId] [int] NOT NULL,---added 10/16/17
CONSTRAINT [PK_Hull] PRIMARY KEY CLUSTERED 
(
[HullId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[UserIndex](
[UserIndexId] [int] IDENTITY(1,1) NOT NULL,
[Cac] [varchar](50) NOT NULL,
[FirstName] [varchar](100) NOT NULL,
[LastName] [varchar](100) NOT NULL,
CONSTRAINT [PK_UserIndex] PRIMARY KEY CLUSTERED 
(
[UserIndexId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[UserStatistic](
[UserStatisticId] [int] IDENTITY(1,1) NOT NULL,
[UserPermissionId] [int] NOT NULL,
[AccessedTimestamp] [datetime2] NOT NULL,
CONSTRAINT [PK_UserStatistic] PRIMARY KEY CLUSTERED 
(
[UserStatisticId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[Permission](
[ModuleId] [int] IDENTITY(1,1) NOT NULL,
[ModuleName] [varchar](100) NOT NULL,
CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
[ModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--case matched table
CREATE TABLE [dbo].[UserPermission](
[UserPermissionId] [int] IDENTITY(1,1) NOT NULL,
[UserIndexId] [int] NOT NULL,
[ModuleId] [int] NOT NULL,
CONSTRAINT [PK_UserPermission] PRIMARY KEY CLUSTERED 
(
[UserPermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---APP SCHEMA
---AUTHORIZED APP VENDORS
--case matched table
CREATE TABLE [dbo].[AuthAppVendor](
[AuthAppVendorId] [int] IDENTITY(1,1) NOT NULL,
[AuthAppVendorName] [varchar](50) NOT NULL,--authorized app vendor name
CONSTRAINT [PK_AuthAppVendor] PRIMARY KEY CLUSTERED 
(
[AuthAppVendorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---AUTHORIZED APP INDEX
--case matched table
CREATE TABLE [dbo].[AuthApp](
[AuthAppId] [int] IDENTITY(1,1) NOT NULL,
[AuthAppName][varchar](50) NOT NULL,--authorized app name
[AuthAppVendorId] [int] NOT NULL,
CONSTRAINT [PK_AuthApp] PRIMARY KEY CLUSTERED 
(
[AuthAppId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---AUTHORIZED RELEASED APP INDEX
--case matched table
CREATE TABLE [dbo].[AuthAppRelease](
[AuthAppReleaseId] [int] IDENTITY(1,1) NOT NULL,
[AuthAppId] [int] NOT NULL,
[AuthVersion] [varchar](50) NOT NULL,--released version
[AuthNumber] [varchar](50) NOT NULL,--release number
[AuthAppReleaseDate] [datetime2] NOT NULL,--date version was released
[TimeDateRecord] [datetime2] NOT NULL,--date record was updated
CONSTRAINT [PK_AuthAppRelease] PRIMARY KEY CLUSTERED 
(
[AuthAppReleaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---AUTHORIZED Hull RELEASED APP INDEX
--case matched table
CREATE TABLE [dbo].[AuthAppServerRelease](
[AuthAppServerReleaseId] [int] IDENTITY(1,1) NOT NULL,
[ServerId] [int] NOT NULL, --hull app+version was released to
[AuthAppReleaseId] [int] NOT NULL,--identifier for released app+version
[ServerAppReleaseDate] [datetime2] NOT NULL,--date version was released to hull
[TimeDateRecord] [datetime2] NOT NULL,--date record was updated
CONSTRAINT [PK_AuthAppServerRelease] PRIMARY KEY CLUSTERED 
(
[AuthAppServerReleaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---Installed_Afloat APP VENDORS
--case matched table
CREATE TABLE [dbo].[InstalledAfloatAppVendor](
[InstalledAfloatAppVendorId] [int] IDENTITY(1,1) NOT NULL,
[InstalledAfloatAppVendorName] [varchar](50) NOT NULL,--afloat installed app vendor name
CONSTRAINT [PK_InstalledAfloatAppVendor] PRIMARY KEY CLUSTERED 
(
[InstalledAfloatAppVendorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---Afloat APP INDEX
--case matched table
CREATE TABLE [dbo].[AfloatApp](
[AfloatAppId] [int] IDENTITY(1,1) NOT NULL,
[AfloatAppName][varchar](50) NOT NULL,--afloat installed app name
[InstalledAfloatAppVendorId] [int] NOT NULL,
CONSTRAINT [PK_AfloatApp] PRIMARY KEY CLUSTERED 
(
[AfloatAppId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--AfloatAppVersion
--case matched table
CREATE TABLE [dbo].[AfloatAppVersion](
[AfloatAppVersionId] [int] IDENTITY(1,1) NOT NULL,
[AfloatAppId] [int] NOT NULL,
[AfloatVersion] [varchar](50) NOT NULL,--afloat version
[TimeDateRecord] [datetime2] NOT NULL,--date record was updated
CONSTRAINT [PK_AfloatAppVersion] PRIMARY KEY CLUSTERED 
(
[AfloatAppVersionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---Afloat Hull APP Installed
--case matched table
CREATE TABLE [dbo].[InstalledAfloatAppServer](
[InstalledAfloatAppServerId] [int] IDENTITY(1,1) NOT NULL,
[ServerId] [int] NOT NULL,--hull apps were installed on
[AfloatAppVersionId] [int] NOT NULL,--installed app version
[InstalledDate] [datetime2] NOT NULL,--date version was installed
[TimeDateRecord] [datetime2] NOT NULL,--date record was updated
CONSTRAINT [PK_InstalledAfloatAppServer] PRIMARY KEY CLUSTERED 
(
[InstalledAfloatAppServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--COMPLIANCE STATUS DATA DICTIONARY
--case matched table
CREATE TABLE [dbo].[ComplianceStatus](
[ComplianceStatusId] [int] IDENTITY(1,1) NOT NULL,
[ComplianceStatusName] [varchar](50) NOT NULL,
CONSTRAINT [PK_ComplianceStatus] PRIMARY KEY CLUSTERED 
(
[ComplianceStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---ServerAppVersionMonitoring
--case matched table
CREATE TABLE [dbo].[ServerAppVersionMonitoring](
[ServerAppVersionMonitoringId] [int] IDENTITY(1,1) NOT NULL,
[ServerId] [int] NOT NULL,--hull+server identifier
[AuthAppServerReleaseId] [int],--authorized applications for hull+server
[InstalledAfloatAppServerId] [int],--applicaitons installed on hull+server
[ComplianceStatusId] [int] NOT NULL,
[Variance] [int],--version difference
[TimeDateRecord] [datetime2] NOT NULL,--date record was updated
CONSTRAINT [PK_ServerAppVersionMonitoring] PRIMARY KEY CLUSTERED 
(
[ServerAppVersionMonitoringId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--STORED UNAUTHORIZED PROGRAMS THAT HAVE BEEN Installed
--case matched table
/*
CREATE TABLE [dbo].[UnauthorizedProgram](
[UnauthorizedProgramId] [int] IDENTITY(1,1) NOT NULL,
[InstalledAfloatAppServerId] [int] NOT NULL,
[ComplianceStatusId] [int] NOT NULL,
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_UnauthorizedProgram] PRIMARY KEY CLUSTERED 
(
[UnauthorizedProgramId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
*/

CREATE TABLE [dbo].[BusinessRuleConditionType](
[BusinessRuleConditionTypeId] [int] IDENTITY(1,1) NOT NULL,
[BusinessRuleConditionTypeName] [varchar](50) NOT NULL,
CONSTRAINT [PK_BusinessRuleConditionType] PRIMARY KEY CLUSTERED 
(
[BusinessRuleConditionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[BusinessRuleOperator](
[BusinessRuleOperatorId] [int] IDENTITY(1,1) NOT NULL,
[BusinessRuleOperatorName] [varchar](50)  NOT NULL,
CONSTRAINT [PK_BusinessRuleOperator] PRIMARY KEY CLUSTERED 
(
[BusinessRuleOperatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[BusinessRuleTargetArea](
[BusinessRuleTargetAreaId] [int] IDENTITY(1,1) NOT NULL,
[BusinessRuleTargetAreaGroup] [varchar](50) NOT NULL,
[BusinessRuleTargetAreaTable] [varchar](50) NOT NULL,
[BusinessRuleTargetAreaField] [varchar](50) NOT NULL,
[BusinessRuleTargetAreaLevelId] int NOT NULL,
CONSTRAINT [PK_BusinessRuleTargetArea] PRIMARY KEY CLUSTERED 
(
[BusinessRuleTargetAreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[BusinessRuleTargetAreaLevel](
[BusinessRuleTargetAreaLevelId] [int] IDENTITY(1,1) NOT NULL,
[BusinessRuleTargetAreaLevelName] [varchar](50) NOT NULL,
CONSTRAINT [PK_BusinessRuleTargetAreaLevelId] PRIMARY KEY CLUSTERED 
(
[BusinessRuleTargetAreaLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[BusinessRuleSensitivityType](
[BusinessRuleSensitivityTypeId] [int] IDENTITY(1,1) NOT NULL,
[BusinessRuleSensitivityTypeName] [varchar](50)  NOT NULL,
CONSTRAINT [PK_BusinessRuleSensitivityType] PRIMARY KEY CLUSTERED 
(
[BusinessRuleSensitivityTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[BusinessRuleType](
[BusinessRuleTypeId] [int] IDENTITY(1,1) NOT NULL,
[BusinessRuleTypeName] [varchar](50)  NOT NULL,
CONSTRAINT [PK_BusinessRuleType] PRIMARY KEY CLUSTERED 
(
[BusinessRuleTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[BusinessRule](
[BusinessRuleId] [int] IDENTITY(1,1) NOT NULL,
[BusinessRuleTypeId] [int],
[BusinessRuleConditionTypeId] [int] NOT NULL,
[BusinessRuleOperatorId] [int] NOT NULL,
[BusinessRuleSensitivityTypeId] [int],
[StatusId] [int],
[ConditionOne] [varchar](100) NOT NULL,
[Sensitivity] [varchar](100),
[ConditionTwo] [varchar] (100),
CONSTRAINT [PK_BusinessRule] PRIMARY KEY CLUSTERED 
(
[BusinessRuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[StatusUser](
[StatusUserId] [int] IDENTITY(1,1) NOT NULL,
[BusinessRuleUserId] [int] NOT NULL,
[TargetAnchorId] [int] NOT NULL,
[TargetAnchorDetail] [int]	NOT NULL,
CONSTRAINT [PK_StatusUser] PRIMARY KEY CLUSTERED 
(
[StatusUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--updated on 20180204
CREATE TABLE [dbo].[BusinessRuleUser](
[BusinessRuleUserId] [int] IDENTITY(1,1) NOT NULL,
[UserPermissionId] [int] NOT NULL,
[BusinessRuleGroupId] [int] NOT NULL,
[BusinessRulePriority] [int] NOT NULL,
[BusinessRuleOneTargetAreaId] [int] NOT NULL,
[BusinessRuleOneTargetAreaTwoId] [int] NOT NULL,
[BusinessRuleTwoTargetAreaId] [int] NOT NULL,
CONSTRAINT [PK_BusinessRuleUser] PRIMARY KEY CLUSTERED 
(
[BusinessRuleUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--added on 20180204
CREATE TABLE [dbo].[BusinessRuleGroup](
[BusinessRuleGroupId] [int] IDENTITY(1,1) NOT NULL,
[BusinessRuleOneId] [int] NOT NULL,
[BusinessRuleTwoId] [int],
CONSTRAINT [PK_BusinessRuleGroup] PRIMARY KEY CLUSTERED 
(
[BusinessRuleGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--updated on 20180204
CREATE TABLE [dbo].[DsUpdateActivity](
[DsUpdateActivityId] [int] IDENTITY(1,1) NOT NULL,
[ServerId] [int] NOT NULL,
[ContactTimestamp] [datetime2] NOT NULL,
[Activity] [varchar](255) NOT NULL,
[Executor] [varchar](50) NOT NULL,
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_DsUpdateActivity] PRIMARY KEY CLUSTERED 
(
[DsUpdateActivityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--updated on 20180204
CREATE TABLE [dbo].[CertMonitoring](
[CertMonitoringId] [int] IDENTITY(1,1) NOT NULL,
[ServerId] [int] NOT NULL,
[DaysToExpire] [int] NOT NULL,
[NotBefore] [datetime2] NOT NULL,
[NotAfter] [datetime2] NOT NULL,
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_CertMonitoring] PRIMARY KEY CLUSTERED 
(
[CertMonitoringId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--niaps version release table
CREATE TABLE [dbo].[NiapsVersionRelease](
[NiapsVersionReleaseId] [int] IDENTITY(1,1) NOT NULL,
[ReleaseVersion] [varchar](50) NOT NULL,
[ReleaseNumber] [varchar](50) NOT NULL,
[ReleaseDate] [datetime2] NOT NULL,
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_NiapsVersionRelease] PRIMARY KEY CLUSTERED 
(
[NiapsVersionReleaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--updated 20180204
--installed niaps versions table
CREATE TABLE [dbo].[InstalledNiapsVersion](
[InstalledNiapsVersionId] [int] IDENTITY(1,1) NOT NULL,
[ServerId] [int] NOT NULL,
[NiapsVersionReleaseId] [int] NOT NULL,
[InstalledTimestamp] [datetime2] NOT NULL,
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_InstalledNiapsVersion] PRIMARY KEY CLUSTERED 
(
[InstalledNiapsVersionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[NiapsVersionServerRelease](
[NiapsVersionServerReleaseId] [int] IDENTITY(1,1) NOT NULL,
[ServerId] [int] NOT NULL,
[NiapsVersionReleaseId] [int] NOT NULL,
[ReleaseDate] [datetime2] NOT NULL,
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_NiapsVersionServerRelease] PRIMARY KEY CLUSTERED 
(
[NiapsVersionServerReleaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--updated 20180204
--niaps version derived compliance table
CREATE TABLE [dbo].[NiapsVersionMonitoring](
[NiapsVersionMonitoringId] [int] IDENTITY(1,1) NOT NULL,
[ServerId] [int] NOT NULL,
[NiapsVersionServerReleaseId] [int],
[AuthVersionBehindCount] [int],
[Descriptor] [varchar](100),
[TimeDateRecord] [datetime2] NOT NULL,
CONSTRAINT [PK_NiapsVersionMonitoring] PRIMARY KEY CLUSTERED 
(
[NiapsVersionMonitoringId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--updated on 20180204
--case matched table
CREATE TABLE [dbo].[Statistic](
[StatisticId] [int] IDENTITY(1,1) NOT NULL,
[StatisticName] [varchar](100) NOT NULL,
CONSTRAINT [PK_Statistic] PRIMARY KEY CLUSTERED 
(
[StatisticId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--added on 20180204
CREATE TABLE [dbo].[StatisticServer](
[StatisticServerId] [int] IDENTITY(1,1) NOT NULL,
[ServerId] [int] NOT NULL,
[StatisticId] [int] NOT NULL,
CONSTRAINT [PK_StatisticServer] PRIMARY KEY CLUSTERED 
(
[StatisticServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--added on 20180204
CREATE TABLE [dbo].[UsageStatisticServer](
[UsageStatisticServerId] [int] IDENTITY(1,1) NOT NULL,
[StatisticServerId] [int] NOT NULL,
[Data] [varchar](255) NOT NULL,
[DataDate] datetime2 NOT NULL,
[TimeDateRecord] datetime2 NOT NULL,
CONSTRAINT [PK_UsageStatisticServer] PRIMARY KEY CLUSTERED 
(
[UsageStatisticServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--updated on 20180204
CREATE TABLE [dbo].[Drive](
[DriveId] [int] IDENTITY(1,1) NOT NULL,
[DriveLetter] [varchar](1) NOT NULL,
CONSTRAINT [PK_Drive] PRIMARY KEY CLUSTERED 
(
[DriveId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--added on 20180204
CREATE TABLE [dbo].[DriveServer](
[DriveServerId] [int] IDENTITY(1,1) NOT NULL,
[DriveId] [int] NOT NULL,
[ServerId] [int] NOT NULL,
CONSTRAINT [PK_DriveServer] PRIMARY KEY CLUSTERED 
(
[DriveServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--added on 20180204 
CREATE TABLE [dbo].[StatisticDriveServer](
[StatisticDriveServerId] [int] IDENTITY(1,1) NOT NULL,
[DriveServerId] [int] NOT NULL,
[DriveSize] [int] NOT NULL,
[FreeSpace] [int] NOT NULL,
[DataDate] datetime2 NOT NULL,
[TimeDateRecord] datetime2 NOT NULL,
CONSTRAINT [PK_StatisticDriveServer] PRIMARY KEY CLUSTERED 
(
[StatisticDriveServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--added on 20180204
ALTER TABLE [dbo].[DriveServer]  WITH CHECK ADD  CONSTRAINT [FK_DriveServer_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[DriveServer] CHECK CONSTRAINT [FK_DriveServer_TO_Server]
GO

--added on 20180204
ALTER TABLE [dbo].[DriveServer]  WITH CHECK ADD  CONSTRAINT [FK_DriveServer_TO_Drive] FOREIGN KEY([DriveId])
REFERENCES [dbo].[Drive] ([DriveId])
GO
ALTER TABLE [dbo].[DriveServer] CHECK CONSTRAINT [FK_DriveServer_TO_Drive]
GO

--added on 20180204
ALTER TABLE [dbo].[StatisticDriveServer]  WITH CHECK ADD  CONSTRAINT [FK_StatisticDriveServer_TO_DriveServer] FOREIGN KEY([DriveServerId])
REFERENCES [dbo].[DriveServer] ([DriveServerId])
GO
ALTER TABLE [dbo].[StatisticDriveServer] CHECK CONSTRAINT [FK_StatisticDriveServer_TO_DriveServer]
GO

--added on 20180204
ALTER TABLE [dbo].[StatisticServer]  WITH CHECK ADD  CONSTRAINT [FK_StatisticServer_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[StatisticServer] CHECK CONSTRAINT [FK_StatisticServer_TO_Server]
GO

--added on 20180204
ALTER TABLE [dbo].[StatisticServer]  WITH CHECK ADD  CONSTRAINT [FK_StatisticServer_TO_Statistic] FOREIGN KEY([StatisticId])
REFERENCES [dbo].[Statistic] ([StatisticId])
GO
ALTER TABLE [dbo].[StatisticServer] CHECK CONSTRAINT [FK_StatisticServer_TO_Statistic]
GO

--added on 20180204
ALTER TABLE [dbo].[UsageStatisticServer]  WITH CHECK ADD  CONSTRAINT [FK_UsageStatisticServer_TO_StatisticServer] FOREIGN KEY([StatisticServerId])
REFERENCES [dbo].[StatisticServer] ([StatisticServerId])
GO
ALTER TABLE [dbo].[UsageStatisticServer] CHECK CONSTRAINT [FK_UsageStatisticServer_TO_StatisticServer]
GO

--updated 20180204
ALTER TABLE [dbo].[InstalledNiapsVersion]  WITH NOCHECK ADD  CONSTRAINT [FK_InstalledNiapsVersion_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[InstalledNiapsVersion] CHECK CONSTRAINT [FK_InstalledNiapsVersion_TO_Server]
GO

--updated 20180204
ALTER TABLE [dbo].[NiapsVersionServerRelease]  WITH NOCHECK ADD  CONSTRAINT [FK_NiapsVersionServerRelease_TO_NiapsVersionRelease] FOREIGN KEY([NiapsVersionReleaseId])
REFERENCES [dbo].[NiapsVersionRelease] ([NiapsVersionReleaseId])
GO
ALTER TABLE [dbo].[NiapsVersionServerRelease] CHECK CONSTRAINT [FK_NiapsVersionServerRelease_TO_NiapsVersionRelease]
GO

--updated 20180204
ALTER TABLE [dbo].[NiapsVersionServerRelease]  WITH NOCHECK ADD  CONSTRAINT [FK_NiapsVersionServerRelease_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[NiapsVersionServerRelease] CHECK CONSTRAINT [FK_NiapsVersionServerRelease_TO_Server]
GO

--updated 20180204
ALTER TABLE [dbo].[NiapsVersionMonitoring]  WITH NOCHECK ADD  CONSTRAINT [FK_NiapsVersionMonitoring_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[NiapsVersionMonitoring] CHECK CONSTRAINT [FK_NiapsVersionMonitoring_TO_Server]
GO

--updated 20180204
ALTER TABLE [dbo].[NiapsVersionMonitoring]  WITH NOCHECK ADD  CONSTRAINT [FK_NiapsVersionMonitoring_TO_NiapsVersionServerRelease] FOREIGN KEY([NiapsVersionServerReleaseId])
REFERENCES [dbo].[NiapsVersionServerRelease] ([NiapsVersionServerReleaseId])
GO
ALTER TABLE [dbo].[NiapsVersionMonitoring] CHECK CONSTRAINT [FK_NiapsVersionMonitoring_TO_NiapsVersionServerRelease]
GO

--updated on 20180204
ALTER TABLE [dbo].[CertMonitoring]  WITH CHECK ADD  CONSTRAINT [FK_CertMonitoring_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[CertMonitoring] CHECK CONSTRAINT [FK_CertMonitoring_TO_Server]
GO
--updated on 20180204
ALTER TABLE [dbo].[DsUpdateActivity]  WITH CHECK ADD  CONSTRAINT [FK_DsUpdateActivity_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[DsUpdateActivity] CHECK CONSTRAINT [FK_DsUpdateActivity_TO_Server]
GO
--updated on 20180204
ALTER TABLE [dbo].[BusinessRuleUser]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRuleUser_TO_BusinessRuleGroup] FOREIGN KEY([BusinessRuleGroupId])
REFERENCES [dbo].[BusinessRuleGroup] ([BusinessRuleGroupId])
GO
ALTER TABLE [dbo].[BusinessRuleUser] CHECK CONSTRAINT [FK_BusinessRuleUser_TO_BusinessRuleGroup]
GO
--added on 20180204
ALTER TABLE [dbo].[BusinessRuleGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRuleGroup_TO_BusinessRuleOneId] FOREIGN KEY([BusinessRuleOneId])
REFERENCES [dbo].[BusinessRule] ([BusinessRuleId])
GO
ALTER TABLE [dbo].[BusinessRuleGroup] CHECK CONSTRAINT [FK_BusinessRuleGroup_TO_BusinessRuleOneId]
GO
--added on 20180204
ALTER TABLE [dbo].[BusinessRuleGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRuleGroup_TO_BusinessRuleTwoId] FOREIGN KEY([BusinessRuleTwoId])
REFERENCES [dbo].[BusinessRule] ([BusinessRuleId])
GO
ALTER TABLE [dbo].[BusinessRuleGroup] CHECK CONSTRAINT [FK_BusinessRuleGroup_TO_BusinessRuleTwoId]
GO

ALTER TABLE [dbo].[InstalledNiapsVersion]  WITH NOCHECK ADD  CONSTRAINT [FK_InstalledNiapsVersion_TO_NiapsVersionRelease] FOREIGN KEY([NiapsVersionReleaseId])
REFERENCES [dbo].[NiapsVersionRelease] ([NiapsVersionReleaseId])
GO
ALTER TABLE [dbo].[InstalledNiapsVersion] CHECK CONSTRAINT [FK_InstalledNiapsVersion_TO_NiapsVersionRelease]
GO

ALTER TABLE [dbo].[BusinessRule]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRule_TO_BusinessRuleType] FOREIGN KEY([BusinessRuleTypeId])
REFERENCES [dbo].[BusinessRuleType] ([BusinessRuleTypeId])
GO
ALTER TABLE [dbo].[BusinessRule] CHECK CONSTRAINT [FK_BusinessRule_TO_BusinessRuleType]
GO

ALTER TABLE [dbo].[BusinessRule]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRule_TO_BusinessRuleConditionType] FOREIGN KEY([BusinessRuleConditionTypeId])
REFERENCES [dbo].[BusinessRuleConditionType] ([BusinessRuleConditionTypeId])
GO
ALTER TABLE [dbo].[BusinessRule] CHECK CONSTRAINT [FK_BusinessRule_TO_BusinessRuleConditionType]
GO

ALTER TABLE [dbo].[BusinessRule]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRule_TO_BusinessRuleOperator] FOREIGN KEY([BusinessRuleOperatorId])
REFERENCES [dbo].[BusinessRuleOperator] ([BusinessRuleOperatorId])
GO
ALTER TABLE [dbo].[BusinessRule] CHECK CONSTRAINT [FK_BusinessRule_TO_BusinessRuleOperator]
GO

ALTER TABLE [dbo].[BusinessRule]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRule_TO_BusinessRuleSensitivityType] FOREIGN KEY([BusinessRuleSensitivityTypeId])
REFERENCES [dbo].[BusinessRuleSensitivityType] ([BusinessRuleSensitivityTypeId])
GO
ALTER TABLE [dbo].[BusinessRule] CHECK CONSTRAINT [FK_BusinessRule_TO_BusinessRuleSensitivityType]
GO

ALTER TABLE [dbo].[BusinessRule]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRule_TO_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([StatusId])
GO
ALTER TABLE [dbo].[BusinessRule] CHECK CONSTRAINT [FK_BusinessRule_TO_Status]
GO

ALTER TABLE [dbo].[BusinessRuleUser]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRuleUser_TO_UserPermission] FOREIGN KEY([UserPermissionId])
REFERENCES [dbo].[UserPermission] ([UserPermissionId])
GO
ALTER TABLE [dbo].[BusinessRuleUser] CHECK CONSTRAINT [FK_BusinessRuleUser_TO_UserPermission]
GO

ALTER TABLE [dbo].[StatusUser]  WITH NOCHECK ADD  CONSTRAINT [FK_StatusUser_TO_BusinessRuleUser] FOREIGN KEY([BusinessRuleUserId])
REFERENCES [dbo].[BusinessRuleUser] ([BusinessRuleUserId])
GO
ALTER TABLE [dbo].[StatusUser] CHECK CONSTRAINT [FK_StatusUser_TO_BusinessRuleUser]
GO

ALTER TABLE [dbo].[StatusUser]  WITH NOCHECK ADD  CONSTRAINT [FK_StatusUser_TO_TargetAnchor] FOREIGN KEY([TargetAnchorId])
REFERENCES [dbo].[TargetAnchor] ([TargetAnchorId])
GO
ALTER TABLE [dbo].[StatusUser] CHECK CONSTRAINT [FK_StatusUser_TO_TargetAnchor]
GO

ALTER TABLE [dbo].[BusinessRuleUser]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRuleUser_TO_BusinessRuleOneTargetArea] FOREIGN KEY([BusinessRuleOneTargetAreaId])
REFERENCES [dbo].[BusinessRuleTargetArea] ([BusinessRuleTargetAreaId])
GO
ALTER TABLE [dbo].[BusinessRuleUser] CHECK CONSTRAINT [FK_BusinessRuleUser_TO_BusinessRuleOneTargetArea]
GO

ALTER TABLE [dbo].[BusinessRuleUser]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRuleUser_TO_BusinessRuleOneTargetAreaTwo] FOREIGN KEY([BusinessRuleOneTargetAreaTwoId])
REFERENCES [dbo].[BusinessRuleTargetArea] ([BusinessRuleTargetAreaId])
GO
ALTER TABLE [dbo].[BusinessRuleUser] CHECK CONSTRAINT [FK_BusinessRuleUser_TO_BusinessRuleOneTargetAreaTwo]
GO

ALTER TABLE [dbo].[BusinessRuleUser]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRuleUser_TO_BusinessRuleTwoTargetArea] FOREIGN KEY([BusinessRuleTwoTargetAreaId])
REFERENCES [dbo].[BusinessRuleTargetArea] ([BusinessRuleTargetAreaId])
GO
ALTER TABLE [dbo].[BusinessRuleUser] CHECK CONSTRAINT [FK_BusinessRuleUser_TO_BusinessRuleTwoTargetArea]
GO

ALTER TABLE [dbo].[BusinessRuleTargetArea]  WITH NOCHECK ADD  CONSTRAINT [FK_BusinessRuleTargetArea_TO_BusinessRuleTargetAreaLevel] FOREIGN KEY([BusinessRuleTargetAreaLevelId])
REFERENCES [dbo].[BusinessRuleTargetAreaLevel] ([BusinessRuleTargetAreaLevelId])
GO
ALTER TABLE [dbo].[BusinessRuleTargetArea] CHECK CONSTRAINT [FK_BusinessRuleTargetArea_TO_BusinessRuleTargetAreaLevel]
GO

ALTER TABLE [dbo].[AshorePubLevel]  WITH NOCHECK ADD  CONSTRAINT [FK_AshorePubLevel_TO_ServerPub] FOREIGN KEY([ServerPubId])
REFERENCES [dbo].[ServerPub] ([ServerPubId])
GO
ALTER TABLE [dbo].[AshorePubLevel] CHECK CONSTRAINT [FK_AshorePubLevel_TO_ServerPub]
GO

ALTER TABLE [dbo].[AshoreClientLevel]  WITH NOCHECK ADD  CONSTRAINT [FK_AshoreClientLevel_TO_ServerPub] FOREIGN KEY([ServerPubId])
REFERENCES [dbo].[ServerPub] ([ServerPubId])
GO
ALTER TABLE [dbo].[AshoreClientLevel] CHECK CONSTRAINT [FK_AshoreClientLevel_TO_ServerPub]
GO

ALTER TABLE [dbo].[AfloatPubLevel]  WITH NOCHECK ADD  CONSTRAINT [FK_AfloatPubLevel_TO_ServerPub] FOREIGN KEY([ServerPubId])
REFERENCES [dbo].[ServerPub] ([ServerPubId])
GO
ALTER TABLE [dbo].[AfloatPubLevel] CHECK CONSTRAINT [FK_AfloatPubLevel_TO_ServerPub]
GO

ALTER TABLE [dbo].[AfloatClientLevel]  WITH NOCHECK ADD  CONSTRAINT [FK_AfloatClientLevel_TO_ServerPub] FOREIGN KEY([ServerPubId])
REFERENCES [dbo].[ServerPub] ([ServerPubId])
GO
ALTER TABLE [dbo].[AfloatClientLevel] CHECK CONSTRAINT [FK_AfloatClientLevel_TO_ServerPub]
GO

ALTER TABLE [dbo].[ServerPub]  WITH NOCHECK ADD  CONSTRAINT [FK_ServerPub_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[ServerPub] CHECK CONSTRAINT [FK_ServerPub_TO_Server]
GO

ALTER TABLE [dbo].[ServerPub]  WITH NOCHECK ADD  CONSTRAINT [FK_ServerPub_TO_Pub] FOREIGN KEY([PubId])
REFERENCES [dbo].[Pub] ([PubId])
GO
ALTER TABLE [dbo].[ServerPub] CHECK CONSTRAINT [FK_ServerPub_TO_Pub]
GO

ALTER TABLE [dbo].[HullServer]  WITH NOCHECK ADD  CONSTRAINT [FK_HullServer_TO_Hull] FOREIGN KEY([HullId])
REFERENCES [dbo].[Hull] ([HullId])
GO
ALTER TABLE [dbo].[HullServer] CHECK CONSTRAINT [FK_HullServer_TO_Hull]
GO

ALTER TABLE [dbo].[HullServer]  WITH NOCHECK ADD  CONSTRAINT [FK_HullServer_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[HullServer] CHECK CONSTRAINT [FK_HullServer_TO_Server]
GO

/*ADDED LINKS FOR Hull TYPE AND CLASS TYPE INDEX ON 10/16/17*/
ALTER TABLE [dbo].[Hull]  WITH NOCHECK ADD  CONSTRAINT [FK_Hull_TO_HullType] FOREIGN KEY([HullTypeId])
REFERENCES [dbo].[HullType] ([HullTypeId])
GO
ALTER TABLE [dbo].[Hull] CHECK CONSTRAINT [FK_Hull_TO_HullType]
GO

ALTER TABLE [dbo].[Hull]  WITH NOCHECK ADD  CONSTRAINT [FK_Hull_TO_ClassType] FOREIGN KEY([ClassTypeId])
REFERENCES [dbo].[ClassType] ([ClassTypeId])
GO
ALTER TABLE [dbo].[Hull] CHECK CONSTRAINT [FK_Hull_TO_ClassType]
GO
/*END OF CHANGES FOR Hull TYPE AND CLASS TYPE INDEX ON 10/16/17*/

ALTER TABLE [dbo].[UserPermission]  WITH NOCHECK ADD  CONSTRAINT [FK_UserPermission_TO_UserIndex] FOREIGN KEY([UserIndexId])
REFERENCES [dbo].[UserIndex] ([UserIndexId])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermission_TO_UserIndex]
GO

ALTER TABLE [dbo].[UserPermission]  WITH NOCHECK ADD  CONSTRAINT [FK_UserPermission_TO_Permission] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[Permission] ([ModuleId])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermission_TO_Permission]
GO

ALTER TABLE [dbo].[UserStatistic]  WITH NOCHECK ADD  CONSTRAINT [FK_UserStatistic_TO_UserPermission] FOREIGN KEY([UserPermissionId])
REFERENCES [dbo].[UserPermission] ([UserPermissionId])
GO
ALTER TABLE [dbo].[UserStatistic] CHECK CONSTRAINT [FK_UserStatistic_TO_UserPermission]
GO

ALTER TABLE [dbo].[Pub]  WITH NOCHECK ADD  CONSTRAINT [FK_Pub_TO_PubType] FOREIGN KEY([PubTypeId])
REFERENCES [dbo].[PubType] ([PubTypeId])
GO
ALTER TABLE [dbo].[Pub] CHECK CONSTRAINT [FK_Pub_TO_PubType]
GO

ALTER TABLE [dbo].[Pub]  WITH NOCHECK ADD  CONSTRAINT [FK_Pub_TO_PubScope] FOREIGN KEY([PubScopeId])
REFERENCES [dbo].[PubScope] ([PubScopeId])
GO
ALTER TABLE [dbo].[Pub] CHECK CONSTRAINT [FK_Pub_TO_PubScope]
GO

ALTER TABLE [dbo].[Server]  WITH NOCHECK ADD  CONSTRAINT [FK_Server_TO_ReturnServerCode] FOREIGN KEY([ReturnServerCodeId])
REFERENCES [dbo].[ReturnServerCode] ([ReturnServerCodeId])
GO
ALTER TABLE [dbo].[Server] CHECK CONSTRAINT [FK_Server_TO_ReturnServerCode]
GO

ALTER TABLE [dbo].[Pub]  WITH NOCHECK ADD  CONSTRAINT [FK_Pub_TO_ExecutionMode] FOREIGN KEY([ExecutionModeId])
REFERENCES [dbo].[ExecutionMode] ([ExecutionModeId])
GO
ALTER TABLE [dbo].[Pub] CHECK CONSTRAINT [FK_Pub_TO_ExecutionMode]
GO

ALTER TABLE [dbo].[Pub]  WITH NOCHECK ADD  CONSTRAINT [FK_Pub_TO_Priority] FOREIGN KEY([PriorityId])
REFERENCES [dbo].[Priority] ([PriorityId])
GO
ALTER TABLE [dbo].[Pub] CHECK CONSTRAINT [FK_Pub_TO_Priority]
GO

ALTER TABLE [dbo].[AuthApp]  WITH NOCHECK ADD  CONSTRAINT [FK_AuthApp_TO_AuthAppVendor] FOREIGN KEY([AuthAppVendorId])
REFERENCES [dbo].[AuthAppVendor] ([AuthAppVendorId])
GO
ALTER TABLE [dbo].[AuthApp] CHECK CONSTRAINT [FK_AuthApp_TO_AuthAppVendor]
GO

ALTER TABLE [dbo].[AuthAppRelease]  WITH NOCHECK ADD  CONSTRAINT [FK_AuthAppRelease_TO_AuthApp] FOREIGN KEY([AuthAppId])
REFERENCES [dbo].[AuthApp] ([AuthAppId])
GO
ALTER TABLE [dbo].[AuthAppRelease] CHECK CONSTRAINT [FK_AuthAppRelease_TO_AuthApp]
GO

ALTER TABLE [dbo].[AuthAppServerRelease]  WITH NOCHECK ADD  CONSTRAINT [FK_AuthAppServerRelease_TO_AuthAppRelease] FOREIGN KEY([AuthAppReleaseId])
REFERENCES [dbo].[AuthAppRelease] ([AuthAppReleaseId])
GO
ALTER TABLE [dbo].[AuthAppServerRelease] CHECK CONSTRAINT [FK_AuthAppServerRelease_TO_AuthAppRelease]
GO

ALTER TABLE [dbo].[AuthAppServerRelease]  WITH NOCHECK ADD  CONSTRAINT [FK_AuthAppServerRelease_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[AuthAppServerRelease] CHECK CONSTRAINT [FK_AuthAppServerRelease_TO_Server]
GO

ALTER TABLE [dbo].[AfloatApp]  WITH NOCHECK ADD  CONSTRAINT [FK_AfloatApp_TO_InstalledAfloatAppVendor] FOREIGN KEY([InstalledAfloatAppVendorId])
REFERENCES [dbo].[InstalledAfloatAppVendor] ([InstalledAfloatAppVendorId])
GO
ALTER TABLE [dbo].[AfloatApp] CHECK CONSTRAINT [FK_AfloatApp_TO_InstalledAfloatAppVendor]
GO

ALTER TABLE [dbo].[AfloatAppVersion]  WITH NOCHECK ADD  CONSTRAINT [FK_AfloatAppVersion_TO_AfloatApp] FOREIGN KEY([AfloatAppId])
REFERENCES [dbo].[AfloatApp] ([AfloatAppId])
GO
ALTER TABLE [dbo].[AfloatAppVersion] CHECK CONSTRAINT [FK_AfloatAppVersion_TO_AfloatApp]
GO

ALTER TABLE [dbo].[InstalledAfloatAppServer]  WITH NOCHECK ADD  CONSTRAINT [FK_InstalledAfloatAppServer_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[InstalledAfloatAppServer] CHECK CONSTRAINT [FK_InstalledAfloatAppServer_TO_Server]
GO

ALTER TABLE [dbo].[InstalledAfloatAppServer]  WITH NOCHECK ADD  CONSTRAINT [FK_InstalledAfloatAppServer_TO_AfloatAppVersion] FOREIGN KEY([AfloatAppVersionId])
REFERENCES [dbo].[AfloatAppVersion] ([AfloatAppVersionId])
GO
ALTER TABLE [dbo].[InstalledAfloatAppServer] CHECK CONSTRAINT [FK_InstalledAfloatAppServer_TO_AfloatAppVersion]
GO

ALTER TABLE [dbo].[ServerAppVersionMonitoring]  WITH NOCHECK ADD  CONSTRAINT [FK_ServerAppVersionMonitoring_TO_Server] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Server] ([ServerId])
GO
ALTER TABLE [dbo].[ServerAppVersionMonitoring] CHECK CONSTRAINT [FK_ServerAppVersionMonitoring_TO_Server]
GO

ALTER TABLE [dbo].[ServerAppVersionMonitoring]  WITH NOCHECK ADD  CONSTRAINT [FK_ServerAppVersionMonitoring_TO_InstalledAfloatAppServer] FOREIGN KEY([InstalledAfloatAppServerId])
REFERENCES [dbo].[InstalledAfloatAppServer] ([InstalledAfloatAppServerId])
GO
ALTER TABLE [dbo].[ServerAppVersionMonitoring] CHECK CONSTRAINT [FK_ServerAppVersionMonitoring_TO_InstalledAfloatAppServer]
GO

ALTER TABLE [dbo].[ServerAppVersionMonitoring]  WITH NOCHECK ADD  CONSTRAINT [FK_ServerAppVersionMonitoring_TO_AuthAppServerRelease] FOREIGN KEY([AuthAppServerReleaseId])
REFERENCES [dbo].[AuthAppServerRelease] ([AuthAppServerReleaseId])
GO
ALTER TABLE [dbo].[ServerAppVersionMonitoring] CHECK CONSTRAINT [FK_ServerAppVersionMonitoring_TO_AuthAppServerRelease]
GO

ALTER TABLE [dbo].[ServerAppVersionMonitoring]  WITH NOCHECK ADD  CONSTRAINT [FK_ServerAppVersionMonitoring_TO_ComplianceStatus] FOREIGN KEY([ComplianceStatusId])
REFERENCES [dbo].[ComplianceStatus] ([ComplianceStatusId])
GO
ALTER TABLE [dbo].[ServerAppVersionMonitoring] CHECK CONSTRAINT [FK_ServerAppVersionMonitoring_TO_ComplianceStatus]
GO

ALTER TABLE [dbo].[NiapsVersionServerRelease] ADD  CONSTRAINT [DF_NiapsVersionServerRelease_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[NiapsVersionMonitoring] ADD  CONSTRAINT [DF_NiapsVersionMonitoring_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[NiapsVersionRelease] ADD  CONSTRAINT [DF_NiapsVersionRelease_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[InstalledNiapsVersion] ADD  CONSTRAINT [DF_InstalledNiapsVersion_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[AshorePubLevel] ADD  CONSTRAINT [DF_AshorePubLevel_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[AfloatPubLevel] ADD  CONSTRAINT [DF_AfloatPubLevel_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[AshoreClientLevel] ADD  CONSTRAINT [DF_AshoreClientLevel_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[AfloatClientLevel] ADD  CONSTRAINT [DF_AfloatClientLevel_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[DsUpdateActivity] ADD  CONSTRAINT [DF_DsUpdateActivity_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[AuthAppRelease] ADD  CONSTRAINT [DF_AuthAppRelease_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[AuthAppServerRelease] ADD  CONSTRAINT [DF_AuthAppServerRelease_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[AfloatAppVersion] ADD  CONSTRAINT [DF_AfloatAppVersion_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[InstalledAfloatAppServer] ADD  CONSTRAINT [DF_InstalledAfloatAppServer_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[ServerAppVersionMonitoring] ADD  CONSTRAINT [DF_ServerAppVersionMonitoring_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

ALTER TABLE [dbo].[CertMonitoring] ADD  CONSTRAINT [DF_CertMonitoring_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

--added on 20180204
ALTER TABLE [dbo].[UsageStatisticServer] ADD  CONSTRAINT [DF_UsageStatisticServer_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

--added on 20180204
ALTER TABLE [dbo].[StatisticDriveServer] ADD  CONSTRAINT [DF_StatisticDriveServer_TimeDateRecord]  DEFAULT (getdate()) FOR [TimeDateRecord]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AshorePubLevel', @level2type=N'COLUMN',@level2name=N'ServerPubId'
GO

/*SET OWNER TO SA*/
USE HAMS3X
GO 
ALTER DATABASE HAMS3X set TRUSTWORTHY ON; 
GO 
EXEC dbo.sp_changedbowner @loginame = N'sa', @map = false 
GO

---END DB CREATION