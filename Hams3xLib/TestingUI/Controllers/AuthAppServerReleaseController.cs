﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hams3xLib.Model;


namespace TestingUI.Controllers
{
    public class AuthAppServerReleaseController : Controller
    {
        // GET: AuthAppServerRelease
        public ActionResult Index()
        {
            return View();
        }

        // GET: AuthAppServerRelease/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AuthAppServerRelease/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AuthAppServerRelease/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                SpContext spContext = new SpContext();

                var authAppReleaseModel = new AuthAppServerReleaseModel
                {
                    AuthVersion = "1.8",
                    AuthAppReleaseDate = DateTime.Now,
                    AuthAppName = "Java",
                    AuthAppVendorName = "Sun",
                    ServerId = 1,
                    ServerAppReleaseDate = DateTime.Now
                };
                spContext.PostAuthAppServerRelease(authAppReleaseModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AuthAppServerRelease/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AuthAppServerRelease/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AuthAppServerRelease/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AuthAppServerRelease/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
