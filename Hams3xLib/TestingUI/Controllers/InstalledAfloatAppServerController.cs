﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hams3xLib.Model;


namespace TestingUI.Controllers
{
    public class InstalledAfloatAppServerController : Controller
    {
        // GET: InstalledAfloatAppServer
        public ActionResult Index()
        {
            return View();
        }

        // GET: InstalledAfloatAppServer/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: InstalledAfloatAppServer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InstalledAfloatAppServer/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                SpContext spContext = new SpContext();

                var installedAfloatAppServer = new InstalledAfloatAppServerModel
                {
                    ReturnServerCodeName = "AA100",
                    Servername = "DDG1000USV01",
                    AfloatVersion = "1.8",
                    AfloatAppName = "Java",
                    AfloatAppVendorName = "Sun",
                    InstalledDate = DateTime.Now,
                    InstallationAccount = "Joe",
                    UninstallDate = null,
                    UninstallAccount = null
                };
                spContext.PostInstalledAfloatAppServer(installedAfloatAppServer);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: InstalledAfloatAppServer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: InstalledAfloatAppServer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: InstalledAfloatAppServer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: InstalledAfloatAppServer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
