//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hams3xLib
{
    using System;
    using System.Collections.Generic;
    
    public partial class AuthAppRelease
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AuthAppRelease()
        {
            this.AuthAppServerReleases = new HashSet<AuthAppServerRelease>();
        }
    
        public int AuthAppReleaseId { get; set; }
        public int AuthAppId { get; set; }
        public string AuthVersion { get; set; }
        public string AuthNumber { get; set; }
        public System.DateTime AuthAppReleaseDate { get; set; }
        public System.DateTime TimeDateRecord { get; set; }
    
        public virtual AuthApp AuthApp { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AuthAppServerRelease> AuthAppServerReleases { get; set; }
    }
}
