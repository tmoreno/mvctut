//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hams3xLib
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserPermission
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserPermission()
        {
            this.BusinessRuleUsers = new HashSet<BusinessRuleUser>();
            this.UserStatistics = new HashSet<UserStatistic>();
        }
    
        public int UserPermissionId { get; set; }
        public int UserIndexId { get; set; }
        public int ModuleId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessRuleUser> BusinessRuleUsers { get; set; }
        public virtual Permission Permission { get; set; }
        public virtual UserIndex UserIndex { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserStatistic> UserStatistics { get; set; }
    }
}
