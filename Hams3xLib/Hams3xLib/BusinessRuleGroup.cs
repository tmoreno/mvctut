//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hams3xLib
{
    using System;
    using System.Collections.Generic;
    
    public partial class BusinessRuleGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BusinessRuleGroup()
        {
            this.BusinessRuleUsers = new HashSet<BusinessRuleUser>();
        }
    
        public int BusinessRuleGroupId { get; set; }
        public int BusinessRuleOneId { get; set; }
        public Nullable<int> BusinessRuleTwoId { get; set; }
    
        public virtual BusinessRule BusinessRule { get; set; }
        public virtual BusinessRule BusinessRule1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessRuleUser> BusinessRuleUsers { get; set; }
    }
}
