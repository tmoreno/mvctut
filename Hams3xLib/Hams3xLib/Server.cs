//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hams3xLib
{
    using System;
    using System.Collections.Generic;
    
    public partial class Server
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Server()
        {
            this.AuthAppServerReleases = new HashSet<AuthAppServerRelease>();
            this.CertMonitorings = new HashSet<CertMonitoring>();
            this.DriveServers = new HashSet<DriveServer>();
            this.DsUpdateActivities = new HashSet<DsUpdateActivity>();
            this.HullServers = new HashSet<HullServer>();
            this.InstalledAfloatAppServers = new HashSet<InstalledAfloatAppServer>();
            this.InstalledNiapsVersions = new HashSet<InstalledNiapsVersion>();
            this.NiapsVersionMonitorings = new HashSet<NiapsVersionMonitoring>();
            this.NiapsVersionServerReleases = new HashSet<NiapsVersionServerRelease>();
            this.ServerAppVersionMonitorings = new HashSet<ServerAppVersionMonitoring>();
            this.ServerPubs = new HashSet<ServerPub>();
            this.StatisticServers = new HashSet<StatisticServer>();
        }
    
        public int ServerId { get; set; }
        public string Servername { get; set; }
        public int ReturnServerCodeId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AuthAppServerRelease> AuthAppServerReleases { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CertMonitoring> CertMonitorings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DriveServer> DriveServers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DsUpdateActivity> DsUpdateActivities { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HullServer> HullServers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InstalledAfloatAppServer> InstalledAfloatAppServers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InstalledNiapsVersion> InstalledNiapsVersions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NiapsVersionMonitoring> NiapsVersionMonitorings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NiapsVersionServerRelease> NiapsVersionServerReleases { get; set; }
        public virtual ReturnServerCode ReturnServerCode { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServerAppVersionMonitoring> ServerAppVersionMonitorings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServerPub> ServerPubs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StatisticServer> StatisticServers { get; set; }
    }
}
