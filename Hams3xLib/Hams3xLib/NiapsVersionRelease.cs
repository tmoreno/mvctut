//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hams3xLib
{
    using System;
    using System.Collections.Generic;
    
    public partial class NiapsVersionRelease
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public NiapsVersionRelease()
        {
            this.InstalledNiapsVersions = new HashSet<InstalledNiapsVersion>();
            this.NiapsVersionServerReleases = new HashSet<NiapsVersionServerRelease>();
        }
    
        public int NiapsVersionReleaseId { get; set; }
        public string ReleaseVersion { get; set; }
        public string ReleaseNumber { get; set; }
        public System.DateTime ReleaseDate { get; set; }
        public System.DateTime TimeDateRecord { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InstalledNiapsVersion> InstalledNiapsVersions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NiapsVersionServerRelease> NiapsVersionServerReleases { get; set; }
    }
}
