﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
namespace Hams3xLib.Model
{
    //model for AuthAppServerRelease
    //metadata for releasing app to a specific server
    public class AuthAppServerReleaseModel 
    {
        public string AuthVersion { get; set; }
        public DateTime AuthAppReleaseDate { get; set; }
        public string AuthAppName { get; set; }
        public string AuthAppVendorName { get; set; }
        public int ServerId { get; set; }
        public DateTime ServerAppReleaseDate { get; set; }
    }

    //model for InstalledAfloatAppServer
    //metadata for upserting installed software of a specific server
    public class InstalledAfloatAppServerModel
    {
        public string ReturnServerCodeName { get; set; }
        public string Servername { get; set; }
        public string AfloatVersion { get; set; }
        public string AfloatAppName { get; set; }
        public string AfloatAppVendorName { get; set; }
        public DateTime InstalledDate { get; set; }
        public string InstallationAccount { get; set; }
        public DateTime? UninstallDate { get; set; }
        public string UninstallAccount { get; set; }
    }

    public class AuthAppProc
    {
        public string AuthAppName { get; set; }
        public string AuthAppVendorName { get; set; }
    }

    //obj for instantiating unit testing methods
    public class SpContext
    {
        //instantiates db object
        private HAMS3XEntities dbHamsEntity = new HAMS3XEntities();
        
        //
        public SpContext()
        {

        }

        //instantiates AuthAppServerReleaseModel object for POST operation
        public void PostAuthAppServerRelease(AuthAppServerReleaseModel authAppServerReleaseModel)
        {
            dbHamsEntity.USP_AuthAppServerRelease(authAppServerReleaseModel.AuthVersion,
                authAppServerReleaseModel.AuthAppReleaseDate, authAppServerReleaseModel.AuthAppName,
                authAppServerReleaseModel.AuthAppVendorName, authAppServerReleaseModel.ServerId,
                authAppServerReleaseModel.ServerAppReleaseDate);
        }

        //instantiates InstalledAfloatAppServer object for POST operation
        public void PostInstalledAfloatAppServer(InstalledAfloatAppServerModel installedAfloatAppServerModel)
        {
            dbHamsEntity.USP_InstalledAfloatAppServer(installedAfloatAppServerModel.ReturnServerCodeName,
                installedAfloatAppServerModel.Servername, installedAfloatAppServerModel.AfloatVersion,
                installedAfloatAppServerModel.AfloatAppName, installedAfloatAppServerModel.AfloatAppVendorName,
                installedAfloatAppServerModel.InstalledDate, installedAfloatAppServerModel.InstallationAccount,
                installedAfloatAppServerModel.UninstallDate, installedAfloatAppServerModel.UninstallAccount);
        }

        public void PostAuthApp(AuthAppProc authAppProc)
        {
            dbHamsEntity.USP_AuthApp(authAppProc.AuthAppName, authAppProc.AuthAppVendorName);
        }
    }
}
