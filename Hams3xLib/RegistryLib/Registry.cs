﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistryLib
{
    /// <summary>
    /// Custom Registry
    /// </summary>
    public class CustomRegistry : IRegistry
    {
        private IRegistryKey localMachine = new CustomRegistryKey();
        IRegistryKey IRegistry.LocalMachine { get => localMachine; }
        public string MachineFolder { get; set; }
        string IRegistry.MachineFolder { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        /// <summary>
        /// Store Value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="property"></param>
        /// <param name="value"></param>
        public void SetValue(string key, string property, string value)
        {
            // find out if folder key exists
            if(!Directory.Exists(key))
            {
                Directory.CreateDirectory(key);
            }

            // find out if file exists
            string fullPath = string.Format(@"{0}\{1}", key, property);
            FileStream fs = new FileStream(fullPath, FileMode.OpenOrCreate);
            fs.Write(UTF8Encoding.UTF8.GetBytes(value), 0, value.Length);
            fs.Flush();
            fs.Close();

        }
    }

    public class CustomRegistryKey : IRegistryKey
    {
        public IRegistryKey Key { get; set; }
        private IDictionary<string, IDictionary<string, string>> dictionary = null;

        public IRegistryKey OpenSubKey(string registryKey)
        {
            // Only for testing... what if registryKey is null or length is screwed up... ?
            CustomRegistryKey customRegistryKey = new CustomRegistryKey();

            var x = registryKey.Split('\\');
            if(x.Count() > 1)
            {
                dictionary = new Dictionary<string, IDictionary<string, string>>();
                customRegistryKey.SetDictionary(dictionary);

                DirectoryInfo di = new DirectoryInfo(registryKey);
                DirectoryInfo[] subdirs = di.GetDirectories();
                foreach (DirectoryInfo subdir in subdirs)
                {
                    IDictionary<string, string> subDict = new Dictionary<string, string>();
                    dictionary.Add(subdir.Name, subDict);
                    FileInfo[] files = subdir.GetFiles();
                    foreach (FileInfo file in files)
                    {
                        string value = File.ReadAllText(file.FullName);
                        subDict.Add(file.Name, value);
                    }
                }

            }
            else
            {
                IDictionary<string, string> subDict = dictionary[registryKey];
                IDictionary<string, IDictionary<string, string>> newDictionary = new Dictionary<string, IDictionary<string, string>>();
                newDictionary.Add(registryKey, subDict);
                customRegistryKey.SetDictionary(newDictionary);
            }

            return customRegistryKey;
        }

        public void SetDictionary(IDictionary<string, IDictionary<string, string>> dict)
        {
            dictionary = dict;
        }

        string[] IRegistryKey.GetSubKeyNames()
        {
            return dictionary.Select(a => a.Key).ToArray<string>();
        }

        object IRegistryKey.GetValue(string name)
        {
            object obj = null;
            if(dictionary.Count() > 0)
            {
                string[] keys = dictionary.Keys.ToArray<string>();
                IDictionary<string, string> subdict = dictionary[keys[0]];
                obj = subdict[name];
            }
            return obj;
        }
    }

    public class LocalMachineRegistryKey : IRegistryKey
    {
        private Microsoft.Win32.RegistryKey key;
        public IRegistryKey OpenSubKey(string registryKey)
        {
            key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(registryKey);
            Win32RegistryKey win32Registry = new Win32RegistryKey();
            win32Registry.Key = key;
            return win32Registry;
        }

        public void SetDictionary(IDictionary<string, IDictionary<string, string>> dict)
        {
            throw new NotImplementedException();
        }

        string[] IRegistryKey.GetSubKeyNames()
        {
            return key.GetSubKeyNames();
        }

        object IRegistryKey.GetValue(string name)
        {
            return key.GetValue(name);
        }
    }

    public class Win32Registry : IRegistry
    {
        private IRegistryKey localMachine = new LocalMachineRegistryKey();
        IRegistryKey IRegistry.LocalMachine { get => localMachine; }
        public string MachineFolder { get; set; }
        string IRegistry.MachineFolder { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        void IRegistry.SetValue(string key, string property, string value)
        {
            throw new NotImplementedException();
        }
    }

    public class Win32RegistryKey : IRegistryKey
    {
        private Microsoft.Win32.RegistryKey key;

        public RegistryKey Key { get => key; set => key = value; }

        public IRegistryKey OpenSubKey(string registryKey)
        {
            Win32RegistryKey win32Registry = new Win32RegistryKey();
            win32Registry.Key = key.OpenSubKey(registryKey);
            return win32Registry;
        }

        public void SetDictionary(IDictionary<string, IDictionary<string, string>> dict)
        {
            throw new NotImplementedException();
        }

        string[] IRegistryKey.GetSubKeyNames()
        {
            return Key.GetSubKeyNames();
        }

        object IRegistryKey.GetValue(string name)
        {
            return Key.GetValue(name);
        }
    }

    public interface IRegistry
    {
        IRegistryKey LocalMachine { get; }
        string MachineFolder { get; set; }
        void SetValue(string key, string property, string value);

    }

    public interface IRegistryKey
    {
        void SetDictionary(IDictionary<string, IDictionary<string, string>> dict);
        IRegistryKey OpenSubKey(string registryKey);
        string[] GetSubKeyNames();
        object GetValue(string name);
    }
}
