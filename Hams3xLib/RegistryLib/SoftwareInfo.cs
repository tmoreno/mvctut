﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistryLib
{

    public class SoftwareInfo
    {
        public string DisplayName { get; set; }
        public string Publisher { get; set; }
        public string DisplayVersion { get; set; }
        public DateTime InstallDate { get; set; }
        public string InstallationAccount { get; set; }

    }
}
