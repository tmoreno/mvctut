﻿--INITIAL DATA LOAD

USE [HAMS3X]
GO
--load db and sp's
--load data dictionary info
EXEC USP_BusinessRuleType 'Dashboard';
EXEC USP_BusinessRuleType 'Reporting';
EXEC USP_BusinessRuleType 'Alerts';
--businessrule datatypes
EXEC USP_BusinessRuleConditionType 'datetime2';
EXEC USP_BusinessRuleConditionType 'varchar(100)';
EXEC USP_BusinessRuleConditionType 'int';
--business rule operators
EXEC USP_BusinessRuleOperator '<';
EXEC USP_BusinessRuleOperator '>';
EXEC USP_BusinessRuleOperator '=';
EXEC USP_BusinessRuleOperator '!=';
EXEC USP_BusinessRuleOperator '>=';
EXEC USP_BusinessRuleOperator '<=';
--sensitivity type of business rules
EXEC USP_BusinessRuleSensitivityType 'MINUTE';
EXEC USP_BusinessRuleSensitivityType 'HOUR';
EXEC USP_BusinessRuleSensitivityType 'DAY';
EXEC USP_BusinessRuleSensitivityType 'INT';
--possible status fields
EXEC USP_Status 'RED';
EXEC USP_Status 'YELLOW';
EXEC USP_Status 'GREEN';
--businessruletargetarea levels
EXEC USP_BusinessRuleTargetAreaLevel 'Group';
EXEC USP_BusinessRuleTargetAreaLevel 'Table';
EXEC USP_BusinessRuleTargetAreaLevel 'Field';
--compliance status load
EXEC USP_ComplianceStatus 'UNAUTHORIZED SOFTWARE';
EXEC USP_ComplianceStatus 'AUTHORIZED AND CURRENT';
EXEC USP_ComplianceStatus 'UNAUTHORIZED VERSION';
EXEC USP_ComplianceStatus 'AUTHORIZED AND DELINQUENT VERSION';
EXEC USP_ComplianceStatus 'SOFTWARE MISSING';
EXEC USP_ComplianceStatus 'MISSING';
EXEC USP_ComplianceStatus 'UNAUTHORIZED FOR INSTALLED SERVER';

--load business rule target area to the index of tables that can be used for evaluation
EXEC USP_BusinessRuleTargetArea 'Replication','DsUpdateActivity','ContactTimestamp','1';
EXEC USP_BusinessRuleTargetArea 'Replication','DsUpdateActivity','ContactTimestamp','2';
EXEC USP_BusinessRuleTargetArea 'Replication','DsUpdateActivity','ContactTimestamp','3';
EXEC USP_BusinessRuleTargetArea 'Replication','DsUpdateActivity','Activity','1';
EXEC USP_BusinessRuleTargetArea 'Replication','DsUpdateActivity','Activity','2';
EXEC USP_BusinessRuleTargetArea 'Replication','DsUpdateActivity','Activity','3';
EXEC USP_BusinessRuleTargetArea 'Replication','DsUpdateActivity','Executor','1';
EXEC USP_BusinessRuleTargetArea 'Replication','DsUpdateActivity','Executor','2';
EXEC USP_BusinessRuleTargetArea 'Replication','DsUpdateActivity','Executor','3';
EXEC USP_BusinessRuleTargetArea 'Replication','AshorePubLevel','AshorePubLevel','2';
EXEC USP_BusinessRuleTargetArea 'Replication','AfloatClientLevel','AfloatClientLevel','2';
EXEC USP_BusinessRuleTargetArea 'Replication','AshoreClientLevel','AshoreClientLevel','2';
EXEC USP_BusinessRuleTargetArea 'Replication','AfloatPubLevel','AfloatPubLevel','2';
EXEC USP_BusinessRuleTargetArea 'Configuration Management','NiapsVersionMonitoring','AuthVersionBehindCount','2';
EXEC USP_BusinessRuleTargetArea 'Certificate Monitoring','CertMonitoring','DaysToExpire','2';
EXEC USP_BusinessRuleTargetArea 'NULL','NULL','NULL','1';
EXEC USP_BusinessRuleTargetArea 'Software Compliance','ServerAppVersionMonitoring','ComplianceStatusId','2';
EXEC USP_BusinessRuleTargetArea 'System Health','StatisticDriveServer','DriveSize','3';
EXEC USP_BusinessRuleTargetArea 'SystemHealth','StatisticDriveServer','FreeSpace','3';
EXEC USP_BusinessRuleTargetArea 'System Health','Drive','DriveLetter','3';
EXEC USP_BusinessRuleTargetArea 'System Health','UsageStatisticServer','Data','3';
EXEC USP_BusinessRuleTargetArea 'System Health','Statistic','StatisticName','3';
--target anchor 
EXEC USP_TargetAnchor 'HullServerId';
EXEC USP_TargetAnchor 'ServerPubId';
EXEC USP_TargetAnchor 'ServerAppVersionMonitoringId';
EXEC USP_TargetAnchor 'DsUpdateActivityId';
EXEC USP_TargetAnchor 'NiapsVersionMonitoringId';
EXEC USP_TargetAnchor 'StatisticDriveServerId';
EXEC USP_TargetAnchor 'DriveServerId';
EXEC USP_TargetAnchor 'CertMonitoringId';
EXEC USP_TargetAnchor 'StatisticServerId';
EXEC USP_TargetAnchor 'UsageStatisticServerId';
--users
EXEC USP_UserPermission '45678456789','Stephen','Neese','Module One';
EXEC USP_UserPermission '90888456789','Toby','Moreno','Module One';
EXEC USP_UserPermission '45678453289','First','Last','Module One';
EXEC USP_UserPermission '45678453289','First','Last','Module Two';

--add hull+server entities
EXEC USP_HullServer 'DDG-1000','USS ZUMWALT','SURF','DDG-1000','AA100','DDG1000USV01';
EXEC USP_HullServer 'DDG-1001','USS MICHAEL MONSOOR','SURF','DDG-1000','AA101','DDG1001USV01';

delete from BusinessRuleUser
delete from BusinessRuleGroup
delete from BusinessRule
select * from BusinessRuleUser
select * from BusinessRuleGroup
select * from BusinessRule
select * from BusinessRuleTargetArea

--BUSINESS RULES FOR REPLICATION > DS UPDATE ACTIVITY
--if contact time >= 48 hours from current timestamp red
EXEC USP_BusinessRuleUser '1','1','1','1','5','2','1','2','48','CURRENT_TIMESTAMP','16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','1','1','1','5','2','1','2','48','CURRENT_TIMESTAMP','16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','1','1','1','5','2','1','2','48','CURRENT_TIMESTAMP','16',null,null,null,'16',null;
--if contact time < 48 hours from current timestamp yellow
EXEC USP_BusinessRuleUser '1','2','1','1','1','2','2','2','48','CURRENT_TIMESTAMP','16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','2','1','1','1','2','2','2','48','CURRENT_TIMESTAMP','16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','2','1','1','1','2','2','2','48','CURRENT_TIMESTAMP','16',null,null,null,'16',null;
--if contact time < 24 hours from current timestamp green
EXEC USP_BusinessRuleUser '1','3','1','1','1','2','3','2','24','CURRENT_TIMESTAMP','16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','3','1','1','1','2','3','2','24','CURRENT_TIMESTAMP','16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','3','1','1','1','2','3','2','24','CURRENT_TIMESTAMP','16',null,null,null,'16',null;

--BUSINESS RULES FOR SOFTWARE COMPLIANCE > SERVER APP VERSION MONITORING
--IF compliance status = 4 yellow 
EXEC USP_BusinessRuleUser '1','1','1','3','3','4','2','17','4',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','1','1','3','3','4','2','17','4',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','1','1','3','3','4','2','17','4',null,'16',null,null,null,'16',null;
--IF compliance status = 2 green
EXEC USP_BusinessRuleUser '1','2','1','3','3','4','3','17','2',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','2','1','3','3','4','3','17','2',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','2','1','3','3','4','3','17','2',null,'16',null,null,null,'16',null;
--IF compliance status = 1 red
EXEC USP_BusinessRuleUser '1','3','1','3','3','4','1','17','1',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','3','1','3','3','4','1','17','1',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','3','1','3','3','4','1','17','1',null,'16',null,null,null,'16',null;
--IF compliance status = 3 red
EXEC USP_BusinessRuleUser '1','4','1','3','3','4','1','17','3',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','4','1','3','3','4','1','17','3',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','4','1','3','3','4','1','17','3',null,'16',null,null,null,'16',null;
--IF compliance status = 5 red
EXEC USP_BusinessRuleUser '1','5','1','3','3','4','1','17','5',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','5','1','3','3','4','1','17','5',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','5','1','3','3','4','1','17','5',null,'16',null,null,null,'16',null;
--IF compliance status = 7 red
EXEC USP_BusinessRuleUser '1','6','1','3','3','4','1','17','7',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','6','1','3','3','4','1','17','7',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','6','1','3','3','4','1','17','7',null,'16',null,null,null,'16',null;

--BUSINESS RULES FOR CONFIGURATION MANAGEMENT > NIAPS VERSION MONITORING
--if behind count = 0 green
EXEC USP_BusinessRuleUser '1','1','1','3','3','4','3','14','0',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','1','1','3','3','4','3','14','0',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','1','1','3','3','4','3','14','0',null,'16',null,null,null,'16',null;
--if behind count = 1 yellow
EXEC USP_BusinessRuleUser '1','2','1','3','3','4','2','14','1',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','2','1','3','3','4','2','14','1',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','2','1','3','3','4','2','14','1',null,'16',null,null,null,'16',null;
--if behind count > 1 red
EXEC USP_BusinessRuleUser '1','3','1','3','2','4','1','14','1',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','3','1','3','2','4','1','14','1',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','3','1','3','2','4','1','14','1',null,'16',null,null,null,'16',null;
--if behind count = null red
EXEC USP_BusinessRuleUser '1','4','1','3','3','4','1','14',null,null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','4','1','3','3','4','1','14',null,null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','4','1','3','3','4','1','14',null,null,'16',null,null,null,'16',null;

--BUSINESS RULES FOR REPLICATION > AFLOAT CLIENT LEVEL---------------------------------------------------------------
--if AfloatClientLevel = AshorePubLevel green
EXEC USP_BusinessRuleUser '1','1','1','3','3',null,'3','11',null,null,'10',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','1','1','3','3',null,'3','11',null,null,'10',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','1','1','3','3',null,'3','11',null,null,'10',null,null,null,'16',null;
--if AfloatClientLevel > 0 levels from AshorePubLevel yellow
EXEC USP_BusinessRuleUser '1','2','1','3','2','4','2','11','0',null,'10',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','2','1','3','2','4','2','11','0',null,'10',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','2','1','3','2','4','2','11','0',null,'10',null,null,null,'16',null;
--if AfloatClientLevel > 1 levels from AshorePubLevel red
EXEC USP_BusinessRuleUser '1','3','1','3','2','4','1','11','1',null,'10',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','3','1','3','2','4','1','11','1',null,'10',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','3','1','3','2','4','1','11','1',null,'10',null,null,null,'16',null;

--BUSINESS RULES FOR REPLICATION > ASHORE CLIENT LEVEL
--if AshoreClientLevel = AfloatPubLevel green
EXEC USP_BusinessRuleUser '1','1','1','3','3',null,'3','12',null,null,'13',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','1','1','3','3',null,'3','12',null,null,'13',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','1','1','3','3',null,'3','12',null,null,'13',null,null,null,'16',null;
--if AshoreClientLevel > 0 levels from AfloatPubLevel yellow
EXEC USP_BusinessRuleUser '1','2','1','3','2','4','2','12','0',null,'13',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','2','1','3','2','4','2','12','0',null,'13',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','2','1','3','2','4','2','12','0',null,'13',null,null,null,'16',null;
--if AshoreClientLevel > 1 levels from AfloatPubLevel red
EXEC USP_BusinessRuleUser '1','3','1','3','2','4','1','12','1',null,'13',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','3','1','3','2','4','1','12','1',null,'13',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','3','1','3','2','4','1','12','1',null,'13',null,null,null,'16',null;

--BUSINESS RULES FOR SYSTEM HEALTH > FREE SPACE 
--if A  drive free space > 10485760 green
EXEC USP_BusinessRuleUser '1','1','1','3','2','4','3','19','10485760',null,'16','2','3','2','20','A';
EXEC USP_BusinessRuleUser '2','1','1','3','2','4','3','19','10485760',null,'16','2','3','2','20','A';
EXEC USP_BusinessRuleUser '3','1','1','3','2','4','3','19','10485760',null,'16','2','3','2','20','A';
--if A  drive free space =< 10485760 yellow
EXEC USP_BusinessRuleUser '1','2','1','3','6','4','2','19','10485760',null,'16','2','3','2','20','A';
EXEC USP_BusinessRuleUser '2','2','1','3','6','4','2','19','10485760',null,'16','2','3','2','20','A';
EXEC USP_BusinessRuleUser '3','2','1','3','6','4','2','19','10485760',null,'16','2','3','2','20','A';
--if A  drive free space < 5242880 red
EXEC USP_BusinessRuleUser '1','3','1','3','1','4','1','19','5242880',null,'16','2','3','2','20','A';
EXEC USP_BusinessRuleUser '2','3','1','3','1','4','1','19','5242880',null,'16','2','3','2','20','A';
EXEC USP_BusinessRuleUser '3','3','1','3','1','4','1','19','5242880',null,'16','2','3','2','20','A';
--BUSINESS RULES FOR SYSTEM HEALTH > DRIVE > E DRIVE FREE SPACE
--if B drive free space > 20971520 green
EXEC USP_BusinessRuleUser '1','1','1','3','2','4','3','19','20971520',null,'16','2','3','2','20','B';
EXEC USP_BusinessRuleUser '2','1','1','3','2','4','3','19','20971520',null,'16','2','3','2','20','B';
EXEC USP_BusinessRuleUser '3','1','1','3','2','4','3','19','20971520',null,'16','2','3','2','20','B';
--if B drive free space =< 20971520 yellow
EXEC USP_BusinessRuleUser '1','2','1','3','6','4','2','19','20971520',null,'16','2','3','2','20','B';
EXEC USP_BusinessRuleUser '2','2','1','3','6','4','2','19','20971520',null,'16','2','3','2','20','B';
EXEC USP_BusinessRuleUser '3','2','1','3','6','4','2','19','20971520',null,'16','2','3','2','20','B';
--if B drive free space < 5242880 red
EXEC USP_BusinessRuleUser '1','3','1','3','1','4','1','19','5242880',null,'16','2','3','2','20','B';
EXEC USP_BusinessRuleUser '2','3','1','3','1','4','1','19','5242880',null,'16','2','3','2','20','B';
EXEC USP_BusinessRuleUser '3','3','1','3','1','4','1','19','5242880',null,'16','2','3','2','20','B';
--BUSINESS RULES FOR SYSTEM HEALTH > DRIVE > G DRIVE FREE SPACE
--if C drive free space > 209715200 green
EXEC USP_BusinessRuleUser '1','1','1','3','2','4','3','19','209715200',null,'16','2','3','2','20','C';
EXEC USP_BusinessRuleUser '2','1','1','3','2','4','3','19','209715200',null,'16','2','3','2','20','C';
EXEC USP_BusinessRuleUser '3','1','1','3','2','4','3','19','209715200',null,'16','2','3','2','20','C';
--if C drive free space =< 209715200 yellow
EXEC USP_BusinessRuleUser '1','2','1','3','6','4','2','19','209715200',null,'16','2','3','2','20','C';
EXEC USP_BusinessRuleUser '2','2','1','3','6','4','2','19','209715200',null,'16','2','3','2','20','C';
EXEC USP_BusinessRuleUser '3','2','1','3','6','4','2','19','209715200',null,'16','2','3','2','20','C';
--if C drive free space < 20971520 red
EXEC USP_BusinessRuleUser '1','3','1','3','1','4','1','19','20971520',null,'16','2','3','2','20','C';
EXEC USP_BusinessRuleUser '2','3','1','3','1','4','1','19','20971520',null,'16','2','3','2','20','C';
EXEC USP_BusinessRuleUser '3','3','1','3','1','4','1','19','20971520',null,'16','2','3','2','20','C';

--BUSINESS RULES FOR CERTIFICATE MONITORING > CERT MONITORING
--IF DaysToExpire > 30 days 
EXEC USP_BusinessRuleUser '1','2','1','3','2','4','3','15','30',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','2','1','3','2','4','3','15','30',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','2','1','3','2','4','3','15','30',null,'16',null,null,null,'16',null;
--IF DaysToExpire >= 15 days
EXEC USP_BusinessRuleUser '1','1','1','3','5','4','2','15','15',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','1','1','3','5','4','2','15','15',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','1','1','3','5','4','2','15','15',null,'16',null,null,null,'16',null;
--IF DaysToExpire < 15 days
EXEC USP_BusinessRuleUser '1','3','1','3','1','4','1','15','15',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '2','3','1','3','1','4','1','15','15',null,'16',null,null,null,'16',null;
EXEC USP_BusinessRuleUser '3','3','1','3','1','4','1','15','15',null,'16',null,null,null,'16',null;

--BUSINESS RULES FOR SYSTEM HEALTH > USAGE STATISTIC > CPU
--if cpu >= 80 red 
EXEC USP_BusinessRuleUser '1','1','1','3','5','4','1','21','80',null,'16','2','3','2','22','Cpu';
EXEC USP_BusinessRuleUser '2','1','1','3','5','4','1','21','80',null,'16','2','3','2','22','Cpu';
EXEC USP_BusinessRuleUser '3','1','1','3','5','4','1','21','80',null,'16','2','3','2','22','Cpu';
--if cpu < 80 yellow
EXEC USP_BusinessRuleUser '1','2','1','3','1','4','2','21','80',null,'16','2','3','2','22','Cpu';
EXEC USP_BusinessRuleUser '2','2','1','3','1','4','2','21','80',null,'16','2','3','2','22','Cpu';
EXEC USP_BusinessRuleUser '3','2','1','3','1','4','2','21','80',null,'16','2','3','2','22','Cpu';
--if cpu < 60 greeen
EXEC USP_BusinessRuleUser '1','3','1','3','1','4','3','21','60',null,'16','2','3','2','22','Cpu';
EXEC USP_BusinessRuleUser '2','3','1','3','1','4','3','21','60',null,'16','2','3','2','22','Cpu';
EXEC USP_BusinessRuleUser '3','3','1','3','1','4','3','21','60',null,'16','2','3','2','22','Cpu';
--BUSINESS RULES FOR SYSTEM HEALTH > USAGE STATISTIC > MEM
--if mem >= 80 red
EXEC USP_BusinessRuleUser '1','1','1','3','5','4','1','21','80',null,'16','2','3','2','22','Mem';
EXEC USP_BusinessRuleUser '2','1','1','3','5','4','1','21','80',null,'16','2','3','2','22','Mem';
EXEC USP_BusinessRuleUser '3','1','1','3','5','4','1','21','80',null,'16','2','3','2','22','Mem';
--if mem < 80 yellow
EXEC USP_BusinessRuleUser '1','2','1','3','1','4','2','21','80',null,'16','2','3','2','22','Mem';
EXEC USP_BusinessRuleUser '2','2','1','3','1','4','2','21','80',null,'16','2','3','2','22','Mem';
EXEC USP_BusinessRuleUser '3','2','1','3','1','4','2','21','80',null,'16','2','3','2','22','Mem';
--if mem < 60 greeen
EXEC USP_BusinessRuleUser '1','3','1','3','1','4','3','21','60',null,'16','2','3','2','22','Mem';
EXEC USP_BusinessRuleUser '2','3','1','3','1','4','3','21','60',null,'16','2','3','2','22','Mem';
EXEC USP_BusinessRuleUser '3','3','1','3','1','4','3','21','60',null,'16','2','3','2','22','Mem';
--BUSINESS RULES FOR SYSTEM HEALTH > USAGE STATISTIC > SERVER CODE
--if server code >= 500 red
EXEC USP_BusinessRuleUser '1','1','1','3','5','4','1','21','500',null,'16','2','3','2','22','ServerCode';
EXEC USP_BusinessRuleUser '2','1','1','3','5','4','1','21','500',null,'16','2','3','2','22','ServerCode';
EXEC USP_BusinessRuleUser '3','1','1','3','5','4','1','21','500',null,'16','2','3','2','22','ServerCode';
--if server code < 500 yellow
EXEC USP_BusinessRuleUser '1','2','1','3','1','4','2','21','500',null,'16','2','3','2','22','ServerCode';
EXEC USP_BusinessRuleUser '2','2','1','3','1','4','2','21','500',null,'16','2','3','2','22','ServerCode';
EXEC USP_BusinessRuleUser '3','2','1','3','1','4','2','21','500',null,'16','2','3','2','22','ServerCode';
--if server code < 100 greeen
EXEC USP_BusinessRuleUser '1','3','1','3','1','4','3','21','100',null,'16','2','3','2','22','ServerCode';
EXEC USP_BusinessRuleUser '2','3','1','3','1','4','3','21','100',null,'16','2','3','2','22','ServerCode';
EXEC USP_BusinessRuleUser '3','3','1','3','1','4','3','21','100',null,'16','2','3','2','22','ServerCode';
