﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DsCollectorLib;
using System.Collections.Generic;
using Hams3xLib.Model;
using RegistryLib;
using System.Configuration;

namespace TestingUI.Tests.Collector
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestCollectAppInfo()
        {
            IRegistry registry = new CustomRegistry();
            //string rootFolder = ConfigurationManager.AppSettings["rootFolder"];
            string registryKey = @"C:\Users\tmoreno\work\simulated_ships\DDG-1000\DDG1000USV01\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            DsCollector dsCollector = new DsCollector(registry);
            IList<SoftwareInfo> list = dsCollector.GetApplicationList(registryKey);

            foreach (SoftwareInfo SoftwareInfo in list)
            {
                if (SoftwareInfo.DisplayName != "" && SoftwareInfo.DisplayName != null && SoftwareInfo.Publisher != null)
                {
                    Console.WriteLine(SoftwareInfo.DisplayName);
                    Console.WriteLine(SoftwareInfo.Publisher);
                    Console.WriteLine(SoftwareInfo.DisplayVersion);
                }
                else {
                    //Assert.Fail();
                }
            }

        }

        [TestMethod]
        public void TestCollectWin32AppInfo()
        {
            IRegistry registry = new Win32Registry();
            DsCollector dsCollector = new DsCollector(registry);
            string registryKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            IList<SoftwareInfo> list = dsCollector.GetApplicationList(registryKey);

            foreach (SoftwareInfo SoftwareInfo in list)
            {
                //Console.WriteLine(SoftwareInfo.ToString());
                if (SoftwareInfo.DisplayName != "" && SoftwareInfo.DisplayName != null && SoftwareInfo.Publisher != null)
                {
                    Console.WriteLine(SoftwareInfo.DisplayName);
                }
                else
                {
                    //Assert.Fail();
                }
            }

        }

        [TestMethod]
        public void TestWriteCollectedAppInfo()
        {
            SpContext spContext = new SpContext();
            string registryKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            IRegistry registry = (IRegistry)(new CustomRegistryKey());

            DsCollector dsCollector = new DsCollector(registry);
            IList<SoftwareInfo> list = dsCollector.GetApplicationList(registryKey);

            foreach (SoftwareInfo SoftwareInfo in list)
            {
                if (SoftwareInfo.DisplayName != "" && SoftwareInfo.DisplayName != null && SoftwareInfo.Publisher != null)
                {
                    var authApp = new AuthAppProc
                    {
                        AuthAppName = SoftwareInfo.DisplayName,
                        AuthAppVendorName = SoftwareInfo.Publisher
                    };

                    Console.WriteLine(string.Format("appname : {0} vendor : {1}", authApp.AuthAppName, authApp.AuthAppVendorName));

                    spContext.PostAuthApp(authApp);
                }
                else
                {
                    Console.WriteLine("this should be fix..");
                    //Assert.Fail();
                }
            }

        }

    }
}
