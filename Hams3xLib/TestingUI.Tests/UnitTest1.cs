﻿using DsCollectorLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;
using RegistryLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistryLib.Tests
{
    [TestClass()]
    public class UnitTest1
    {
        [TestMethod()]
        public void InitSetValueTest()
        {
            string registryKey = @"C:\Users\tmoreno\work\simulated_ships\DDG-1000\DDG1000USV01\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\";

            IRegistry registry = new CustomRegistry();
            //registry.SetValue(registryKey, "DisplayVersion", "9");

            string win32RegistryKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            Microsoft.Win32.RegistryKey wreg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(win32RegistryKey);
            foreach(string subkey in wreg.GetSubKeyNames())
            {
                Console.WriteLine(subkey);

                string regKey = string.Format(@"{0}\{1}", registryKey, subkey);

                if (!Directory.Exists(regKey))
                {
                    Directory.CreateDirectory(regKey);
                }

                Microsoft.Win32.RegistryKey xkey = wreg.OpenSubKey(subkey);

                string[] properties = new string[] { "DisplayName", "DisplayVersion", "DisplayVendor", "InstallDate" };
                foreach(string property in properties)
                {
                    string value = String.Empty;
                    if(xkey.GetValue(property) != null)
                    {
                        value = (string)xkey.GetValue(property);
                        registry.SetValue(regKey, property, value);
                    }
                }
            }

        }

        [TestMethod()]
        public void SetKeyValueTest()
        {
            string registryKey = @"C:\Users\tmoreno\work\simulated_ships\DDG-1000\DDG1000USV01\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\";
            string subkey = "{26A24AE4-039D-4CA4-87B4-2F32180161F0}";

            string regKey = string.Format(@"{0}\{1}", registryKey, subkey);

            if (!Directory.Exists(regKey))
            {
                Directory.CreateDirectory(regKey);
            }

            IRegistry registry = new CustomRegistry();

            var list = new[]
            {
               new {
                    Attribute = "DisplayName",
                    Value = "Java 8 Update 162"
                },
               new {
                    Attribute = "DisplayVendor",
                    Value = "Oracle"
                },
               new {
                    Attribute = "DisplayVersion",
                    Value = "8.0.1610.13"
                }
            };

            list.ToList().ForEach(x =>
            {
                Console.WriteLine(x.Attribute + ":" + x.Value);
                registry.SetValue(regKey, x.Attribute, x.Value);
            });
        }
    }
}

namespace TestingUI.Tests
{
    class UnitTest1
    {
    }
}
