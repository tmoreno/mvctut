USE [HAMS3X]
GO

ALTER TABLE [dbo].[InstalledAfloatAppServer]
ADD [InstallationAccount] varchar(100) NOT NULL,
[UninstallDate] datetime2,
[UninstallAccount] varchar(100);
GO

ALTER TABLE [dbo].[BusinessRule]
ALTER COLUMN [StatusId] int;
GO

CREATE VIEW CurrentlyInstalled
AS
SELECT * FROM InstalledAfloatAppServer
WHERE InstalledAfloatAppServer.UninstallDate IS NULL;
GO

