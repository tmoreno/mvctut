---START OF STORED PROCEDURES
USE [HAMS3X]
GO --

/*CREATE UPSERT FOR STATUS INDEX*/
CREATE PROCEDURE [dbo].[USP_Status]--StatusId(GENERATED OR RETURNED),StatusName(PASSED),DISPLAY_INDICTORId(COLLECTED)
	@statusName varchar(50)--PASS StatusName FOR INSERT
AS
	--print 'USP_Status';
	DECLARE @statusId int;--DECLARE StatusId
	SET @statusId = 0;

	-- find StatusId if StatusName already exists
	SELECT @statusId = StatusId FROM Status
	WHERE StatusName = @statusName;

	IF @statusId = 0--if StatusName does not exist create new StatusId
	BEGIN
		INSERT INTO Status (StatusName)
		VALUES (@statusName);
		--set the inserted statusId
		set @statusId = @@IDENTITY
	END
RETURN @statusId--return statuId
GO
/*
SAMPLE
EXEC USP_Status 'RED';
EXEC USP_Status 'YELLOW';
EXEC USP_Status 'GREEN';
EXEC USP_Status 'NOT SET';
END UPSERT FOR STATUS INDEX
updated upsert
*/

/*CREATE UPSERT FOR Hull TYPE INDEX*/
CREATE PROCEDURE [dbo].[USP_HullType]--HullTypeId(GENERATED OR RETURNED),HullTypeName(PASSED)
	@hullTypeName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_HullType';
	DECLARE @hullTypeId int;--DECLARE HullTypeId
	SET @hullTypeId = 0;--SET HullTypeId

	--find HullTypeId if HullTypeName already exists
	SELECT @hullTypeId = HullTypeId FROM HullType
	WHERE HullTypeName = @hullTypeName;

	---if HullTypeName does not exist, INSERT new HullTypeName
	IF @hullTypeId = 0
	BEGIN
		INSERT INTO HullType(HullTypeName)
		VALUES (@hullTypeName);
		--set the inserted hullTypeId
		set @hullTypeId = @@IDENTITY
	END
return @hullTypeId--RETURN HullTypeId
GO --
/*
SAMPLE
EXEC USP_HullType 'SURF';
EXEC USP_HullType 'AIR';
EXEC USP_HullType 'SUB';
EXEC USP_HullType 'FAC';
END UPSERT FOR Hull TYPE INDEX
updated upsert
*/

/*CREATE UPSERT FOR CLASS TYPE INDEX*/
CREATE PROCEDURE [dbo].[USP_ClassType]--ClassTypeId(GENERATED OR RETURNED),ClassTypeName(PASSED)
	@classTypeName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_ClassType';
	DECLARE @classTypeId int;--DECLARE ClassTypeId
	SET @classTypeId = 0;--SET ClassTypeId

	-- find the ClassTypeId if the ClassTypeName already exists
	SELECT @classTypeId = ClassTypeId FROM ClassType
	WHERE ClassTypeName = @classTypeName;

	---if current ClassTypeName does not exist INSERT it
	IF @classTypeId = 0
	BEGIN
		INSERT INTO ClassType(ClassTypeName)
		VALUES (@classTypeName);
		--set the inserted classTypeId
		set @classTypeId = @@IDENTITY
	END
return @classTypeId--return ClassTypeId 
GO --
/*
SAMPLE
EXEC USP_ClassType 'DDG-51';
EXEC USP_ClassType 'DDG-1000';
EXEC USP_ClassType 'LHD-1';
EXEC USP_ClassType 'LCC-19';
END UPSERT FOR CLASS TYPE INDEX
updated upsert
*/

/*CREATE UPSERT FOR RETURN SERVER CODE INDEX*/
CREATE PROCEDURE [dbo].[USP_ReturnServerCode]--RETURN_ServerCODE(GENERATED OR RETURNED),ReturnServerCodeName(PASSED)
	@returnServerCodeName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_ReturnServerCode';
	DECLARE @returnServerCodeId int;--DECLARE ReturnServerCodeId
	SET @returnServerCodeId = 0;--SET ReturnServerCodeId

	-- find the ReturnServerCodeName if it exists
	SELECT @returnServerCodeId = ReturnServerCodeId FROM ReturnServerCode
	WHERE ReturnServerCodeName = @returnServerCodeName;

	---if current returnServerCodeName is not present insert it
	IF @returnServerCodeId = 0
	BEGIN
		INSERT INTO ReturnServerCode(ReturnServerCodeName)
		VALUES (@returnServerCodeName);
		--set the inserted returnServerCodeId
		set @returnServerCodeId = @@IDENTITY
	END
return @returnServerCodeId--return the RETURN_CODE_ServerId
GO --
/*
SAMPLE
EXEC USP_ReturnServerCode 'AA101';
EXEC USP_ReturnServerCode 'AA102';
EXEC USP_ReturnServerCode 'AA103';
EXEC USP_ReturnServerCode 'AA104';
END UPSERT FOR RETURN SERVER CODE INDEX
updated upsert
*/

/*CREATE UPSERT FOR Hull INDEX*/
CREATE PROCEDURE [dbo].[USP_Hull]--Hull(PASSED),HullName(PASSED),StatusId(COLLECTED),HullTypeId(COLLECTED),ClassTypeId(COLLECTED),ReturnServerCodeId(COLLECTED)
	@hull varchar(16),--PASSED FOR INSERT
	@hullName varchar(50),--PASSED FOR INSERT
	@hullTypeName varchar(50),--PASSED FOR USP CALL
	@classTypeName varchar(50)--PASSED FOR USP CALL
AS
	--print 'USP_Hull';
	DECLARE @hullId int;--DECLARE HullId
	DECLARE @hullTypeId int;--DECLARE HullTypeId
	DECLARE @classTypeId int;--DECLARE ClassTypeId
	SET @hullId = 0;--SET HullId

	-- find the current hullId if Hull AND HullName already exist
	SELECT @hullId = HullId FROM Hull
	WHERE Hull = @hull AND HullName = @hullName;

	--find hullTypeId, insert it it does not exist
	EXEC @hullTypeId = USP_HullType @hullTypeName;
	--find classTypeId, insert if it does not exist
	EXEC @classTypeId = USP_ClassType @classTypeName;

	---if current hullId is not present insert the hullId
	IF @hullId = 0
	BEGIN
		INSERT INTO Hull (Hull,HullName,HullTypeId,ClassTypeId)
		VALUES (@hull,@hullName,@hullTypeId,@classTypeId);
		--set the inserted hullId
		set @hullId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE Hull
		SET HullTypeId = @hullTypeId
		WHERE HullTypeId <> @hullTypeId AND HullId = @hullId;
		UPDATE Hull
		SET ClassTypeId = @classTypeId
		WHERE ClassTypeId <> @classTypeId AND HullId = @hullId;
	END
return @hullId--return HullId
GO --
/*
SAMPLE
EXEC USP_Hull 'DDG-1001','USS MICHAEL MONSOOR','SURF','DDG-1000';
EXEC USP_Hull 'DDG-1000','USS ZUMWALT','SURF','DDG-1000';
EXEC USP_Hull 'DDG-1000','USS ZUMWALT','FAC','DDG-1000';
EXEC USP_Hull 'DDG-1000','USS ZUMWALT','SURF','DDG-1000';
END UPSERT FOR Hull INDEX
updated upsert
*/

/*CREATE UPSERT FOR SERVER INDEX*/
CREATE PROCEDURE [dbo].[USP_Server]--ServerId(GENERATED OR RETURNED),Servername(PASSED)
	@servername varchar(50),--PASSED FOR INSERT
	@returnServerCodeName varchar(50)--PASSED FOR USP CALL
AS
	--print 'USP_Server';
	DECLARE @serverId int;--DECLARE ServerId
	DECLARE @returnServerCodeId int;--DECLARE ReturnServerCodeId

	SET @serverId = 0;--SET ServerId

	-- find the serverId for Servername if Servername already exists
	SELECT @serverId = ServerId FROM Server
	WHERE Servername = @servername;
	
	--find returnServerCodeId, insert if it does not exist
	EXEC @returnServerCodeId = USP_ReturnServerCode @returnServerCodeName;

	IF @serverId = 0
	BEGIN
		INSERT INTO Server (Servername,ReturnServerCodeId)
		VALUES (@servername,@returnServerCodeId);
		--set the inserted serverId
		set @serverId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE Server
		SET ReturnServerCodeId = @returnServerCodeId
		WHERE ReturnServerCodeId <> @returnServerCodeId AND ServerId = @serverId;

	END
RETURN @serverId--return serverId
GO --
/*
SAMPLE
EXEC USP_Server 'DDG1000USV01','AA100';
EXEC USP_Server 'DDG1001USV01','AA102';
EXEC USP_Server 'DDG1001USV01','AA103';
END UPSERT FOR SERVER INDEX
updated upsert
*/

/*CREATE UPSERT FOR Hull SERVER INDEX*/
CREATE PROCEDURE [dbo].[USP_HullServer]--ServerId(GENERATED OR RETURNED),HullId(COLLECTED),StatusId(COLLECTED)
	@hull varchar(16),--PASSED FOR USP CALL
	@hullName varchar(50),--PASSED FOR USP CALL
	@hullTypeName varchar(50),--PASSED FOR USP CALL
	@classTypeName varchar(50),--PASSED FOR USP CALL
	@returnServerCodeName varchar(50),--PASSED FOR USP CALL
	@servername varchar(50)--PASSED FOR USP CALL
AS
	--print 'USP_HullServer';
	DECLARE @hullServerId int;
	DECLARE @hullId int;
	DECLARE @serverId int;

	---init hull_server_id var
	SET @hullServerId = 0;

	---find hullId, insert if it does not exist
	EXEC @hullId = USP_Hull @hull,@hullName,@hullTypeName,@classTypeName;

	--find serverId, insert if it does not exist
	EXEC @serverId = USP_Server @servername,@returnServerCodeName;

	---find HullServerId if HullId + ServerId already exist
	SELECT @hullServerId = HullServerId FROM HullServer
	WHERE HullId = @hullId AND ServerId = @serverId

	---insert new HullServer if non exist
	IF @hullServerId = 0
	BEGIN
		INSERT INTO HullServer (ServerId, HullId)
		VALUES (@serverId, @hullId);
		SET @hullServerId = @@IDENTITY
	END
RETURN @hullServerId--returns HullServerId
GO --
/*END UPSERT FOR Hull SERVER INDEX
SAMPLE
EXEC USP_HullServer 'DDG-1001','USS MICHAEL MONSOOR','SURF','DDG-1000','AA101','DDG1001USV01';
EXEC USP_HullServer 'DDG-1000','USS ZUMWALT','SURF','DDG-1000','AA100','DDG1000USV01';
EXEC USP_HullServer 'DDG-1000','USS ZUMWALT','FAC','LHD-1','AA203','DDG1000USV01';
EXEC USP_HullServer 'DDG-1000','USS ZUMWALT','FAC','LHD-1','AA203','DDG1000USV02';
EXEC USP_HullServer 'DDG-1000','USS ZUMWALT','SURF','DDG-1000','AA100','DDG1000USV01';
updated upsert
*/



/*CREATE UPSERT FOR AuthAppVendor*/
CREATE PROCEDURE [dbo].[USP_AuthAppVendor]
	@authAppVendorName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_AuthAppVendor';
	DECLARE @authAppVendorId int;--DECLARE AuthAppVendorId
	SET @authAppVendorId = 0;--SET SET AuthAppVendorId

	--write in AuthAppVendorId if already present
	SELECT @authAppVendorId = AuthAppVendorId FROM AuthAppVendor
	WHERE AuthAppVendorName = @authAppVendorName;

	---if current authAppVendorName is not present create new authAppVendorName and id
	IF @authAppVendorId = 0
	BEGIN
		INSERT INTO AuthAppVendor (AuthAppVendorName)
		VALUES (@authAppVendorName);
		--set the inserted authAppVendorId
		set @authAppVendorId = @@IDENTITY
	END
return @authAppVendorId--RETURNS AuthAppVendorId
GO --
/*
SAMPLE 
EXEC USP_AuthAppVendor 'ORACLE';
END UPSERT FOR AuthAppVendor
updated upsert
*/

/*CREATE UPSERT FOR AuthApp*/
CREATE PROCEDURE [dbo].[USP_AuthApp]
	@authAppName varchar(50),--PASSED FOR INSERT
	@authAppVendorName varchar(50)--PASSED FOR USP CALL
AS
	--print 'USP_AuthApp';
	DECLARE @authAppId int;--DECLARE AuthAppId
	DECLARE @authAppVendorId int;--DECLARE AuthAppVendorId
	SET @authAppId = 0;--SET SET AuthAppId

	--find authAppVendorId
	EXEC @authAppVendorId = USP_AuthAppVendor @authAppVendorName;

	--write in AuthAppId if already present
	SELECT @authAppId = AuthAppId FROM AuthApp
	WHERE AuthAppName = @authAppName AND AuthAppVendorId = @authAppVendorId;

	---if current authAppName is not present create new authAppName and id
	IF @authAppId = 0
	BEGIN
		INSERT INTO AuthApp (AuthAppName,AuthAppVendorId)
		VALUES (@authAppName,@authAppVendorId);
		--set the inserted authAppId
		set @authAppId = @@IDENTITY
	END
return @authAppId--RETURNS AuthAppId
GO --
/*
SAMPLE
EXEC USP_AuthApp 'JAVA','ACTIVE','ORACLE','ACTIVE';
END UPSERT FOR AuthApp 
updated upsert
*/

/*CREATE UPSERT FOR InstalledAfloatAppVendor*/
CREATE PROCEDURE [dbo].[USP_InstalledAfloatAppVendor]
	@installedAfloatAppVendorName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_InstalledAfloatAppVendor';
	DECLARE @installedAfloatAppVendorId int;--DECLARE InstalledAfloatAppVendorId

	SET @installedAfloatAppVendorId = 0;--SET InstalledAfloatAppVendorId

	--write in InstalledAfloatAppVendorId if already present
	SELECT @installedAfloatAppVendorId = InstalledAfloatAppVendorId FROM InstalledAfloatAppVendor
	WHERE InstalledAfloatAppVendorName = @installedAfloatAppVendorName;

	---if current installedAfloatAppVendorName is not present create new installedAfloatAppVendorName and id
	IF @installedAfloatAppVendorId = 0
	BEGIN
		INSERT INTO InstalledAfloatAppVendor (InstalledAfloatAppVendorName)
		VALUES (@installedAfloatAppVendorName);
		--set the inserted installedAfloatAppVendorId
		set @installedAfloatAppVendorId = @@IDENTITY
	END
return @installedAfloatAppVendorId--RETURNS InstalledAfloatAppVendorId
GO --
/*
SAMPLE
EXEC USP_InstalledAfloatAppVendor 'ORACLE','ACTIVE';
END UPSERT FOR InstalledAfloatAppVendor
updated upsert
*/


/*CREATE UPSERT FOR AfloatApp*/
CREATE PROCEDURE [dbo].[USP_AfloatApp]
	--for afloatAppIndex
	@afloatAppName varchar(50),--PASSED FOR INSERT
	--for installedAfloatAppVendor stored procedure
	@installedAfloatAppVendorName varchar(50)--PASSED FOR USP CALL
AS
	--print 'USP_AfloatApp';
	DECLARE @afloatAppId int;--DECLARE AfloatAppId
	DECLARE @installedAfloatAppVendorId int;--DECLARE AfloatAppVendorId
	SET @afloatAppId = 0;--SET SET AfloatAppId

	--find installedAfloatAppVendorId
	EXEC @installedAfloatAppVendorId = USP_InstalledAfloatAppVendor @installedAfloatAppVendorName;

	--write in AfloatAppId if already present
	SELECT @afloatAppId = AfloatAppId FROM AfloatApp
	WHERE AfloatAppName = @afloatAppName AND InstalledAfloatAppVendorId = @installedAfloatAppVendorId;

	---if current afloatAppName is not present create new afloatAppName and id
	IF @afloatAppId = 0
	BEGIN
		INSERT INTO AfloatApp (AfloatAppName,InstalledAfloatAppVendorId)
		VALUES (@afloatAppName,@installedAfloatAppVendorId);
		--set the inserted afloatAppId
		set @afloatAppId = @@IDENTITY
	END
return @afloatAppId--RETURNS AfloatAppId
GO --
/*
SAMPLE
EXEC USP_AfloatApp 'JAVA','ACTIVE','ORACLE','ACTIVE';
END UPSERT FOR AfloatApp 
updated upsert
*/

/*CREATE UPSERT FOR AfloatAppVersion*/
CREATE PROCEDURE [dbo].[USP_AfloatAppVersion]
	--for release
	@afloatVersion varchar(50),--PASSED FOR INSERT
	--for afloatAppId call
	@afloatAppName varchar(50),--PASSED FOR USP CALL
	@afloatAppVendorName varchar(50)--PASSED FOR USP CALL
AS
	--print 'USP_AfloatAppVersion';
	DECLARE @afloatAppVersionId int;--DECLARE AfloatAppVersionId
	DECLARE @afloatAppId int;--DECLARE AfloatAppId
	SET @afloatAppVersionId = 0;--SET SET AfloatAppVersionId

	--collect afloatAppId
	EXEC @afloatAppId = USP_AfloatApp @afloatAppName,@afloatAppVendorName;

	--find afloatAppVersionId if APP+VERSION has already been installed
	SELECT @afloatAppVersionId = AfloatAppVersionId FROM AfloatAppVersion
	WHERE AfloatAppId = @afloatAppId AND AfloatVersion = @afloatVersion;

	---if APP+VERSION is has not been released, release APP+VERSION and create an id
	IF @afloatAppVersionId = 0
	BEGIN
		INSERT INTO AfloatAppVersion (AfloatAppId,AfloatVersion)
		VALUES (@afloatAppId,@afloatVersion);
		--set the inserted afloatAppVersionId
		set @afloatAppVersionId = @@IDENTITY
	END
return @afloatAppVersionId--RETURNS AfloatAppVersionId
GO --
/*
SAMPLE
EXEC USP_AfloatAppVersion '1.8_101','JAVA','ACTIVE','ORACLE','ACTIVE';
END UPSERT FOR AfloatAppVersion
updated upsert
*/

/*CREATE UPSERT FOR ComplianceStatus*/
CREATE PROCEDURE [dbo].[USP_ComplianceStatus]--ComplianceStatusId(GENERATED OR RETURNED),ComplianceStatusName(PASSED),DISPLAY_INDICTORId(COLLECTED)
	@complianceStatusName varchar(50)--PASS ComplianceStatusName FOR INSERT
AS
	--print 'USP_ComplianceStatus';
	DECLARE @complianceStatusId int;
	SET @complianceStatusId = 0;

	-- find ComplianceStatusId if ComplianceStatusName already exists
	SELECT @complianceStatusId = ComplianceStatusId FROM ComplianceStatus
	WHERE ComplianceStatusName = @complianceStatusName;

	IF @complianceStatusId = 0--if ComplianceStatusName does not exist create new ComplianceStatusName and id
	BEGIN
		INSERT INTO ComplianceStatus (ComplianceStatusName)
		VALUES (@complianceStatusName);
		--set the inserted complianceStatusId
		set @complianceStatusId = @@IDENTITY
	END
RETURN @complianceStatusId--return ComplianceStatusId
GO --
/*
SAMPLE
EXEC USP_ComplianceStatus 'NOTIFICATION SENT','ACTIVE';
EXEC USP_ComplianceStatus 'UNDER INVESTIGATION','ACTIVE';
END UPSERT FOR ComplianceStatus INDEX
updated upsert
*/

/*CREATE UPSERT FOR Priority INDEX*/
CREATE PROCEDURE [dbo].[USP_Priority]---PriorityId(GENERATED OR RETURNED),PriorityName(PASSED)
	@priorityName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_Priority';
	DECLARE @priorityId int;--DECLARE PriorityId
	SET @priorityId = 0;--SET PriorityId

	SELECT @priorityId = PriorityId FROM Priority
	WHERE Priority = @priorityName;--write in PriorityId if already present

	---if current priorityId is not present create new priorityId
	IF @priorityId = 0
	BEGIN
		INSERT INTO Priority (Priority)
		VALUES (@priorityName);
		--set the inserted priorityId
		set @priorityId = @@IDENTITY
	END
return @priorityId--RETURNS PriorityId
GO --
/*
SAMPLE
EXEC USP_Priority 'HIGH','ACTIVE';
EXEC USP_Priority 'MEDIUM','ACTIVE';
EXEC USP_Priority 'LOW','ACTIVE';
END UPSERT FOR Priority INDEX
updated upsert
*/

/*CREATE UPSERT FOR ExecutionMode INDEX*/
CREATE PROCEDURE [dbo].[USP_ExecutionMode]---ExecutionModeId(GENERATED OR RETURNED),ExecutionModeName(PASSED)
	@executionModeName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_ExecutionMode';
	DECLARE @executionModeId int;--DECLARE ExecutionModeId
	SET @executionModeId = 0;--SET ExecutionModeId

	SELECT @executionModeId = ExecutionModeId FROM ExecutionMode
	WHERE ExecutionMode = @executionModeName;--write in ExecutionModeId if already present

	---if current executionModeId is not present create new executionModeId
	IF @executionModeId = 0
	BEGIN
		INSERT INTO ExecutionMode (ExecutionMode)
		VALUES (@executionModeName);
		--set the inserted executionModeId
		set @executionModeId = @@IDENTITY
	END
return @executionModeId--RETURNS ExecutionModeId
GO --
/*
SAMPLE
EXEC USP_ExecutionMode 'AUTO','ACTIVE';
EXEC USP_ExecutionMode 'AUTO','ACTIVE';
EXEC USP_ExecutionMode 'MANUAL','ACTIVE';
END UPSERT FOR ExecutionMode INDEX
updated upsert
*/

/*CREATE UPSERT FOR PubType INDEX*/
CREATE PROCEDURE [dbo].[USP_PubType]---PubTypeId(GENERATED OR RETURNED),PubTypeName(PASSED)
	@pubTypeName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_PubType';
	DECLARE @pubTypeId int;--DECLARE PubTypeId
	SET @pubTypeId = 0;--SET PubTypeId

	SELECT @pubTypeId = PubTypeId FROM PubType
	WHERE PubTypeName = @pubTypeName;--write in PubTypeId if already present

	---if current pubTypeId is not present create new pubTypeId
	IF @pubTypeId = 0
	BEGIN
		INSERT INTO PubType (PubTypeName)
		VALUES (@pubTypeName);
		--set the inserted pubTypeId
		set @pubTypeId = @@IDENTITY
	END
return @pubTypeId--RETURNS PubTypeId
GO --
/*
SAMPLE
EXEC USP_PubType 'RESIDENT','ACTIVE';
EXEC USP_PubType 'PASSTHROUGH','ACTIVE';
EXEC USP_PubType 'RESIDENT','ACTIVE';
END UPSERT FOR PubType INDEX
updated upsert
*/

/*CREATE UPSERT FOR PubScope INDEX*/
CREATE PROCEDURE [dbo].[USP_PubScope]---PubScopeId(GENERATED OR RETURNED),PubScopeName(PASSED)
	@pubScopeName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_PubScope';
	DECLARE @pubScopeId int;--DECLARE PubScopeId
	SET @pubScopeId = 0;--SET PubScopeId

	SELECT @pubScopeId = PubScopeId FROM PubScope
	WHERE PubScopeName = @pubScopeName;--write in PubScopeId if already present

	---if current pubScopeId is not present create new pubScopeId
	IF @pubScopeId = 0
	BEGIN
		INSERT INTO PubScope (PubScopeName)
		VALUES (@pubScopeName);
		--set the inserted pubScopeId
		set @pubScopeId = @@IDENTITY
	END
return @pubScopeId--RETURNS PubScopeId
GO --
/*
SAMPLE
EXEC USP_PubScope 'HullSPECIFIC','ACTIVE';
EXEC USP_PubScope 'CLASS_SPECIFIC','ACTIVE';
EXEC USP_PubScope 'HullSPECIFIC','ACTIVE';
END UPSERT FOR PubScope INDEX
updated upsert
*/

/*CREATE UPSERT FOR Pub INDEX*/
CREATE PROCEDURE [dbo].[USP_Pub]---PubId(GENERATED OR RETURNED),PubName(PASSED)
	@pubName varchar(50),--PASSED FOR INSERT
	@pubDescription varchar(255),--PASSED FOR INSERT
	--passed for priority usp call
	@priorityName varchar(50),
	--passed for execution_mode usp call
	@executionModeName varchar(50),
	--passed for pub_type usp call
	@pubTypeName varchar(50),
	--passed for pub_scope usp call
	@pubScopeName varchar(50)
AS
	--print 'USP_Pub';
	DECLARE @pubId int;--DECLARE PubId
	DECLARE @priorityId int;
	DECLARE @executionModeId int;
	DECLARE @pubTypeId int;
	DECLARE @pubScopeId int;
	SET @pubId = 0;--SET PubId
	SET @priorityId = 0;
	SET @executionModeId = 0;
	SET @pubTypeId = 0;
	SET @pubScopeId = 0;

	SELECT @pubId = PubId FROM Pub
	WHERE PubName = @pubName;--write in PubId if already present

	--find PriorityId, INSERT if it does not exist
	EXEC @priorityId = USP_Priority @priorityName;

	--find ExecutionModeId, INSERT if it does not exist
	EXEC @executionModeId = USP_ExecutionMode @executionModeName;

	--find PubTypeId, INSERT if it does not exist
	EXEC @pubTypeId = USP_PubType @pubTypeName;

	--find PubScopeId, INSERT if it does not exist
	EXEC @pubScopeId = USP_PubScope @pubScopeName;

	---if current pubId is not present create new pubId
	IF @pubId = 0
	BEGIN
		INSERT INTO Pub (PubName,PubTypeId,PubScopeId,PubDescription,ExecutionModeId,PriorityId)
		VALUES (@pubName,@pubTypeId,@pubScopeId,@pubDescription,@executionModeId,@priorityId);
		--set the inserted pubId
		set @pubId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE Pub
		SET PubTypeId = @pubTypeId
		WHERE PubTypeId <> @pubTypeId AND PubId = @pubId;
		UPDATE Pub
		SET PubScopeId = @pubScopeId
		WHERE PubScopeId <> @pubScopeId AND PubId = @pubId;
		UPDATE Pub
		SET PubScopeId = @pubScopeId
		WHERE PubScopeId <> @pubScopeId AND PubId = @pubId;
		UPDATE Pub
		SET ExecutionModeId = @executionModeId
		WHERE ExecutionModeId <> @executionModeId AND PubId = @pubId;
		UPDATE Pub
		SET PriorityId = @priorityId
		WHERE PriorityId <> @priorityId AND PubId = @pubId;
		UPDATE Pub
		SET PubDescription = @pubDescription
		WHERE PubDescription <> @pubDescription AND PubId = @pubId;
	END
return @pubId--RETURNS PubId
GO --
/*
SAMPLE
EXEC USP_Pub 'SUAN','ACTIVE','Pub CONTAINING SOFTWARE UPDATES','RESIDENT','ACTIVE','ALL_HullS','ACTIVE','AUTO','ACTIVE','HIGH','ACTIVE';
END UPSERT FOR Pub INDEX
updated upsert
*/

/*CREATE UPSERT FOR SERVER Pub INDEX*/
CREATE PROCEDURE [dbo].[USP_ServerPub]--ServerId(GENERATED OR RETURNED),PubId(COLLECTED)
	--
	--for pub index usp call
	--
	@pubName varchar(50),--PASSED FOR INSERT
	@pubDescription varchar(255),--PASSED FOR INSERT
	--passed for priority usp call
	@priorityName varchar(50),
	--passed for execution_mode usp call
	@executionModeName varchar(50),
	--passed for pub_type usp call
	@pubTypeName varchar(50),
	--passed for pub_scope usp call
	@pubScopeName varchar(50),
	--
	--for server index usp call
	--
	@servername varchar(50),--PASSED FOR INSERT
	@returnServerCodeName varchar(50)--PASSED FOR USP CALL
AS
	--print 'USP_ServerPub';
	DECLARE @serverPubId int;
	DECLARE @pubId int;
	DECLARE @serverId int;

	---init hull_server_id var
	SET @serverPubId = 0;
	
	--find PubId if it exists already, INSERT if no
	EXEC @pubId = USP_Pub @pubName,@pubDescription,@priorityName,@executionModeName,@pubTypeName,@pubScopeName;

	--find ServerId if it exists already, INSERT if no
	EXEC @serverId = USP_Server @servername,@returnServerCodeName;

	---find ServerPubId if PubId + ServerId already exist
	SELECT @serverPubId = ServerPubId FROM ServerPub
	WHERE PubId = @pubId AND ServerId = @serverId

	---insert new ServerPub if non exist
	IF @serverPubId = 0
	BEGIN
		INSERT INTO ServerPub (ServerId, PubId)
		VALUES (@serverId, @pubId);
		SET @serverPubId = @@IDENTITY
	END
RETURN @serverPubId--returns ServerPubId
GO --
/*END UPSERT FOR SERVER Pub INDEX
SAMPLE
EXEC USP_ServerPub 'SUAN','ACTIVE','Pub CONTAINING SOFTWARE UPDATES','RESIDENT','ACTIVE','ALL_HullS','ACTIVE','AUTO','ACTIVE','HIGH','ACTIVE','GREEN','ACTIVE','DDG1000USV01','ACTIVE','AA01','ACTIVE';
*/

/*CREATE UPSERT FOR AfloatPubLevel*/
CREATE PROCEDURE [dbo].[USP_AfloatPubLevel]---AfloatPubLevelId(GENERATED OR RETURNED),AfloatPubLevel(PASSED)
	@afloatPubLevel varchar(50),--PASSED FOR INSERT_1
	@afloatPubReleaseTimestamp varchar(50),--PASSED FOR INSERT_2
	--SERVERPUB PARAMETERS
	--
	--for pub index usp call
	--
	@pubName varchar(50),--PASSED FOR INSERT_3
	@pubDescription varchar(255),--PASSED FOR INSERT_5
	--passed for priority usp call_6
	@priorityName varchar(50),--_call 7
	--passed for execution_mode usp call_9
	@executionModeName varchar(50),--call_10
	--passed for pub_type usp call_10
	@pubTypeName varchar(50),--11
	--passed for pub_scope usp call
	@pubScopeName varchar(50),--13
	--
	--for server index usp call
	--
	@servername varchar(50),--PASSED FOR INSERT
	@returnServerCodeName varchar(50),--PASSED FOR USP CALL
	---amendment inserts
	@amendmentName varchar(50),
	@amendmentContent varchar(max),
	@size bigint,
	@compulsory varchar(50),
	@postProcessingFile varchar(255),
	@dependency varchar(50),
	@amendmentCreationTimestamp datetime2
AS
	--print 'USP_AfloatPubLevel';
	DECLARE @afloatPubLevelId int;--DECLARE AfloatPubLevelId
	DECLARE @serverPubId int;
	DECLARE @tableName varchar(50) = 'AfloatPubLevel';
	SET @afloatPubLevelId = 0;--SET AfloatPubLevelId

	--find ServerPubId insert if it does not exist
	EXEC @serverPubId = USP_ServerPub @pubName,@pubDescription,@priorityName,@executionModeName,@pubTypeName,@pubScopeName,@servername,@returnServerCodeName;

	SELECT @afloatPubLevelId = AfloatPubLevelId FROM AfloatPubLevel
	WHERE AfloatPubLevel = @afloatPubLevel AND ServerPubId = @serverPubId;--write in AfloatPubLevelId if already present

	---if current afloatPubId is not present create new afloatPubId
	IF @afloatPubLevelId = 0
	BEGIN
		INSERT INTO AfloatPubLevel (ServerPubId,AfloatPubLevel,AfloatPubReleaseTimestamp,AmendmentName,AmendmentContent,Size,Compulsory,PostProcessingFile,Dependency,AmendmentCreationTimestamp)
		VALUES (@serverPubId,@afloatPubLevel,@afloatPubReleaseTimestamp,@amendmentName,@amendmentContent,@size,@compulsory,@postProcessingFile,@dependency,@amendmentCreationTimestamp);
		--set the inserted afloatPubId
		set @afloatPubLevelId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE AfloatPubLevel
		SET AfloatPubReleaseTimestamp = @afloatPubReleaseTimestamp
		WHERE AfloatPubReleaseTimestamp <> @afloatPubReleaseTimestamp AND AfloatPubLevelId = @afloatPubLevelId;
		UPDATE AfloatPubLevel
		SET AmendmentName = @amendmentName
		WHERE AmendmentName <> @amendmentName AND AfloatPubLevelId = @afloatPubLevelId;
		UPDATE AfloatPubLevel
		SET Size = @size
		WHERE Size <> @size AND AfloatPubLevelId = @afloatPubLevelId;
		UPDATE AfloatPubLevel
		SET Compulsory = @compulsory
		WHERE Compulsory <> @compulsory AND AfloatPubLevelId = @afloatPubLevelId;
		UPDATE AfloatPubLevel
		SET PostProcessingFile = @postProcessingFile
		WHERE PostProcessingFile <> @postProcessingFile AND AfloatPubLevelId = @afloatPubLevelId;
		UPDATE AfloatPubLevel
		SET AmendmentCreationTimestamp = @amendmentCreationTimestamp
		WHERE AmendmentCreationTimestamp <> @amendmentCreationTimestamp AND AfloatPubLevelId = @afloatPubLevelId;
		UPDATE AfloatPubLevel
		SET AmendmentContent = @amendmentContent
		WHERE AmendmentContent <> @amendmentContent AND AfloatPubLevelId = @afloatPubLevelId;
	END
	EXEC USP_BusinessRuleTargetAreaExecutor @tableName;
return @afloatPubLevelId--RETURNS AfloatPubLevelId
GO --
/*
SAMPLE
USE [HAMS3]
GO --
EXEC USP_AfloatPubLevel '0','2017-05-02 11:40:32.537','SUAN','ACTIVE','Pub CONTAINING SOFTWARE UPDATES','RESIDENT','ACTIVE','ALL_HullS','ACTIVE','AUTO','ACTIVE','HIGH','ACTIVE','GREEN','ACTIVE','DDG1000USV01','ACTIVE','AA01','ACTIVE','HULL-SPECIFIC_DDG-1000_SUAN','8678987','Y','C:\PATH','NULL','2017-05-02 11:40:32.537';
END UPSERT FOR AfloatPubLevel
updated upsert
*/

/*CREATE UPSERT FOR PERMISSION INDEX*/
CREATE PROCEDURE [dbo].[USP_Permission]---ModuleId(GENERATED OR RETURNED), Module(PASSED)
	@module varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_Permission';
	DECLARE @moduleId int;--declare ModuleId
	SET @moduleId = 0;--set ModuleId

	--
	SELECT @moduleId = ModuleId FROM Permission
	WHERE ModuleName = @module;--write in ModuleId if already present

	IF @moduleId = 0--if ModuleId is not present INSERT new Module and generate new ModuleId
	BEGIN
		INSERT INTO Permission(ModuleName)
		VALUES (@module);
		--set the inserted moduleId
		set @moduleId = @@IDENTITY
	END
return @moduleId--PROC RETURNS ModuleId
GO --
/*
SAMPLE
EXEC USP_Permission 'MODULE1';
EXEC USP_Permission 'MODULE2';
EXEC USP_Permission 'MODULE3';
END UPSERT FOR PERMISSION INDEX
updated upsert
*/

/*CREATE UPSERT FOR USER INDEX*/
CREATE PROCEDURE [dbo].[USP_UserIndex]---UserIndexId(GENERATED OR RETURNED), User(PASSED)
	@cac varchar(50),--PASSED FOR INSERT
	@firstName varchar(100),
	@lastName varchar(100)
AS
	--print 'USP_UserIndex';
	DECLARE @userIndexId int;--declare UserIndexId
	SET @userIndexId = 0;--set UserIndexId

	SELECT @userIndexId = UserIndexId FROM UserIndex
	WHERE Cac = @cac;--write in UserIndexId if already present

	IF @userIndexId = 0--if UserIndexId is not present INSERT new User and generate new UserIndexId
	BEGIN
		INSERT INTO UserIndex(Cac,FirstName,LastName)
		VALUES (@cac,@firstName,@lastName);
		--set the inserted userIndexId
		set @userIndexId = @@IDENTITY
	END
return @userIndexId--PROC RETURNS UserIndexId
GO --
/*
SAMPLE
EXEC USP_UserIndex '45678456789','Stephen','Neese';
END UPSERT FOR USER INDEX
updated upsert
*/

/*CREATE UPSERT FOR UserStatistic*/
CREATE PROCEDURE [dbo].[USP_UserStatistic]---UserStatisticId(GENERATED OR RETURNED), User(PASSED)
	@userPermissionId int,
	@accessedTimestamp datetime2
AS
	--print 'USP_UserStatistic';
	DECLARE @userStatisticId int;--declare UserStatisticId
	SET @userStatisticId = 0;--set UserStatisticId

	SELECT @userStatisticId = UserStatisticId FROM UserStatistic
	WHERE UserPermissionId = @userPermissionId AND AccessedTimestamp = @accessedTimestamp;--write in UserStatisticId if already present

	IF @userStatisticId = 0--if UserStatisticId is not present INSERT new User and generate new UserStatisticId
	BEGIN
		INSERT INTO UserStatistic(UserPermissionId,AccessedTimestamp)
		VALUES (@userPermissionId,@accessedTimestamp);
		--set the inserted userId
		set @userStatisticId = @@IDENTITY
	END
return @userStatisticId--PROC RETURNS UserStatisticId
GO --
/*
SAMPLE
EXEC USP_UserStatistic '45678456789','Stephen','Neese','ACTIVE','Main Dashboard','2017-05-02 11:50:32.537';
END UPSERT FOR UserStatistic
updated upsert
*/
/*CREATE UPSERT FOR UserPermission*/
CREATE PROCEDURE [dbo].[USP_UserPermission]---UserPermissionId(GENERATED OR RETURNED), User(PASSED)
	@cac varchar(50),--PASSED FOR INSERT
	@firstName varchar(100),
	@lastName varchar(100),
	@module varchar(50)
AS
	--print 'USP_UserPermission';
	DECLARE @userPermissionId int;--declare UserPermissionId
	DECLARE @userId int;
	DECLARE @moduleId int;
	SET @userPermissionId = 0;--set UserPermissionId

	EXEC @userId = USP_UserIndex @cac,@firstName,@lastName;

	EXEC @moduleId = USP_Permission @module;

	SELECT @userPermissionId = UserPermissionId FROM UserPermission
	WHERE UserIndexId = @userId AND ModuleId = @moduleId;--write in UserPermissionId if already present

	IF @userPermissionId = 0--if UserPermissionId is not present INSERT new User and generate new UserPermissionId
	BEGIN
		INSERT INTO UserPermission(UserIndexId,ModuleId)
		VALUES (@userId,@moduleId);
		--set the inserted userId
		set @userPermissionId = @@IDENTITY
	END
return @userPermissionId--PROC RETURNS UserPermissionId
GO --
/*
SAMPLE
EXEC USP_UserPermission '45678456789','Stephen','Neese','ACTIVE','MODULE1';
END UPSERT FOR UserPermission
updated upsert
*/

/*CREATE UPSERT FOR BUSINESS RULE TARGET AREA INDEX*/
CREATE PROCEDURE [dbo].[USP_BusinessRuleTargetArea]---BusinessRuleTargetAreaId(GENERATED OR RETURNED), BusinessRuleTargetAreaTable(PASSED)
	@businessRuleTargetAreaGroup varchar(50),
	@businessRuleTargetAreaTable varchar(50),
	@businessRuleTargetAreaField varchar(50),
	@businessRuleTargetAreaLevelId int
AS
	--print 'USP_BusinessRuleTargetArea';
	DECLARE @businessRuleTargetAreaId int;
	SET @businessRuleTargetAreaId = 0;

	--
	SELECT @businessRuleTargetAreaId = BusinessRuleTargetAreaId FROM BusinessRuleTargetArea
	WHERE BusinessRuleTargetAreaGroup = @businessRuleTargetAreaGroup 
	AND BusinessRuleTargetAreaTable = @businessRuleTargetAreaTable 
	AND BusinessRuleTargetAreaField = @businessRuleTargetAreaField 
	AND BusinessRuleTargetAreaLevelId = @businessRuleTargetAreaLevelId;--write in BusinessRuleTargetAreaId if already present

	IF @businessRuleTargetAreaId = 0--if BusinessRuleTargetAreaId is not present INSERT new BusinessRuleTargetAreaTable and generate new BusinessRuleTargetAreaId
	BEGIN
		INSERT INTO BusinessRuleTargetArea(BusinessRuleTargetAreaGroup,BusinessRuleTargetAreaTable,BusinessRuleTargetAreaField,BusinessRuleTargetAreaLevelId)
		VALUES (@businessRuleTargetAreaGroup,@businessRuleTargetAreaTable,@businessRuleTargetAreaField,@businessRuleTargetAreaLevelId);
		--set the inserted businessRuleTargetAreaId
		set @businessRuleTargetAreaId = @@IDENTITY
	END
return @businessRuleTargetAreaId--PROC RETURNS BusinessRuleTargetAreaId
GO --
/*
SAMPLE
EXEC USP_BusinessRuleTargetArea 'Hull';
EXEC USP_BusinessRuleTargetArea 'HullServer';
EXEC USP_BusinessRuleTargetArea 'ServerPub';
EXEC USP_BusinessRuleTargetArea 'DsUpdateActivity';
EXEC USP_BusinessRuleTargetArea 'ServerAppVersionMonitoring';
END UPSERT FOR BUSINESS RULE TARGET AREA INDEX
updated upsert
*/


/*CREATE UPSERT FOR BUSINESS RULE CONDITION TYPE INDEX*/
CREATE PROCEDURE [dbo].[USP_BusinessRuleConditionType]---BusinessRuleConditionTypeId(GENERATED OR RETURNED), BusinessRuleConditionTypeName(PASSED)
	@businessRuleConditionTypeName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_BusinessRuleConditionType';
	DECLARE @businessRuleConditionTypeId int;--declare BusinessRuleConditionTypeId
	SET @businessRuleConditionTypeId = 0;--set BusinessRuleConditionTypeId

	--
	SELECT @businessRuleConditionTypeId = BusinessRuleConditionTypeId FROM BusinessRuleConditionType
	WHERE BusinessRuleConditionTypeName = @businessRuleConditionTypeName;--write in BusinessRuleConditionTypeId if already present

	IF @businessRuleConditionTypeId = 0--if BusinessRuleConditionTypeId is not present INSERT new BusinessRuleConditionTypeName and generate new BusinessRuleConditionTypeId
	BEGIN
		INSERT INTO BusinessRuleConditionType(BusinessRuleConditionTypeName)
		VALUES (@businessRuleConditionTypeName);
		--set the inserted businessRuleConditionTypeId
		set @businessRuleConditionTypeId = @@IDENTITY
	END
return @businessRuleConditionTypeId--PROC RETURNS BusinessRuleConditionTypeId
GO --
/*
SAMPLE
EXEC USP_BusinessRuleConditionType 'datetime2';
EXEC USP_BusinessRuleConditionType 'varchar(100)';
END UPSERT FOR BUSINESS RULE CONDITION TYPE INDEX
updated upsert
*/

/*CREATE UPSERT FOR BUSINESS RULE OPERATOR INDEX*/
CREATE PROCEDURE [dbo].[USP_BusinessRuleOperator]---BusinessRuleOperatorId(GENERATED OR RETURNED), BusinessRuleOperatorName(PASSED)
	@businessRuleOperatorName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_BusinessRuleOperator';
	DECLARE @businessRuleOperatorId int;--declare BusinessRuleOperatorId
	SET @businessRuleOperatorId = 0;--set BusinessRuleOperatorId

	--
	SELECT @businessRuleOperatorId = BusinessRuleOperatorId FROM BusinessRuleOperator
	WHERE BusinessRuleOperatorName = @businessRuleOperatorName;--write in BusinessRuleOperatorId if already present

	IF @businessRuleOperatorId = 0--if BusinessRuleOperatorId is not present INSERT new BusinessRuleOperatorName and generate new BusinessRuleOperatorId
	BEGIN
		INSERT INTO BusinessRuleOperator(BusinessRuleOperatorName)
		VALUES (@businessRuleOperatorName);
		--set the inserted businessRuleOperatorId
		set @businessRuleOperatorId = @@IDENTITY
	END
return @businessRuleOperatorId--PROC RETURNS BusinessRuleOperatorId
GO --
/*
SAMPLE
EXEC USP_BusinessRuleOperator '<';
EXEC USP_BusinessRuleOperator '>';
EXEC USP_BusinessRuleOperator '=';
EXEC USP_BusinessRuleOperator '!=';
EXEC USP_BusinessRuleOperator '=>';
EXEC USP_BusinessRuleOperator '<=';
END UPSERT FOR BUSINESS RULE OPERATOR INDEX
updated upsert
*/

/*CREATE UPSERT FOR BUSINESS RULE SENSITIVITY TYPE INDEX*/
CREATE PROCEDURE [dbo].[USP_BusinessRuleSensitivityType]---BusinessRuleSensitivityTypeId(GENERATED OR RETURNED), BusinessRuleSensitivityTypeName(PASSED)
	@businessRuleSensitivityTypeName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_BusinessRuleSensitivityType';
	DECLARE @businessRuleSensitivityTypeId int;--declare BusinessRuleSensitivityTypeId
	SET @businessRuleSensitivityTypeId = 0;--set BusinessRuleSensitivityTypeId

	--
	SELECT @businessRuleSensitivityTypeId = BusinessRuleSensitivityTypeId FROM BusinessRuleSensitivityType
	WHERE BusinessRuleSensitivityTypeName = @businessRuleSensitivityTypeName;--write in BusinessRuleSensitivityTypeId if already present

	IF @businessRuleSensitivityTypeId = 0--if BusinessRuleSensitivityTypeId is not present INSERT new BusinessRuleSensitivityTypeName and generate new BusinessRuleSensitivityTypeId
	BEGIN
		INSERT INTO BusinessRuleSensitivityType(BusinessRuleSensitivityTypeName)
		VALUES (@businessRuleSensitivityTypeName);
		--set the inserted businessRuleSensitivityTypeId
		set @businessRuleSensitivityTypeId = @@IDENTITY
	END
return @businessRuleSensitivityTypeId--PROC RETURNS BusinessRuleSensitivityTypeId
GO --
/*
SAMPLE
EXEC USP_BusinessRuleSensitivityType 'MINUTE';
EXEC USP_BusinessRuleSensitivityType 'HOUR';
EXEC USP_BusinessRuleSensitivityType 'DAY';
EXEC USP_BusinessRuleSensitivityType 'Level(s)';
END UPSERT FOR BUSINESS RULE SENSITIVITY TYPE INDEX
updated upsert
*/

/*CREATE UPSERT FOR BUSINESS RULE  TYPE INDEX*/
CREATE PROCEDURE [dbo].[USP_BusinessRuleType]---BusinessRuleTypeId(GENERATED OR RETURNED), BusinessRuleTypeName(PASSED)
	@businessRuleTypeName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_BusinessRuleType';
	DECLARE @businessRuleTypeId int;--declare BusinessRuleTypeId
	SET @businessRuleTypeId = 0;--set BusinessRuleTypeId

	--
	SELECT @businessRuleTypeId = BusinessRuleTypeId FROM BusinessRuleType
	WHERE BusinessRuleTypeName = @businessRuleTypeName;--write in BusinessRuleTypeId if already present

	IF @businessRuleTypeId = 0--if BusinessRuleTypeId is not present INSERT new BusinessRuleTypeName and generate new BusinessRuleTypeId
	BEGIN
		INSERT INTO BusinessRuleType(BusinessRuleTypeName)
		VALUES (@businessRuleTypeName);
		--set the inserted businessRuleTypeId
		set @businessRuleTypeId = @@IDENTITY
	END
return @businessRuleTypeId--PROC RETURNS BusinessRuleTypeId
GO --
/*
SAMPLE
EXEC USP_BusinessRuleType 'Dashboard';
EXEC USP_BusinessRuleType 'Reporting';
EXEC USP_BusinessRuleType 'Alerts';
END UPSERT FOR BUSINESS RULE  TYPE INDEX
updated upsert
*/


/*CREATE UPSERT FOR BUSINESS RULE INDEX*/
CREATE PROCEDURE [dbo].[USP_BusinessRule]---BusinessRuleId(GENERATED OR RETURNED), BusinessRuleTable(PASSED)
	@businessRuleTypeId int,
	@businessRuleConditionTypeId int,
	@businessRuleOperatorId int,
	@businessRuleSensitivityTypeId int,
	@businessRuleStatusId int,
	@conditionOne varchar(100),
	@sensitivity varchar(100),
	@conditionTwo varchar(100)
AS
	--print 'USP_BusinessRule';
	DECLARE @businessRuleId int;
	SET @businessRuleId = 0;

	--
	SELECT @businessRuleId = BusinessRuleId FROM BusinessRule
	WHERE BusinessRuleTypeId = @businessRuleTypeId AND BusinessRuleConditionTypeId = @businessRuleConditionTypeId AND 
	BusinessRuleOperatorId = @businessRuleOperatorId AND BusinessRuleSensitivityTypeId = @businessRuleSensitivityTypeId 
	AND StatusId = @businessRuleStatusId AND ConditionOne = @conditionOne AND Sensitivity = @sensitivity AND ConditionTwo = @conditionTwo;

	IF @businessRuleId = 0--if BusinessRuleId is not present INSERT new BusinessRuleTable and generate new BusinessRuleId
	BEGIN
		INSERT INTO BusinessRule(BusinessRuleTypeId,BusinessRuleConditionTypeId,BusinessRuleOperatorId,BusinessRuleSensitivityTypeId,StatusId,ConditionOne,Sensitivity,ConditionTwo)
		VALUES (@businessRuleTypeId,@businessRuleConditionTypeId,@businessRuleOperatorId,@businessRuleSensitivityTypeId,@businessRuleStatusId,@conditionOne,@sensitivity,@conditionTwo);
		--set the inserted businessRuleId
		set @businessRuleId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE BusinessRule
		SET BusinessRuleConditionTypeId = @businessRuleConditionTypeId
		WHERE BusinessRuleConditionTypeId <> @businessRuleConditionTypeId AND BusinessRuleId = @businessRuleId;
		UPDATE BusinessRule
		SET BusinessRuleOperatorId = @businessRuleOperatorId
		WHERE BusinessRuleOperatorId <> @businessRuleOperatorId AND BusinessRuleId = @businessRuleId;		
		UPDATE BusinessRule
		SET BusinessRuleSensitivityTypeId = @businessRuleSensitivityTypeId
		WHERE BusinessRuleSensitivityTypeId <> @businessRuleSensitivityTypeId AND BusinessRuleId = @businessRuleId;		
		UPDATE BusinessRule
		SET ConditionOne = @conditionOne
		WHERE ConditionOne <> @conditionOne AND BusinessRuleId = @businessRuleId;	
		UPDATE BusinessRule
		SET Sensitivity = @sensitivity WHERE BusinessRuleId = @businessRuleId;
		UPDATE BusinessRule
		SET ConditionTwo = @conditionTwo WHERE BusinessRuleId = @businessRuleId;
	END
return @businessRuleId--PROC RETURNS BusinessRuleId
GO --
/*
SAMPLE
EXEC USP_Status 'GREEN';
EXEC USP_BusinessRule '1','1','1','1','1','DsUpdateActivity.ContactTimestamp','3','CURRENT_TIMESTAMP';
END UPSERT FOR BUSINESS RULE INDEX
updated upsert
*/


/*CREATE UPSERT FOR AshorePubLevel*/
CREATE PROCEDURE [dbo].[USP_AshorePubLevel]---AshorePubLevelId(GENERATED OR RETURNED),AshorePubLevel(PASSED)
	@ashorePubLevel varchar(50),--PASSED FOR INSERT_1
	@ashorePubReleaseTimestamp varchar(50),--PASSED FOR INSERT_2
	--SERVERPUB PARAMETERS
	--
	--for pub index usp call
	--
	@pubName varchar(50),--PASSED FOR INSERT_3
	@pubDescription varchar(255),--PASSED FOR INSERT_5
	--passed for priority usp call_6
	@priorityName varchar(50),--_call 7
	--passed for execution_mode usp call_9
	@executionModeName varchar(50),--call_10
	--passed for pub_type usp call_10
	@pubTypeName varchar(50),--11
	--passed for pub_scope usp call
	@pubScopeName varchar(50),--13
	--
	--for server index usp call
	--
	@servername varchar(50),--PASSED FOR INSERT
	@returnServerCodeName varchar(50),--PASSED FOR USP CALL
	---amendment inserts
	@amendmentName varchar(50),
	@amendmentContent varchar(max),
	@size bigint,
	@compulsory varchar(50),
	@postProcessingFile varchar(255),
	@dependency varchar(50),
	@amendmentCreationTimestamp datetime2
AS
	--print 'USP_AshorePubLevel';
	DECLARE @ashorePubLevelId int;--DECLARE AshorePubLevelId
	DECLARE @serverPubId int;
	DECLARE @tableName varchar(50) = 'AshorePubLevel';
	SET @ashorePubLevelId = 0;--SET AshorePubLevelId

	--find ServerPubId insert if it does not exist
	EXEC @serverPubId = USP_ServerPub @pubName,@pubDescription,@priorityName,@executionModeName,@pubTypeName,@pubScopeName,@servername,@returnServerCodeName;

	SELECT @ashorePubLevelId = AshorePubLevelId FROM AshorePubLevel
	WHERE AshorePubLevel = @ashorePubLevel AND ServerPubId = @serverPubId;--write in AshorePubLevelId if already present

	---if current AshorePubId is not present create new AshorePubId
	IF @ashorePubLevelId = 0
	BEGIN 
		INSERT INTO AshorePubLevel (ServerPubId,AshorePubLevel,AshorePubReleaseTimestamp,AmendmentName,AmendmentContent,Size,Compulsory,PostProcessingFile,Dependency,AmendmentCreationTimestamp)
		VALUES (@serverPubId,@ashorePubLevel,@ashorePubReleaseTimestamp,@amendmentName,@amendmentContent,@size,@compulsory,@postProcessingFile,@dependency,@amendmentCreationTimestamp);
		--set the inserted AshorePubId
		set @ashorePubLevelId = @@IDENTITY;
	END
	ELSE
	BEGIN
		UPDATE AshorePubLevel
		SET AshorePubReleaseTimestamp = @ashorePubReleaseTimestamp
		WHERE AshorePubReleaseTimestamp <> @ashorePubReleaseTimestamp AND AshorePubLevelId = @ashorePubLevelId;
		UPDATE AshorePubLevel
		SET AmendmentName = @amendmentName
		WHERE AmendmentName <> @amendmentName AND AshorePubLevelId = @ashorePubLevelId;
		UPDATE AshorePubLevel
		SET Size = @size
		WHERE Size <> @size AND AshorePubLevelId = @ashorePubLevelId;
		UPDATE AshorePubLevel
		SET Compulsory = @compulsory
		WHERE Compulsory <> @compulsory AND AshorePubLevelId = @ashorePubLevelId;
		UPDATE AshorePubLevel
		SET PostProcessingFile = @postProcessingFile
		WHERE PostProcessingFile <> @postProcessingFile AND AshorePubLevelId = @ashorePubLevelId;
		UPDATE AshorePubLevel
		SET AmendmentCreationTimestamp = @amendmentCreationTimestamp
		WHERE AmendmentCreationTimestamp <> @amendmentCreationTimestamp AND AshorePubLevelId = @ashorePubLevelId;
		UPDATE AshorePubLevel
		SET AmendmentContent = @amendmentContent
		WHERE AmendmentContent <> @amendmentContent AND AshorePubLevelId = @ashorePubLevelId;
	END
	EXEC USP_BusinessRuleTargetAreaExecutor @tableName;
return @ashorePubLevelId--RETURNS AshorePubLevelId
GO --
/*
SAMPLE
USE [HAMS3]
GO --
EXEC USP_AshorePubLevel '1','2017-05-02 11:40:32.537','SUAN','Pub CONTAINING SOFTWARE UPDATES','RESIDENT','ALL_HullS','AUTO','HIGH','DDG1000USV01','AA01','HULL-SPECIFIC_DDG-1000_SUAN','8678987','Y','C:\PATH','NULL','2017-05-02 11:40:32.537';
END UPSERT FOR AshorePubLevel
updated upsert
*/

/*CREATE UPSERT FOR AshoreClientLevel*/
CREATE PROCEDURE [dbo].[USP_AshoreClientLevel]---AshoreClientLevelId(GENERATED OR RETURNED),AshoreClientLevel(PASSED)
	@ashoreClientLevel varchar(50),--PASSED FOR INSERT_1
	@ashoreClientPubExtractionTimestamp varchar(50),--PASSED FOR INSERT_2
	--SERVERPUB PARAMETERS
	--
	--for pub index usp call
	--
	@pubName varchar(50),--PASSED FOR INSERT_3
	@pubDescription varchar(255),--PASSED FOR INSERT_5
	--passed for priority usp call_6
	@priorityName varchar(50),--_call 7
	--passed for execution_mode usp call_9
	@executionModeName varchar(50),--call_10
	--passed for pub_type usp call_10
	@pubTypeName varchar(50),--11
	--passed for pub_scope usp call
	@pubScopeName varchar(50),--13
	--
	--for server index usp call
	--
	@postProcessingSuccess varchar(3),
	@servername varchar(50),--PASSED FOR INSERT
	@returnServerCodeName varchar(50)--PASSED FOR USP CALL
AS
	--print 'USP_AshoreClientLevel';
	DECLARE @ashoreClientLevelId int;--DECLARE AshoreClientLevelId
	DECLARE @serverClientId int;
	DECLARE @tableName varchar(50) = 'AshoreClientLevel';
	SET @ashoreClientLevelId = 0;--SET AshoreClientLevelId
	--find ServerPubId insert if it does not exist
	EXEC @serverClientId = USP_ServerPub @pubName,@pubDescription,@priorityName,@executionModeName,@pubTypeName,@pubScopeName,@servername,@returnServerCodeName;

	SELECT @ashoreClientLevelId = AshoreClientLevelId FROM AshoreClientLevel
	WHERE AshoreClientLevel = @ashoreClientLevel AND ServerPubId = @serverClientId;--write in AshoreClientLevelId if already present

	---if current AshoreClientId is not present create new AshoreClientId
	IF @ashoreClientLevelId = 0
	BEGIN
		INSERT INTO AshoreClientLevel (ServerPubId,AshoreClientLevel,PostProcessingSuccess,AshoreClientPubExtractionTimestamp)
		VALUES (@serverClientId,@ashoreClientLevel,@postProcessingSuccess,@ashoreClientPubExtractionTimestamp);
		--set the inserted AshoreClientId
		set @ashoreClientLevelId = @@IDENTITY;
	END
	ELSE
	BEGIN
		UPDATE AshoreClientLevel
		SET AshoreClientPubExtractionTimestamp = @ashoreClientPubExtractionTimestamp
		WHERE AshoreClientPubExtractionTimestamp <> @ashoreClientPubExtractionTimestamp AND AshoreClientLevelId = @ashoreClientLevelId;
		UPDATE AshoreClientLevel
		SET PostProcessingSuccess = @postProcessingSuccess
		WHERE PostProcessingSuccess <> @postProcessingSuccess AND AshoreClientLevelId = @ashoreClientLevelId;
	END
	EXEC USP_BusinessRuleTargetAreaExecutor @tableName;
return @ashoreClientLevelId--RETURNS AshoreClientLevelId
GO --
/*
SAMPLE
USE [HAMS3]
GO --
EXEC USP_AshoreClientLevel '0','2017-05-02 11:40:32.537','SUAN','Client CONTAINING SOFTWARE UPDATES','RESIDENT','ALL_HullS','AUTO','HIGH','DDG1000USV01','AA01';
END UPSERT FOR AshoreClientLevel
updated upsert
*/

/*CREATE UPSERT FOR AfloatClientLevel*/
CREATE PROCEDURE [dbo].[USP_AfloatClientLevel]---AfloatClientLevelId(GENERATED OR RETURNED),AfloatClientLevel(PASSED)
	@afloatClientLevel varchar(50),--PASSED FOR INSERT_1
	@afloatClientPubExtractionTimestamp varchar(50),--PASSED FOR INSERT_2
	--SERVERPUB PARAMETERS
	--
	--for pub index usp call
	--
	@pubName varchar(50),--PASSED FOR INSERT_3
	@pubDescription varchar(255),--PASSED FOR INSERT_5
	--passed for priority usp call_6
	@priorityName varchar(50),--_call 7
	--passed for execution_mode usp call_9
	@executionModeName varchar(50),--call_10
	--passed for pub_type usp call_10
	@pubTypeName varchar(50),--11
	--passed for pub_scope usp call
	@pubScopeName varchar(50),--13
	--
	--for server index usp call
	--
	@postProcessingSuccess varchar(3),--this not part of server index usp call, but part of client level specific data
	@servername varchar(50),--PASSED FOR INSERT
	@returnServerCodeName varchar(50)--PASSED FOR USP CALL
AS
	--print 'USP_AfloatClientLevel';
	DECLARE @afloatClientLevelId int;--DECLARE AfloatClientLevelId
	DECLARE @serverClientId int;
	DECLARE @tableName varchar(50) = 'AfloatClientLevel';
	SET @afloatClientLevelId = 0;--SET AfloatClientLevelId

	--find ServerPubId insert if it does not exist
	EXEC @serverClientId = USP_ServerPub @pubName,@pubDescription,@priorityName,@executionModeName,@pubTypeName,@pubScopeName,@servername,@returnServerCodeName;

	SELECT @afloatClientLevelId = AfloatClientLevelId FROM AfloatClientLevel
	WHERE AfloatClientLevel = @afloatClientLevel AND ServerPubId = @serverClientId;--write in AfloatClientLevelId if already present
	---if current AfloatClientId is not present create new AfloatClientId
	IF @afloatClientLevelId = 0
	BEGIN
		INSERT INTO AfloatClientLevel (ServerPubId,AfloatClientLevel,PostProcessingSuccess,AfloatClientPubExtractionTimestamp)
		VALUES (@serverClientId,@afloatClientLevel,@postProcessingSuccess,@afloatClientPubExtractionTimestamp);
		--set the inserted AfloatClientId
		set @afloatClientLevelId = @@IDENTITY;
	END
	ELSE
	BEGIN
		UPDATE AfloatClientLevel
		SET AfloatClientPubExtractionTimestamp = @afloatClientPubExtractionTimestamp
		WHERE AfloatClientPubExtractionTimestamp <> @afloatClientPubExtractionTimestamp AND AfloatClientLevelId = @afloatClientLevelId;
		UPDATE AfloatClientLevel
		SET PostProcessingSuccess = @postProcessingSuccess
		WHERE PostProcessingSuccess <> @postProcessingSuccess AND AfloatClientLevelId = @afloatClientLevelId;

	END
	EXEC USP_BusinessRuleTargetAreaExecutor @tableName;
return @afloatClientLevelId--RETURNS AfloatClientLevelId
GO --
/*
SAMPLE
USE [HAMS3]
GO --
EXEC USP_AfloatClientLevel '0','2017-05-02 11:40:32.537','SUAN','Client CONTAINING SOFTWARE UPDATES','RESIDENT','ALL_HullS','AUTO','HIGH','DDG1000USV01','AA01';
END UPSERT FOR AfloatClientLevel
updated upsert
*/

/*start find binding table*/
CREATE FUNCTION BindingTable
(@tableOne varchar(100),
@tableTwo varchar(100))
RETURNS varchar(100)
AS
BEGIN
DECLARE @return varchar(100)
SELECT @return = (SELECT OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName
FROM sys.foreign_keys AS f 
INNER JOIN sys.foreign_key_columns AS fc 
   ON f.OBJECT_ID = fc.constraint_object_id
   WHERE OBJECT_NAME(f.parent_object_id) = @tableOne
INTERSECT
SELECT OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName
FROM sys.foreign_keys AS f 
INNER JOIN sys.foreign_key_columns AS fc 
   ON f.OBJECT_ID = fc.constraint_object_id
   WHERE OBJECT_NAME(f.parent_object_id) = @tableTwo);
RETURN @return
END
GO --
/*end find binding table*/

/*CREATE UPSERT FOR TARGET ANCHOR UPSERT*/
CREATE PROCEDURE [dbo].[USP_TargetAnchor]---TargetAnchorId(GENERATED OR RETURNED), TargetAnchorTable(PASSED)
	@targetAnchorName varchar(100)
AS
	--print 'USP_TargetAnchor';
	DECLARE @targetAnchorId int;
	SET @targetAnchorId = 0;

	--
	SELECT @targetAnchorId = TargetAnchorId FROM TargetAnchor
	WHERE TargetAnchorName = @targetAnchorName;--write in TargetAnchorId if already present

	IF @targetAnchorId = 0--if TargetAnchorId is not present INSERT new TargetAnchorTable and generate new TargetAnchorId
	BEGIN
		INSERT INTO TargetAnchor(TargetAnchorName)
		VALUES (@targetAnchorName);
		--set the inserted targetAnchorId
		set @targetAnchorId = @@IDENTITY
	END
return @targetAnchorId--PROC RETURNS TargetAnchorId
GO --
/*
SAMPLE
EXEC USP_TargetAnchor 'ServerPub';
EXEC USP_TargetAnchor 'HullServer';
END UPSERT FOR TARGET ANCHOR UPSERT
updated upsert
*/

/*CREATE UPSERT FOR BusinessRuleTargetAreaLevel*/
CREATE PROCEDURE [dbo].[USP_BusinessRuleTargetAreaLevel]---BusinessRuleTargetAreaLevelId(GENERATED OR RETURNED), BusinessRuleTargetAreaLevelName(PASSED)
	@businessRuleTargetAreaLevelName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_BusinessRuleTargetAreaLevel';
	DECLARE @businessRuleTargetAreaLevelId int;--declare BusinessRuleTargetAreaLevelId
	SET @businessRuleTargetAreaLevelId = 0;--set BusinessRuleTargetAreaLevelId

	--
	SELECT @businessRuleTargetAreaLevelId = BusinessRuleTargetAreaLevelId FROM BusinessRuleTargetAreaLevel
	WHERE BusinessRuleTargetAreaLevelName = @businessRuleTargetAreaLevelName;--write in BusinessRuleTargetAreaLevelId if already present

	IF @businessRuleTargetAreaLevelId = 0--if BusinessRuleTargetAreaLevelId is not present INSERT new BusinessRuleTargetAreaLevelName and generate new BusinessRuleTargetAreaLevelId
	BEGIN
		INSERT INTO BusinessRuleTargetAreaLevel(BusinessRuleTargetAreaLevelName)
		VALUES (@businessRuleTargetAreaLevelName);
		--set the inserted businessRuleTargetAreaLevelId
		set @businessRuleTargetAreaLevelId = @@IDENTITY
	END
return @businessRuleTargetAreaLevelId--PROC RETURNS BusinessRuleTargetAreaLevelId
GO --
/*
SAMPLE
EXEC USP_BusinessRuleTargetAreaLevel 'Group';
EXEC USP_BusinessRuleTargetAreaLevel 'Table';
EXEC USP_BusinessRuleTargetAreaLevel 'Field';
END UPSERT FOR BusinessRuleTargetAreaLevel
updated upsert
*/

CREATE PROCEDURE [dbo].[USP_BusinessRuleUser]---BusinessRuleId(GENERATED OR RETURNED), BusinessRuleTable(PASSED)
	@userPermissionId int,
	@businessRulePriority int,
	@businessRuleTypeId int,
	@businessRuleOneConditionTypeId int,
	@businessRuleOneOperatorId int,
	@businessRuleOneSensitivityTypeId int,
	@businessRuleStatusId int,
	@businessRuleOneTargetAreaId int,
	@businessRuleOneSensitivity varchar(100),
	@businessRuleOneConditionTwo varchar(100),
	@businessRuleOneTargetAreaTwoId int,
	@businessRuleTwoConditionTypeId int,
	@businessRuleTwoOperatorId int,
	@businessRuleTwoSensitivityTypeId int,
	@businessRuleTwoTargetAreaId int,
	@businessRuleTwoSensitivity varchar(100)
AS 
	--print 'USP_BusinessRuleUser';
	DECLARE @businessRuleGroupId int = 0;
	DECLARE @businessRuleUserId int = 0;
	DECLARE @businessRuleOneConditionOne varchar(100);
	DECLARE @businessRuleTwoConditionOne varchar(100);

	SELECT @businessRuleOneConditionOne = BusinessRuleTargetAreaTable FROM BusinessRuleTargetArea WHERE BusinessRuleTargetAreaId = @businessRuleOneTargetAreaId;
	SELECT @businessRuleTwoConditionOne = BusinessRuleTargetAreaTable FROM BusinessRuleTargetArea WHERE BusinessRuleTargetAreaId = @businessRuleTwoTargetAreaId;

	EXEC @businessRuleGroupId = USP_BusinessRuleGroup @businessRuleTypeId,@businessRuleOneConditionTypeId,@businessRuleOneOperatorId,@businessRuleOneSensitivityTypeId,
	@businessRuleStatusId,@businessRuleOneConditionOne,@businessRuleOneSensitivity,@businessRuleOneConditionTwo,@businessRuleTwoConditionTypeId,@businessRuleTwoOperatorId,
	@businessRuleTwoSensitivityTypeId,@businessRuleTwoConditionOne,@businessRuleTwoSensitivity;

	--
			DECLARE @userIndexId int = 0;
			SELECT @userIndexId = UserIndexId FROM UserPermission WHERE UserPermissionId = @userPermissionId;
			/*DECLARE @userIndexIdAlreadyPresent int = 0;
			SELECT @userIndexId = UserIndexId FROM UserPermission WHERE UserPermissionId = @userPermissionId;*/
			DROP TABLE IF EXISTS #TEMP_UserPermissionIdEvaluation
			CREATE TABLE #TEMP_UserPermissionIdEvaluation
			(TEMP_BusinessRuleUserId int,TEMP_UserPermissionId int,TEMP_BusinessRulePriorityId int,TEMP_BusinessRuleTargetAreaId int,TEMP_Sensitivity varchar(100),TEMP_UserIndexId int)
			
			IF @businessRuleTwoSensitivity IS NULL
			BEGIN
				INSERT INTO #TEMP_UserPermissionIdEvaluation(TEMP_BusinessRuleUserId,TEMP_UserPermissionId,TEMP_BusinessRulePriorityId,TEMP_BusinessRuleTargetAreaId,TEMP_Sensitivity,TEMP_UserIndexId)
				SELECT
				a.BusinessRuleUserId,
				a.UserPermissionId,
				a.BusinessRulePriority,
				a.BusinessRuleOneTargetAreaId,
				null,	
				b.UserIndexId
				FROM BusinessRuleUser a
				LEFT JOIN UserPermission b
				ON a.UserPermissionId = b.UserPermissionId
				WHERE a.BusinessRulePriority = @businessRulePriority 
				AND a.BusinessRuleOneTargetAreaId = @businessRuleOneTargetAreaId 
				AND b.UserIndexId = @userIndexId;
			END
			ELSE
			BEGIN
				INSERT INTO #TEMP_UserPermissionIdEvaluation(TEMP_BusinessRuleUserId,TEMP_UserPermissionId,TEMP_BusinessRulePriorityId,TEMP_BusinessRuleTargetAreaId,TEMP_Sensitivity,TEMP_UserIndexId)
				SELECT
				a.BusinessRuleUserId,
				a.UserPermissionId,
				a.BusinessRulePriority,
				a.BusinessRuleOneTargetAreaId,
				d.Sensitivity,			
				b.UserIndexId
				FROM BusinessRuleUser a
				LEFT JOIN UserPermission b
				ON a.UserPermissionId = b.UserPermissionId
				LEFT JOIN BusinessRuleGroup c
				ON a.BusinessRuleGroupId = c.BusinessRuleGroupId
				LEFT JOIN BusinessRule d
				ON c.BusinessRuleTwoId = d.BusinessRuleId
				WHERE a.BusinessRulePriority = @businessRulePriority 
				AND a.BusinessRuleOneTargetAreaId = @businessRuleOneTargetAreaId 
				AND b.UserIndexId = @userIndexId
				AND d.Sensitivity = @businessRuleTwoSensitivity;
			END

			SELECT @businessRuleUserId = MAX(TEMP_BusinessRuleUserId) FROM #TEMP_UserPermissionIdEvaluation;
			IF @businessRuleUserId IS NULL
			BEGIN
				SET @businessRuleUserId = 0;
			END
	IF @businessRuleUserId = 0
	BEGIN
		INSERT INTO BusinessRuleUser(UserPermissionId,BusinessRuleGroupId,BusinessRulePriority,BusinessRuleOneTargetAreaId,BusinessRuleOneTargetAreaTwoId,BusinessRuleTwoTargetAreaId)
		VALUES (@userPermissionId,@businessRuleGroupId,@businessRulePriority,@businessRuleOneTargetAreaId,@businessRuleOneTargetAreaTwoId,@businessRuleTwoTargetAreaId);
		--set the inserted businessRuleId
		set @businessRuleUserId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE BusinessRuleUser
		SET BusinessRuleGroupId = @businessRuleGroupId
		WHERE BusinessRuleGroupId <> @businessRuleGroupId AND BusinessRuleUserId = @businessRuleUserId;
		UPDATE BusinessRuleUser
		SET BusinessRuleOneTargetAreaTwoId = @businessRuleOneTargetAreaTwoId
		WHERE BusinessRuleOneTargetAreaTwoId <> @businessRuleOneTargetAreaTwoId AND BusinessRuleUserId = @businessRuleUserId;	
		UPDATE BusinessRuleUser
		SET UserPermissionId = @userPermissionId
		WHERE UserPermissionId <> @userPermissionId AND BusinessRuleUserId = @businessRuleUserId;
		UPDATE BusinessRuleUser
		SET BusinessRuleTwoTargetAreaId = @businessRuleTwoTargetAreaId
		WHERE BusinessRuleTwoTargetAreaId <> @businessRuleTwoTargetAreaId AND BusinessRuleUserId = @businessRuleUserId;
	END
RETURN @businessRuleUserId--PROC RETURNS BusinessRuleId
GO --
CREATE PROCEDURE [dbo].[USP_NiapsVersionRelease]
	@releaseVersion varchar(50),
	@releaseDate datetime2
AS
	--print 'USP_NiapsVersionRelease';
	DECLARE @niapsVersionReleaseId int;--DECLARE NiapsVersionId
	SET @niapsVersionReleaseId = 0;--SET NiapsVersionId
	DECLARE @releaseNumber int;

	SELECT @niapsVersionReleaseId = NiapsVersionReleaseId FROM NiapsVersionRelease
	WHERE ReleaseVersion = @releaseVersion;--write in NiapsVersionId if already present

	SELECT @releaseNumber = MAX(ReleaseNumber) FROM NiapsVersionRelease;
	IF @releaseNumber IS NULL
	BEGIN
		SET @releaseNumber = 0;
	END
	---if current niapsVersionId is not present create new niapsVersionId
	IF @niapsVersionReleaseId = 0
	BEGIN
		SET @releaseNumber = @releaseNumber + 1;
		INSERT INTO NiapsVersionRelease (ReleaseVersion,ReleaseNumber,ReleaseDate)
		VALUES (@releaseVersion,@releaseNumber,@releaseDate);
		--set the inserted niapsVersionId
		set @niapsVersionReleaseId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE NiapsVersionRelease
		SET ReleaseDate = @releaseDate 
		WHERE ReleaseDate <> @releaseDate AND NiapsVersionReleaseId = @niapsVersionReleaseId;
	END
return @niapsVersionReleaseId--RETURNS NiapsVersionId
GO --
CREATE PROCEDURE [dbo].[USP_AuthAppRelease]
	--for release
	@authVersion varchar(50),--PASSED FOR INSERT
	@authAppReleaseDate datetime2,--PASSED FOR INSERT
	--for authAppId call
	@authAppName varchar(50),--PASSED FOR USP CALL
	@authAppVendorName varchar(50)--PASSED FOR USP CALL
AS
	--print 'USP_AuthAppRelease';
	DECLARE @authAppReleaseId int = 0;--DECLARE AuthAppReleaseId
	DECLARE @authAppId int;--DECLARE AuthAppId
	DECLARE @releaseNumber int = 0;

	--collect authAppId
	EXEC @authAppId = USP_AuthApp @authAppName,@authAppVendorName;
	
	SELECT @releaseNumber = MAX(AuthNumber) FROM AuthAppRelease WHERE AuthAppId = @authAppId;
	IF @releaseNumber IS NULL
	BEGIN
		SET @releaseNumber = 0;
	END
	--find authAppReleaseId if APP+VERSION has already been released
	SELECT @authAppReleaseId = AuthAppReleaseId FROM AuthAppRelease
	WHERE AuthAppId = @authAppId AND AuthVersion = @authVersion;

	---if APP+VERSION is has not been released, release APP+VERSION and create an id
	IF @authAppReleaseId = 0
	BEGIN
		SET @releaseNumber = @releaseNumber + 1;
		INSERT INTO AuthAppRelease (AuthAppId,AuthVersion,AuthNumber,AuthAppReleaseDate)
		VALUES (@authAppId,@authVersion,@releaseNumber,@authAppReleaseDate);
		--set the inserted authAppReleaseId
		set @authAppReleaseId = @@IDENTITY
	END
	/*ELSE
	BEGIN
		UPDATE AuthAppRelease
		SET AuthAppReleaseDate = @authAppReleaseDate
		WHERE AuthAppReleaseDate <> @authAppReleaseDate AND AuthAppReleaseId = @authAppReleaseId;
	END*/
return @authAppReleaseId--RETURNS AuthAppReleaseId
GO --
CREATE PROCEDURE [dbo].[USP_InstalledAfloatAppServer]
	--var list for Server
	@returnServerCodeName varchar(50),--PASSED FOR USP CALL
	@servername varchar(50),--PASSED FOR USP CALL
	--var list for AfloatAppVersion
	@afloatVersion varchar(50),--PASSED FOR USP CALL
	@afloatAppName varchar(50),--PASSED FOR USP CALL
	@afloatAppVendorName varchar(50),--PASSED FOR USP CALL
	--var for date program/app was installed
	@installedDate datetime2 = null, --for insert
	@installationAccount varchar(100) = null,
	@uninstallDate datetime2 = null,
	@uninstallAccount varchar(100) = null
AS
	--print 'USP_InstalledAfloatAppServer';
	DECLARE @serverId int = 0;
	DECLARE @afloatAppVersionId int = 0;
	DECLARE @installedAfloatAppServerId int = 0;

	--find ServerId if it exists already, INSERT if no
	EXEC @serverId = USP_Server @servername,@returnServerCodeName;

	--collect afloatServerAppVersionId, if APP+VERSION does not exist, create it
	EXEC @afloatAppVersionId = USP_AfloatAppVersion @afloatVersion,@afloatAppName,@afloatAppVendorName;

	IF @installedDate IS NOT NULL
	BEGIN
		--find installedAfloatAppServerId if Hull+SERVER+APP+VERSION has already been installed
		SELECT @installedAfloatAppServerId = InstalledAfloatAppServerId FROM InstalledAfloatAppServer 
		WHERE AfloatAppVersionId = @afloatAppVersionId AND ServerId = @serverId and InstalledDate = @installedDate;

		IF @installedAfloatAppServerId = 0
		BEGIN
			INSERT INTO InstalledAfloatAppServer (ServerId,AfloatAppVersionId,InstalledDate,InstallationAccount,UninstallDate,UninstallAccount)
			VALUES (@serverId,@afloatAppVersionId,@installedDate,@installationAccount,@uninstallDate,@uninstallAccount);
			--set the inserted installedAfloatAppServerId
			SET @installedAfloatAppServerId = @@IDENTITY;
		END
		EXEC USP_SoftwareComplianceEvaluation @installedAfloatAppServerId,null,null;
	END
	ELSE
	BEGIN
		DROP TABLE IF EXISTS #Uninstalled;
		CREATE TABLE #Uninstalled (TEMP_InstalledAfloatAppServerId int);

		INSERT INTO #Uninstalled(TEMP_InstalledAfloatAppServerId)
		SELECT InstalledAfloatAppServerId FROM InstalledAfloatAppServer
		WHERE AfloatAppVersionId = @afloatAppVersionId AND ServerId = @serverId AND InstalledDate < @uninstallDate AND UninstallDate IS NULL;
		
		UPDATE InstalledAfloatAppServer
		SET UninstallDate = @uninstallDate,UninstallAccount = @uninstallAccount
		WHERE AfloatAppVersionId = @afloatAppVersionId AND ServerId = @serverId AND InstalledDate < @uninstallDate AND UninstallDate IS NULL;
		
		WHILE(1=1)
		BEGIN
			SELECT @installedAfloatAppServerId = MIN(TEMP_InstalledAfloatAppServerId)
			FROM #Uninstalled WHERE TEMP_InstalledAfloatAppServerId > @installedAfloatAppServerId;
			--print @installedAfloatAppServerId;
			IF @installedAfloatAppServerId IS NULL BREAK;
			EXEC USP_SoftwareComplianceEvaluation @installedAfloatAppServerId,null,null;
		END
		EXEC USP_SoftwareComplianceEvaluation null,null,'1';
	END
RETURN @installedAfloatAppServerId--RETURNS InstalledAfloatAppServerId
GO --

CREATE PROCEDURE [dbo].[USP_AuthAppServerRelease]
	--for authAppServerRelease call
	@authVersion varchar(50),--PASSED FOR INSERT
	@authAppReleaseDate datetime2,--PASSED FOR INSERT
	@authAppName varchar(50),--PASSED FOR USP CALL
	@authAppVendorName varchar(50),--PASSED FOR USP CALL
	--for server call
	@serverId int,
	--for insert
	@serverAppReleaseDate datetime2
AS
	--print 'USP_AuthAppServerRelease';
	DECLARE @authAppId int = 0;--INIT
	DECLARE @authAppServerReleaseId int = 0;--DECLARE AuthAppServerReleaseId
	DECLARE @authAppReleaseId int = 0;--DECLARE AuthAppReleaseId

	--collect authAppReleaseId if App has already been released, if not release it
	EXEC @authAppReleaseId = USP_AuthAppRelease @authVersion,@authAppReleaseDate,@authAppName,@authAppVendorName;

	--find authAppReleaseId if APP+VERSION has already been released
	SELECT @authAppServerReleaseId = AuthAppServerReleaseId FROM AuthAppServerRelease
	WHERE AuthAppReleaseId = @authAppReleaseId AND ServerId = @serverId;

	---if APP+VERSION is has not been released to the appropriated hull, release Hull+APP+VERSION and create an id
	IF @authAppServerReleaseId = 0
	BEGIN
		INSERT INTO AuthAppServerRelease (ServerId,AuthAppReleaseId,ServerAppReleaseDate)
		VALUES (@serverId,@authAppReleaseId,@serverAppReleaseDate);
		--set the inserted authAppServerReleaseId
		set @authAppServerReleaseId = @@IDENTITY
	END
	SELECT @authAppId = (SELECT DISTINCT AuthAppRelease.AuthAppId FROM AuthAppServerRelease
	LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId);
	/*ELSE
	BEGIN
		UPDATE AuthAppServerRelease
		SET ServerAppReleaseDate = @serverAppReleaseDate
		WHERE ServerAppReleaseDate <> @serverAppReleaseDate AND AuthAppServerReleaseId = @authAppServerReleaseId;
	END*/
EXEC USP_SoftwareComplianceEvaluation null,@authAppId,1;
return @authAppServerReleaseId--RETURNS AuthAppServerReleaseId
GO --
CREATE PROCEDURE [dbo].[USP_ValidateAppVendor]
	@installedAfloatAppServerId int
AS
	--print 'USP_ValidateAppVendor';
	DECLARE @appValid int = null;
	--test for unauthorized software
	CREATE TABLE #TEMP_ValidateSoftware
	(TEMP_CompliantVendor varchar(50),TEMP_CompliantSoftware varchar(50))
	
	INSERT INTO #TEMP_ValidateSoftware
	SELECT InstalledAfloatAppVendor.InstalledAfloatAppVendorName,AfloatApp.AfloatAppName
	FROM CurrentlyInstalled LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
	LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
	LEFT JOIN InstalledAfloatAppVendor ON AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId
	WHERE CurrentlyInstalled.InstalledAfloatAppServerId = @installedAfloatAppServerId
	INTERSECT
	SELECT AuthAppVendor.AuthAppVendorName,AuthApp.AuthAppName
	FROM AuthApp LEFT JOIN AuthAppVendor ON AuthApp.AuthAppVendorId = AuthAppVendor.AuthAppVendorId;

	SELECT @appValid = COUNT(TEMP_CompliantVendor) FROM #TEMP_ValidateSoftware;

RETURN @appValid;--returns 0 if app was not found, 1 if app was found
GO --
CREATE PROCEDURE [dbo].[USP_ValidateAppVersionVendor]
	@installedAfloatAppServerId int
AS
	--print 'USP_ValidateAppVersionVendor';
	DECLARE @appVersionValid int = null;
	--test for unauthorized software
	CREATE TABLE #TEMP_ValidateSoftwareVersion
	(TEMP_CompliantVersion varchar(50),TEMP_CompliantVendor varchar(50),TEMP_CompliantSoftware varchar(50))
	
	INSERT INTO #TEMP_ValidateSoftwareVersion
	SELECT AfloatAppVersion.AfloatVersion,InstalledAfloatAppVendor.InstalledAfloatAppVendorName,AfloatApp.AfloatAppName
	FROM CurrentlyInstalled LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
	LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
	LEFT JOIN InstalledAfloatAppVendor ON AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId
	WHERE CurrentlyInstalled.InstalledAfloatAppServerId = @installedAfloatAppServerId
	INTERSECT
	SELECT AuthAppRelease.AuthVersion,AuthAppVendor.AuthAppVendorName,AuthApp.AuthAppName
	FROM AuthAppRelease LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId 
	LEFT JOIN AuthAppVendor ON AuthApp.AuthAppVendorId = AuthAppVendor.AuthAppVendorId;
	
	SELECT @appVersionValid = COUNT(TEMP_CompliantVersion) FROM #TEMP_ValidateSoftwareVersion;

RETURN @appVersionValid;
GO --
CREATE PROCEDURE [dbo].[USP_ValidateServerAppVersionVendor]
	@installedAfloatAppServerId int
AS
	--print 'USP_ValidateServerAppVersionVendor';
	DECLARE @appServerValid int = null;
	--test for unauthorized software
	CREATE TABLE #TEMP_ValidateServerSoftwareVersion
	(TEMP_ServerId int,TEMP_CompliantVersion varchar(50),TEMP_CompliantVendor varchar(50),TEMP_CompliantSoftware varchar(50))
	
	INSERT INTO #TEMP_ValidateServerSoftwareVersion
	SELECT CurrentlyInstalled.ServerId,AfloatAppVersion.AfloatVersion,InstalledAfloatAppVendor.InstalledAfloatAppVendorName,AfloatApp.AfloatAppName
	FROM CurrentlyInstalled LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
	LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
	LEFT JOIN InstalledAfloatAppVendor ON AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId
	WHERE CurrentlyInstalled.InstalledAfloatAppServerId = @installedAfloatAppServerId
	INTERSECT
	SELECT AuthAppServerRelease.ServerId,AuthAppRelease.AuthVersion,AuthAppVendor.AuthAppVendorName,AuthApp.AuthAppName
	FROM AuthAppServerRelease LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
	LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId 
	LEFT JOIN AuthAppVendor ON AuthApp.AuthAppVendorId = AuthAppVendor.AuthAppVendorId;
	
	SELECT @appServerValid = COUNT(TEMP_CompliantVersion) FROM #TEMP_ValidateServerSoftwareVersion;

RETURN @appServerValid;
GO --
CREATE PROCEDURE [dbo].[USP_ValidateVersionBehindOrUpToDate]
	@installedAfloatAppServerId int
AS
	--print 'USP_ValidateVersionBehindOrUpToDate';
	DECLARE @installedReleaseNumber int = null;
	DECLARE @currentReleaseNumber int = null;
	DECLARE @serverId int = null;
	DECLARE @versionDifference int = null;
	DECLARE @appName varchar(100) = null;
	DECLARE @vendorName varchar(100) = null;

	SELECT @serverId = ServerId FROM InstalledAfloatAppServer WHERE InstalledAfloatAppServerId = @installedAfloatAppServerId;
	SELECT @appName = AfloatApp.AfloatAppName FROM InstalledAfloatAppServer
	LEFT JOIN AfloatAppVersion ON InstalledAfloatAppServer.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
	LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
	WHERE InstalledAfloatAppServerId = @installedAfloatAppServerId;
	SELECT @vendorName = InstalledAfloatAppVendor.InstalledAfloatAppVendorName FROM InstalledAfloatAppServer
	LEFT JOIN AfloatAppVersion ON InstalledAfloatAppServer.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
	LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
	LEFT JOIN InstalledAfloatAppVendor ON AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId
	WHERE InstalledAfloatAppServerId = @installedAfloatAppServerId;

	SELECT @installedReleaseNumber = (SELECT MAX(AuthAppRelease.AuthNumber) AS CRN
	FROM CurrentlyInstalled 
	LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
	LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
	LEFT JOIN InstalledAfloatAppVendor ON AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId
	LEFT JOIN AuthAppRelease ON AfloatAppVersion.AfloatVersion = AuthAppRelease.AuthVersion
	LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId
	LEFT JOIN AuthAppVendor ON AuthApp.AuthAppVendorId = AuthAppVendor.AuthAppVendorId
	WHERE AuthApp.AuthAppName = AfloatApp.AfloatAppName AND CurrentlyInstalled.InstalledAfloatAppServerId = @installedAfloatAppServerId);

	SELECT @currentReleaseNumber = (SELECT MAX(AuthAppRelease.AuthNumber) AS CRN FROM InstalledAfloatAppServer
	LEFT JOIN AfloatAppVersion ON InstalledAfloatAppServer.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
	LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
	LEFT JOIN InstalledAfloatAppVendor ON AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId
	INNER JOIN AuthApp ON AfloatApp.AfloatAppName = AuthApp.AuthAppName
	INNER JOIN AuthAppVendor ON InstalledAfloatAppVendor.InstalledAfloatAppVendorName = AuthAppVendor.AuthAppVendorName
	INNER JOIN AuthAppServerRelease ON InstalledAfloatAppServer.ServerId = AuthAppServerRelease.ServerId
	LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
	WHERE InstalledAfloatAppServer.ServerId = @serverId AND AuthApp.AuthAppName = @appName AND AuthAppVendor.AuthAppVendorName = @vendorName);

	SET @versionDifference = @currentReleaseNumber - @installedReleaseNumber;

RETURN @versionDifference;
GO --
CREATE PROCEDURE [dbo].[USP_SoftwareComplianceEvaluation]
	@installedAfloatAppServerId int = null,--variable used when this SP is triggered by USP_InstalledAfloatAppServer
	@authAppId int = null,--variable used when this SP is triggered by USP_AuthAppServerRelease
	@checkMissing int = null--check for missing flag, 0 is false, 1 is true
AS
	--print 'USP_SoftwareComplianceEvaluation';
	DECLARE @complianceStatusName varchar(50) = null;
	DECLARE @complianceStatusId int = 0;
	DECLARE @serverId int = null;
	DECLARE @versionDifference int = null;
	DECLARE @serverAppVersionMonitoringId int = null;
	DECLARE @afloatAppId int = null;
	DECLARE @authAppServerReleaseId int = null;
	DECLARE @uninstalled datetime2 = null;
	DECLARE @tableName varchar(100) = 'ServerAppVersionMonitoring';
	DECLARE @evaluatedInstalledAfloatAppServerId int = 0;


	--selects ServerId of installedafloatappserverid 
	SELECT @serverId = ServerId FROM InstalledAfloatAppServer WHERE InstalledAfloatAppServerId = @installedAfloatAppServerId;
	SELECT @afloatAppId = AfloatApp.AfloatAppId FROM InstalledAfloatAppServer
	LEFT JOIN AfloatAppVersion ON InstalledAfloatAppServer.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
	LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
	WHERE InstalledAfloatAppServerId = @installedAfloatAppServerId;


	--build a temporary table to store all InstalledAfloatAppServerId's where
	--Server+Software+Vendor matches
	DROP TABLE IF EXISTS #InstalledAfloatAppServerIdList;
	CREATE TABLE #InstalledAfloatAppServerIdList
	(TEMP_InstalledAfloatAppServerId int,TEMP_ServerAppVersionMonitoringId int,TEMP_ComplianceStatusId int,TEMP_VersionDifference int);
	/*
	INSERT INTO #InstalledAfloatAppServerIdList
	VALUES(@installedAfloatAppServerId,null,null,null);*/
	INSERT INTO #InstalledAfloatAppServerIdList
	SELECT CurrentlyInstalled.InstalledAfloatAppServerId,null,null,null
	FROM CurrentlyInstalled
	LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
	LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
	WHERE CurrentlyInstalled.ServerId = @serverId AND AfloatApp.AfloatAppId = @afloatAppId /*AND InstalledAfloatAppServerId <> @installedAfloatAppServerId*/;

	--collect all the installedAfloatAppServerId's, generate a status for each one and then
	--retain based on this order
	--lowest precedence listed first
	--delinquent,current,not released to server,unreleased version
	
	WHILE @installedAfloatAppServerId IS NOT NULL
	BEGIN
		DECLARE @appValid int = null;
		DECLARE @appVersionValid int = null;
		DECLARE @appServerValid int = null;

		IF @evaluatedInstalledAfloatAppServerId = 0
		BEGIN 
			SET @evaluatedInstalledAfloatAppServerId = 1;
			SET @installedAfloatAppServerId = 0;
		END

		SELECT @installedAfloatAppServerId = MIN(TEMP_InstalledAfloatAppServerId) FROM #InstalledAfloatAppServerIdList
		WHERE TEMP_InstalledAfloatAppServerId > @installedAfloatAppServerId;
		
		IF @installedAfloatAppServerId IS NULL BREAK;

		SELECT @complianceStatusId = TEMP_ComplianceStatusId FROM #InstalledAfloatAppServerIdList
		WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;

		SELECT @serverAppVersionMonitoringId = TEMP_ServerAppVersionMonitoringId FROM #InstalledAfloatAppServerIdList
		WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;

		SELECT @versionDifference = TEMP_VersionDifference FROM #InstalledAfloatAppServerIdList
		WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;

		EXEC @appValid = USP_ValidateAppVendor @installedAfloatAppServerId;
		IF @appValid = 0
		BEGIN
			SET @complianceStatusName = 'UNAUTHORIZED SOFTWARE';
			EXEC @complianceStatusId = USP_ComplianceStatus @complianceStatusName;
			UPDATE #InstalledAfloatAppServerIdList
			SET TEMP_ComplianceStatusId = @complianceStatusId
			WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;
		END
		ELSE
		BEGIN
			DECLARE @appName varchar(100) = null;
			DECLARE @vendorName varchar(100) = null;
			SELECT @appName = AfloatApp.AfloatAppName FROM InstalledAfloatAppServer
			LEFT JOIN AfloatAppVersion ON InstalledAfloatAppServer.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
			LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
			WHERE InstalledAfloatAppServerId = @installedAfloatAppServerId;
			SELECT @vendorName = InstalledAfloatAppVendor.InstalledAfloatAppVendorName FROM InstalledAfloatAppServer
			LEFT JOIN AfloatAppVersion ON InstalledAfloatAppServer.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
			LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
			LEFT JOIN InstalledAfloatAppVendor ON AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId
			WHERE InstalledAfloatAppServerId = @installedAfloatAppServerId;

			--selects the AuthAppServerReleaseId that should be installed on the ship(latest) using afloat info for variable filtering
			SELECT @authAppServerReleaseId = (SELECT AuthAppServerRelease.AuthAppServerReleaseId
			FROM CurrentlyInstalled LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
			LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
			LEFT JOIN AuthAppServerRelease ON CurrentlyInstalled.ServerId = AuthAppServerRelease.ServerId
			LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
			LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId
			WHERE AuthAppRelease.AuthNumber = 
			(SELECT MAX(AuthAppRelease.AuthNumber) AS CRN FROM InstalledAfloatAppServer
			LEFT JOIN AfloatAppVersion ON InstalledAfloatAppServer.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
			LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
			LEFT JOIN InstalledAfloatAppVendor ON AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId
			INNER JOIN AuthApp ON AfloatApp.AfloatAppName = AuthApp.AuthAppName
			INNER JOIN AuthAppVendor ON InstalledAfloatAppVendor.InstalledAfloatAppVendorName = AuthAppVendor.AuthAppVendorName
			INNER JOIN AuthAppServerRelease ON InstalledAfloatAppServer.ServerId = AuthAppServerRelease.ServerId
			LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
			WHERE InstalledAfloatAppServer.ServerId = @serverId AND AuthApp.AuthAppName = @appName AND AuthAppVendor.AuthAppVendorName = @vendorName)
			AND AuthApp.AuthAppName = AfloatApp.AfloatAppName AND InstalledAfloatAppServerId = @installedAfloatAppServerId);

			EXEC @appVersionValid = USP_ValidateAppVersionVendor @installedAfloatAppServerId;

			IF @appVersionValid = 0
			BEGIN
				SET @complianceStatusName = 'UNAUTHORIZED VERSION';
				EXEC @complianceStatusId = USP_ComplianceStatus @complianceStatusName;
				UPDATE #InstalledAfloatAppServerIdList
				SET TEMP_ComplianceStatusId = @complianceStatusId
				WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;
			END
			ELSE
			BEGIN
				EXEC @appServerValid = USP_ValidateServerAppVersionVendor @installedAfloatAppServerId;
				IF @appServerValid = 0
				BEGIN
					SET @complianceStatusName = 'UNAUTHORIZED FOR INSTALLED SERVER'
					EXEC @complianceStatusId = USP_ComplianceStatus @complianceStatusName;
					UPDATE #InstalledAfloatAppServerIdList
					SET TEMP_ComplianceStatusId = @complianceStatusId
					WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;
				END
				ELSE
				BEGIN
					EXEC @versionDifference = USP_ValidateVersionBehindOrUpToDate @installedAfloatAppServerId;
					UPDATE #InstalledAfloatAppServerIdList
					SET TEMP_VersionDifference = @versionDifference
					WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;
					IF @versionDifference = 0
					BEGIN
						SET @complianceStatusName = 'AUTHORIZED AND CURRENT';
					END
					ELSE
					BEGIN
						SET @complianceStatusName = 'AUTHORIZED AND DELINQUENT VERSION';
					END
					EXEC @complianceStatusId = USP_ComplianceStatus @complianceStatusName;
					UPDATE #InstalledAfloatAppServerIdList
					SET TEMP_ComplianceStatusId = @complianceStatusId
					WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;
				END
			END
		END
		--selects the ServerAppVersionMonitoringId if the passed server+auth/afloatapp present
		SELECT @serverAppVersionMonitoringId = (SELECT ServerAppVersionMonitoring.ServerAppVersionMonitoringId FROM ServerAppVersionMonitoring 
		LEFT JOIN AuthAppServerRelease ON ServerAppVersionMonitoring.AuthAppServerReleaseId = AuthAppServerRelease.AuthAppServerReleaseId
		LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
		LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId
		LEFT JOIN CurrentlyInstalled ON ServerAppVersionMonitoring.InstalledAfloatAppServerId = CurrentlyInstalled.InstalledAfloatAppServerId
		LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
		LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
		LEFT JOIN Server ON ServerAppVersionMonitoring.ServerId = Server.ServerId
		WHERE AuthApp.AuthAppId = (SELECT AuthApp.AuthAppId FROM AuthAppServerRelease 
		LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
		LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId
		WHERE AuthAppServerRelease.AuthAppServerReleaseId = @authAppServerReleaseId)
		AND AfloatApp.AfloatAppId = (SELECT AfloatApp.AfloatAppId FROM CurrentlyInstalled 
		LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
		LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
		WHERE CurrentlyInstalled.InstalledAfloatAppServerId = @installedAfloatAppServerId)
		AND Server.ServerId = @serverId);
		IF @serverAppVersionMonitoringId IS NULL
		BEGIN
			SELECT @serverAppVersionMonitoringId = (SELECT ServerAppVersionMonitoring.ServerAppVersionMonitoringId FROM ServerAppVersionMonitoring 
			LEFT JOIN AuthAppServerRelease ON ServerAppVersionMonitoring.AuthAppServerReleaseId = AuthAppServerRelease.AuthAppServerReleaseId
			LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
			LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId
			LEFT JOIN CurrentlyInstalled ON ServerAppVersionMonitoring.InstalledAfloatAppServerId = CurrentlyInstalled.InstalledAfloatAppServerId
			LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
			LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
			LEFT JOIN Server ON ServerAppVersionMonitoring.ServerId = Server.ServerId
			WHERE AuthApp.AuthAppId IS NULL
			AND AfloatApp.AfloatAppId = (SELECT AfloatApp.AfloatAppId FROM CurrentlyInstalled 
			LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
			LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
			WHERE CurrentlyInstalled.InstalledAfloatAppServerId = @installedAfloatAppServerId)
			AND Server.ServerId = @serverId);
			IF @serverAppVersionMonitoringId IS NULL
			BEGIN
				SELECT @serverAppVersionMonitoringId = (SELECT ServerAppVersionMonitoring.ServerAppVersionMonitoringId FROM ServerAppVersionMonitoring 
				LEFT JOIN AuthAppServerRelease ON ServerAppVersionMonitoring.AuthAppServerReleaseId = AuthAppServerRelease.AuthAppServerReleaseId
				LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
				LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId
				LEFT JOIN CurrentlyInstalled ON ServerAppVersionMonitoring.InstalledAfloatAppServerId = CurrentlyInstalled.InstalledAfloatAppServerId
				LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
				LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
				LEFT JOIN Server ON ServerAppVersionMonitoring.ServerId = Server.ServerId
				WHERE AuthApp.AuthAppId = (SELECT AuthApp.AuthAppId FROM AuthAppServerRelease 
				LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
				LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId
				WHERE AuthAppServerRelease.AuthAppServerReleaseId = @authAppServerReleaseId)
				AND AfloatApp.AfloatAppId IS NULL
				AND Server.ServerId = @serverId);
			END
		END
		IF @serverAppVersionMonitoringId IS NULL
		BEGIN
			INSERT INTO ServerAppVersionMonitoring(ServerId,AuthAppServerReleaseId,InstalledAfloatAppServerId,ComplianceStatusId,Variance)
			VALUES(@serverId,@authAppServerReleaseId,@installedAfloatAppServerId,@complianceStatusId,@versionDifference)
			--set identity
			SET @serverAppVersionMonitoringId = @@IDENTITY;
		END
		UPDATE #InstalledAfloatAppServerIdList
		SET TEMP_ServerAppVersionMonitoringId = @serverAppVersionMonitoringId
		WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;
	END
	IF @evaluatedInstalledAfloatAppServerId <> 0
	BEGIN
		SELECT @installedAfloatAppServerId = (SELECT MAX(TEMP_InstalledAfloatAppServerId) FROM #InstalledAfloatAppServerIdList
		LEFT JOIN ComplianceStatus ON TEMP_ComplianceStatusId = ComplianceStatus.ComplianceStatusId
		WHERE ComplianceStatus.ComplianceStatusName = 'UNAUTHORIZED SOFTWARE');
		IF @installedAfloatAppServerId IS NULL
		BEGIN
			SELECT @installedAfloatAppServerId = (SELECT MAX(TEMP_InstalledAfloatAppServerId) FROM #InstalledAfloatAppServerIdList
			LEFT JOIN ComplianceStatus ON TEMP_ComplianceStatusId = ComplianceStatus.ComplianceStatusId
			WHERE ComplianceStatus.ComplianceStatusName = 'UNAUTHORIZED VERSION');
			IF @installedAfloatAppServerId IS NULL
			BEGIN
				SELECT @installedAfloatAppServerId = (SELECT MAX(TEMP_InstalledAfloatAppServerId) FROM #InstalledAfloatAppServerIdList
				LEFT JOIN ComplianceStatus ON TEMP_ComplianceStatusId = ComplianceStatus.ComplianceStatusId
				WHERE ComplianceStatus.ComplianceStatusName = 'UNAUTHORIZED FOR INSTALLED SERVER');
				IF @installedAfloatAppServerId IS NULL
				BEGIN
					SELECT @installedAfloatAppServerId = (SELECT MAX(TEMP_InstalledAfloatAppServerId) FROM #InstalledAfloatAppServerIdList
					LEFT JOIN ComplianceStatus ON TEMP_ComplianceStatusId = ComplianceStatus.ComplianceStatusId
					WHERE ComplianceStatus.ComplianceStatusName = 'AUTHORIZED AND CURRENT');
					IF @installedAfloatAppServerId IS NULL
					BEGIN
						SELECT @installedAfloatAppServerId = (SELECT TOP 1 TEMP_InstalledAfloatAppServerId FROM #InstalledAfloatAppServerIdList
						LEFT JOIN ComplianceStatus ON TEMP_ComplianceStatusId = ComplianceStatus.ComplianceStatusId
						WHERE ComplianceStatus.ComplianceStatusName = 'AUTHORIZED AND DELINQUENT VERSION'
						ORDER BY TEMP_VersionDifference ASC);
					END
				END
			END
		END
		SELECT @serverAppVersionMonitoringId = TEMP_ServerAppVersionMonitoringId FROM #InstalledAfloatAppServerIdList
		WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;
		SELECT @complianceStatusId = TEMP_ComplianceStatusId FROM #InstalledAfloatAppServerIdList
		WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;
		SELECT @versionDifference = TEMP_VersionDifference FROM #InstalledAfloatAppServerIdList
		WHERE TEMP_InstalledAfloatAppServerId = @installedAfloatAppServerId;
	END
	IF @installedAfloatAppServerId IS NOT NULL
	BEGIN
		UPDATE ServerAppVersionMonitoring
		SET AuthAppServerReleaseId = @authAppServerReleaseId
		WHERE AuthAppServerReleaseId <> @authAppServerReleaseId AND ServerAppVersionMonitoringId = @serverAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET AuthAppServerReleaseId = @authAppServerReleaseId
		WHERE AuthAppServerReleaseId IS NULL AND @authAppServerReleaseId IS NOT NULL AND ServerAppVersionMonitoringId = @serverAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET AuthAppServerReleaseId = null
		WHERE AuthAppServerReleaseId IS NOT NULL AND @authAppServerReleaseId IS NULL AND ServerAppVersionMonitoringId = @serverAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET InstalledAfloatAppServerId = @installedAfloatAppServerId
		WHERE InstalledAfloatAppServerId <> @installedAfloatAppServerId AND ServerAppVersionMonitoringId = @serverAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET InstalledAfloatAppServerId = @installedAfloatAppServerId
		WHERE InstalledAfloatAppServerId IS NULL AND @installedAfloatAppServerId IS NOT NULL AND ServerAppVersionMonitoringId = @serverAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET InstalledAfloatAppServerId = null
		WHERE InstalledAfloatAppServerId IS NOT NULL AND @installedAfloatAppServerId IS NULL AND ServerAppVersionMonitoringId = @serverAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET ComplianceStatusId = @complianceStatusId
		WHERE ComplianceStatusId <> @complianceStatusId AND ServerAppVersionMonitoringId = @serverAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET Variance = @versionDifference
		WHERE Variance <> @versionDifference AND ServerAppVersionMonitoringId = @serverAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET Variance = @versionDifference
		WHERE Variance IS NULL AND @versionDifference IS NOT NULL AND ServerAppVersionMonitoringId = @serverAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET Variance = null
		WHERE Variance IS NOT NULL AND @versionDifference IS NULL AND ServerAppVersionMonitoringId = @serverAppVersionMonitoringId;
	END
	IF @authAppId IS NOT NULL
	BEGIN
		DECLARE @walkVar int = 0;
		DROP TABLE IF EXISTS #TEMP_UpdateReleasedAuthApp
		CREATE TABLE #TEMP_UpdateReleasedAuthApp
		(TEMP_InstalledAfloatAppServerId int,TEMP_ServerId int)
		INSERT INTO #TEMP_UpdateReleasedAuthApp
		SELECT MAX(CurrentlyInstalled.InstalledAfloatAppServerId), CurrentlyInstalled.ServerId
		FROM CurrentlyInstalled
		INNER JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
		INNER JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
		INNER JOIN InstalledAfloatAppVendor ON AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId
		INNER JOIN AuthAppVendor ON InstalledAfloatAppVendor.InstalledAfloatAppVendorName = AuthAppVendor.AuthAppVendorName
		INNER JOIN AuthApp ON AfloatApp.AfloatAppName = AuthApp.AuthAppName
		WHERE AuthApp.AuthAppId = @authAppId
		GROUP BY CurrentlyInstalled.ServerId;
			
		WHILE (1=1)
		BEGIN
			SELECT @walkVar = MIN(TEMP_InstalledAfloatAppServerId) FROM #TEMP_UpdateReleasedAuthApp WHERE TEMP_InstalledAfloatAppServerId > @walkVar;
			IF @walkVar IS NULL BREAK;
			EXEC USP_SoftwareComplianceEvaluation @walkVar,null,null;
		END
	END
	IF @checkMissing = 1
	BEGIN
		--determines if all software (software+vendor) released to the hull is installed (**NOTE**does not look at version)
		DROP TABLE IF EXISTS #TEMP_MissingSoftware;
		CREATE TABLE #TEMP_MissingSoftware
		(TEMP_Vendor varchar(50),TEMP_Software varchar(50),TEMP_ServerId int);
		INSERT INTO #TEMP_MissingSoftware
		SELECT AuthAppVendor.AuthAppVendorName,AuthApp.AuthAppName,AuthAppServerRelease.ServerId
		FROM AuthAppServerRelease LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
		LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId
		LEFT JOIN AuthAppVendor ON AuthApp.AuthAppVendorId = AuthAppVendor.AuthAppVendorId
		EXCEPT
		SELECT InstalledAfloatAppVendor.InstalledAfloatAppVendorName,AfloatApp.AfloatAppName,CurrentlyInstalled.ServerId
		FROM CurrentlyInstalled LEFT JOIN AfloatAppVersion ON CurrentlyInstalled.AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
		LEFT JOIN AfloatApp ON AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
		LEFT JOIN InstalledAfloatAppVendor ON AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId;

		--determines AuthAppId of missing software and binds to the serverId
		DROP TABLE IF EXISTS #TEMP_MissingSoftwareAppId;
		CREATE TABLE #TEMP_MissingSoftwareAppId
		(TEMP_Id int IDENTITY(1,1),TEMP_ServerId int,TEMP_AuthAppId int);
		CREATE CLUSTERED INDEX ixMissingSoftwareAppId ON #TEMP_MissingSoftwareAppId(TEMP_Id,TEMP_ServerId,TEMP_AuthAppId);
		INSERT INTO #TEMP_MissingSoftwareAppId
		SELECT #TEMP_MissingSoftware.TEMP_ServerId,AuthApp.AuthAppId
		FROM #TEMP_MissingSoftware LEFT JOIN AuthApp ON #TEMP_MissingSoftware.TEMP_Software = AuthApp.AuthAppName
		LEFT JOIN AuthAppVendor ON AuthApp.AuthAppVendorId = AuthAppVendor.AuthAppVendorId
		WHERE TEMP_Vendor = AuthAppVendor.AuthAppVendorName;
		
		--determines the current software version that should be installed for missing software
		DROP TABLE IF EXISTS #TEMP_MissingSoftwareCurrentRelease;
		CREATE TABLE #TEMP_MissingSoftwareCurrentRelease
		(TEMP_MissingSoftwareId int,TEMP_AuthAppServerReleaseId int);
		CREATE CLUSTERED INDEX ixMissingSoftwareCurrentRelease ON #TEMP_MissingSoftwareCurrentRelease(TEMP_MissingSoftwareId,TEMP_AuthAppServerReleaseId);
		INSERT INTO #TEMP_MissingSoftwareCurrentRelease
		SELECT #TEMP_MissingSoftwareAppId.TEMP_Id,AuthAppServerRelease.AuthAppServerReleaseId FROM #TEMP_MissingSoftwareAppId
		LEFT JOIN AuthApp ON #TEMP_MissingSoftwareAppId.TEMP_AuthAppId = AuthApp.AuthAppId
		LEFT JOIN AuthAppRelease ON AuthApp.AuthAppId = AuthAppRelease.AuthAppId
		LEFT JOIN AuthAppServerRelease ON AuthAppRelease.AuthAppReleaseId = AuthAppServerRelease.AuthAppReleaseId
		WHERE AuthAppRelease.AuthNumber = (SELECT MAX(AuthAppRelease.AuthNumber) AS CurrentReleaseNumber
		FROM AuthAppServerRelease LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
		LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId
		WHERE AuthApp.AuthAppId = #TEMP_MissingSoftwareAppId.TEMP_AuthAppId AND AuthAppServerRelease.ServerId = #TEMP_MissingSoftwareAppId.TEMP_ServerId)
		AND AuthApp.AuthAppId = #TEMP_MissingSoftwareAppId.TEMP_AuthAppId AND AuthAppServerRelease.ServerId = #TEMP_MissingSoftwareAppId.TEMP_ServerId;

		--determines if the missing software has already been accounted for in the monitoring table
		DROP TABLE IF EXISTS #TEMP_CheckForMonitoringStatus;
		CREATE TABLE #TEMP_CheckForMonitoringStatus
		(TEMP_MissingSoftwareId int,TEMP_ServerAppVersionMonitoringId int);
		CREATE CLUSTERED INDEX ixCheckForMonitoringStatus ON #TEMP_CheckForMonitoringStatus(TEMP_MissingSoftwareId,TEMP_ServerAppVersionMonitoringId);
		INSERT INTO #TEMP_CheckForMonitoringStatus
		SELECT #TEMP_MissingSoftwareAppId.TEMP_Id,ServerAppVersionmonitoring.ServerAppVersionMonitoringId
		FROM #TEMP_MissingSoftwareAppId
		LEFT JOIN ServerAppVersionmonitoring ON #TEMP_MissingSoftwareAppId.TEMP_ServerId = ServerAppVersionmonitoring.ServerId
		LEFT JOIN AuthAppServerRelease ON ServerAppVersionmonitoring.AuthAppServerReleaseId = AuthAppServerRelease.AuthAppServerReleaseId
		LEFT JOIN AuthAppRelease ON AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId
		LEFT JOIN AuthApp ON AuthAppRelease.AuthAppId = AuthApp.AuthAppId
		WHERE AuthApp.AuthAppId = #TEMP_MissingSoftwareAppId.TEMP_AuthAppId;
		
		--bring together missing software, current release, and monitoring status for upsert
		DROP TABLE IF EXISTS #TEMP_MissingSoftwareMetadata;
		CREATE TABLE #TEMP_MissingSoftwareMetadata
		(TEMP_ServerId int,TEMP_MissingSoftwareCurrentRelease int,TEMP_ServerAppVersionMonitoringId int);
		INSERT INTO #TEMP_MissingSoftwareMetadata
		SELECT #TEMP_MissingSoftwareAppId.TEMP_ServerId,#TEMP_MissingSoftwareCurrentRelease.TEMP_AuthAppServerReleaseId,#TEMP_CheckForMonitoringStatus.TEMP_ServerAppVersionMonitoringId
		FROM #TEMP_MissingSoftwareAppId LEFT JOIN #TEMP_MissingSoftwareCurrentRelease ON #TEMP_MissingSoftwareAppId.TEMP_Id = #TEMP_MissingSoftwareCurrentRelease.TEMP_MissingSoftwareId
		LEFT JOIN #TEMP_CheckForMonitoringStatus ON #TEMP_MissingSoftwareCurrentRelease.TEMP_MissingSoftwareId = #TEMP_CheckForMonitoringStatus.TEMP_MissingSoftwareId;
		SET @complianceStatusName = 'SOFTWARE MISSING';
		EXEC @complianceStatusId = USP_ComplianceStatus @complianceStatusName;
		UPDATE ServerAppVersionMonitoring
		SET AuthAppServerReleaseId = TEMP_MissingSoftwareCurrentRelease
		FROM #TEMP_MissingSoftwareMetadata 
		WHERE AuthAppServerReleaseId <> TEMP_MissingSoftwareCurrentRelease AND ServerAppVersionMonitoringId = TEMP_ServerAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET AuthAppServerReleaseId = TEMP_MissingSoftwareCurrentRelease
		FROM #TEMP_MissingSoftwareMetadata 
		WHERE AuthAppServerReleaseId IS NULL AND ServerAppVersionMonitoringId = TEMP_ServerAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET InstalledAfloatAppServerId = NULL
		FROM #TEMP_MissingSoftwareMetadata
		WHERE ServerAppVersionMonitoringId = TEMP_ServerAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET Variance = NULL
		FROM #TEMP_MissingSoftwareMetadata
		WHERE ServerAppVersionMonitoringId = TEMP_ServerAppVersionMonitoringId;
		UPDATE ServerAppVersionMonitoring
		SET ComplianceStatusId = @complianceStatusId
		FROM #TEMP_MissingSoftwareMetadata
		WHERE ServerAppVersionMonitoringId = TEMP_ServerAppVersionMonitoringId;

		INSERT INTO ServerAppVersionMonitoring(ServerId,AuthAppServerReleaseId,ComplianceStatusId)
		SELECT TEMP_ServerId,TEMP_MissingSoftwareCurrentRelease,@complianceStatusId
		FROM #TEMP_MissingSoftwareMetadata WHERE TEMP_ServerAppVersionMonitoringId IS NULL;		
	END
	EXEC USP_BusinessRuleTargetAreaExecutor @tableName;
RETURN
GO--
CREATE PROCEDURE [dbo].[USP_BusinessRuleGroup]
	@businessRuleTypeId int,
	@businessRuleOneConditionTypeId int,
	@businessRuleOneOperatorId int,
	@businessRuleOneSensitivityTypeId int,
	@businessRuleStatusId int,
	@businessRuleOneConditionOne varchar(100),
	@businessRuleOneSensitivity varchar(100),
	@businessRuleOneConditionTwo varchar(100),
	@businessRuleTwoConditionTypeId int,
	@businessRuleTwoOperatorId int,
	@businessRuleTwoSensitivityTypeId int,
	@businessRuleTwoConditionOne varchar(100),
	@businessRuleTwoSensitivity varchar(100)
AS
	--print 'USP_BusinessRuleGroup';
	DECLARE @businessRuleGroupId int = 0;
	DECLARE @businessRuleOneId int = 0;
	DECLARE @businessRuleTwoId int = 0;

	EXEC @businessRuleOneId = USP_BusinessRule @businessRuleTypeId,@businessRuleOneConditionTypeId,@businessRuleOneOperatorId,@businessRuleOneSensitivityTypeId,
	@businessRuleStatusId,@businessRuleOneConditionOne,@businessRuleOneSensitivity,@businessRuleOneConditionTwo;

	IF @businessRuleTwoConditionOne <> 'NULL'
	BEGIN
		EXEC @businessRuleTwoId = USP_BusinessRule null,@businessRuleTwoConditionTypeId,@businessRuleTwoOperatorId,@businessRuleTwoSensitivityTypeId,
		null,@businessRuleTwoConditionOne,@businessRuleTwoSensitivity,null;
	END
	ELSE
	BEGIN
		SET @businessRuleTwoId = null;
	END

	SELECT @businessRuleGroupId = BusinessRuleGroupId FROM BusinessRuleGroup 
	WHERE @businessRuleOneId = BusinessRuleOneId AND @businessRuleTwoId = BusinessRuleTwoId;
	IF @businessRuleGroupId = 0 AND @businessRuleTwoId IS NULL
	BEGIN
		SELECT @businessRuleGroupId = BusinessRuleGroupId FROM BusinessRuleGroup 
		WHERE @businessRuleOneId = BusinessRuleOneId AND BusinessRuleTwoId IS NULL;
	END
	
	IF @businessRuleGroupId = 0
	BEGIN
		INSERT INTO BusinessRuleGroup(BusinessRuleOneId,BusinessRuleTwoId)
		VALUES(@businessRuleOneId,@businessRuleTwoId);
		--set id
		SET @businessRuleGroupId = @@identity;
	END
RETURN @businessRuleGroupId
GO
CREATE PROCEDURE [dbo].[USP_Statistic]
	@statisticName varchar(100)
AS
	--print 'USP_Statistic';
	DECLARE @statisticId int = 0;

	---collect StatisticId if statistic name already exists
	SELECT @statisticId = StatisticId FROM Statistic WHERE StatisticName = @statisticName;

	---if statistic name daes do not exist insert
	IF @statisticId = 0
	BEGIN
		INSERT INTO Statistic (StatisticName)
		VALUES (@statisticName);
		--set the inserted statisticId
		SET @statisticId = @@IDENTITY;
	END
RETURN @statisticId --RETURNS UsageStatisticId
GO --
CREATE PROCEDURE [dbo].[USP_StatisticServer]
	@serverId int,
	@statisticName varchar(100)
AS
	--print 'USP_StatisticServer';
	DECLARE @statisticId int = 0;
	DECLARE @statisticServerId int = 0;

	--collect StatisticId
	EXEC @statisticId = USP_Statistic @statisticName;
	--collect StatisticServerId if exists
	SELECT @statisticServerId = StatisticServerId FROM StatisticServer WHERE ServerId = @serverId AND StatisticId = @statisticId;
	
	IF @statisticServerId = 0
	BEGIN
		INSERT INTO StatisticServer(ServerId,StatisticId)
		VALUES (@serverId,@statisticId);
		--set id
		SET @statisticServerId = @@IDENTITY;
	END
RETURN @statisticServerId;
GO
CREATE PROCEDURE [dbo].[USP_UsageStatisticServer]
	@serverId int,
	@statisticName varchar(100),
	@data varchar(255),
	@dataDate datetime2
AS
	--print 'USP_UsageStatisticServer';
	DECLARE @statisticServerId int = 0;
	DECLARE @usageStatisticServerId int = 0;
	DECLARE @tableName varchar(100) = 'UsageStatisticServer';

	--collect statisticServerId
	EXEC @statisticServerId = USP_StatisticServer @serverId,@statisticName;

	--collect UsageStatisticServerId if StatisticServer + Data exists
	SELECT @usageStatisticServerId = UsageStatisticServerId FROM UsageStatisticServer WHERE StatisticServerId = @statisticServerId AND Data = @data AND DataDate = @dataDate;
	
	IF @usageStatisticServerId = 0
	BEGIN
		INSERT INTO UsageStatisticServer(StatisticServerId,Data,DataDate)
		VALUES(@statisticServerId,@data,@dataDate)
		--set id
		SET @usageStatisticServerId = @@IDENTITY;
	END
	;WITH CTE_Cleanup
	AS(SELECT ROW_NUMBER() OVER(ORDER BY DataDate DESC) AS RowNum,* FROM UsageStatisticServer WHERE StatisticServerId = @statisticServerId)
	DELETE FROM CTE_Cleanup WHERE RowNum > 10;
	EXEC USP_BusinessRuleTargetAreaExecutor @tableName;
RETURN @usageStatisticServerId;
GO
/*CREATE INSERT FOR Drive TABLE*/
CREATE PROCEDURE [dbo].[USP_Drive]
	--drive letter
	@driveLetter varchar(1)
AS
	--print 'USP_Drive';
	DECLARE @driveId int = 0;

	---collect DriveId if exists
	SELECT @driveId = DriveId FROM Drive WHERE DriveLetter = @driveLetter;

	IF @driveId = 0 --if DriveLetter does not already exist create it
	BEGIN
		INSERT INTO Drive (DriveLetter)
		VALUES (@driveLetter);
		--set inserted driveId
		SET @driveId = @@IDENTITY
	END
RETURN @driveId
GO --
/*CREATE INSERT FOR DriveServer TABLE*/
CREATE PROCEDURE [dbo].[USP_DriveServer]
	--ServerId and drive letter
	@serverId int,
	@driveLetter varchar(1)
AS
	--print 'USP_DriveServer';
	DECLARE @driveServerId int = 0;
	DECLARE @driveId int = 0;

	--collect DriveId
	EXEC @driveId = USP_Drive @driveLetter;
	--collect DriveServerId if exists
	SELECT @driveServerId = DriveServerId FROM DriveServer WHERE ServerId = @serverId AND DriveId = @driveId;

	IF @driveServerId = 0
	BEGIN
		INSERT INTO DriveServer(ServerId,DriveId)
		VALUES (@serverId,@driveId);
		--set id
		SET @driveServerId = @@IDENTITY;
	END
RETURN @driveServerId;
GO --
/*CREATE UPSERT FOR StatisticDriveServer*/
CREATE PROCEDURE [dbo].[USP_StatisticDriveServer]
	--ServerId, drive letter, drive size, drive free space, data retrieval date
	@serverId int,
	@driveLetter varchar(1),
	@size int,
	@freeSpace int,
	@dataDate datetime2
AS
	--print 'USP_StatisticDriveServer';
	DECLARE @tableName varchar(100) = 'StatisticDriveServer';
	DECLARE @statisticDriveServerId int = 0;
	DECLARE @driveServerId int = 0;

	--collect DriveServerId
	EXEC @driveServerId = USP_DriveServer @serverId,@driveLetter;

	--collect StatisticDriveServerId if it exists
	SELECT @statisticDriveServerId = StatisticDriveServerId FROM StatisticDriveServer 
	WHERE DriveServerId = @driveServerId AND DriveSize = @size AND FreeSpace = @freeSpace AND DataDate = @dataDate;

	IF @statisticDriveServerId = 0
	BEGIN
		INSERT INTO StatisticDriveServer (DriveServerId,DriveSize,FreeSpace,DataDate)
		VALUES (@driveServerId,@size,@freeSpace,@dataDate);
		--set id
		SET @statisticDriveServerId = @@IDENTITY;
	END
	;WITH CTE_Cleanup
	AS(SELECT ROW_NUMBER() OVER(ORDER BY DataDate DESC) AS RowNum,* FROM StatisticDriveServer WHERE DriveServerId = @driveServerId)
	DELETE FROM CTE_Cleanup WHERE RowNum > 10;
	EXEC USP_BusinessRuleTargetAreaExecutor @tableName;
RETURN @statisticDriveServerId;
GO --
CREATE PROCEDURE [dbo].[USP_DsUpdateActivity]--HullServerId(COLLECTED),ContactTimestamp(PASSED),Activity(PASSED),Executor(PASSED)
	--Server parameters
	@serverId int,
	--dsUpdateActivity parameters
	@contactTimestamp datetime2,
	@activity varchar(255),
	@executor varchar(50)
AS
	--print 'USP_DsUpdateActivity';
	DECLARE @dsUpdateActivityId int = 0;--DECLARE DsUpdateActivityId
	DECLARE @tableName varchar(50) = 'DsUpdateActivity';
	
	-- find if DsUpdateActivityId if HullServer already exists
	SELECT @dsUpdateActivityId = DsUpdateActivityId FROM DsUpdateActivity
	WHERE ServerId = @serverId;

	---if a server does not already exist insert
	IF @dsUpdateActivityId = 0
	BEGIN
		INSERT INTO DsUpdateActivity (ServerId,ContactTimestamp,Activity,Executor)
		VALUES (@serverId,@contactTimestamp,@activity,@executor);
		--set the inserted hullId
		set @dsUpdateActivityId = @@IDENTITY
	END
	---else update contact time if it more recent
	ELSE
	BEGIN
		UPDATE DsUpdateActivity
		SET ContactTimestamp = @contactTimestamp, Activity = @activity, Executor = @executor
		WHERE @dsUpdateActivityId = DsUpdateActivityId AND @contactTimestamp > ContactTimestamp;
	END
	EXEC USP_BusinessRuleTargetAreaExecutor @tableName;
RETURN @dsUpdateActivityId --RETURNS DsUpdateActivityId
GO --
/*CREATE UPSERT FOR CERT MONITORING*/
CREATE PROCEDURE [dbo].[USP_CertMonitoring]---CertMonitoringId(GENERATED OR RETURNED), CertMonitoringName(PASSED)
	@serverId int,
	@notBefore datetime2,
	@notAfter datetime2
AS
	--print 'USP_CertMonitoring';
	DECLARE @certMonitoringId int = 0;--declare CertMonitoringId
	DECLARE @daysToExpire int;
	DECLARE @tableName varchar(100) = 'CertMonitoring';

	--calculate days until cert expires
	SELECT @daysToExpire = DATEDIFF(DAY,CURRENT_TIMESTAMP,@notAfter);
	--write in CertMonitoringId if already present
	SELECT @certMonitoringId = CertMonitoringId FROM CertMonitoring
	WHERE ServerId = @serverId;
	--if CertMonitoringId is not present INSERT new CertMonitoringName and generate new CertMonitoringId
	IF @certMonitoringId = 0
	BEGIN
		INSERT INTO CertMonitoring(ServerId,DaysToExpire,NotBefore,NotAfter)
		VALUES (@serverId,@daysToExpire,@notBefore,@notAfter);
		--set the inserted certMonitoringId
		set @certMonitoringId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE CertMonitoring
		SET DaysToExpire = @daysToExpire
		WHERE ServerId = @serverId AND DaysToExpire <> @daysToExpire;
		UPDATE CertMonitoring
		SET NotBefore = @notBefore
		WHERE ServerId = @serverId AND NotBefore <> @notBefore;
		UPDATE CertMonitoring
		SET NotAfter = @notAfter
		WHERE ServerId = @serverId AND NotAfter <> @notAfter;
	END
	EXEC USP_BusinessRuleTargetAreaExecutor @tableName;
return @certMonitoringId--PROC RETURNS CertMonitoringId
GO --
/*CREATE UPSERT FOR NiapsVersionMonitoring TABLE*/
CREATE PROCEDURE [dbo].[USP_NiapsVersionMonitoring]
	@serverId int
AS
	--print 'USP_NiapsVersionMonitoring';
	--DECLARE @serverId int = 1;
	DECLARE @niapsVersionMonitoringId int = 0;
	DECLARE @descriptor varchar(100);
	DECLARE @exceptServerId int = null;
	DECLARE @niapsVersionServerReleaseId int;
	DECLARE @releaseNumber int;
	DECLARE @currentReleaseNumber int;
	DECLARE @versionDifference int;
	DECLARE @shipExistsInInstallTable int = null;
	DECLARE @tableName varchar(100) = 'NiapsVersionMonitoring';

	--check to see if server has an entry for an installed niaps version
	SELECT @shipExistsInInstallTable = ServerId FROM InstalledNiapsVersion WHERE ServerId = @serverId;
	--if server has entry(ies) for installed niaps version evaluate if installed version is authorized
	IF @shipExistsInInstallTable IS NOT NULL
	BEGIN
		DROP TABLE IF EXISTS #TEMP_CompareReleasedToInstalled
		CREATE TABLE #TEMP_CompareReleasedToInstalled
		(TEMP_ServerId int,TEMP_NiapsVersionReleaseId int)

		INSERT INTO #TEMP_CompareReleasedToInstalled(TEMP_ServerId,TEMP_NiapsVersionReleaseId)
		SELECT ServerId,NiapsVersionReleaseId FROM InstalledNiapsVersion WHERE ServerId = @serverId
		EXCEPT
		SELECT ServerId,NiapsVersionReleaseId FROM NiapsVersionServerRelease WHERE ServerId = @serverId;

		SELECT @exceptServerId = TEMP_ServerId FROM #TEMP_CompareReleasedToInstalled;
		IF @exceptServerId IS NOT NULL--if value is found, set installed version to be unauthorized
		BEGIN
			SET @descriptor = 'UNAUTHORIZED VERSION';
			SET @versionDifference = null;
			SET @niapsVersionServerReleaseId = null;
		END
		ELSE
		BEGIN
			SET @descriptor = null;
			--if a value is not found evaluate if version is behind or current, and if behind, how many release behind
			--creates temp table of all installed versions for specified server
			DROP TABLE IF EXISTS #TEMP_HighestReleaseNumberInstalled
			CREATE TABLE #TEMP_HighestReleaseNumberInstalled
			(TEMP_NiapsVersionReleaseId int,TEMP_ReleaseNumber int)
			INSERT INTO #TEMP_HighestReleaseNumberInstalled(TEMP_NiapsVersionReleaseId,TEMP_ReleaseNumber)
			SELECT 
			InstalledNiapsVersion.NiapsVersionReleaseId,
			NiapsVersionRelease.ReleaseNumber
			FROM InstalledNiapsVersion
			LEFT JOIN NiapsVersionRelease
			ON InstalledNiapsVersion.NiapsVersionReleaseId = NiapsVersionRelease.NiapsVersionReleaseId
			WHERE InstalledNiapsVersion.ServerId = @serverId;
			--collect highest release number found for specific server
			SELECT @releaseNumber = MAX(TEMP_ReleaseNumber) FROM #TEMP_HighestReleaseNumberInstalled;
			--collect the NiapsVersionServerReleaseId for latest release installed
			SELECT @niapsVersionServerReleaseId = NiapsVersionReleaseId FROM NiapsVersionRelease WHERE ReleaseNumber = @releaseNumber;

			--creates temp table for all released versions for specified server
			DROP TABLE IF EXISTS #TEMP_ReleaseNumberByServer
			CREATE TABLE #TEMP_ReleaseNumberByServer
			(TEMP_NiapsVersionReleaseId int,TEMP_ReleaseNumber int)
			INSERT INTO #TEMP_ReleaseNumberByServer(TEMP_NiapsVersionReleaseId,TEMP_ReleaseNumber)
			SELECT 
			NiapsVersionServerRelease.NiapsVersionReleaseId,
			NiapsVersionRelease.ReleaseNumber
			FROM NiapsVersionServerRelease
			LEFT JOIN NiapsVersionRelease
			ON NiapsVersionServerRelease.NiapsVersionReleaseId = NiapsVersionRelease.NiapsVersionReleaseId
			WHERE NiapsVersionServerRelease.ServerId = @serverId;
			--collects the current release number that should be installed on specified hull
			SELECT @currentReleaseNumber = MAX(TEMP_ReleaseNumber) FROM #TEMP_ReleaseNumberByServer;
			--sets the variance between release numbers between current release version and installed version
			SET @versionDifference = @currentReleaseNumber - @releaseNumber;
		END
		--if server is already being monitored collects id
		SELECT @niapsVersionMonitoringId = NiapsVersionMonitoringId FROM NiapsVersionMonitoring
		WHERE ServerId = @serverId;
		
		IF @niapsVersionMonitoringId = 0 --if NiapsVersionMonitoring does not already exist create it
		BEGIN
			INSERT INTO NiapsVersionMonitoring (ServerId,NiapsVersionServerReleaseId,AuthVersionBehindCount,Descriptor)
			VALUES (@serverId,@niapsVersionServerReleaseId,@versionDifference,@descriptor);
			--set inserted installedNiapsVersionServerReleaseId
			SET @niapsVersionMonitoringId = @@IDENTITY
		END
		--if server is being monitored, updates any field that has changed
		ELSE
		BEGIN
			UPDATE NiapsVersionMonitoring
			SET NiapsVersionServerReleaseId = @niapsVersionServerReleaseId
			WHERE NiapsVersionServerReleaseId <> @niapsVersionServerReleaseId AND NiapsVersionMonitoringId = @niapsVersionMonitoringId;
			UPDATE NiapsVersionMonitoring
			SET NiapsVersionServerReleaseId = @niapsVersionServerReleaseId
			WHERE NiapsVersionServerReleaseId IS NULL AND @niapsVersionServerReleaseId IS NOT NULL AND NiapsVersionMonitoringId = @niapsVersionMonitoringId;
			UPDATE NiapsVersionMonitoring
			SET NiapsVersionServerReleaseId = null
			WHERE NiapsVersionServerReleaseId IS NOT NULL AND @niapsVersionServerReleaseId IS NULL AND NiapsVersionMonitoringId = @niapsVersionMonitoringId;
			UPDATE NiapsVersionMonitoring
			SET AuthVersionBehindCount = @versionDifference
			WHERE AuthVersionBehindCount <> @versionDifference AND NiapsVersionMonitoringId = @niapsVersionMonitoringId;
			UPDATE NiapsVersionMonitoring
			SET AuthVersionBehindCount = @versionDifference
			WHERE AuthVersionBehindCount IS NULL AND @versionDifference IS NOT NULL AND NiapsVersionMonitoringId = @niapsVersionMonitoringId;
			UPDATE NiapsVersionMonitoring
			SET AuthVersionBehindCount = null
			WHERE AuthVersionBehindCount IS NOT NULL AND @versionDifference IS NULL AND NiapsVersionMonitoringId = @niapsVersionMonitoringId;
			UPDATE NiapsVersionMonitoring
			SET Descriptor = @descriptor
			WHERE Descriptor <> @descriptor AND NiapsVersionMonitoringId = @niapsVersionMonitoringId;
			UPDATE NiapsVersionMonitoring
			SET Descriptor = @descriptor
			WHERE Descriptor IS NULL AND @descriptor IS NOT NULL AND NiapsVersionMonitoringId = @niapsVersionMonitoringId;
			UPDATE NiapsVersionMonitoring
			SET Descriptor = null
			WHERE Descriptor IS NOT NULL AND @descriptor IS NULL AND NiapsVersionMonitoringId = @niapsVersionMonitoringId;
		END
	END
	EXEC USP_BusinessRuleTargetAreaExecutor @tableName; --sends monitoring table to executor for business rule execution
RETURN @niapsVersionMonitoringId
GO --
/*CREATE UPSERT FOR InstalledNiapsVersion TABLE*/
--behavior: inserts if server + release is not found, if found can update the installation date 
CREATE PROCEDURE [dbo].[USP_InstalledNiapsVersion]
	@serverId int,
	@installedVersion varchar(50),
	@installedTimestamp datetime2
AS
	--print 'USP_InstalledNiapsVersion';
	DECLARE @niapsVersionReleaseId int = 0;
	DECLARE @installedNiapsVersionId int = 0;

	--collect the release id that matches installed version
	SELECT @niapsVersionReleaseId = NiapsVersionReleaseId FROM NiapsVersionRelease
	WHERE ReleaseVersion = @installedVersion;
	--check to see if server + installed version is already recorded
	SELECT @installedNiapsVersionId = InstalledNiapsVersionId FROM InstalledNiapsVersion
	WHERE ServerId = @serverId AND NiapsVersionReleaseId = @niapsVersionReleaseId; 

	IF @installedNiapsVersionId = 0 --if InstalledNiapsVersion does not already exist create it
	BEGIN
		INSERT INTO InstalledNiapsVersion (ServerId,NiapsVersionReleaseId,InstalledTimestamp)
		VALUES (@serverId,@niapsVersionReleaseId,@installedTimestamp);
		--set inserted installedNiapsVersionReleaseId
		SET @installedNiapsVersionId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE InstalledNiapsVersion
		SET InstalledTimestamp = @installedTimestamp
		WHERE InstalledTimestamp <> @installedTimestamp AND InstalledNiapsVersionId = @installedNiapsVersionId;
	END
EXEC USP_NiapsVersionMonitoring @serverId;--server passed to monitoring table to trigger evaluation
RETURN @installedNiapsVersionId
GO --
/*CREATE UPSERT FOR InstalledNiapsVersion TABLE*/
--behavior: binds released niaps version to ship they are released to, if already released to ship can updated release date
CREATE PROCEDURE [dbo].[USP_NiapsVersionServerRelease]
	@serverId int,--server being released to
	@niapsVersionReleaseId varchar(50),--version being released
	@releaseDate datetime2--date it is being released
AS
	--print 'USP_NiapsVersionServerRelease';
	DECLARE @niapsVersionServerReleaseId int = 0;--init for pk

	--collects server + release if already existing
	SELECT @niapsVersionServerReleaseId = NiapsVersionServerReleaseId FROM NiapsVersionServerRelease
	WHERE ServerId = @serverId AND NiapsVersionReleaseId = @niapsVersionReleaseId;

	IF @niapsVersionServerReleaseId = 0 --if NiapsVersionServerRelease does not already exist create it
	BEGIN
		INSERT INTO NiapsVersionServerRelease (ServerId,NiapsVersionReleaseId,ReleaseDate)
		VALUES (@serverId,@niapsVersionReleaseId,@releaseDate);
		--set inserted installedNiapsVersionReleaseId
		SET @niapsVersionServerReleaseId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE NiapsVersionServerRelease
		SET ReleaseDate = @releaseDate
		WHERE ReleaseDate <> @releaseDate AND NiapsVersionServerReleaseId = @niapsVersionServerReleaseId;
	END
EXEC USP_NiapsVersionMonitoring @serverId;--upon release, server is passed to monitoring table for evaluation
RETURN @niapsVersionServerReleaseId
GO --
CREATE PROCEDURE [dbo].[USP_BusinessRuleTargetAreaExecutor]---BusinessRuleTargetAreaExecutorId(GENERATED OR RETURNED), BusinessRuleTargetAreaExecutorName(PASSED)
	@tableName varchar(50)--PASSED FOR INSERT
AS
	--print 'USP_BusinessRuleTargetAreaExecutor';
	DECLARE @businessRuleUserId int = 0;
	DECLARE @statusUserId int;
	DROP TABLE IF EXISTS #BusinessRuleExecution
	--create temp table for business rule execution, for passed table name, collect all business rules applicable and order them for execution
	CREATE TABLE #BusinessRuleExecution
		(TEMP_BusinessRuleUserId int,TEMP_BusinessRulePriority int,TEMP_BusinessRuleTargetAreaTable varchar(100),TEMP_BusinessRuleTargetAreaTwoTable varchar(100))
		INSERT INTO #BusinessRuleExecution(TEMP_BusinessRuleUserId,TEMP_BusinessRulePriority,TEMP_BusinessRuleTargetAreaTable,TEMP_BusinessRuleTargetAreaTwoTable)
		SELECT BusinessRuleUser.BusinessRuleUserId,
		BusinessRuleUser.BusinessRulePriority,
		a.BusinessRuleTargetAreaTable,
		b.BusinessRuleTargetAreaTable
		FROM BusinessRuleUser
		LEFT JOIN BusinessRuleTargetArea a
		ON BusinessRuleUser.BusinessRuleOneTargetAreaId = a.BusinessRuleTargetAreaId
		LEFT JOIN BusinessRuleTargetArea b
		ON BusinessRuleUser.BusinessRuleOneTargetAreaTwoId = b.BusinessRuleTargetAreaId
		WHERE a.BusinessRuleTargetAreaTable = @tableName
		OR b.BusinessRuleTargetAreaTable = @tableName
		ORDER BY BusinessRulePriority;

	WHILE (1=1)
	BEGIN
		--"walks" the execution list table processing each
		SELECT @businessRuleUserId = MIN(TEMP_BusinessRuleUserId) FROM #BusinessRuleExecution WHERE TEMP_BusinessRuleUserId > @businessRuleUserId;
		IF @businessRuleUserId IS NULL BREAK;
		EXEC USP_StatusUser @businessRuleUserId;
	END
RETURN
GO --
--SP for business rule evaluation
CREATE PROCEDURE [dbo].[USP_StatusUser]
	@businessRuleUserId int
AS
	--print 'USP_StatusUser';
	--DECLARE @businessRuleUserId int = 1;
	DECLARE @businessRuleGroupId int = null;
	DECLARE @businessRuleOneId int = null;
	DECLARE @businessRuleTwoId int = null;
	DECLARE @businessRuleOneTargetAreaOneId int = null;
	DECLARE @businessRuleOneTargetAreaTwoId int = null;
	DECLARE @businessRuleTwoTargetAreaOneId int = null;
	DECLARE @businessRuleOneTargetAreaOneTableName varchar(100) = null;
	DECLARE @businessRuleOneTargetAreaTwoTableName varchar(100) = null;
	DECLARE @businessRuleTwoTargetAreaOneTableName varchar(100) = null;
	DECLARE @businessRuleOneTargetAreaOneFieldName varchar(100) = null;
	DECLARE @businessRuleOneTargetAreaTwoFieldName varchar(100) = null;
	DECLARE @businessRuleTwoTargetAreaOneFieldName varchar(100) = null;
	DECLARE @busOneTarOnePriFieldName varchar(100) = null;
	DECLARE @busOneTarTwoPriFieldName varchar(100) = null;
	DECLARE @busTwoTarOnePriFieldName varchar(100) = null;
	DECLARE @businessRuleTypeId int = null;
	DECLARE @businessRuleOneConditionTypeId int = null;
	DECLARE @businessRuleTwoConditionTypeId int = null;
	DECLARE @busOneConTypeName varchar(100) = null;
	DECLARE @busTwoConTypeName varchar(100) = null;
	DECLARE @businessRuleOneOperatorId int = null;
	DECLARE @businessRuleTwoOperatorId int = null;
	DECLARE @busOneOperatorName varchar(100) = null;
	DECLARE @busTwoOperatorName varchar(100) = null;
	DECLARE @businessRuleOneSensitivityTypeId int = null;
	DECLARE @businessRuleTwoSensitivityTypeId int = null;
	DECLARE @busOneSenTypeName varchar(100) = null;
	DECLARE @busTwoSenTypeName varchar(100) = null;
	DECLARE @busOneConOne varchar(100) = null;
	DECLARE @busTwoConOne varchar(100) = null;
	DECLARE @busOneSen varchar(100) = null;
	DECLARE @busTwoSen varchar(100) = null;
	DECLARE @busOneConTwo varchar(100) = null;
	DECLARE @bindingTable varchar(100) = null;
	DECLARE @bindingField varchar(100) = null;
	DECLARE @targetAnchorId int = null;
	DECLARE @serverIdentifier varchar(100) = null;
	DECLARE @flagTrue int = null;
	DECLARE @businessRuleTargetAreaDetail int = 0;
	DECLARE @targetAnchorDetail int = 0;

	--collect business rule group
	SELECT @businessRuleGroupId = BusinessRuleGroupId FROM BusinessRuleUser
	WHERE BusinessRuleUserId = @businessRuleUserId;
	--collect business rule one
	SELECT @businessRuleOneId = BusinessRuleOneId FROM BusinessRuleGroup
	WHERE BusinessRuleGroupId = @businessRuleGroupId;
	--collect business rule two
	SELECT @businessRuleTwoId = BusinessRuleTwoId FROM BusinessRuleGroup
	WHERE BusinessRuleGroupId = @businessRuleGroupId;
	--collect business rule one target area one key
	SELECT @businessRuleOneTargetAreaOneId = BusinessRuleOneTargetAreaId FROM BusinessRuleUser
	WHERE BusinessRuleUserId = @businessRuleUserId;
	--collect business rule one target area two key
	SELECT @businessRuleOneTargetAreaTwoId = BusinessRuleOneTargetAreaTwoId FROM BusinessRuleUser
	WHERE BusinessRuleUserId = @businessRuleUserId;
	--collect business rule two target area key
	SELECT @businessRuleTwoTargetAreaOneId = BusinessRuleTwoTargetAreaId FROM BusinessRuleUser
	WHERE BusinessRuleUserId = @businessRuleUserId;
	--collect table names for business rules
	SELECT @businessRuleOneTargetAreaOneTableName = BusinessRuleTargetAreaTable FROM BusinessRuleTargetArea
	WHERE BusinessRuleTargetAreaId = @businessRuleOneTargetAreaOneId;
	SELECT @businessRuleOneTargetAreaTwoTableName = BusinessRuleTargetAreaTable FROM BusinessRuleTargetArea
	WHERE BusinessRuleTargetAreaId = @businessRuleOneTargetAreaTwoId;
	SELECT @businessRuleTwoTargetAreaOneTableName = BusinessRuleTargetAreaTable FROM BusinessRuleTargetArea
	WHERE BusinessRuleTargetAreaId = @businessRuleTwoTargetAreaOneId;
	--collect field names for business rules
	SELECT @businessRuleOneTargetAreaOneFieldName = BusinessRuleTargetAreaField FROM BusinessRuleTargetArea
	WHERE BusinessRuleTargetAreaId = @businessRuleOneTargetAreaOneId;
	SELECT @businessRuleOneTargetAreaTwoFieldName = BusinessRuleTargetAreaField FROM BusinessRuleTargetArea
	WHERE BusinessRuleTargetAreaId = @businessRuleOneTargetAreaTwoId;
	SELECT @businessRuleOneTargetAreaOneFieldName = BusinessRuleTargetAreaField FROM BusinessRuleTargetArea
	WHERE BusinessRuleTargetAreaId = @businessRuleTwoTargetAreaOneId;
	--collect primary key field names for target area tables
	SET @busOneTarOnePriFieldName = @businessRuleOneTargetAreaOneTableName + 'Id';
	SET @busOneTarTwoPriFieldName = @businessRuleOneTargetAreaTwoTableName + 'Id';
	SET @busTwoTarOnePriFieldName = @businessRuleTwoTargetAreaOneTableName + 'Id';
	--collect business rule group information for evaluations
	--only the first business rule within a group contains a business rule type
	SELECT @businessRuleTypeId = BusinessRuleTypeId FROM BusinessRule
	WHERE BusinessRuleId = @businessRuleOneId;
	--condition type determines initial evaluation code block based on type of data business
	--rule will be performing a test on
	SELECT @businessRuleOneConditionTypeId = BusinessRuleConditionTypeId FROM BusinessRule
	WHERE BusinessRuleId = @businessRuleOneId;
	SELECT @businessRuleTwoConditionTypeId = BusinessRuleConditionTypeId FROM BusinessRule
	WHERE BusinessRuleId = @businessRuleTwoId;
	SELECT @busOneConTypeName = BusinessRuleConditionTypeName FROM BusinessRuleConditionType
	WHERE BusinessRuleConditionTypeId = @businessRuleOneConditionTypeId;
	SELECT @busTwoConTypeName = BusinessRuleConditionTypeName FROM BusinessRuleConditionType
	WHERE BusinessRuleConditionTypeId = @businessRuleTwoConditionTypeId;
	--collect operators for business rules
	SELECT @businessRuleOneOperatorId = BusinessRuleOperatorId FROM BusinessRule
	WHERE BusinessRuleId = @businessRuleOneId;
	SELECT @businessRuleTwoOperatorId = BusinessRuleOperatorId FROM BusinessRule
	WHERE BusinessRuleId = @businessRuleTwoId;
	SELECT @busOneOperatorName = BusinessRuleOperatorName FROM BusinessRuleOperator
	WHERE BusinessRuleOperatorId = @businessRuleOneOperatorId;
	SELECT @busTwoOperatorName = BusinessRuleOperatorName FROM BusinessRuleOperator
	WHERE BusinessRuleOperatorId = @businessRuleTwoOperatorId;
	--collect sensitivity info for business rules
	SELECT @businessRuleOneSensitivityTypeId = BusinessRuleSensitivityTypeId FROM BusinessRule
	WHERE BusinessRuleId = @businessRuleOneId;
	SELECT @businessRuleTwoSensitivityTypeId = BusinessRuleSensitivityTypeId FROM BusinessRule
	WHERE BusinessRuleId = @businessRuleTwoId;
	SELECT @busOneSenTypeName = BusinessRuleSensitivityTypeName FROM BusinessRuleSensitivityType
	WHERE BusinessRuleSensitivityTypeId = @businessRuleOneSensitivityTypeId;
	SELECT @busTwoSenTypeName = BusinessRuleSensitivityTypeName FROM BusinessRuleSensitivityType
	WHERE BusinessRuleSensitivityTypeId = @businessRuleTwoSensitivityTypeId;
	--collect business rule conditions and sensitivity
	SELECT @busOneConOne = BusinessRuleTargetAreaField FROM BusinessRuleTargetArea
	WHERE BusinessRuleTargetAreaId = @businessRuleOneTargetAreaOneId;
	SELECT @busTwoConOne = BusinessRuleTargetAreaField FROM BusinessRuleTargetArea
	WHERE BusinessRuleTargetAreaId = @businessRuleTwoTargetAreaOneId;
	SELECT @busOneSen = Sensitivity FROM BusinessRule
	WHERE BusinessRuleId = @businessRuleOneId;
	SELECT @busTwoSen = Sensitivity FROM BusinessRule
	WHERE BusinessRuleId = @businessRuleTwoId;
	SELECT @busOneConTwo = ConditionTwo FROM BusinessRule
	WHERE BusinessRuleId = @businessRuleOneId;

	--if there is a second target area for first business rule find the common binding field between the two
	IF @businessRuleOneTargetAreaTwoTableName <> 'NULL'--this is not a null value, but instead the string NULL
	BEGIN
		--print 'UDF_BindingTable';
		SELECT @bindingTable = dbo.BindingTable(@businessRuleOneTargetAreaOneTableName,@businessRuleOneTargetAreaTwoTableName);
		SET @bindingField = @bindingTable + 'Id';
		SELECT @targetAnchorId = TargetAnchorId FROM TargetAnchor
		WHERE TargetAnchorName = @bindingField;
	END
	--else find the anchor field, if server identifier (group by) field and busOneTarOnePriFieldName are the same use alt block
	ELSE
	BEGIN
		DECLARE @stringExecutionListTableOne nvarchar(max) = null;
		DECLARE @stringExecutionListTableOneAlt nvarchar(max) = null;
		DECLARE @stringExecutionListTableTwo nvarchar(max) = null;
		DECLARE @stringExecutionListTableTwoAlt nvarchar(max) = null;
		DECLARE @tableUsed varchar(100) = null;
		DECLARE @parentTable varchar(100) = null;
		SELECT @serverIdentifier = COLUMN_NAME,@tableUsed = TABLE_NAME
		FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @businessRuleOneTargetAreaOneTableName
		AND COLUMN_NAME IN (SELECT TargetAnchorName FROM TargetAnchor);
		SELECT @targetAnchorId = TargetAnchorId FROM TargetAnchor WHERE TargetAnchorName = @busOneTarOnePriFieldName;
		SELECT @parentTable =  TABLE_NAME
		FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE a
		WHERE a.CONSTRAINT_NAME LIKE 'PK_%'
		AND COLUMN_NAME = @serverIdentifier;


		IF @serverIdentifier <> @busOneTarOnePriFieldName AND @businessRuleTwoTargetAreaOneTableName = 'NULL'
		BEGIN
			DROP TABLE IF EXISTS #TEMP_ExecutionListTableOne;
			CREATE TABLE #TEMP_ExecutionListTableOne
			(TEMP_ServerIdentifier varchar(100),TEMP_TimeDateRecord varchar(100),TEMP_BusOneTarOnePriFieldName varchar(100));
			SET @stringExecutionListTableOne = 
			N'INSERT INTO #TEMP_ExecutionListTableOne(TEMP_ServerIdentifier,TEMP_TimeDateRecord,TEMP_BusOneTarOnePriFieldName)' + CHAR(13)+CHAR(10) + 
			N'SELECT a.' + @serverIdentifier + N', a.TimeDateRecord, a.' + @busOneTarOnePriFieldName + CHAR(13)+CHAR(10) + 
			N'FROM ' + @businessRuleOneTargetAreaOneTableName+ N' a'  + CHAR(13)+CHAR(10) + 
			N'INNER JOIN (' + CHAR(13)+CHAR(10) + 
				N'SELECT ' + @serverIdentifier + N', MAX(TimeDateRecord) AS TimeDateRecord' + CHAR(13)+CHAR(10) + 
				N'FROM ' + @businessRuleOneTargetAreaOneTableName+ CHAR(13)+CHAR(10) + 
				N'GROUP BY ' + @serverIdentifier + CHAR(13)+CHAR(10) + 
			N') b ON a.' + @serverIdentifier + N' = b.' + @serverIdentifier + N' AND a.TimeDateRecord = b.TimeDateRecord' + CHAR(13)+CHAR(10) + 
			N'ORDER BY ' + @busOneTarOnePriFieldName + N';';
			EXEC sp_executesql @stringExecutionListTableOne;
		END
		ELSE IF @serverIdentifier = @busOneTarOnePriFieldName AND @businessRuleTwoTargetAreaOneTableName = 'NULL'
		BEGIN
			DROP TABLE IF EXISTS #TEMP_ExecutionListTableOneAlt;
			CREATE TABLE #TEMP_ExecutionListTableOneAlt
			(TEMP_ServerIdentifier varchar(100),TEMP_TimeDateRecord varchar(100));
			SET @stringExecutionListTableOneAlt = 
			N'INSERT INTO #TEMP_ExecutionListTableOneAlt(TEMP_ServerIdentifier,TEMP_TimeDateRecord)' + CHAR(13)+CHAR(10) + 
			N'SELECT a.' + @serverIdentifier + N', a.TimeDateRecord' + CHAR(13)+CHAR(10) + 
			N'FROM ' + @businessRuleOneTargetAreaOneTableName+ N' a'  + CHAR(13)+CHAR(10) + 
			N'INNER JOIN (' + CHAR(13)+CHAR(10) + 
				N'SELECT ' + @serverIdentifier + N', MAX(TimeDateRecord) AS TimeDateRecord' + CHAR(13)+CHAR(10) + 
				N'FROM ' + @businessRuleOneTargetAreaOneTableName+ CHAR(13)+CHAR(10) + 
				N'GROUP BY ' + @serverIdentifier + CHAR(13)+CHAR(10) + 
			N') b ON a.' + @serverIdentifier + N' = b.' + @serverIdentifier + N' AND a.TimeDateRecord = b.TimeDateRecord' + CHAR(13)+CHAR(10) + 
			N'ORDER BY ' + @serverIdentifier + N';';
			EXEC sp_executesql @stringExecutionListTableOneAlt;
		END
		ELSE IF @serverIdentifier <> @busOneTarOnePriFieldName AND @businessRuleTwoTargetAreaOneTableName <> 'NULL'
		BEGIN
			DROP TABLE IF EXISTS #TEMP_ExecutionListTableTwo;
			CREATE TABLE #TEMP_ExecutionListTableTwo
			(TEMP_ServerIdentifier varchar(100),TEMP_TimeDateRecord varchar(100),TEMP_BusOneTarOnePriFieldName varchar(100));
			SET @stringExecutionListTableTwo = 
			N'INSERT INTO #TEMP_ExecutionListTableTwo(TEMP_ServerIdentifier,TEMP_TimeDateRecord,TEMP_BusOneTarOnePriFieldName)' + CHAR(13)+CHAR(10) + 
			N'SELECT a.' + @serverIdentifier + N', a.TimeDateRecord, a.' + @busOneTarOnePriFieldName + CHAR(13)+CHAR(10) + 
			N'FROM ' + @businessRuleOneTargetAreaOneTableName + N' a'  + CHAR(13)+CHAR(10) + 
			N'INNER JOIN (' + CHAR(13)+CHAR(10) + 
				N'SELECT ' + @serverIdentifier + N', MAX(TimeDateRecord) AS TimeDateRecord' + CHAR(13)+CHAR(10) + 
				N'FROM ' + @businessRuleOneTargetAreaOneTableName + CHAR(13)+CHAR(10) + 
				N'GROUP BY ' + @serverIdentifier + CHAR(13)+CHAR(10) + 
			N') b ON a.' + @serverIdentifier + N' = b.' + @serverIdentifier + N' AND a.TimeDateRecord = b.TimeDateRecord' + CHAR(13)+CHAR(10) + 
			N'INNER JOIN ' + @parentTable + N' c ' + N'ON a.' + @serverIdentifier + N' = c.' + @serverIdentifier + CHAR(13) + CHAR (10) + 
			N'INNER JOIN ' + @businessRuleTwoTargetAreaOneTableName + N' d ON c.' + @busTwoTarOnePriFieldName + N' = d.'
			 + @busTwoTarOnePriFieldName + CHAR(13) + CHAR (10) + 
			N'WHERE d.' + @busTwoConOne + N' = ' + CHAR(39) + @busTwoSen + CHAR(39) + CHAR(13) + CHAR (10) + 
			N'ORDER BY ' + @busOneTarOnePriFieldName + N';';
			EXEC sp_executesql @stringExecutionListTableTwo;
		END	
		ELSE
		BEGIN
			DROP TABLE IF EXISTS #TEMP_ExecutionListTableTwoAlt;
			CREATE TABLE #TEMP_ExecutionListTableTwoAlt
			(TEMP_ServerIdentifier varchar(100),TEMP_TimeDateRecord varchar(100));
			SET @stringExecutionListTableTwoAlt = 
			N'INSERT INTO #TEMP_ExecutionListTableTwoAlt(TEMP_ServerIdentifier,TEMP_TimeDateRecord)' + CHAR(13)+CHAR(10) + 
			N'SELECT a.' + @serverIdentifier + N', a.TimeDateRecord' + CHAR(13)+CHAR(10) + 
			N'FROM ' + @businessRuleOneTargetAreaOneTableName + N' a'  + CHAR(13)+CHAR(10) + 
			N'INNER JOIN (' + CHAR(13)+CHAR(10) + 
				N'SELECT ' + @serverIdentifier + N', MAX(TimeDateRecord) AS TimeDateRecord' + CHAR(13)+CHAR(10) + 
				N'FROM ' + @businessRuleOneTargetAreaOneTableName + CHAR(13)+CHAR(10) + 
				N'GROUP BY ' + @serverIdentifier + CHAR(13)+CHAR(10) + 
			N') b ON a.' + @serverIdentifier + N' = b.' + @serverIdentifier + N' AND a.TimeDateRecord = b.TimeDateRecord' + CHAR(13)+CHAR(10) + 
			N'INNER JOIN ' + @parentTable + N' c ' + N'ON a.' + @serverIdentifier + N' = c.' + @serverIdentifier + CHAR(13) + CHAR (10) + 
			N'INNER JOIN ' + @businessRuleTwoTargetAreaOneTableName + N' d ON c.' + @busTwoTarOnePriFieldName + N' = d.'
			 + @busTwoTarOnePriFieldName + CHAR(13) + CHAR (10) + 
			N'WHERE d.' + @busTwoConOne + N' = ' + CHAR(39) + @busTwoSen + CHAR(39) + CHAR(13) + CHAR (10) + 
			N'ORDER BY ' + @serverIdentifier + N';';
			EXEC sp_executesql @stringExecutionListTableTwoAlt;
		END
	END
	WHILE (1=1)
	BEGIN
		--evaluating business rules where first business rule data type is a time
		IF @busOneConTypeName = 'datetime2'
		BEGIN
			DECLARE @stringTimeOne nvarchar(max) = null;
			DECLARE @stringParamTimeOne nvarchar(max) = null;
			DECLARE @dateDifference int = null;
			DECLARE @stringDateDifference nvarchar(max) = null;
			DECLARE @stringParamDateDifference nvarchar(max) = null;
			DECLARE @stringRuleTest nvarchar(max) = null;
			DECLARE @stringParamRuleTest nvarchar(max) = null;
			--init variable for determining if tested condition is true
			SET @flagTrue = 0;
			--determine which execution list to use, then walk the list
			IF OBJECT_ID('tempdb..#TEMP_ExecutionListTableOne') IS NOT NULL
			BEGIN
				SELECT @businessRuleTargetAreaDetail = MIN(TEMP_BusOneTarOnePriFieldName)
				FROM #TEMP_ExecutionListTableOne WHERE TEMP_BusOneTarOnePriFieldName > @businessRuleTargetAreaDetail;
				SELECT @targetAnchorDetail = TEMP_ServerIdentifier
				FROM #TEMP_ExecutionListTableOne WHERE TEMP_BusOneTarOnePriFieldName = @businessRuleTargetAreaDetail;
			END
			ELSE IF OBJECT_ID('tempdb..#TEMP_ExecutionListTableOneAlt') IS NOT NULL
			BEGIN 
				SELECT @businessRuleTargetAreaDetail = MIN(TEMP_ServerIdentifier) FROM #TEMP_ExecutionListTableOneAlt
				WHERE TEMP_ServerIdentifier > @businessRuleTargetAreaDetail;
				SELECT @targetAnchorDetail = @businessRuleTargetAreaDetail;
			END
			ELSE IF OBJECT_ID('tempdb..#TEMP_ExecutionListTableTwo') IS NOT NULL
			BEGIN
				SELECT @businessRuleTargetAreaDetail = MIN(TEMP_BusOneTarOnePriFieldName)
				FROM #TEMP_ExecutionListTableTwo WHERE TEMP_BusOneTarOnePriFieldName > @businessRuleTargetAreaDetail;
				SELECT @targetAnchorDetail = TEMP_ServerIdentifier
				FROM #TEMP_ExecutionListTableTwo WHERE TEMP_BusOneTarOnePriFieldName = @businessRuleTargetAreaDetail;
			END
			ELSE
			BEGIN
				SELECT @businessRuleTargetAreaDetail = MIN(TEMP_ServerIdentifier) FROM #TEMP_ExecutionListTableTwoAlt
				WHERE TEMP_ServerIdentifier > @businessRuleTargetAreaDetail;
				SELECT @targetAnchorDetail = @businessRuleTargetAreaDetail;
			END
			-- declare variable for storing condition one from business rule one target area one 
			DECLARE @timeOne datetime2;
			--collect condition one from business rule one target area one 
			SET @stringTimeOne = N'SELECT @timeOne = ' + @busOneConOne + N' FROM ' + @businessRuleOneTargetAreaOneTableName
			 + N' WHERE ' + @busOneTarOnePriFieldName + N' = @businessRuleTargetAreaDetail';
			IF @businessRuleTargetAreaDetail IS NULL BREAK;
			SET @stringParamTimeOne = N'@timeOne datetime2 OUTPUT,@businessRuleTargetAreaDetail int';
			EXEC sp_executesql @stringTimeOne,@stringParamTimeOne,@timeOne OUTPUT,@businessRuleTargetAreaDetail;
			--collect date difference between current time and time being passed from table value
			SET @stringDateDifference = N'SET @dateDifference = DATEDIFF(' + @busOneSenTypeName + N',@timeOne,'
			 + @busOneConTwo + N')';
			SET @stringParamDateDifference = N'@dateDifference int OUTPUT,@timeOne datetime2';
			EXEC sp_executesql @stringDateDifference,@stringParamDateDifference,@dateDifference OUTPUT,@timeOne;
			--test for boolean result of business rule with referenced information
			SET @stringRuleTest = N'IF @dateDifference ' + @busOneOperatorName + N' @busOneSen'
			 + CHAR(13) + CHAR(10) + N'BEGIN' + CHAR(13) + CHAR(10) +
			 + N'SET @flagTrue = 1' + CHAR(13) + CHAR(10) + N'END';
			SET @stringParamRuleTest = N'@dateDifference int,@flagTrue int OUTPUT,@busOneSen int';
			EXEC sp_executesql @stringRuleTest,@stringParamRuleTest,@dateDifference,@flagTrue OUTPUT,@busOneSen;
			--if business rule tests true, pass data to post the status
			IF @flagTrue = 1
			BEGIN
				EXEC USP_PostStatus @businessRuleUserId,@targetAnchorId,@targetAnchorDetail;
			END
		END
		--evaluating business rules where the business rule compares one target area against another target area
		IF @busOneConTypeName = 'int' AND @businessRuleOneTargetAreaTwoTableName <> 'NULL'
		BEGIN
			--int variables
			DECLARE @compareVarOne int = 0;
			DECLARE @compareVarTwo int = 0;
			DECLARE @stringCreateTempTableOne nvarchar(max) = null;
			DECLARE @stringCreateTempTableTwo nvarchar(max) = null;
			
			DROP TABLE IF EXISTS #TEMP_TableOne
			CREATE TABLE #TEMP_TableOne
			(TEMP_Binding varchar(100),TEMP_ConditionOne varchar(100))
			--grabs most recent records for target area one
			set @stringCreateTempTableOne =
			N'INSERT INTO #TEMP_TableOne(TEMP_Binding,TEMP_ConditionOne)' + CHAR(13)+CHAR(10) + 
			N'SELECT a.' + @bindingField + N',a.' + @busOneConOne + CHAR(13)+CHAR(10) + 
			N'FROM ' + @businessRuleOneTargetAreaOneTableName + N' a' + CHAR(13)+CHAR(10) +
			N'INNER JOIN ( ' + N'SELECT ' + @bindingField + N',MAX(TimeDateRecord) AS TDR' + CHAR(13)+CHAR(10) +
			N'FROM ' + @busOneConOne + N' GROUP BY ' + @bindingField + N') b' + CHAR(13)+CHAR(10) + 
			N'ON a.' + @bindingField + N' = b.' + @bindingField + N' AND a.TimeDateRecord = b.TDR;'; 
			
			EXEC sp_executesql @stringCreateTempTableOne;
			
			DROP TABLE IF EXISTS #TEMP_TableTwo
			CREATE TABLE #TEMP_TableTwo
			(TEMP_Binding varchar(100),TEMP_ConditionTwo varchar(100))
			--grabs most recent records for target area two
			set @stringCreateTempTableTwo =
			N'INSERT INTO #TEMP_TableTwo(TEMP_Binding,TEMP_ConditionTwo)' + CHAR(13)+CHAR(10) + 
			N'SELECT a.' + @bindingField + N',a.' + @businessRuleOneTargetAreaTwoFieldName + CHAR(13)+CHAR(10) + 
			N'FROM ' + @businessRuleOneTargetAreaTwoTableName + N' a' + CHAR(13)+CHAR(10) + 
			N'INNER JOIN ( ' + N'SELECT ' + @bindingField + N',MAX(TimeDateRecord) AS TDR' + CHAR(13)+CHAR(10) +
			N'FROM ' + @businessRuleOneTargetAreaTwoFieldName + N' GROUP BY ' + @bindingField + N') b' + CHAR(13)+CHAR(10) + 
			N'ON a.' + @bindingField + N' = b.' + @bindingField + N' AND a.TimeDateRecord = b.TDR;'; 
			
			EXEC sp_executesql @stringCreateTempTableTwo;

			DROP TABLE IF EXISTS #TEMP_Evaluation
			CREATE TABLE #TEMP_Evaluation
			(TEMP_ConditionOne varchar(100),TEMP_ConditionTwo varchar(100), TEMP_Binding varchar(100))
			--binds the first and second target area fields onto the same record using the share fk in the tables
			INSERT INTO #TEMP_Evaluation(TEMP_ConditionOne,TEMP_ConditionTwo,TEMP_Binding)
			SELECT
			#TEMP_TableOne.TEMP_ConditionOne,
			#TEMP_TableTwo.TEMP_ConditionTwo,
			#TEMP_TableTwo.TEMP_Binding
			FROM #TEMP_TableOne
			FULL JOIN #TEMP_TableTwo
			ON #TEMP_TableOne.TEMP_Binding = #TEMP_TableTwo.TEMP_Binding;
			
			--evaluation table is "walked"
			WHILE (1=1)
			BEGIN
				SET @flagTrue = 0;
				DECLARE @stringDirectComparison nvarchar(max);
				DECLARE @stringParamDirectComparison nvarchar(max);
				DECLARE @stringIndirectComparison nvarchar(max);
				DECLARE @stringParamIndirectComparison nvarchar(max);
				SELECT @targetAnchorDetail = MIN(TEMP_Binding) FROM #TEMP_Evaluation WHERE TEMP_Binding > @targetAnchorDetail;
				SELECT @compareVarOne = TEMP_ConditionOne FROM #TEMP_Evaluation WHERE TEMP_Binding = @targetAnchorDetail;
				SELECT @compareVarTwo = TEMP_ConditionTwo FROM #TEMP_Evaluation WHERE TEMP_Binding = @targetAnchorDetail;
				IF @targetAnchorDetail IS NULL BREAK;--break logic if no additional records are present
				--execution block if there is no sensitivity, indicating a direct comparison
				IF @busOneSen IS NULL
					BEGIN
						SET @stringDirectComparison = 
						N'IF @compareVarOne ' + @busOneOperatorName + N' @compareVarTwo' + CHAR(13)+CHAR(10) + N'BEGIN' + CHAR(13)+CHAR(10) + 
						N'SET @flagTrue = 1;' + CHAR(13)+CHAR(10) + N'END';
						SET @stringParamDirectComparison = 
						N'@compareVarOne int,@compareVarTwo int,@flagTrue int OUTPUT';
						EXEC sp_executesql @stringDirectComparison,@stringParamDirectComparison,@compareVarOne,@compareVarTwo,@flagTrue OUTPUT;
					END
				--execution block if there is a sensitivity, indicating a indirect comparison
				ELSE
					BEGIN
						SET @stringIndirectComparison = 
						N'IF (@compareVarTwo - @compareVarOne) ' + @busOneOperatorName + N' @busOneSen' + CHAR(13)+CHAR(10) + N'BEGIN' + CHAR(13)+CHAR(10) + 
						N'SET @flagTrue = 1;' + CHAR(13)+CHAR(10) + N'END';
						SET @stringParamIndirectComparison = 
						N'@compareVarOne int,@compareVarTwo int,@busOneSen decimal,@flagTrue int OUTPUT';
						EXEC sp_executesql @stringIndirectComparison,@stringParamIndirectComparison,@compareVarOne,@compareVarTwo,@busOneSen,@flagTrue OUTPUT;
					END
					IF @flagTrue = 1
					BEGIN
						EXEC USP_PostStatus @businessRuleUserId,@targetAnchorId,@targetAnchorDetail;
					END
			END
			IF @targetAnchorDetail IS NULL BREAK;
		END
		
		IF @busOneConTypeName = 'int' AND @businessRuleOneTargetAreaTwoTableName = 'NULL'
		BEGIN
			DECLARE @stringGetTestInt nvarchar(max) = null;
			DECLARE @stringParamGetTestInt nvarchar(max) = null;
			DECLARE @stringEvaluateInt nvarchar(max) = null;
			DECLARE @stringParamEvaluateInt nvarchar(max) = null;
			DECLARE @testInt int = null;
			--converts null value to num value so it doesn't break evaluation
			IF @busOneSen IS NULL
			BEGIN
				SET @busOneSen = -1;
			END
			SET @flagTrue = 0;
			--determine which execution list to use, then walk the list
			IF OBJECT_ID('tempdb..#TEMP_ExecutionListTableOne') IS NOT NULL
			BEGIN
				--print 'p1';
				SELECT @businessRuleTargetAreaDetail = MIN(TEMP_BusOneTarOnePriFieldName)
				FROM #TEMP_ExecutionListTableOne WHERE TEMP_BusOneTarOnePriFieldName > @businessRuleTargetAreaDetail;
				SELECT @targetAnchorDetail = TEMP_ServerIdentifier
				FROM #TEMP_ExecutionListTableOne WHERE TEMP_BusOneTarOnePriFieldName = @businessRuleTargetAreaDetail;
			END
			ELSE IF OBJECT_ID('tempdb..#TEMP_ExecutionListTableOneAlt') IS NOT NULL
			BEGIN 
				SELECT @businessRuleTargetAreaDetail = MIN(TEMP_ServerIdentifier) FROM #TEMP_ExecutionListTableOneAlt
				WHERE TEMP_ServerIdentifier > @businessRuleTargetAreaDetail;
				SELECT @targetAnchorDetail = @businessRuleTargetAreaDetail;
			END
			ELSE IF OBJECT_ID('tempdb..#TEMP_ExecutionListTableTwo') IS NOT NULL
			BEGIN
				SELECT @businessRuleTargetAreaDetail = MIN(TEMP_BusOneTarOnePriFieldName)
				FROM #TEMP_ExecutionListTableTwo WHERE TEMP_BusOneTarOnePriFieldName > @businessRuleTargetAreaDetail;
				SELECT @targetAnchorDetail = TEMP_ServerIdentifier
				FROM #TEMP_ExecutionListTableTwo WHERE TEMP_BusOneTarOnePriFieldName = @businessRuleTargetAreaDetail;
			END
			ELSE
			BEGIN
				SELECT @businessRuleTargetAreaDetail = MIN(TEMP_ServerIdentifier) FROM #TEMP_ExecutionListTableTwoAlt
				WHERE TEMP_ServerIdentifier > @businessRuleTargetAreaDetail;
				SELECT @targetAnchorDetail = @businessRuleTargetAreaDetail;
			END
			--get value for business rule test
			SET @stringGetTestInt = N'SELECT @testInt = ' + @busOneConOne + N' FROM ' + @businessRuleOneTargetAreaOneTableName+ N' WHERE ' + @busOneTarOnePriFieldName + N' = @businessRuleTargetAreaDetail';
			IF @businessRuleTargetAreaDetail IS NULL BREAK;
			SET @stringParamGetTestInt = 
			N'@testInt int OUTPUT,@businessRuleTargetAreaDetail int';
			EXEC sp_executesql @stringGetTestInt,@stringParamGetTestInt,@testInt OUTPUT,@businessRuleTargetAreaDetail;
			IF @testInt IS null
			BEGIN
				SET @testInt = -1;
			END
			--evaluate business rule
			SET @stringEvaluateInt = N'IF @testInt ' + @busOneOperatorName + N' @busOneSen' + CHAR(13)+CHAR(10) + N'BEGIN' + CHAR(13)+CHAR(10) 
			+ N'SET @flagTrue = 1' + CHAR(13)+CHAR(10) + N'END';
			SET @stringParamEvaluateInt = 
			N'@testInt int,@busOneSen decimal,@flagTrue int OUTPUT';
			EXEC sp_executesql @stringEvaluateInt,@stringParamEvaluateInt,@testInt,@busOneSen,@flagTrue OUTPUT;
			--if test is true, post status
			IF @flagTrue = 1
			BEGIN
				EXEC USP_PostStatus @businessRuleUserId,@targetAnchorId,@targetAnchorDetail;
			END
		END

		IF @busOneConTypeName = 'varchar(100)' AND @businessRuleOneTargetAreaTwoTableName <> 'NULL'
		BEGIN
			PRINT 'VARCHAR COMPARISION EVALUATION';
		END

		IF @busOneConTypeName = 'varchar(100)' AND @businessRuleOneTargetAreaTwoTableName = 'NULL'
		BEGIN
			PRINT 'VARCHAR SINGLE EVALUATION';
		END
	END
RETURN
GO
/*CREATE UPSERT FOR UPSERT STATUS*/
CREATE PROCEDURE [dbo].[USP_PostStatus]---PostStatusId(GENERATED OR RETURNED), PostStatusName(PASSED)
	@businessRuleUserId int,
	@targetAnchorId int,
	@targetAnchorDetail int
AS
	--print 'USP_PostStatus';
	DECLARE @sqlStringTest nvarchar(max);
	DECLARE @sqlStringTestParam nvarchar(max);
	DECLARE @businessRuleTargetAreaId int = 0;
	DECLARE @businessRuleTargetAreaLevelId int = 0;
	DECLARE @businessRuleTargetAreaLevelName varchar(100) = null;
	SELECT @businessRuleTargetAreaId = BusinessRuleOneTargetAreaId FROM BusinessRuleUser WHERE BusinessRuleUserId = @businessRuleUserId;
	SELECT @businessRuleTargetAreaLevelId = BusinessRuleTargetAreaLevelId FROM BusinessRuleTargetArea WHERE BusinessRuleTargetAreaId = @businessRuleTargetAreaId;
	SELECT @businessRuleTargetAreaLevelName = BusinessRuleTargetAreaLevelName FROM BusinessRuleTargetAreaLevel WHERE BusinessRuleTargetAreaLevelId = @businessRuleTargetAreaLevelId; 
	DECLARE @userPermissionId int = 0;
	DECLARE @userIndexId int = 0;
	DECLARE @businessRuleUserIdAlreadyPresent int = 0;
	DECLARE @statusUserId int = 0;
	DECLARE @businessRuleTargetAreaLevel varchar(100) = null;
	SELECT @userPermissionId = UserPermissionId FROM BusinessRuleUser WHERE BusinessRuleUserId = @businessRuleUserId;
	SELECT @userIndexId = UserIndexId FROM UserPermission WHERE UserPermissionId = @userPermissionId;

	IF @businessRuleTargetAreaLevelName = 'Group'
	BEGIN
		SELECT @businessRuleTargetAreaLevel = BusinessRuleTargetAreaGroup FROM BusinessRuleTargetArea WHERE BusinessRuleTargetAreaId = @businessRuleTargetAreaId;
	END
	IF @businessRuleTargetAreaLevelName = 'Table'
	BEGIN
		SELECT @businessRuleTargetAreaLevel = BusinessRuleTargetAreaTable FROM BusinessRuleTargetArea WHERE BusinessRuleTargetAreaId = @businessRuleTargetAreaId;
	END
	IF @businessRuleTargetAreaLevelName = 'Field'
	BEGIN
		SELECT @businessRuleTargetAreaLevel = BusinessRuleTargetAreaField FROM BusinessRuleTargetArea WHERE BusinessRuleTargetAreaId = @businessRuleTargetAreaId;
	END

	DROP TABLE IF EXISTS #TEMP_BusinessRuleUserIdEvaluation
	CREATE TABLE #TEMP_BusinessRuleUserIdEvaluation
	(TEMP_BusinessRuleUserId int,TEMP_TargetAnchorId int,TEMP_TargetAnchorDetail int,TEMP_UserPermissionId int,TEMP_BusinessRuleOneTargetAreaId int,TEMP_UserIndexId int,TEMP_BusinessRuleTargetAreaLevel varchar(100))
			
	SET @sqlStringTest = 
	N'INSERT INTO #TEMP_BusinessRuleUserIdEvaluation(TEMP_BusinessRuleUserId,TEMP_TargetAnchorId,TEMP_TargetAnchorDetail,TEMP_UserPermissionId,TEMP_BusinessRuleOneTargetAreaId, TEMP_UserIndexId,TEMP_BusinessRuleTargetAreaLevel)' + CHAR(13)+CHAR(10) + 
	N'SELECT' + CHAR(13)+CHAR(10) + 
	N'StatusUser.BusinessRuleUserId,' + CHAR(13)+CHAR(10) + 
	N'StatusUser.TargetAnchorId,' + CHAR(13)+CHAR(10) + 
	N'StatusUser.TargetAnchorDetail,' + CHAR(13)+CHAR(10) + 
	N'BusinessRuleUser.UserPermissionId,' + CHAR(13)+CHAR(10) + 
	N'BusinessRuleUser.BusinessRuleOneTargetAreaId,' + CHAR(13)+CHAR(10) + 
	N'UserPermission.UserIndexId,' + CHAR(13)+CHAR(10) + 
	N'BusinessRuleTargetArea.BusinessRuleTargetArea' + @businessRuleTargetAreaLevelName + CHAR(13)+CHAR(10) + 
	N'FROM StatusUser' + CHAR(13)+CHAR(10) + 
	N'LEFT JOIN BusinessRuleUser' + CHAR(13)+CHAR(10) + 
	N'ON StatusUser.BusinessRuleUserId = BusinessRuleUser.BusinessRuleUserId' + CHAR(13)+CHAR(10) + 
	N'LEFT JOIN UserPermission' + CHAR(13)+CHAR(10) + 
	N'ON BusinessRuleUser.UserPermissionId = UserPermission.UserPermissionId' + CHAR(13)+CHAR(10) + 
	N'LEFT JOIN BusinessRuleTargetArea' + CHAR(13)+CHAR(10) + 
	N'ON BusinessRuleUser.BusinessRuleOneTargetAreaId = BusinessRuleTargetArea.BusinessRuleTargetAreaId' + CHAR(13)+CHAR(10) + 
	N'WHERE StatusUser.TargetAnchorId = @targetAnchorId AND StatusUser.TargetAnchorDetail = @targetAnchorDetail ' + CHAR(13)+CHAR(10) + 
	N'AND UserPermission.UserIndexId = @userIndexId AND ' + CHAR(13)+CHAR(10) + 
	N'BusinessRuleTargetArea.BusinessRuleTargetArea' + @businessRuleTargetAreaLevelName + N' = @businessRuleTargetAreaLevel;';	
			
	set @sqlStringTestParam = N'@targetAnchorId int,@targetAnchorDetail int,@userIndexId int,@businessRuleTargetAreaLevel varchar(100)';
	EXEC sp_executesql @sqlStringTest,@sqlStringTestParam,@targetAnchorId,@targetAnchorDetail,@userIndexId,@businessRuleTargetAreaLevel;		

	SELECT @businessRuleUserIdAlreadyPresent = TEMP_BusinessRuleUserId FROM #TEMP_BusinessRuleUserIdEvaluation;
	SELECT @statusUserId = StatusUserId FROM StatusUser WHERE BusinessRuleUserId = @businessRuleUserIdAlreadyPresent; 

	IF @businessRuleUserIdAlreadyPresent = 0 
		BEGIN
			INSERT INTO StatusUser(BusinessRuleUserId,TargetAnchorId,TargetAnchorDetail)
			VALUES(@businessRuleUserId,@targetAnchorId,@targetAnchorDetail)
			SET @statusUserId = @@IDENTITY;
		END
	ELSE
		BEGIN
			UPDATE StatusUser
			SET BusinessRuleUserId = @businessRuleUserId WHERE BusinessRuleUserId <> @businessRuleUserId AND StatusUserId = @statusUserId;
		END
return
GO --
/*
SAMPLE
EXEC USP_PostStatus @businessRuleUserId,@targetAnchorId,@targetAnchorDetail,@businessRuleTargetAreaId,@businessRuleStatusId;
END UPSERT FOR UPSERT STATUS
updated upsert
*/
