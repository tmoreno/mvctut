﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Hams3xLib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hams3xLib.Model.Tests
{
    [TestClass()]
    public class SpContextTests
    {
        [TestMethod()]
        public void SpContextTest()
        {
            
        }

        [TestMethod()]
        public void PostAuthAppServerReleaseTest()
        {
            SpContext spContext = new SpContext();

            var authAppReleaseModel = new AuthAppServerReleaseModel
            {
                AuthVersion = "1.8",
                AuthAppReleaseDate = DateTime.Now,
                AuthAppName = "Java",
                AuthAppVendorName = "Sun",
                ServerId = 1,
                ServerAppReleaseDate = DateTime.Now
            };
            spContext.PostAuthAppServerRelease(authAppReleaseModel);
        }

        [TestMethod()]
        public void PostInstalledAfloatAppServerTest()
        {
            SpContext spContext = new SpContext();

            var installedAfloatAppServer = new InstalledAfloatAppServerModel
            {
                ReturnServerCodeName = "AA100",
                Servername = "DDG1000USV01",
                AfloatVersion = "1.8",
                AfloatAppName = "Java",
                AfloatAppVendorName = "Sun",
                InstalledDate = DateTime.Now,
                InstallationAccount = "Joe",
                UninstallDate = null,
                UninstallAccount = null
            };
            spContext.PostInstalledAfloatAppServer(installedAfloatAppServer);
        }

        public void PostAuthAppTest()
        {
            SpContext spContext = new SpContext();

            var authApp = new AuthAppProc
            {
                AuthAppName = "Java",
                AuthAppVendorName = "Sun"
            };
        }
    }
}