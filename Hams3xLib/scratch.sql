﻿CREATE VIEW ComplianceStatusView
AS
select ServerAppVersionMonitoring.*,ComplianceStatus.ComplianceStatusName
from ServerAppVersionMonitoring
left join ComplianceStatus on ServerAppVersionMonitoring.ComplianceStatusId = ComplianceStatus.ComplianceStatusId;
GO

CREATE VIEW AuthorizedAppsView
AS
select AuthAppServerRelease.AuthAppServerReleaseId ,AuthAppServerRelease.ServerId,AuthAppRelease.AuthVersion,AuthApp.AuthAppName,AuthAppVendor.AuthAppVendorName from AuthAppServerRelease
left join AuthAppRelease on AuthAppServerRelease.AuthAppReleaseId = AuthAppRelease.AuthAppReleaseId 
left join AuthApp on AuthAppRelease.AuthAppId = AuthApp.AuthAppId
left join AuthAppVendor on AuthApp.AuthAppVendorId = AuthAppVendor.AuthAppVendorId;
GO

CREATE VIEW CurrentlyInstalledView
AS
select CurrentlyInstalled. InstalledAfloatAppServerId,CurrentlyInstalled. ServerId,AfloatAppVersion.AfloatVersion,AfloatApp.AfloatAppName,InstalledAfloatAppVendor.InstalledAfloatAppVendorName
from CurrentlyInstalled left join AfloatAppVersion on CurrentlyInstalled. AfloatAppVersionId = AfloatAppVersion.AfloatAppVersionId
left join AfloatApp on AfloatAppVersion.AfloatAppId = AfloatApp.AfloatAppId
left join InstalledAfloatAppVendor on AfloatApp.InstalledAfloatAppVendorId = InstalledAfloatAppVendor.InstalledAfloatAppVendorId;
GO

CREATE VIEW StatusView
AS
select StatusUser.TargetAnchorDetail,Status.StatusName
FROM StatusUser LEFT JOIN BusinessRuleUser ON StatusUser.BusinessRuleUserId = BusinessRuleUser.BusinessRuleUserId
LEFT JOIN BusinessRuleGroup ON BusinessRuleUser.BusinessRuleGroupId = BusinessRuleGroup.BusinessRuleGroupId
LEFT JOIN BusinessRule ON BusinessRuleGroup.BusinessRuleOneId = BusinessRule.BusinessRuleId
LEFT JOIN Status ON BusinessRule.StatusId = Status.StatusId;
GO
