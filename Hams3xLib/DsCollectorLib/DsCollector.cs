﻿using RegistryLib;
using System;
using System.Collections.Generic;

namespace DsCollectorLib
{

    public class DsCollector
    {

        readonly IRegistry Registry;

        public DsCollector(IRegistry registry)
        {
            this.Registry = registry;
        }
        public IList<SoftwareInfo> GetApplicationList(string registryKey)
        {
           

            IRegistryKey key = Registry.LocalMachine.OpenSubKey(registryKey);
            IList<SoftwareInfo> list = new List<SoftwareInfo>();

            foreach (string subkey in key.GetSubKeyNames())
            {
                Console.WriteLine(subkey);
                IRegistryKey rkey = key.OpenSubKey(subkey);

                list.Add(new SoftwareInfo
                {
                    DisplayName = (string)rkey.GetValue("DisplayName"),
                    Publisher = (string)rkey.GetValue("Publisher"), // to do - date
                    DisplayVersion = (string)rkey.GetValue("DisplayVersion")
                });
                Console.WriteLine((string)rkey.GetValue("DisplayName"));
            }

            return list;
        }

    }
}
