﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ashore_HAMS.Models3;
using Ashore_HAMS.Controllers;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Ashore_HAMS.Tests.Controllers
{
    [TestClass]
    public class ControllerUnitTest
    {
        //TO DO : https://stackoverflow.com/questions/483091/render-a-view-as-a-string/484932#484932


        [TestMethod]
        public  async Task TestDetails()
        {
            AUTH_APP_INDEXController aUTH_APP_INDEXController = new AUTH_APP_INDEXController();
            ViewResult result = (ViewResult)await aUTH_APP_INDEXController.Details(1);

            Console.WriteLine(aUTH_APP_INDEXController.ViewBag.DX);
            AUTH_APP_INDEX model = (AUTH_APP_INDEX)result.Model;
            Console.WriteLine(model.AUTH_APP_NAME);

        }

        [TestMethod]
        public async Task TestAuthAppIndexIndex()
        {
            AUTH_APP_INDEXController aUTH_APP_INDEXController = new AUTH_APP_INDEXController();
            ViewResult result = (ViewResult)await aUTH_APP_INDEXController.Index();

            Dictionary<int, AuthAppReleaseCompletedModel> dict = (Dictionary<int, AuthAppReleaseCompletedModel>)aUTH_APP_INDEXController.ViewBag.AuthAppReleaseCount;

            IEnumerable<AUTH_APP_INDEX> model = (IEnumerable<AUTH_APP_INDEX>)result.Model;
            
            foreach(AUTH_APP_INDEX item in model)
            {
                Console.WriteLine(item.AUTH_APP_NAME);
                foreach(AUTH_APP_RELEASE apr in item.AUTH_APP_RELEASE)
                {
                    Console.WriteLine(apr.AUTH_VERSION);
                    if (dict.ContainsKey(apr.AUTH_APP_RELEASE_ID))
                    {
                        Console.WriteLine(string.Format("{0}:{1}:{2}:{3}",
                            apr.AUTH_APP_RELEASE_ID,
                            dict[apr.AUTH_APP_RELEASE_ID].PercentCompleted,
                            dict[apr.AUTH_APP_RELEASE_ID].BarColorCode,
                            dict[apr.AUTH_APP_RELEASE_ID].VENDOR_APP_VERSION));
                    }
                }
            }

            int x = -1;
            if(aUTH_APP_INDEXController.ViewData["x"] != null)
            {
                x = (int)aUTH_APP_INDEXController.ViewData["x"];
            }

            Console.WriteLine(string.Format("city{0}:=", x));

        }

        [TestMethod]
        public void TestAuthorizedReleaseCount()
        {
            AUTH_APP_INDEXController aUTH_APP_INDEXController = new AUTH_APP_INDEXController();
            var x = aUTH_APP_INDEXController.AuthorizedReleaseCount;

            foreach(AuthAppReleaseCompletedModel item in x)
            {
                Console.Write(item.AUTH_APP_NAME);
                Console.WriteLine(item.AUTH_VERSION);
            }

        }

        [TestMethod]
        public void TestAuthorizedInstalledView()
        {
            AUTH_APP_INDEXController aUTH_APP_INDEXController = new AUTH_APP_INDEXController();
            var x = aUTH_APP_INDEXController.AuthorizedInstalledViews;
            foreach(AuthorizedInstalledView item in x)
            {
                Console.WriteLine(string.Format("{0}:{1}", item.VendorAppVersion, item.AuthorizedInstalledCount));
            }
        }

        [TestMethod]
        public void TestGetAuthAppSummary()
        {
            AUTH_APP_INDEXController aUTH_APP_INDEXController = new AUTH_APP_INDEXController();
            Console.WriteLine(aUTH_APP_INDEXController.GetAuthAppSummary());
        }





    }
}
