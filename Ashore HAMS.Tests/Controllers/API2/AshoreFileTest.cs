﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ashore_HAMS.Models3;
using Ashore_HAMS.Controllers.API2;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Web.Http;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Web.Http.Results;
using MVCTut.Controllers;
using static Ashore_HAMS.Controllers.API2.AfloatFileHullServersController;

namespace Ashore_HAMS.Tests.Controllers.API2
{
    [TestClass]
    public class AshoreFileTest
    {
        [TestMethod]
        public void PostAshoreFileTest()
        {
            AshoreFile ashoreFile = new AshoreFile()
            {
                Filename = "test.exe",
                Date = DateTime.Now,
                Hash = "1234"
            };

            AshoreFilesController ashoreFilesController = new AshoreFilesController();
            IHttpActionResult actionResult = ashoreFilesController.PostAshoreFile(ashoreFile);
            var result = (CreatedAtRouteNegotiatedContentResult<AshoreFile>)actionResult;
            Console.WriteLine(result.RouteValues["id"]);

        }

        [TestMethod]
        public void PostAshoreFileHullServersTest()
        {
            UploadController uploadController = new UploadController();

            AshoreFileHullServersController ashoreFileHullServersController = new AshoreFileHullServersController();
            foreach (int sid in uploadController.GetHullServerIds())
            {
                AshoreFileHullServer ashoreFileHullServer = new AshoreFileHullServer()
                {
                    HULL_SERVER_ID = sid,
                    AshoreFileId = 8
                };

                IHttpActionResult actionResult = ashoreFileHullServersController.PostAshoreFileHullServer(ashoreFileHullServer);
                var result = (CreatedAtRouteNegotiatedContentResult<AshoreFileHullServer>)actionResult;
                Console.WriteLine(result.RouteValues["id"]);
            }


        }

        [TestMethod]
        public void PostAshoreFileRestApiTest()
        {
            string json = JsonConvert.SerializeObject(new
            {
                Filename = "test.exe",
                Date = DateTime.Now,
                Hash = "1234"
            });

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var response = client.PostAsync("http://localhost:59033/api/AshoreFiles", content).Result;

            var jsonString = response.Content.ReadAsStringAsync();
            System.Diagnostics.Trace.WriteLine(jsonString.Result);

        }

        [TestMethod]
        public void PostAfloatFileHullServersTest()
        {
            AfloatFileHullServerClientModel afloatFileHullServer = new AfloatFileHullServerClientModel()
            {
                HULL_SERVER_ID = 1,
                FileName = "LINQPad5Setup.exe",
                Date = DateTime.Now,
                Hash = "234323",
                Progress = 5444
            };

            AfloatFileHullServersController afloatFileHullServersController = new AfloatFileHullServersController();
            IHttpActionResult actionResult = afloatFileHullServersController.PostAfloatFileHullServer(afloatFileHullServer);
            var result = (CreatedAtRouteNegotiatedContentResult<AfloatFileHullServer>)actionResult;
            Console.WriteLine(result.RouteValues["id"]);

        }

        [TestMethod]
        public void PostAfloatFileHullServersRestApiTest()
        {
            string json = JsonConvert.SerializeObject(new
            {
                HULL_SERVER_ID = 1,
                FileName = "testfile.zip",
                Date = DateTime.Now,
                Hash = "234323",
                Progress = 5444
            });

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var response = client.PostAsync("http://localhost:59034/api/AfloatFileHullServers", content).Result;

            var jsonString = response.Content.ReadAsStringAsync();
            System.Diagnostics.Trace.WriteLine(jsonString.Result);

        }

        [TestMethod]
        public void ClearAfloatFileHullServersTest()
        {
            UploadController uploadController = new UploadController();
            uploadController.ClearAfloatFileIfExists(18);
        }


    }
}
