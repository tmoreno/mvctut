﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using Ashore_HAMS.Controllers.API2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using NLog;
using static Ashore_HAMS.Controllers.API2.ReplicationMatchViewsController;

namespace Ashore_HAMS.Tests.Controllers.API2
{
    [TestClass]
    public class ReplicationMatchViewsUnitTest
    {
        private Logger logger = LogManager.GetCurrentClassLogger();        

        [TestMethod]
        public void PostReplicationMatchViewsTest()
        {
            int[] ids = new int[] { 8,9,10 };

            ReplicationMatchViewsController replicationMatchViewsController = new ReplicationMatchViewsController();
            IHttpActionResult actionResult = replicationMatchViewsController.PostReplicationMatchView(ids);
            var r = (OkNegotiatedContentResult<List<HullServerView>>)actionResult;
            //logger.Info(r.ToString());
            IList<HullServerView> list = (List<HullServerView>)r.Content;
            foreach(HullServerView h in list)
            {
                logger.Info(h.SERVERNAME);
            }
        }

        [TestMethod]
        public void RESTApiPostReplicationMatchViewsTest()
        {
            string json = "[1,2,3]";

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var response = client.PostAsync("http://localhost:59034/api/ReplicationMatchViews", content).Result;

            var jsonString = response.Content.ReadAsStringAsync();
            logger.Info(jsonString.Result);

        }

        [TestMethod]
        public void RESTApiGetReplicationMatchViewsTest()
        {
            string id = "10";

            HttpClient client = new HttpClient();
            var response = client.GetAsync(string.Format("http://localhost:59034/api/ReplicationMatchViews/{0}",id)).Result;

            var jsonString = response.Content.ReadAsStringAsync();
            logger.Info(jsonString.Result);

            IList<GetReplicationFileForDownloadModel> list = JsonConvert.DeserializeObject<IList<GetReplicationFileForDownloadModel>>(jsonString.Result);
            
            if(list.Count() > 0)
            {
                var item = list.First<GetReplicationFileForDownloadModel>();
                logger.Info(item.Filename);
            }

        }

    }
}
