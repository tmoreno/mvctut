﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ashore_HAMS.Controllers.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ashore_HAMS.Controllers.API.Tests
{
    [TestClass()]
    public class AuthorizedApplicationControllerTests
    {
        [TestMethod()]
        public void GetTest()
        {
            AuthorizedApplicationController authorizedApplicationController = new AuthorizedApplicationController();
            AuthorizedApplicationController.JsonObj jsonObj = authorizedApplicationController.Get();
            Console.WriteLine(jsonObj.AuthAppSummary);
        }
    }
}