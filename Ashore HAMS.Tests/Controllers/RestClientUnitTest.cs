﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace Ashore_HAMS.Tests.Controllers
{





    [TestClass]
    public class RestClientUnitTest
    {
        [TestMethod]
        public void TestPostAuthHullAppReleaseApi()
        {
            string json = JsonConvert.SerializeObject(new
            {
                HULL_SERVER_ID = 0,
                AUTH_APP_RELEASE_ID = 3004
            });

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var response = client.PostAsync("http://localhost:59033/api/AUTH_HULL_APP_RELEASE", content).Result;

            var jsonString = response.Content.ReadAsStringAsync();
            System.Diagnostics.Trace.WriteLine(jsonString.Result);


        }

        [TestMethod]
        public void TestGetAuthHullAppReleaseApi()
        {
            HttpClient client = new HttpClient();
            var response = client.GetAsync("http://localhost:59033/api/AUTH_HULL_APP_RELEASE").Result;
            var jsonString = response.Content.ReadAsStringAsync();
            System.Diagnostics.Trace.WriteLine(jsonString.Result);
        }

        [TestMethod]
        public void TestGetAuthHullAppReleaseApiByJobID()
        {
            HttpClient client = new HttpClient();
            var response = client.GetAsync("http://localhost:59033/api/AUTH_HULL_APP_RELEASE/job/").Result;
            var jsonString = response.Content.ReadAsStringAsync();
            System.Diagnostics.Trace.WriteLine(jsonString.Result);
        }
    }
}
