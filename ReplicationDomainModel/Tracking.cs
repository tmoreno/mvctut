﻿using System;
using System.Collections.Generic;

namespace ReplicationDomainModel
{
    public class Tracking //Change to Tracking class
    {
        public string Id { get; set; }
        public IList<InfoSet0> AxwayInfo { get; set; } 
        public IList<InfoSet1> iOraInfo { get; set; } 
        public IList<InfoSet2> CatalogueInfo { get; set; }
        public bool DSUpdate { get; set; }
    }

    public class InfoSet0 //enable Axway information deserialize
    {
        
    }

    public class InfoSet1 //enable iOra inforamtion deserialize
    {

    }

    public class InfoSet2 //enable catalogue deserialize
    {

    }

}
