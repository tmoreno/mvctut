﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AxwayS.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ReplicationDomainModel;

namespace ReplicationMonitoring.Controllers
{

    
    [Produces("application/json")]
    [Route("api/Status")]
    public class StatusController : Controller
    {
        // GET: api/Status
        [HttpGet]
        public IList<TransferEntry> Get()
        {

            HttpClient client = new HttpClient();

            var response = client.GetAsync("http://localhost:59146/api/Transfers").Result;
            var jsonString = response.Content.ReadAsStringAsync();
            //Console.WriteLine(jsonString.Result);
            

            IList<TransferEntry> list = JsonConvert.DeserializeObject<IList<TransferEntry>>(jsonString.Result);


            Tracking tracker0 = new Tracking
            {
                Id = "3"

            };

            return list;
        }

        // GET: api/Status/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/Status
        [HttpPost]
        public void Post([FromBody]string value)
        {
            int x = 0;
        }
        
        // PUT: api/Status/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
