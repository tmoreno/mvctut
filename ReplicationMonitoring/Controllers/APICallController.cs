﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReplicationDomainModel;

namespace ReplicationMonitoring.Controllers
{
    public class APICallController : Controller
    {
        // GET: APICall
        public ActionResult Index()
        {

            Tracking track0 = new Tracking
            {
                Id = "3"
            };
            return View();
        }

        // GET: APICall/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: APICall/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: APICall/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: APICall/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: APICall/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: APICall/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: APICall/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}