﻿USE [HAMS9]
GO

DECLARE	@return_value Int

EXEC	@return_value = [dbo].[USP_ADD_AFLOAT_HULL_APP_INSTALLED2]
		@hullServerId = 1,
		@afloatVersion = N'0',
		@afloatAppName = N'NoName',
		@afloatAppDisplayIndicatorName = N'Active',
		@afloatAppVendorName = N'Oracle',
		@afloatAppVendorDisplayIndicatorName = N'Active',
		@installedDate = '12/15/2017'

SELECT	@return_value as 'Return Value'

GO
